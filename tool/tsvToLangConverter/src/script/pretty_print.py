#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import json

with open(sys.argv[1], 'r') as fp:
    obj = json.loads(fp.read())
    print json.dumps(obj, indent=2, sort_keys=True, ensure_ascii=False).encode('utf8')
