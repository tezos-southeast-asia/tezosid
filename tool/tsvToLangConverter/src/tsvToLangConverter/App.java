package tsvToLangConverter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.util.ArrayList;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class App {

  public static void main(String[] args) throws IOException {
    System.setProperty("file.encoding","UTF-8");
    String rawFileLocation = args[0];
    String lang=args[1];
    JSONObject extractedData= new JSONObject();
    FileInputStream fstream = new FileInputStream(rawFileLocation);
    BufferedReader br = new BufferedReader(new InputStreamReader(fstream,"UTF-8"));
    String header=br.readLine();
    int colNum=findColumn(header.split("\t"),lang);
    if(colNum==-1){
      System.err.println("Language "+lang+" not Found");
    }else{
      br.readLine();//Advances to the next line
      String line;
      while((line=br.readLine())!=null){
        String[] temp=line.split("\t");
        if(temp.length<=1){
          System.err.println("Entry with less than 2 columns found: "+line);
          continue;
        }
        if(colNum>=temp.length){//To account for empty cells on the right side
          extractedData.put(temp[0], temp[2]);
        }else{
          String extractedText=temp[colNum];
          if(extractedText.equals("")||extractedText==""){
            extractedText=temp[2];
          }
          extractedData.put(temp[0], extractedText);
        }
      }  
      writeToFile(lang,extractedData,args[2]);
      System.out.println("Extraction Done");
    }
    br.close();
  }
  private static void writeToFile(String fileName, JSONObject dataSet,String location) throws IOException{
    Path path = FileSystems.getDefault().getPath(location, fileName+".json");
    BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path.toString()), false));
    bw.write(dataSet.toJSONString().replace(" \\/ ", "/").replace("\\\\","\\"));
    bw.close();
  }
  private static int findColumn(String [] columns, String lang){
    boolean found =false;
    int index=0;
    for(int i=0;i<columns.length; i++ ){
      String text=columns[i].toString().replaceAll("\\s", "");
      if(!text.contains("(")){
        continue;
      }
      text=text.split("\\(")[1].split("\\)")[0];
      if(text.toUpperCase().equals(lang.toString().toUpperCase())){
        found=true;
        index=i;
        break;
      }
    }
    if(found){
      return index;
    }else{
      return -1;
    }
  }
}
