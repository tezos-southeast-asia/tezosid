# Google Sheet .tsv file to JSON converter

**_Important_**

Before you starting, see this [doc](https://gitlab.com/tsa-allstars/tezosid-config/blob/master/README.md)

## Requirement for Input file:
- File must be in .tsv (Tab Separated value) format

- language must match one of the headers in the first row of the excel sheet or program will not work. 
Also best if the headers are a single word if there are more than 2 combine with "_" or any symbol of your choice as the program will mistaken them as individual arguments.

- python 2.7 is required

## Usage

Here are the current translation files.

```
tezosId/tezosId/src/languageProvider/locales/*.json
```

All commands below should be ran on `tezosid/tool/tsvToLangConverter/src/`. Otherwises, it will fail.
To order the translation item by alphabet for easily diff

```
make format
```

To build the code for parsing .tsv file:

```
make build
```

Put .tsv file to `tezosid/tool/tsvToLangConverter/src/data/` and the naming should be `i18n.<data>.tsv`

To run the converter and get the ordering well translation files:

```
make
```
