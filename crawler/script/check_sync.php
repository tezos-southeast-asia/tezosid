<?php

require('default_variables.php');

// Create connection
$link = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if (!$link) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT unix, hash, level, timestamp FROM block ORDER BY `unix` DESC LIMIT 1";
$result = mysqli_query($link, $sql);
$row = mysqli_fetch_assoc($result);

mysqli_close($link);


echo '<pre>';
print_r($row);
echo '</pre>';


$date = new DateTime();
echo $now = $date->getTimestamp();


$myfile = fopen($daily_block_path . "/sync_log.txt", "a") or die("Unable to open file!");


if(($now-$row['unix']) > 300) {     //300 seconds = 5 mins
    $txt = date("Y-m-d h:i:sa")." Timezome :: ".date_default_timezone_get()." :: Not inserted in database in last 5 min\n";
}
else{
    //$txt = date("Y-m-d h:i:sa")." Timezome :: ".date_default_timezone_get()." :: Inserted in database in last 5 min\n";
}
fwrite($myfile, $txt);
fclose($myfile);
?>
