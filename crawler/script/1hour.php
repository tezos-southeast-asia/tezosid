<?php
{
require('default_variables.php');


// Create connection
$link = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if (!$link) {
    die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT operations, unix FROM block WHERE unix > UNIX_TIMESTAMP( NOW() - INTERVAL 24 HOUR )";
$result = mysqli_query($link, $sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $result_array[] = $row;
    }
} else {
    echo "0 results";
}
mysqli_close($link);
}


// Create connection
$link = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if (!$link) {
    die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT amount, kind, unix FROM operation WHERE (unix > UNIX_TIMESTAMP( NOW() - INTERVAL 24 HOUR ) AND (kind='transaction'))";
$response = mysqli_query($link, $sql);
$transaction = array();
if ($response->num_rows > 0) {
    // output data of each row
    while($row = $response->fetch_assoc()) {
        array_push($transaction, $row);
    }
} else {
    echo "0 results";
}

mysqli_close($link);

$myFile = $daily_block_path . "/dailyblock.json";
$arr_data = array(); // create empty array

    foreach($result_array as $block) {
        //get the block details
        $count = $block["operations"];

        $totalops += $count;
    }
     $totalblock = count($result_array);

     foreach($transaction as $tran) {
        //get the block details
        $counttran = $tran["amount"];

        $tranamount += $counttran;
    }
     $totaltran = count($transaction);

     $data = array(
        'operation'=> $totalops,
        'block'=> $totalblock,
        'tranamount' => $tranamount,
        'totaltran' => $totaltran,
        'unix' => time()
     );

       //Convert updated array to JSON
	   $jsondata = json_encode($data, JSON_PRETTY_PRINT);
       
       print $jsondata;
	   //write json data into data.json file
	   if(file_put_contents($myFile, $jsondata)) {
	        echo 'Data successfully saved';
	    }
	   else 
	        echo "error";

?>
