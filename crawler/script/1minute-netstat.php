<?php
require('default_variables.php');
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => $base_url."/network/stat",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/json"
  ),
));



$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
}
 
$data = json_decode($response, true);
 
//get the netstat details
$total_sent = $data["total_sent"];
$total_recv = $data['total_recv'];
$current_inflow = $data['current_inflow'];
$current_outflow = $data['current_outflow'];

//$date = date_create();
//$timestamp = date_format($date, 'Y-m-d H:i:s') . "\n";
//print_r($timestamp);
//$timezone = date_default_timezone_get();
//print_r($timezone);  


$link = mysqli_connect($net_stat_servername, $net_stat_username, $net_stat_password, $net_stat_dbname);

// Check connection
if (!$link) {
    die("Connection failed: " . mysqli_connect_error());
}
echo "Connected successfully";

$sql="INSERT INTO `1minute-netstat` (total_sent, total_recv, current_inflow, current_outflow)
VALUES ('$total_sent', '$total_recv', '$current_inflow', '$current_outflow')";

if (mysqli_query($link, $sql)) {
  echo "New 1minute-netstat successfully";
} else {
  echo "1minute-netstat Error: " . $sql . "<br>" . mysqli_error($link);
}

mysqli_close($link);
?>
