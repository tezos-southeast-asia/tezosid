<?php
require('default_variables.php');


// Create connection
$link = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if (!$link) {
    die("Connection failed: " . mysqli_connect_error());
}
$sql = "select * from (select b.uid, b.hash, b.timestamp, b.unix from block as b left join operation as o on b.uid = o.block_uid where o.block_uid is null and b.operations > 0 limit 5) as t order by rand() limit 1";
$result = mysqli_query($link, $sql);


    while($row = mysqli_fetch_assoc($result)) {
      print_r($row);
      $append = $row["hash"];
      $timestamp = $row["timestamp"];
      $unix = $row["unix"];
      $block_uid = $row["uid"];
    }

mysqli_close($link);

//$url = "http://node1.tezos.id:8732/blocks/" . $append . "/proto/operations/";
$url = $base_url."/chains/main/blocks/" . $append . "/operations/";
$curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/json"
      ),
    ));
    
    $response = curl_exec($curl);
    $err = curl_error($curl);
    
    curl_close($curl);
    
    if ($err) {
      echo "cURL Error #:" . $err;
    } else {}
    
        
    $dataset = json_decode($response , true);
    

    foreach($dataset as $outputs) {
        if(count($outputs) > 0){
          foreach ($outputs as $output){
            //get the block details
            $hash = $output["hash"];
            $branch = $output["branch"];
            $source = $output["contents"][0]["source"];
            $public_key = $output["contents"][0]["public_key"];
            $fee = $output["contents"][0]["fee"];
            $counter = $output["contents"][0]["counter"];
            $signature = $output["signature"];
            $inblock = $append;
            $operations = $output["contents"][0]["metadata"]["balance_updates"];
            $kind = $output["contents"][0]["kind"];
            $block = '';
            if ($kind == "endorsement")  {
              $slot = implode(", ", (array)$output["contents"][0]["metadata"]["slots"]);
              $level =$output["contents"][0]["level"];
              $amount = "";
              $destination = "";
              $delegate = $output["contents"][0]["metadata"]["delegate"];
            } else {
              $slot = "";
              $level = "";
              $amount = $output["contents"][0]["amount"];
              $destination = $output["contents"][0]["destination"];
              $delegate = "";
            }
            $nonce = "";
            $id = "";
            $managerPubkey = "";
            $balance = "";
            $spendable = "";
            $delegatable = "";
            $scripttext = "";
            {
              if (is_array($scripttext)) {
                $scriptfile = json_encode($opsdata["script"]);
                $script = substr($scriptfile,0,5000);
              }
              else{
                //$script = $opsdata["script"];
                $script = "";
              } 
            }
            //$parameter = $opsdata["parameters"];
            $parameter = "";
            {
              if (is_array($parameter)) {
                $parameters = json_encode($opsdata["parameters"]);
              }
              else {
                //$parameters = $opsdata["parameters"];
                $parameters = $parameter;
              }
            }

              // Create connection
              $connect = mysqli_connect($servername, $username, $password, $dbname);
              // Check connection
              if (!$connect) {
                die("Connection failed: " . mysqli_connect_error());
              }
              echo '<br>';
              $sql =  "INSERT IGNORE into operation(block_uid, hash, kind, block, slot, level, nonce, id, amount, destination, managerPubkey, balance, spendable, delegatable, delegate, script, parameters, timestamp, unix, branch, source, public_key, fee, counter, signature, inblock)
              VALUES ('$block_uid', '$hash', '$kind','$block', '$slot', '$level', '$nonce', '$id', '$amount', '$destination', '$managerPubkey', '$balance', '$spendable', '$delegatable', '$delegate', '$script', '$parameters', '$timestamp', '$unix', '$branch', '$source', '$public_key', '$fee', '$counter', '$signature', '$inblock')";
              echo '<br>';

              if (mysqli_query($connect, $sql)) {
                echo "New operation added successfully <br>";
              } else {
                echo "Operation Error: " . $sql . "<br>" . mysqli_error($connect);
              }

              mysqli_close($connect);
          }
        }
    }

?>
