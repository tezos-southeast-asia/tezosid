<?php
require('default_variables.php');
$curl = curl_init();
$url = $base_url."/network/peers";
curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

 
$dataset = json_decode($response, true);

for($i = 0, $l = count($dataset); $i < $l; ++$i) {
  $seen_addr = $dataset[$i][1]['last_seen'][0]['addr'];
  $seen_ip = substr($seen_addr, 7);
  $ip_split = explode('.', $seen_ip);
  $HEXIP = sprintf('%02x%02x%02x%02x', $ip_split[0], $ip_split[1], $ip_split[2], $ip_split[3]);
  $new_ip = '0x'. $HEXIP;
  
// Create connection
$link = mysqli_connect($geo_servername, $geo_username, $geo_password, $geo_dbname);

// Check connection
if (!$link) {
    die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT country FROM dbip_lookup WHERE ip_start <= $new_ip AND $new_ip <= ip_end LIMIT 1";

$result = mysqli_query($link, $sql);
if (mysqli_query($link, $sql)) {
    while($row = mysqli_fetch_assoc($result)) {
      $country = $row["country"];
    }
    $countrycode = $country;
    array_push($dataset[$i],$countrycode);
} else {
  echo "select dbip_loop Error: " . $sql . "<br>" . mysqli_error($conn);
}
mysqli_close($link);
}
// Create connection
$link = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if (!$link) {
    die("Connection failed: " . mysqli_connect_error());
}

//get the block details
$timestamp = time();
foreach($dataset as $data) {
$peerid = $data[0];
$score = $data[1]['score'];
$trusted = $data[1]['trusted'];
$state = $data[1]['state'];
$reachable_addr = $data[1]['reachable_at']['addr'];
$reachable_ip = substr($reachable_addr, 7);
$reachable_port = $data[1]['reachable_at']['port'];
$total_sent = $data[1]['stat']['total_sent'];
$total_recv = $data[1]['stat']['total_recv'];
$current_inflow = $data[1]['stat']['current_inflow'];
$current_outflow = $data[1]['stat']['current_outflow'];
$rejected_addr = $data[1]['last_rejected_connection'][0]['addr'];
$rejected_ip = substr($rejected_addr, 7);
$rejected_port = $data[1]['last_rejected_connection'][0]['port'];
$rejected_time = $data[1]['last_rejected_connection'][1];
$rejected_unix = date("U",strtotime($rejected_time));
$established_addr = $data[1]['last_established_connection'][0]['addr'];
$established_ip = substr($established_addr, 7);
$established_port = $data[1]['last_established_connection'][0]['port'];
$established_time = $data[1]['last_established_connection'][1];
$established_unix = date("U",strtotime($established_time));
$disconnection_addr = $data[1]['last_disconnection'][0]['addr'];
$disconnection_ip = substr($disconnection_addr, 7);
$disconnection_port = $data[1]['last_disconnection'][0]['port'];
$disconnection_time = $data[1]['last_disconnection'][1];
$disconnection_unix = date("U",strtotime($disconnection_time));
$seen_addr = $data[1]['last_seen'][0]['addr'];
$seen_ip = substr($seen_addr, 7);
$seen_port = $data[1]['last_seen'][0]['port'];
$seen_time = $data[1]['last_seen'][1];
$seen_unix = date("U",strtotime($seen_time));
$miss_addr = $data[1]['last_miss'][0]['addr'];
$miss_ip = substr($miss_addr, 7);
$miss_port = $data[1]['last_miss'][0]['port'];
$miss_time = $data[1]['last_miss'][1];
$miss_unix = date("U",strtotime($miss_time));
$countryprepare =$data[2];
{
if ($countryprepare == "ZZ") {
    $countrycode = "";
}
else $countrycode = $countryprepare;
}

$rejected_time = mysqli_real_escape_string($link, $rejected_time);
$established_time = mysqli_real_escape_string($link, $established_time);
$disconnection_time = mysqli_real_escape_string($link, $disconnection_time);
$seen_time = mysqli_real_escape_string($link, $seen_time);
$miss_time = mysqli_real_escape_string($link, $miss_time);


$sql="INSERT into peerid(peerid, score, trusted, state, reachable_addr, reachable_ip, reachable_port, total_sent, total_recv, current_inflow, current_outflow, 
rejected_addr, rejected_ip, rejected_port, rejected_time, rejected_unix, 
established_addr, established_ip, established_port, established_time, established_unix, 
disconnection_addr, disconnection_ip, disconnection_port, disconnection_time, disconnection_unix, 
seen_addr, seen_ip, seen_port, seen_time, seen_unix, 
miss_addr, miss_ip, miss_port, miss_time, miss_unix, timestamp, countrycode) 
VALUES ('$peerid', '$score', '$trusted', '$state', '$reachable_addr', '$reachable_ip', '$reachable_port', '$total_sent', '$total_recv', '$current_inflow', '$current_outflow',
'$rejected_addr', '$rejected_ip', '$rejected_port', '$rejected_time', '$rejected_unix',
'$established_addr', '$established_ip', '$established_port', '$established_time', '$established_unix',
'$disconnection_addr', '$disconnection_ip', '$disconnection_port', '$disconnection_time', '$disconnection_unix',
'$seen_addr', '$seen_ip', '$seen_port', '$seen_time', '$seen_unix',
'$miss_addr', '$miss_ip', '$miss_port', '$miss_time', '$miss_unix','$timestamp', '$countrycode'
)";

if (mysqli_query($link, $sql)) {
  echo "New peer successfully";
} else {
  echo "Peer Error: " . $sql . "<br>" . mysqli_error($conn);
}
}
mysqli_close($link);
print $reachable_ip;
}
?>
