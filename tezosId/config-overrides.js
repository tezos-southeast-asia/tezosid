

const webpack = require('webpack')
const fs = require('fs')
const RobotstxtPlugin = require('robotstxt-webpack-plugin');
const SitemapWebpackPlugin = require('sitemap-webpack-plugin').default;

const connectionConfig = require('./src/customApp/configs/connectionConfigs').connectionConfig
const routes = require('./.routes.json')
const { tezosIdWeb } = connectionConfig

module.exports = function override(config, env) {
  // Add overrided webpack configs here
  if (fs.existsSync('./src/customApp/configs/tezosid-config/configs')) {
    config.plugins.push(new webpack.NormalModuleReplacementPlugin(
      /(.*)\/authConfigs/,
      function(resource) {
        resource.request = resource.request.replace(/authConfigs/, 'tezosid-config/configs/authConfigs');
      }
    ))
  }

  // Generate robots.txt
  config.plugins.push(new RobotstxtPlugin({
    tmp: config,
    policy: [
      {
        userAgent: "*",
        allow: "/",
      }
    ],
    sitemap: `${tezosIdWeb}sitemap.xml`,
    host: tezosIdWeb,
  }))

  // Generate sitemap.xml
  config.plugins.push(new SitemapWebpackPlugin(tezosIdWeb, routes, {
    skipGzip: true,
  }))

  return config;
}
