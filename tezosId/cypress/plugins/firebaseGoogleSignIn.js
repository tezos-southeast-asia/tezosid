const puppeteer = require('puppeteer')

/**
 * @name firebaseGoogleSignIn
 * @param {options.account} string Account
 * @param {options.password} string Password
 * @param {options.loginUrl} string Url of login page
 * @param {options.loginSelector} string a selector on the loginUrl page for the social provider button
 * @param {options.postLoginSelector} string a selector on the app's post-login return page to assert that login is successful
 * @param {options.headless} boolean launch puppeteer in headless more or not
 * @param {options.width} number Width of page viewport
 * @param {options.height} number Height of page viewport
 */
module.exports.firebaseGoogleSignIn = async (options = {}) => {
  validateOptions(options)

  const browser = await puppeteer.launch({headless: !!options.headless, devtools: true})
  const page = await browser.newPage()
  const { width = 1280, height = 800 } = options
  await page.setViewport({ width, height })
  await page.goto(options.loginUrl, { timeout: 2000 })

  const popup = await login({ browser, page, options })
  await typeAccount({ page: popup, options })
  await typePassword({ page: popup, options })

  await handlePostLogin({ page, options })
  await finalizeSession({ browser })

  return {}
}

function validateOptions(options) {
  if (!options.account || !options.password) {
    throw new Error('Account or Password missing for Google login')
  }
}

async function login({ browser, page, options } = {}) {
  const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())))

  await page.waitForSelector(options.loginSelector)
  await page.click(options.loginSelector)
  const popup = await newPagePromise
  return popup;
}

async function typeAccount({ page, options } = {}) {
  await page.waitForSelector('input[type="email"]')
  await page.type('input[type="email"]', options.account)
  await page.click('#identifierNext')
}

async function typePassword({ page, options } = {}) {
  await page.waitForSelector('input[type="password"]', {visible: true})
  await page.type('input[type="password"]', options.password)
  await page.waitForSelector('#passwordNext', {visible: true})
  await page.click('#passwordNext')
}

async function handlePostLogin({ page, options } = {}) {
  await page.waitForSelector(options.postLoginSelector)
}

async function finalizeSession({ browser } = {}) {
  await browser.close()
}