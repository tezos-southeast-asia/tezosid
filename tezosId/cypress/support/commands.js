// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import authConfigs from '../../src/customApp/configs/tezosid-config/configs/authConfigs'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

/**
  * Override cy.visit() to wait more 1500 seconds
  * @type {Cypress.Command}
  * @name cy.visit
  * @example
  * cy.visit(url, options)
  */
Cypress.Commands.overwrite("visit", (originalFn, url, options) => {
  originalFn(url, options)

  // due to client side rendering, we need to wait for a while and then to do tests
  cy.wait(1500)
})

/* Handle Firebase Signin & Signout */
// Initialized firebase app
window.firebaseApp = firebase.initializeApp(authConfigs.firebase)

// Handle Firebase SignIn with email & password by Promise
const firebaseSignInWithEmailAndPassword = ({ email, password }) => new Promise((resolve, reject) => {
  const fbAuth = window.firebaseApp.auth()
  fbAuth.onAuthStateChanged(auth => {
    if (auth) {
      resolve(auth)
    }
  })

  fbAuth.signInWithEmailAndPassword(email, password)
    .catch(reject)
})

// Handle Firebase Signout by Promise
const firebaseSignout = () => new Promise((resolve, reject) => {
  const fbAuth = firebase.auth()

  fbAuth.onAuthStateChanged(auth => {
    if (!auth) {
      resolve()
    }
  })

  fbAuth
    .signOut()
    .catch(reject)
})

/**
  * Login to Firebase auth using FIREBASE_AUTH_EMAIL & FIREBASE_AUTH_PWD environment variable
  * or using email & password arguments
  * @param {String} email - Firebase Email Account
  * @param {String} password - Firebase Account Password
  * @type {Cypress.Command}
  * @name cy.login
  * @example
  * cy.login()
  */
Cypress.Commands.add("login", (email, password) => {
  const fbAuth = window.firebaseApp.auth()
  const authEmail = email || Cypress.env('FIREBASE_AUTH_EMAIL')
  const authPwd = password || Cypress.env('FIREBASE_AUTH_PASSWORD')
  const user = fbAuth.currentUser

  if (!authEmail || !authPwd) {
    throw new Error('Email or Password missing for Firebase login')
  }

  if (fbAuth.currentUser) {
    return new Promise((resolve, reject) => {
      if (user.email === authEmail) {
        cy.log('Authed user already exists, login complete.')
        resolve()
      } else {
        firebaseSignout().then(() => firebaseSignInWithEmailAndPassword({ email: authEmail, password: authPwd }))
          .catch(reject)
      }
    })
  } else {
    return firebaseSignInWithEmailAndPassword({ email: authEmail, password: authPwd })
  }
})

/**
  * Log out of Firebase instance
  * @memberOf Cypress.Chainable#
  * @type {Cypress.Command}
  * @name cy.logout
  * @example
  * cy.logout()
  */
  Cypress.Commands.add('logout', firebaseSignout)
