import { topbar as topbarSelectors } from '../../../helpers/selectors'
import routes from '../../../helpers/routes'

describe('Components - Topbar - Desktop', () => {
  context('Logo', () => {
    before(() => {
      cy.visit(routes.blocks)
    })

    it('Should contain logo image & link', function() {
      cy.get(topbarSelectors.logoWrapper).within(() => {
        cy.get('img').should('have.length', 1)
        cy.get('a').click()
        cy.location('pathname').should('eq', routes.home)
      })
    })
  })

  context('Menus', () => {
    describe('When user logged out', () => {
      before(() => {
        cy.visit(routes.home)
      })

      it('Should contain menu list', function() {
        cy.get(topbarSelectors.menuWrapper).within(() => {
          cy.get(topbarSelectors.menuSubMenuItems).should('have.length', 7)
          cy.get(topbarSelectors.menuItems).as('menuItems').should('have.length', 2)
        })
      })

      it('Should contain Login menu', function() {
        cy.get(topbarSelectors.menuLogin).should('be.visible')
      })
    })

    describe('When user logged in', () => {
      before(() => {
        cy.visit(routes.home)
        cy.login()
      })

      after(() => {
        cy.logout()
      })

      it('Should contain menu list', function() {
        cy.get(topbarSelectors.menuWrapper).within(() => {
          cy.get(topbarSelectors.menuSubMenuItems).should('have.length', 7)
          cy.get(topbarSelectors.menuItems).should('have.length', 2)
        })
      })

      it('Should contain Profile menu', function() {
        cy.get(topbarSelectors.menuProfile).should('be.visible')
      })
    })
  })
})
