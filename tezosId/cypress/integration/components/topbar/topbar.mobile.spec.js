import { topbar as topbarSelectors } from '../../../helpers/selectors'
import routes from '../../../helpers/routes'

describe('Components - Topbar - Mobile', () => {
  describe('Menu', () => {
    const openDrawer = () => {
      cy.get(topbarSelectors.menuDrawerBtn).click()
    }

    const closeDrawer = () => {
      cy.get(topbarSelectors.menuDrawerCloseBtn).click()
    }

    it('Should show/hide menu When topbar drawer/close button is clicked', function() {
      cy.visit(routes.home)

      cy.get(topbarSelectors.menuDrawerBtn).should('be.visible')
      cy.get(topbarSelectors.menuDrawerBtnIcon).should('be.visible')

      openDrawer()
      cy.get(topbarSelectors.menuDrawerWrapper).should('be.visible')
      closeDrawer()
      cy.get(topbarSelectors.menuDrawerWrapper).should('not.be.visible')
    })

    it('Should show selected menu item same as page', function() {
      cy.visit(routes.peers)
      openDrawer()

      cy.get(topbarSelectors.menuDrawerSelectedItem).within(() => {
        cy.get('a').should('have.attr', 'href')
          .and('eq', routes.peers)
      })
    })
  })
})
