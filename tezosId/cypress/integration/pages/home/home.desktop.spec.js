import { home as homeSelectors, table as tableSelectors } from '../../../helpers/selectors'
import routes from '../../../helpers/routes'

describe('Page - Home', function() {
  before(() => {
    cy.visit(routes.home)
  })

  describe('Block summary', () => {
    it('Should have summary', function() {
      cy.get(homeSelectors.summary).should('have.length', 1)
    })

    it('Should have latest block', function() {
      cy.get(homeSelectors.latestBlock).should('have.length', 1)
    })
  })

  describe('Latest 10 TEZOS blocks', () => {
    it('Should have Top 10 block', function() {
      cy.get(homeSelectors.topTenBlocks).within(() => {
        cy.get(tableSelectors.items).should('have.length', 10)
      })
    })
  })

  describe('Last 10 TEZOS transactions', () => {
    it('Should have Top 10 transactions', function() {
      cy.get(homeSelectors.topTenTransactions).within(() => {
        cy.get(tableSelectors.items).should('have.length', 10)
      })
    })
  })
})
