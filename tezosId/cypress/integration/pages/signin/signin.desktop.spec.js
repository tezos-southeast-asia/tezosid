import {
  topbar as topbarSelectors,
  signin as signinSelectors
} from '../../../helpers/selectors'
import routes from '../../../helpers/routes'

describe('Page - Signin', () => {
  describe.skip('Google Account', () => {
    it('Should enable to sign in', () => {
      const account = Cypress.env('FIREBASE_AUTH_GOOGLE_ACCOUNT')
      const password = Cypress.env('FIREBASE_AUTH_GOOGLE_PASSWORD')
      const loginOptions = {
        account,
        password,
        loginUrl: Cypress.config().baseUrl + routes.signin,
        headless: true,
        logs: true,
        loginSelector: signinSelectors.googleBtn,
        postLoginSelector: topbarSelectors.menuProfile
      }

      return cy.task('firebaseGoogleSignIn', loginOptions)
    })
  })

  describe('Firebase Account', () => {
    const checkPage = (route) => {
      const baseUrl = Cypress.config().baseUrl

      cy.visit(route)
      cy.url().should('eq', baseUrl + route)
      cy.get(topbarSelectors.menuProfile).should('be.visible')
    }

    before(() => {
      cy.visit(routes.home)
      cy.login()
    })

    after(() => {
      cy.logout()
    })

    it('Should enable to access correct pages after user signed in', function() {
      cy.get(topbarSelectors.menuProfile).should('be.visible')

      cy.fixture('routes').then((routes)  => {
        routes.common.forEach(checkPage)
        routes.signedIn.forEach(checkPage)
      })
    })
  })
})
