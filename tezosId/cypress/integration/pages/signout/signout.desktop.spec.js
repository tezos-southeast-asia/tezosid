import {
  topbar as topbarSelectors,
} from '../../../helpers/selectors'
import routes from '../../../helpers/routes'

describe('Page - Signout', () => {
  const baseUrl = Cypress.config().baseUrl
  const checkPage = (route) => {
    cy.visit(route)
    cy.url().should('eq', baseUrl + route)
    cy.get(topbarSelectors.menuLogin).should('be.visible')
  }

  describe('Page Access', () => {
    before(() => {
      cy.login()
      cy.visit(routes.signout)
    })

    it('Should redirect to home', function() {
      cy.url().should('eq', baseUrl + routes.home)
    })

    it('Should enable to access correctly after user signed out', function() {
      cy.fixture('routes').then((routes)  => {
        routes.common.forEach(checkPage)
        routes.signedOut.forEach(checkPage)
      })
    })
  })
})
