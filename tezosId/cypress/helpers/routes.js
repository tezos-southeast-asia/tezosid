export default {
  home: '/',
  blocks: '/blocks',
  peers: '/peers',
  signin: '/signin',
  signout: '/signout'
}
