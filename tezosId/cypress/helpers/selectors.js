export const topbar = {
  container: '.isomorphicTopbar',
  logoWrapper: '.isoLogoWrapper',
  menuWrapper: '.ant-menu-root',
  menuSubMenuItems: '.ant-menu-submenu:not(.ant-menu-overflowed-submenu)',
  menuItems: '.ant-menu-item',
  menuProfile: '.topbar__menu__topbar-profile',
  menuLogin: '.topbar__menu__topbar-login',
  menuDrawerBtn: '.topbar__drawer',
  menuDrawerBtnIcon: '.topbar__drawer__icon',
  menuDrawerWrapper: '.ant-drawer',
  menuDrawerCloseBtn: '.ant-drawer-close',
  menuDrawerSelectedItem: '.ant-menu-item-selected'
}

export const home = {
  summary: '.home__block-summary',
  latestBlock: '.home__latest-block',
  topTenBlocks: '.home__top-ten-blocks',
  topTenTransactions: '.home__top-ten-trans'
}

export const signin = {
  googleBtn: '.isoSignInForm .btnGoogle'
}

export const table = {
  items: 'tbody tr'
}