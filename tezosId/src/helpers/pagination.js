export const withPageQuery = function(url, pagination, pageSize) {
  let newUrl = new URL(url);
  newUrl.searchParams.set("p", pagination - 1);
  newUrl.searchParams.set("n", pageSize);
  return newUrl.toString();
};

export const withBakerRightsPageQuery = function(url, level) {
  let newUrl = new URL(url);
  newUrl.searchParams.set("level", level);

  return newUrl.toString();
};
