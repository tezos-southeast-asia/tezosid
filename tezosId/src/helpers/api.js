import axios from 'axios'
import { attach as retryAxiosAttach } from 'retry-axios'

/* TODO: enable to getIdToken when api support Authorization header
import { firebaseApp } from '../redux/auth/rsf'

const getIdToken = () => new Promise((resolve) => {
  try {
    firebaseApp.auth().currentUser.getIdToken().then((idToken) => {
      resolve(idToken);
    }, (error) => {
      resolve(null);
    })
  } catch (error) {
    resolve(null);
  }
});
*/

/**
 * fetchApi is a function to use AXIOS to request API with default options.
 * 
 * @param {object} apiOptions API related options for AXIOS on https://github.com/axios/axios#request-config
 * @param {string} apiOptions.url The URL of API request.
 * @param {string} [apiOptions.method='GET'] The request method to be used when making the request.
 * @param {object} apiOptions.params The URL parameters to be sent with the request.
 * @param {(string|object|ArrayBuffer|ArrayBufferView|URLSearchParams)} apiOptions.data The data to be sent as the request body.
 * @param {number} [apiOptions.timeout=30000] The number of milliseconds before the request times out. By default, 30000 milliseconds.
 * @param {object} apiOptions.headers The headers that the server responded with.
 * @param {object} apiOptions.raxConfig The configs for API request retry. https://www.npmjs.com/package/retry-axios
 * 
 * @return {object} Return an object with data for successful api request or with error for failed api request.
 */

// Attach retryAxios to the global axios object, and retry 3 times by default
retryAxiosAttach()
export const fetchApi = async (apiOptions = {}) => {
  const { url, ...restOptions } = apiOptions
  let defaultOptions = {
    timeout: 30000,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    data: {},
    params: {},
  }
 
  if (!url) {
    console.error('[fetchApi]: No API URL');
    return {
      error: 'No API URL'
    }
  }

  // TODO: enable to getIdToken when api support Authorization header 
  // const idToken = await getIdToken()
  // if (idToken) {
  //   defaultOptions.headers.Authorization = `Bearer ${idToken}`
  // }
  try {
    const mergedOptions = {
      url,
      ...defaultOptions,
      ...restOptions,
    }
    const response = await axios.request(mergedOptions)
    return { data: response.data }
  } catch (error) {
    return { error }
  }
}
