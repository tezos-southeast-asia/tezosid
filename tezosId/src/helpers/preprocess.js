import fs from 'fs'
import _ from 'lodash'
import {
  common as commonMeta,
  domain,
  websiteLdJson,
} from '../customApp/configs/metaConfigs'
import router from '../customApp/router'
import { sitemapPaths } from '../customApp/configs/sitemapConfigs'

// Generate .env file
fs.readFile(__dirname + '/.envTemplate', 'utf8', function (err,data) {
  if (err) return console.error(err)

  const envData = {
    REACT_APP_META_NAME: commonMeta.author,
    REACT_APP_META_DOMAIN: domain,
    REACT_APP_META_LOGO_URL: commonMeta.logoUrl,
    REACT_APP_META_TITLE: commonMeta.title,
    REACT_APP_META_DESCRIPTION: commonMeta.description,
    REACT_APP_META_LDJSON: JSON.stringify(websiteLdJson)
  }
  const compiled = _.template(data);
  const result = compiled(envData);

  fs.writeFile(__dirname + '/../../.env', result, 'utf8', function (err) {
     if (err) return console.error(err)
  })
})

// Generate routes file to generate sitemap
let routes = []
router.forEach(({
  path,
  isHidden,
  isNoSitemap,
  changeFreq,
  priority,
}) => {
  if (!isHidden && !isNoSitemap) {
    routes.push({
      path,
      changeFreq,
      priority,
    })
  }
})

routes = [
  ...routes,
  ...sitemapPaths,
]

fs.writeFile(__dirname + '/../../.routes.json', JSON.stringify(routes), 'utf8', function (err) {
  if (err) return console.error(err)
})
