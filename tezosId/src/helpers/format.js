import moment from "moment";

export const dateFormat = function(dateTime) {
  if (!dateTime) return ""

  let e = new Date(dateTime);

  let date = e.toLocaleDateString().split("/");
  let month = date[0];
  let day = date[1];
  let year = date[2];
  let newDateFormat = year + "/" + month + "/" + day;

  let timezone = /GMT[\\+|\\-]\d+/.exec(e.toString())[0];

  return newDateFormat + " " + e.toLocaleTimeString() + " (" + timezone + ")";
};

export const amountFormat = function(amount) {
  let amount1k = amount / 1000;
  let amount1000k = amount / 1000000;

  if (amount1000k >= 10) {
    amount = amount1000k.toLocaleString() + " ꜩ";
  } else if (amount1k >= 10) {
    amount = amount1k.toLocaleString() + " mꜩ";
  } else if (!amount) {
    amount = 0 + " μꜩ";
  } else {
    amount = amount.toLocaleString() + " μꜩ";
  }
  return amount;
};

export const DEFAUL_DURATION = {
  years: 0,
  months: 0,
  days: 0,
  hours: 0,
  minutes: 0,
  seconds: 0,
  total: 0,
} 

export const durationFormat = ({ diff = 0, format = 'HH:mm:ss'}) => {
  const momentDuration = moment.duration(diff)
  const diffTime = moment.utc(diff).format(format).split(':')
  const duration = {
    years: momentDuration.years(),
    months: momentDuration.months(),
    days: momentDuration.days(),
    hours: diffTime[0],
    minutes: diffTime[1],
    seconds: diffTime[2],
    total: Math.round(diff / 1000),
  }

  return duration
}

export const getCurrentTimeDiff = ({ timestamp, format }) => {
  const currentTime = moment()
  const timeDiff = currentTime.diff(moment(timestamp || currentTime))
  const formattedTimeDiff = durationFormat({ diff: timeDiff, format })

  return formattedTimeDiff
}