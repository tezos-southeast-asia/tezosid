const rowStyle = {
  width: '100%',
  display: 'flex',
  flexFlow: 'row wrap',
};
const colStyle = {
  marginBottom: '16px',
};
const gutter = 16;
const headerStyle = {
  height: {
    large: 110,
    small: 60,
    searchbar: 42
  }
}
const layoutStyle = {
  padding: "5%"
}
const basicStyle = {
  rowStyle,
  colStyle,
  gutter,
  layoutStyle,
  headerStyle,
};

export default basicStyle;
