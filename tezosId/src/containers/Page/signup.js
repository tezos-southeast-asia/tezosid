import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Input from "../../components/uielements/input";
import Checkbox from "../../components/uielements/checkbox";
import Button from "../../components/uielements/button";
import { login } from "../../redux/auth/actions";
import appActions from "../../redux/app/actions";
import Auth0 from "../../helpers/auth0/index";
import IntlMessages from "../../components/utility/intlMessages";
import SignUpStyleWrapper from "./signup.style";
import message from "../../components/uielements/message";

const { clearMenu } = appActions;

class SignUp extends Component {
  state = {
    redirectToReferrer: false
  };
  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }
  handleLogin = () => {
    const { login, clearMenu } = this.props;
    login();
    clearMenu();
    this.props.history.push("/dashboard");
  };

  handleSignUp(){
    message.info("registered!");
   // handleUserName();
  }
/* 
  handleUserName(){
    var registUserID = document.getElementById("registuser").value;
    handlePassWord();
  }

  handlePassWord(){
    var registPassID = document.getElementById("registpwd").value;
  }
*/
  render() {
    return (
      <SignUpStyleWrapper className="isoSignUpPage">
        <div className="isoSignUpContentWrapper">
          <div className="isoSignUpContent">
            <div className="isoLogoWrapper">
              <Link to="/dashboard">
                <IntlMessages id="page.signUpTitle" />
              </Link>
            </div>
            <h1>Registration Page</h1>
            <p>Please fill in this form to create an account.</p>

            <div className="isoSignUpForm">
              <div className="isoInputWrapper isoLeftRightComponent">
                <Input size="large" placeholder="First name" />
                <Input size="large" placeholder="Last name" />
              </div>

              <div className="isoInputWrapper">
                <Input size="large" placeholder="Username" id="registuser"/>
              </div>

              <div className="isoInputWrapper">
                <Input size="large" placeholder="Email" />
              </div>

              <div className="isoInputWrapper">
                <Input size="large" type="password" placeholder="Password" id="registpwd" />
              </div>

              <div className="isoInputWrapper">
                <Input
                  size="large"
                  type="password"
                  placeholder="Confirm Password"
                />
              </div>

              <div className="isoInputWrapper" style={{ marginBottom: "50px" }}>
                <Checkbox>
                  <IntlMessages id="page.signUpTermsConditions" />
                </Checkbox>
              </div>

              <div className="isoInputWrapper">
              <Link to="/">
                <Button type="primary" onClick={this.handleSignUp}>
                
                  <IntlMessages id="page.signUpButton" />
                </Button>
                </Link>
              </div>

              <div className="isoInputWrapper isoOtherLogin">
                
                {Auth0.isValid && (
                  <Button
                    onClick={() => {
                      Auth0.login(this.handleLogin);
                    }}
                    type="primary btnAuthZero"
                  >
                    <IntlMessages id="page.signUpAuth0" />
                  </Button>
                )}
              </div>
              <div className="isoInputWrapper isoCenterComponent isoHelperWrapper">
                <Link to="/signin">
                  <IntlMessages id="page.signUpAlreadyAccount" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </SignUpStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.isLoggedIn
  }),
  { login, clearMenu }
)(SignUp);

/* Put the btnFacebook code below if this if needed

      <IntlMessages id="page.signUpButton" />
                </Button>
                </Link>
              </div>
              
              <div className="isoInputWrapper isoOtherLogin">
------------------------------------------------------------------------------------------              
<Button onClick={this.handleLogin} type="primary btnFacebook">
                  <IntlMessages id="page.signUpFacebook" />
                </Button>
                <Button onClick={this.handleLogin} type="primary btnGooglePlus">
                  <IntlMessages id="page.signUpGooglePlus" />
                </Button>
*/