import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router";
import routes from "../../customApp/router";
import asyncComponent from "../../helpers/AsyncFunc";

class AppRouter extends Component {
  render() {
    const { isLoggedIn } = this.props;
    return (
      <Switch>
        {routes.map(singleRoute => {
          const { path, exact, isLoginRequired, component: Component, isHidden, ...otherProps } = singleRoute;

          return !isHidden && (
            <Route
              exact={exact === false ? false : true}
              key={path}
              path={`/${path}`}
              render={props => (
                isLoginRequired && !isLoggedIn ? (
                  <Redirect key={`signin-redirect-${path}`} to="/signin" />
                ) : (
                  <Component key={`route-comp-${path}`} {...props} />
                )
              )}
              {...otherProps}
            />
          );
        })}
        <Route component={asyncComponent(() => import("../Page/404"))} />
      </Switch>
    );
  }
}

export default AppRouter;
