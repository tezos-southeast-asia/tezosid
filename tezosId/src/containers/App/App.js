import React, { Component } from "react";
import { connect } from "react-redux";
import { Layout, LocaleProvider } from "antd";
import { IntlProvider } from "react-intl";
import { Debounce } from "react-throttle";
import WindowResizeListener from "react-window-size-listener";
import { ThemeProvider } from "styled-components";
import classnames from "classnames";

import AppLocale from '../../languageProvider';
import appActions from "../../redux/app/actions";
import Topbar from "../../customApp/containers/topbar";
// import ThemeSwitcher from "../../containers/ThemeSwitcher";
import AppRouter from "./AppRouter";
import themes from "../../settings/themes";
import AppHolder from "./commonStyle";
import "./global.css";
import Footer from "../../customApp/components/footer";

const { Content } = Layout;
const { toggleAll } = appActions;
export class App extends Component {
  render() {
    const { locale, selectedTheme, height, auth, isOpenSearchBox } = this.props;
    const { isLoggedIn } = auth
    const currentAppLocale = AppLocale[locale];
    const appHeight = window.innerHeight;

    return (
      <LocaleProvider locale={currentAppLocale.antd}>
        <IntlProvider
          locale={currentAppLocale.locale}
          messages={currentAppLocale.messages}
        >
          <ThemeProvider theme={themes[selectedTheme]}>
            <AppHolder>
              <Layout style={{ height: appHeight }}>
                <Debounce time="500" handler="onResize">
                  <WindowResizeListener
                    onResize={windowSize =>
                      this.props.toggleAll(
                        windowSize.windowWidth,
                        windowSize.windowHeight
                      )
                    }
                  />
                </Debounce>
                <Topbar />
                <Layout style={{ flexDirection: "row", overflowX: "hidden" }}>
                  <Layout
                    className="isoContentMainLayout"
                    style={{
                      height: height
                    }}
                  >
                    <Content
                      className={classnames("isomorphicContent", {
                        "isomorphicContent__hasSearchBox": isOpenSearchBox
                      })}
                    >
                      <AppRouter isLoggedIn={isLoggedIn}  />
                    </Content>
                    <Footer />
                  </Layout>
                </Layout>
                {/*<ThemeSwitcher />*/}
              </Layout>
            </AppHolder>
          </ThemeProvider>
        </IntlProvider>
      </LocaleProvider>
    );
  }
}

export default connect(
  state => ({
    auth: state.Auth,
    locale: state.LanguageSwitcher.language.locale,
    selectedTheme: state.ThemeSwitcher.changeThemes.themeName,
    height: state.App.height,
    isOpenSearchBox: state.App.isOpenSearchBox
  }),
  { toggleAll }
)(App);
