import styled from 'styled-components';
import { palette, font } from 'styled-theme';

import basicStyle from "../../settings/basicStyle";

const { headerStyle } = basicStyle
const AppHolder = styled.div`
  font-family: ${font('primary', 0)};

  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  a,
  p,
  li,
  input,
  textarea,
  span,
  div,
  img,
  svg {
    &::selection {
      background: ${palette('primary', 0)};
      color: #fff;
    }
  }

  .ant-row:not(.ant-form-item) {
    ${'' /* margin-left: -8px;
    margin-right: -8px; */};
    &:before,
    &:after {
      display: none;
    }
  }

  .ant-row > div {
    padding: 0;
  }

  .isoLeftRightComponent {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
  }

  .isoCenterComponent {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
  }

  -webkit-overflow-scrolling: touch;
  .trigger {
    font-size: 18px;
    line-height: 64px;
    padding: 0 16px;
    cursor: pointer;
    transition: color 0.3s;
  }

  .trigger:hover {
    color: ${palette('primary', 0)};
  }

  .ant-layout-sider-collapsed .anticon {
    font-size: 16px;
  }

  .ant-layout-sider-collapsed .nav-text {
    display: none;
  }

  .ant-layout {
    background: ${palette('secondary', 1)};

    &.isoContentMainLayout {
      overflow: auto;
      overflow-x: hidden;
      @media only screen and (min-width: 768px) and (max-width: 1220px) {
        width: calc(100% - 80px);
        flex-shrink: 0;
      }

      @media only screen and (max-width: 767px) {
        width: 100%;
        flex-shrink: 0;
      }
    }
  }

  .isomorphicLayout {
    width: calc(100% - 240px);
    flex-shrink: 0;
    overflow-x: hidden !important;

    @media only screen and (max-width: 767px) {
      width: 100%;
    }

    @media only screen and (min-width: 768px) and (max-width: 1220px) {
      width: calc(100% - 80px);
      width: 100%;
    }
  }

  .isomorphicContent {
    padding: ${headerStyle.height.small}px 0 0;
    flex-shrink: 0;
    background: #f1f3f6;
    position: relative;

    &__hasSearchBox {
      padding: ${headerStyle.height.small + headerStyle.height.searchbar}px 0 0;
    }

    @media only screen and (min-width: 1220px) {
      padding: ${headerStyle.height.large}px 0 0;
    }
  }

  .ant-layout-footer {
    font-size: 13px;
    background: #ffffff;
    text-align: center;
    border-top: 1px solid #ededed;
    height: 100;
    width: 100%;
    display: flex;
    flex-direction: column;

    @media (max-width: 767px) {
      padding: 10px 20px;
    }
  }

  ${'' /* button {
    border-radius: 0;
  } */};
`;

export default AppHolder;
