import EnLang from './entries/en-US';
import JaLang from './entries/ja-JP';
import KoLang from './entries/ko-KR';
import ZhTWLang from './entries/zh-Hant-TW';
import ZhCNLang from './entries/zh-Hans-CN';
import { addLocaleData } from 'react-intl';

const AppLocale = {
  en: EnLang,
  ja: JaLang,
  "ko-KR": KoLang,
  "zh-TW": ZhTWLang,
  "zh-CN": ZhCNLang
};
addLocaleData(AppLocale.en.data);
addLocaleData(AppLocale.ja.data);
addLocaleData(AppLocale["ko-KR"].data);
addLocaleData(AppLocale["zh-TW"].data);
addLocaleData(AppLocale["zh-CN"].data);

export default AppLocale;
