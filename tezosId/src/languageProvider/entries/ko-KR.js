import antdLocaleProvider from "antd/lib/locale-provider/ko_KR";
import appLocaleData from "react-intl/locale-data/ko";
import localeMessages from "../locales/ko-KR.json";

const KoLang = {
  messages: {
    ...localeMessages
  },
  antd: antdLocaleProvider,
  locale: "ko",
  data: appLocaleData
};
export default KoLang;
