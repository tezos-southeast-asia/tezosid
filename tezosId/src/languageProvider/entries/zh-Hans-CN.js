import antdLocaleProvider from "antd/lib/locale-provider/zh_CN";
import appLocaleData from 'react-intl/locale-data/zh';
import localeMessages from '../locales/zh-Hans-CN.json';

const ZhHansCNLan = {
  messages: {
    ...localeMessages
  },
  antd: antdLocaleProvider,
  locale: 'zh-Hans-CN',
  data: appLocaleData
};
export default ZhHansCNLan;
