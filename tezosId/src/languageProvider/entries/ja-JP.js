import antdLocaleProvider from "antd/lib/locale-provider/ja_JP";
import appLocaleData from "react-intl/locale-data/ja";
import localeMessages from "../locales/ja-JP.json";

const JaLang = {
  messages: {
    ...localeMessages
  },
  antd: antdLocaleProvider,
  locale: "ja",
  data: appLocaleData
};
export default JaLang;
