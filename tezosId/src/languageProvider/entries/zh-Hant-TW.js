import antdLocaleProvider from "antd/lib/locale-provider/zh_TW";
import appLocaleData from 'react-intl/locale-data/zh';
import localeMessages from '../locales/zh-Hant-TW.json';

const ZhHantTWLan = {
  messages: {
    ...localeMessages
  },
  antd: antdLocaleProvider,
  locale: 'zh-Hant-TW',
  data: appLocaleData
};
export default ZhHantTWLan;
