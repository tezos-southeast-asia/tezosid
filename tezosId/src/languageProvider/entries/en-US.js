import antdLocaleProvider from "antd/lib/locale-provider/en_US";
import appLocaleData from "react-intl/locale-data/en";
import localeMessages from "../locales/en-US.json";

const EnLang = {
  messages: {
    ...localeMessages
  },
  antd: antdLocaleProvider,
  locale: "en-US",
  data: appLocaleData
};
export default EnLang;
