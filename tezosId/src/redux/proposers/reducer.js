import { types, PER_PAGE, DEFAULT_PAGE, TOTAL_PROPOSERS } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  proposers: {
    data: null,
    total: TOTAL_PROPOSERS,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function proposersReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.PROPOSERS_REQUEST:
      return {
        ...state,
        isLoading: true,
        proposers: {
          data: null,
          total: TOTAL_PROPOSERS,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.PROPOSERS_SUCCESS: {
      const proposers = action.data
      return {
        ...state,
        isLoading: false,
        isError: false,
        proposers: {
          ...proposers,
        }
      }
    }
    case types.PROPOSERS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        proposers: initialState.proposers
      }

    default:
      return state
  }
}