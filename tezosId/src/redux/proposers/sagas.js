import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'

import {
    types,
    PER_PAGE,
    DEFAULT_PAGE,
    TOTAL_PROPOSERS,
    fetchProposersSuccess,
    fetchProposersFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseProposers = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = TOTAL_PROPOSERS }) => {
    let rowKey = 1;
    const proposers = data.map(({ period, count, proposal, source }) => {
        const proposer = {
            key: rowKey,
            period: period,
            hash: proposal,
            count: count,
            votes: "0",
            source: source
        };

        //fake votes for noww
        switch (proposer.hash) {
            case "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU":
                proposer.votes = "33145";
                break;
            case "PsBABY5nk4JhdEv1N1pZbt6m6ccB9BfNqa23iKZcHBh23jmRS9f":
                proposer.votes = "9522";
                break;
            case "PtdRxBHvc91c2ea2evV6wkoqnzW7TadTg9aqS9jAn2GbcPGtumD":
                proposer.votes = "200";
                break;
            case "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd":
                proposer.votes = "18181";
                break;
            case "Psd1ynUBhMZAeajwcZJAeq5NrxorM6UCU4GJqxZ7Bx2e9vUWB6z":
                proposer.votes = "7674";
                break;
            default:
                proposer.votes = "19712";
                break;
        }
        rowKey++;
        return proposer;
    })

    return {
        currentPage,
        pageSize,
        total,
        data: proposers
    }
}

function* fetchProposersSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
    const apiOptions = {
        url: connectionConfig.proposers,
    }
    const totalApiOptions = {
        url: connectionConfig.proposersNum,
    }

    const [{ data, error }, { data: total }] = yield all([
        call(fetchApi, apiOptions),
        call(fetchApi, totalApiOptions),
    ])
    let formattedData

    if (data) {
        formattedData = parseProposers({ data, currentPage, pageSize, total })
        yield put(fetchProposersSuccess(formattedData))
    } else {
        yield put(fetchProposersFailure(error))
    }
}

export default function* proposersRootSaga() {
    yield all([
        takeEvery(types.PROPOSERS_REQUEST, fetchProposersSaga),
    ])
}
