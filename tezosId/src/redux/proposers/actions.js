export const PER_PAGE = 5
export const DEFAULT_PAGE = 1
export const TOTAL_PROPOSERS = 0
export const types = {
  PROPOSERS_REQUEST: 'PROPOSERS_REQUEST',
  PROPOSERS_SUCCESS: 'PROPOSERS_SUCCESS',
  PROPOSERS_FAILURE: 'PROPOSERS_FAILURE',
}

export const fetchProposers = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.PROPOSERS_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchProposersSuccess = (data) => ({
  type: types.PROPOSERS_SUCCESS,
  data
})

export const fetchProposersFailure = (error) => ({
  type: types.PROPOSERS_FAILURE,
  error
})