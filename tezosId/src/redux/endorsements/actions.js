export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  ENDORSEMENTS_REQUEST: 'ENDORSEMENTS_REQUEST',
  ENDORSEMENTS_SUCCESS: 'ENDORSEMENTS_SUCCESS',
  ENDORSEMENTS_FAILURE: 'ENDORSEMENTS_FAILURE',
}

export const fetchEndorsements = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.ENDORSEMENTS_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchEndorsementsSuccess = (data) => ({
  type: types.ENDORSEMENTS_SUCCESS,
  data
})

export const fetchEndorsementsFailure = (error) => ({
  type: types.ENDORSEMENTS_FAILURE,
  error
})
