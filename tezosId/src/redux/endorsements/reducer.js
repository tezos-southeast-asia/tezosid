import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  endorsements: {
    data: null,
    total: DEFAULT_TOTAL,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function endorsementsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ENDORSEMENTS_REQUEST:
      return {  
        ...state,
        isLoading: true,
        endorsements: {
          data: null,
          total: state.endorsements.total,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.ENDORSEMENTS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        endorsements : action.data
        // endorsements: {
        //   ...endorsements,
        //   total: DEFAULT_TOTAL,
        // }
      }
    } 
    case types.ENDORSEMENTS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        endorsements: initialState.endorsements
      }

    default:
      return state
  }
}