import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import { dateFormat } from "../../helpers/format";

import {
  PER_PAGE,
  DEFAULT_PAGE,
  DEFAULT_TOTAL,
  types,
  fetchEndorsementsSuccess,
  fetchEndorsementsFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseEndorsements = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = DEFAULT_TOTAL }) => {
  //const endorsements = data.map(({ hash, blockId, inblock, timestamp, slot }) => {
  const endorsements = data.map(({ op, endor }, index) => {
    const { opHash, } = op || {}
    const { blockLevel, blockHash, blockTimestamp, slots, uuid } = endor || {}
    let slot = "";

    for (let i = 0; i < slots.length; i++) {
      slot += slots[i];
      if (i !== slots.length - 1) {
        slot += ', ';
      }
    }

    return {
      id: opHash,
      key: uuid || index,
      opHash: opHash,
      blockId: [blockLevel, blockHash],
      timestamp: dateFormat(blockTimestamp),
      slot
    }
  })

  return {
    currentPage,
    pageSize,
    total,
    data: endorsements
  }
}

function* fetchEndorsementsSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.endorsements,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }

  const totalApiOptions = {
    url: connectionConfig.endorsementsNum,
  }

  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])
  let formattedData

  if (data) {
    formattedData = parseEndorsements({ data, currentPage, pageSize, total })
    yield put(fetchEndorsementsSuccess(formattedData))
  } else {
    yield put(fetchEndorsementsFailure(error))
  }
}

export default function* endorsementsRootSaga() {
  yield all([
    takeEvery(types.ENDORSEMENTS_REQUEST, fetchEndorsementsSaga),
  ])
}
