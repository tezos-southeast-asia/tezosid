import { all } from 'redux-saga/effects';
import authSagas from './auth/saga';
import votingPeriodsSagas from './votingPeriods/sagas';
import voterListSagas from './voterList/sagas';
import bookmarkListRootSagas from './bookmarklist/sagas';
import notificationListRootSaga from './notification/sagas';
import peersSagas from './peers/sagas';
import blocksSagas from './blocks/sagas';
import transactionsSagas from './transactions/sagas';
import endorsementsSagas from './endorsements/sagas';
import delegationsSagas from './delegations/sagas';
import originationsSagas from './originations/sagas';
import accountSagas from './account/sagas';
import bakerRightsSagas from './bakerRights/sagas';
import proposersSagas from './proposers/sagas';
import proposalVoters from './proposalVoters/sagas';
import doubleEndorsement from './doubleEndorsement/sagas';
import doubleBakingSagas from './doubleBaking/sagas';
import protocolsSagas from './protocol/sagas';
import operationsSagas from './operation/sagas';
import blockSagas from './block/sagas';
import allAccountSagas from './allAccount/sagas';
import allContractSagas from './allContract/sagas';
import chartSagas from './charts/sagas'
import noncesSagas from './nonces/sagas';
import activationsSagas from './activations/sagas';
import snapshotsSagas from './snapshots/sagas';

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    votingPeriodsSagas(),
    voterListSagas(),
    bookmarkListRootSagas(),
    notificationListRootSaga(),
    peersSagas(),
    blocksSagas(),
    transactionsSagas(),
    endorsementsSagas(),
    delegationsSagas(),
    originationsSagas(),
    accountSagas(),
    bakerRightsSagas(),
    proposersSagas(),
    proposalVoters(),
    doubleEndorsement(),
    protocolsSagas(),
    doubleBakingSagas(),
    operationsSagas(),
    blockSagas(),
    allAccountSagas(),
    allContractSagas(),
    chartSagas(),
    noncesSagas(),
    activationsSagas(),
    snapshotsSagas()
  ]);
}
