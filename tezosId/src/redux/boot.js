import { store } from './store';
import { checkAuth } from './auth/actions';

export default () =>
  new Promise((resolve) => resolve(
    Promise.all([
      checkAuth(store),
    ])
  ))

