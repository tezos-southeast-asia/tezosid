import actions, {
  getCurrentLanguage
} from './actions';

const initState = {
  language: getCurrentLanguage()
};

export default function(state = initState, action) {
  switch (action.type) {
    case actions.CHANGE_LANGUAGE:
      return {
        ...state,
        language: action.language
      };
    default:
      return state;
  }
}
