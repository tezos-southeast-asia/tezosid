import Cookies from "js-cookie";
import langConfigs from "../../customApp/configs/langConfigs"

export function getCurrentLanguage(lang) {
  const langCookie = Cookies.get("lang")
  const { defaultLanguage, langOptions, options } = langConfigs
  const locale = lang || langCookie || window.navigator.language || defaultLanguage || "en-US";

  const selecetedLanguage = langOptions[locale] || options[0];

  if (!langCookie || selecetedLanguage.locale !== langCookie) {
    /* TODO: [i18n] expire time could be 365 days or 30 days when supported languages are confirmed */
    Cookies.set("lang", selecetedLanguage.locale, { expires: 7 })
  }

  return selecetedLanguage;
}

export function changeLanguage(lang) {
  return {
    type: actions.CHANGE_LANGUAGE,
    language: getCurrentLanguage(lang)
  };
}

const actions = {
  CHANGE_LANGUAGE: "CHANGE_LANGUAGE",
  changeLanguage
};

export default actions;
