import { types } from './actions'

const initState = {
    bookmarks: [],
    followBtnText: "Follow",
    followBtnDisplay: "initial",
    isLoading: false,
    bookmarkAccountInfo: [],
    transactionData: [],
    amount: 0,
    balance: 0,
    totalRecords: 1,
    startIndex: 0,
    lastIndex: 3,
    currentPage:1,
    pageSize:3
}

export default function bookmarklistReducer(state = initState, action = {}) {
    switch (action.type) {
        case types.IS_LOADING:
            return {
                ...state,
                isLoading: action.isLoading
            }
        case types.HAS_ERROR:
            return {
                ...state,
                isLoading: false
            }
        case types.SET_BOOKMARKS_TO_STATE:
            return {
                ...state,
                bookmarks: action.bookmarks,
                totalRecords: action.totalRecords,
                isLoading: false
            }
        case types.UPDATE_BOOKMARK_ALIAS_TO_STATE: {
            const bookmarks = state.bookmarks.map(bookmark => {
                if (bookmark.hash === action.accountHash) {
                    bookmark.alias = action.alias;
                }
                return bookmark;
            });
            return {
                ...state,
                bookmarks: bookmarks,
                isLoading: false
            }
        }
        case types.REMOVE_BOOKMARK_FROM_STATE:
            return {
                ...state,
                bookmarks: state.bookmarks.filter(bookmark => bookmark.hash !== action.accountHash),
                isLoading: false
            }
        case types.UPDATE_FOLLOW_BTN:
            return {
                ...state,
                followBtnText: action.text,
                followBtnDisplay: action.display,
                isLoading: false
            }
        case types.UPDATE_FOLLOW_BTN_TEXT:
            return {
                ...state,
                followBtnText: action.followBtnText,
                isLoading: false
            }
        case types.UPDATE_FOLLOW_BTN_DISPLAY:
            return {
                ...state,
                followBtnDisplay: action.followBtnDisplay,
                isLoading: false
            }
        case types.GET_ACC_INFO_TO_STATE:
            return {
                ...state,
                bookmarkAccountInfo: action.bookmarkAccInfo,
                isLoading: false
            }
        case types.GET_TRANSACTIONS_DATA_TO_STATE:
            return {
                ...state,
                transactionData: action.transactionData
            }
        case types.CALCULATE_TOTAL_NET_BALANCE_TO_STATE:
            return {
                ...state,
                balance: action.balance.balance
            }
        case types.CALCULATE_TOTAL_NET_AMOUNT_TO_STATE:
            return {
                ...state,
                amount: action.amount.amount
            }
        case types.SET_EDIT_VALUE_TO_STATE:
            return {
                ...state,
                bookmarkAccountInfo: action.accountInfos
            }
        case types.SET_RECORD_INDEX_TO_STATE:
            return {
                ...state,
                startIndex: action.startIndex,
                lastIndex: action.lastIndex,
                currentPage:action.currentPage,
                pageSize:action.pageSize
            }
        default:
            return state
    }
}


