import { firebaseApp } from '../auth/rsf';

const db = firebaseApp && firebaseApp.firestore();

export const types = {
    CHECK_FOR_BOOKMARK_FROM_ACC: "CHECK_FOR_BOOKMARK_FROM_ACC",
    ADD_BOOKMARK_FROM_ACC: "ADD_BOOKMARK_FROM_ACC",
    REMOVE_BOOKMARK_FROM_ACC: "REMOVE_BOOKMARK_FROM_ACC",
    REMOVE_BOOKMARK_FROM_PROFILE: "REMOVE_BOOKMARK_FROM_PROFILE",
    GET_BOOKMARK_BY_ACCHASH: "GET_BOOKMARK_BY_ACCHASH",
    SET_BOOKMARKS: "SET_BOOKMARKS",
    UPDATE_BOOKMARK_ALIAS: "UPDATE_BOOKMARK_ALIAS",
    UPDATE_FOLLOW_BTN: "UPDATE_FOLLOW_BTN",
    UPDATE_FOLLOW_BTN_DISPLAY: "UPDATE_FOLLOW_BTN_DISPLAY",
    SET_BOOKMARKS_TO_STATE: "SET_BOOKMARKS_TO_STATE",
    UPDATE_BOOKMARK_ALIAS_TO_STATE: "UPDATE_BOOKMARK_ALIAS_TO_STATE",
    REMOVE_BOOKMARK_FROM_STATE: "REMOVE_BOOKMARK_FROM_STATE",
    GET_ACC_INFO_WITH_BOOKMARK: "GET_ACC_INFO_WITH_BOOKMARK",
    GET_ACC_INFO_TO_STATE: "GET_ACC_INFO_TO_STATE",
    GET_TRANSACTIONS_DATA: "GET_TRANSACTIONS_DATA",
    GET_TRANSACTIONS_DATA_TO_STATE: "GET_TRANSACTIONS_DATA_TO_STATE",
    CALCULATE_TOTAL_NET_AMOUNT_TO_STATE: "CALCULATE_TOTAL_NET_AMOUNT_TO_STATE",
    CALCULATE_TOTAL_NET_BALANCE_TO_STATE: "CALCULATE_TOTAL_NET_BALANCE_TO_STATE",
    SET_EDIT_VALUE: "SET_EDIT_VALUE",
    SET_EDIT_VALUE_TO_STATE: "SET_EDIT_VALUE_TO_STATE",
    SET_RECORD_INDEX: "SET_RECORD_INDEX",
    SET_RECORD_INDEX_TO_STATE: "SET_RECORD_INDEX_TO_STATE",
    IS_LOADING: "IS_LOADING",
    HAS_ERROR: "HAS_ERROR"
}

/* Redux saga actions */
export function checkForBookmarkFromAcc(userUid, accountHash) {
    return { type: types.CHECK_FOR_BOOKMARK_FROM_ACC, userUid, accountHash }
}

export function addBookmarkFromAcc(userUid, accountHash, userEmail) {
    return { type: types.ADD_BOOKMARK_FROM_ACC, userUid, accountHash, userEmail }
}

export function removeBookmarkFromAcc(userUid, accountHash) {
    return { type: types.REMOVE_BOOKMARK_FROM_ACC, userUid, accountHash }
}

export function removeBookmarkFromProfile(userUid, accountHash) {
    return { type: types.REMOVE_BOOKMARK_FROM_PROFILE, userUid, accountHash }
}

export function getBookmarkByAccHash(userUid, accountHash) {
    return { type: types.GET_BOOKMARK_BY_ACCHASH, userUid, accountHash }
}

export function setBookmarks(userUid) {
    return { type: types.SET_BOOKMARKS, userUid }
}

export function updateBookmarkAlias(userUid, accountHash, alias) {
    return { type: types.UPDATE_BOOKMARK_ALIAS, userUid, accountHash, alias }
}

export function getBookmarkAccInfo(bookmarks, startIndex, lastIndex) {
    return { type: types.GET_ACC_INFO_WITH_BOOKMARK, bookmarks, startIndex, lastIndex }
}

export function getTransactionsData(bookmarks, startIndex, lastIndex) {
    return { type: types.GET_TRANSACTIONS_DATA, bookmarks, startIndex, lastIndex }
}

export function setEditValue(accountInfos, accountHash, edit) {
    return { type: types.SET_EDIT_VALUE, accountInfos, accountHash, edit }
}

export function setRecordIndex(currentPage, pageSize) {
    return { type: types.SET_RECORD_INDEX, currentPage, pageSize }
}

/* Actions for reducer */
export function updateFollowBtn(display, text) {
    return { type: types.UPDATE_FOLLOW_BTN, display, text }
}

export function updateFollowBtnDisplay(followBtnDisplay) {
    return { type: types.UPDATE_FOLLOW_BTN_DISPLAY, followBtnDisplay }
}

export function setBookmarksToState(bookmarks, totalRecords) {
    return { type: types.SET_BOOKMARKS_TO_STATE, bookmarks, totalRecords }
}

export function updateBookmarkAliasToState(accountHash, alias) {
    return { type: types.UPDATE_BOOKMARK_ALIAS_TO_STATE, accountHash, alias }
}

export function removeBookmarkFromState(accountHash) {
    return { type: types.REMOVE_BOOKMARK_FROM_STATE, accountHash }
}

export function loadingHandle(bool) {
    return { type: types.IS_LOADING, isLoading: bool };
}

export function errorHandle(e) {
    return { type: types.HAS_ERROR, errorMsg: e };
}

export function getBookmarkAccInfoToState(bookmarkAccInfo) {
    return { type: types.GET_ACC_INFO_TO_STATE, bookmarkAccInfo }
}

export function getTransactionsDataToState(transactionData) {
    return { type: types.GET_TRANSACTIONS_DATA_TO_STATE, transactionData }
}

export function calculateNetAmtToState(amount) {
    return { type: types.CALCULATE_TOTAL_NET_AMOUNT_TO_STATE, amount }
}

export function calculateBalanceToState(balance) {
    return { type: types.CALCULATE_TOTAL_NET_BALANCE_TO_STATE, balance }
}

export function setEditValueToState(accountInfos) {
    return { type: types.SET_EDIT_VALUE_TO_STATE, accountInfos }
}

export function setRecordIndexToState(startIndex, lastIndex, currentPage, pageSize) {
    return { type: types.SET_RECORD_INDEX_TO_STATE, startIndex, lastIndex, currentPage, pageSize }
}

/* Firebase queries */
export async function fbGetAllBookmarks(userUid) {
    const snapshot = await db.collection("users").doc(userUid).collection("bookmarks").get();
    return mapBookmarkSnapshot(snapshot.docs)
}

export function mapBookmarkSnapshot(bookmarkList) {
    if (bookmarkList.length !== 0) {
        let mapBookmarklist = bookmarkList.map(doc => {
            return doc.id;
        });
        return mapBookmarklist;
    } else {
        return [];
    }
}

export async function fbAddBookmark(userUid, accountHash) {
    let docRef = db.collection("users").doc(userUid);
    await docRef.collection("bookmarks").doc(accountHash).set({
        alias: accountHash,
    });
}

export async function fbRemoveBookmark(userUid, accountHash) {
    let docRef = db.collection("users").doc(userUid);
    await docRef.collection("bookmarks").doc(accountHash).delete();
}