import rsf from '../auth/rsf'
import { all, call, put, takeEvery } from 'redux-saga/effects';
import { firebaseApp } from '../auth/rsf';
import { connectionConfig } from '../../customApp/configs/connectionConfigs';
import Amount from "../../customApp/components/amount";
import React from "react";
import { fetchApi } from '../../helpers/api'

import {
    types,
    updateFollowBtn,
    mapBookmarkSnapshot,
    setBookmarksToState,
    updateBookmarkAliasToState,
    removeBookmarkFromState,
    getBookmarkAccInfoToState,
    loadingHandle,
    errorHandle,
    getTransactionsDataToState,
    calculateNetAmtToState,
    calculateBalanceToState,
    setEditValueToState,
    setRecordIndexToState,
} from './actions';
import { addEmailSubsToList } from '../notification/actions';

const db = firebaseApp && firebaseApp.firestore();

function* checkForBookmarkFromAccSaga(provider) {
    try {
        yield put(loadingHandle(true));
        const query = db.collection("users").doc(provider.userUid).collection("bookmarks");
        const snapshot = yield call(rsf.firestore.getCollection, query);
        const mapBookmarklist = mapBookmarkSnapshot(snapshot.docs);
        let text = mapBookmarklist.includes(provider.accountHash) ? "Unfollow" : "Follow";
        yield put(updateFollowBtn("initial", text));
    } catch (e) {
        yield put(errorHandle(e));
    }
}

function* addBookmarkFromAccSaga(provider) {
    try {
        yield put(loadingHandle(true));
        const query = db.collection("users").doc(provider.userUid).collection("bookmarks").doc(provider.accountHash);
        yield call(rsf.firestore.setDocument, query, {
            alias: provider.accountHash,
            emailSub: true
        });

        const notiQuery = db.collection("notification").doc(provider.accountHash).collection("emails").doc(provider.userEmail);
        yield call(rsf.firestore.setDocument, notiQuery, {
            alias: provider.accountHash,
        });

        yield put(updateFollowBtn("initial", "Unfollow"));
    } catch (e) {
        yield put(errorHandle(e));
    }
}

function* updateBookmarkAliasSaga(provider) {
    try {
        yield put(loadingHandle(true));
        const query = db.collection("users").doc(provider.userUid).collection("bookmarks").doc(provider.accountHash);
        yield call(rsf.firestore.updateDocument, query, {
            alias: provider.alias,
        });
        yield put(updateBookmarkAliasToState(provider.accountHash, provider.alias));
    } catch (e) {
        yield put(errorHandle(e));
    }
}

function* removeBookmarkFromAccSaga(provider) {
    try {
        yield put(loadingHandle(true));
        yield call(removeBookmark, provider);
        yield put(updateFollowBtn("initial", "Follow"));
    } catch (e) {
        yield put(errorHandle(e));
    }
}

function* removeBookmarkFromProfileSaga(provider) {
    try {
        yield put(loadingHandle(true));
        yield call(removeBookmark, provider);
        yield put(removeBookmarkFromState(provider.accountHash));
    } catch (e) {
        yield put(errorHandle(e));
    }
}

function* removeBookmark(provider) {
    const query = db.collection("users").doc(provider.userUid).collection("bookmarks").doc(provider.accountHash);
    yield call(rsf.firestore.deleteDocument, query);
}

function* setBookmarkSaga(provider) {
    try {
        yield put(loadingHandle(true));
        const query = db.collection("users").doc(provider.userUid).collection("bookmarks");
        const snapshot = yield call(rsf.firestore.getCollection, query);
        const bookmarkObjList = [];
        const emailSubsArr = [];

        for (let doc of snapshot.docs) {
            let text = doc.data().emailSub ? 'Unsubscribe' : 'Subscribe'
            bookmarkObjList.push({
                hash: doc.id,
                alias: doc.data().alias
            });

            const emailSubObj = { accountHash: doc.id, isLoading: false, subscriptionTxt: text };
            emailSubsArr.push(emailSubObj);
        }
        yield put(addEmailSubsToList(emailSubsArr));
        yield put(setBookmarksToState(bookmarkObjList, bookmarkObjList.length));
    } catch (e) {
        yield put(errorHandle(e));
    }
}

function* getBookmarkAccInfoSaga(provider) {
    try {
        yield put(loadingHandle(true));
        let totalBalance = 0;
        let obj = {};

        const startIndex = provider.startIndex;
        const lastIndex = provider.lastIndex;
        const bookmarks = provider.bookmarks.slice(startIndex, lastIndex);

        for (let bookmark of bookmarks) {
            const label = bookmark.alias;
            const apiOptions = {
                url: connectionConfig.accountDetails_KT + bookmark.hash
            }

            const { data, error } = yield call(fetchApi, apiOptions);
            if (data) {
                const { balance } = data;
                const accountDetails = { balance, edit: false, label }
                obj[bookmark.hash] = accountDetails;
                //totalBalance += parseInt(balance, 10);

            } else {
                yield put(errorHandle(error));
            }
        }
        yield put(getBookmarkAccInfoToState(obj));
        yield put(calculateBalanceToState({ balance: <Amount amount={totalBalance} /> }));

    } catch (e) {
        yield put(errorHandle(e));
    }
}

function* getTransactionsDataSaga(provider) {
    try {
        yield put(loadingHandle(true));
        let netAmount = 0;
        let transactionObj = {};
        const startIndex = provider.startIndex;
        const lastIndex = provider.lastIndex;
        const bookmarks = provider.bookmarks.slice(startIndex, lastIndex);

        for (let bookmark of bookmarks) {
            let url =
                { connectionConfig }.connectionConfig.accountActions.transactions + bookmark.hash;

            const apiOptions = {
                url: url,
                params: {
                    p: 0,
                    n: 5,
                }
            }

            const { data, error } = yield call(fetchApi, apiOptions);
            const transactionDataArr = [];
            if (data) {
                data.map(({ op, tx }) => {
                    const { opHash, } = op || {}
                    const { fee, amount, blockTimestamp, source, destination } = tx || {}
                    const transactionData = { atOpHash: opHash, atFee: fee, atAmount: amount, atTimeStamp: blockTimestamp, atFrom: source, atTo: destination };
                    transactionDataArr.push(transactionData);
                    return op;
                })
                transactionObj[bookmark.hash] = formatTransactionData(transactionDataArr, bookmark.hash, provider.bookmarks);
                // transactionObj[bookmark.hash].map((transaction) => {

                //     transaction.act === "sent" ? netAmount -= parseInt(transaction.rawAmount, 10) : netAmount += parseInt(transaction.rawAmount, 10)
                // })
            } else {
                yield put(errorHandle(error));
            }
        }
        yield put(getTransactionsDataToState(transactionObj));
        yield put(calculateNetAmtToState({ amount: <Amount amount={netAmount} /> }));
    } catch (e) {
        yield put(errorHandle(e));
    }
}

function* setEditValueSaga(provider) {
    provider.accountInfos[provider.accountHash].edit = provider.edit;
    yield put(setEditValueToState(provider.accountInfos));
}

function* setRecordIndex(provider) {
    const currentPage = provider.currentPage;
    const pageSize = provider.pageSize;
    const startIndex = currentPage * pageSize - pageSize;
    const lastIndex = currentPage * pageSize;
    yield put(setRecordIndexToState(startIndex, lastIndex, currentPage, pageSize))
}

const formatTransactionData = (transactionData, hash, bookmarkList) => {
    let dataList = [];

    for (let transaction of transactionData) {
        let fromElement = [transaction.atFrom, hash];
        let toElement = [transaction.atTo, hash];
        let opHash = transaction.atOpHash;
        let fromElementAlias, toElementAlias;

        for (let bookmark of bookmarkList) {
            if (bookmark.hash === transaction.atFrom) {
                fromElementAlias = bookmark.alias;

            }
            if (bookmark.hash === transaction.atTo) {
                toElementAlias = bookmark.alias;
            }
        }

        dataList.push({
            id: transactionData.indexOf(transaction),
            key: transactionData.indexOf(transaction),
            fee: transaction.atFee,
            rawAmount: transaction.atAmount,
            //amount: <Amount amount={transaction.atAmount} />,
            amount: transaction.atAmount,
            timestamp: transaction.atTimeStamp,
            fromElement: fromElement,
            toElement: toElement,
            act: transaction.atFrom === hash ? "sent" : "received",
            action: getActiontype({ to: transaction.atTo, hash }),
            opHash: opHash,
            fromElementAlias: fromElementAlias,
            toElementAlias: toElementAlias
        })
    }
    return dataList;
}

const getActiontype = ({ to, hash }) => {
    let type = '';
    let color = null;

    if (to === hash) {
        color = '#01ab03';
        type = 'Received';
    } else {
        type = 'Sent';
        color = '#dd0303';
    }

    return {
        color,
        type,
    }
}

export default function* bookmarkListRootSaga() {
    yield all([
        takeEvery(types.CHECK_FOR_BOOKMARK_FROM_ACC, checkForBookmarkFromAccSaga),
        takeEvery(types.ADD_BOOKMARK_FROM_ACC, addBookmarkFromAccSaga),
        takeEvery(types.UPDATE_BOOKMARK_ALIAS, updateBookmarkAliasSaga),
        takeEvery(types.REMOVE_BOOKMARK_FROM_ACC, removeBookmarkFromAccSaga),
        takeEvery(types.REMOVE_BOOKMARK_FROM_PROFILE, removeBookmarkFromProfileSaga),
        takeEvery(types.SET_BOOKMARKS, setBookmarkSaga),
        takeEvery(types.GET_ACC_INFO_WITH_BOOKMARK, getBookmarkAccInfoSaga),
        takeEvery(types.GET_TRANSACTIONS_DATA, getTransactionsDataSaga),
        takeEvery(types.SET_EDIT_VALUE, setEditValueSaga),
        takeEvery(types.SET_RECORD_INDEX, setRecordIndex)
    ])
}
