import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  protocols: {
    data: null,
    total: DEFAULT_TOTAL,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function protocolReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.PROTOCOLS_REQUEST:
      return {
        ...state,
        isLoading: true,
        protocols: {
          data: null,
          total: DEFAULT_TOTAL,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.PROTOCOLS_SUCCESS: {
      const protocols = action.data
      return {
        ...state,
        isLoading: false,
        isError: false,
        protocols: {
          ...protocols,
        }
      }
    } 
    case types.PROTOCOLS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        protocols: initialState.protocols
      }

    default:
      return state
  }
}