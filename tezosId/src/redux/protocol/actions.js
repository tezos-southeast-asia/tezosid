export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  PROTOCOLS_REQUEST: 'PROTOCOLS_REQUEST',
  PROTOCOLS_SUCCESS: 'PROTOCOLS_SUCCESS',
  PROTOCOLS_FAILURE: 'PROTOCOLS_FAILURE',
}

export const fetchProtocols = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.PROTOCOLS_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchProtocolsSuccess = (data) => ({
  type: types.PROTOCOLS_SUCCESS,
  data
})

export const fetchProtocolsFailure = (error) => ({
  type: types.PROTOCOLS_FAILURE,
  error
})
