import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'

import {
  PER_PAGE,
  DEFAULT_PAGE,
  DEFAULT_TOTAL,
  types,
  fetchProtocolsSuccess,
  fetchProtocolsFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseProtocols = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = DEFAULT_TOTAL }) => {
  const protocols = data.map(({ protocal, to, from, proto }) => {
    return {
      key: protocal,
      proto:proto,
      hash: protocal,
      start: [from],
      end: [to]
    }
  })

  return {
    currentPage,
    pageSize,
    total:total,
    data: protocols
  }
}

function* fetchProtocolsSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.protocolStartEnd,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }

  const totalApiOptions = {
    url: connectionConfig.protocolNum,
  }

  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])

  let formattedData

  if (data) {
    formattedData = parseProtocols({ data, currentPage, pageSize, total })
    yield put(fetchProtocolsSuccess(formattedData))
  } else {
    yield put(fetchProtocolsFailure(error))
  }
}

export default function* peersRootSaga() {
  yield all([
    takeEvery(types.PROTOCOLS_REQUEST, fetchProtocolsSaga),
  ])
}
