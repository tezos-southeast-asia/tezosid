import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  nonces: {
    data: null,
    total: DEFAULT_TOTAL,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function noncesReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.NONCES_REQUEST:
      return {  
        ...state,
        isLoading: true,
        nonces: {
          data: null,
          total: state.nonces.total,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.NONCES_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        nonces : action.data
        // endorsements: {
        //   ...endorsements,
        //   total: DEFAULT_TOTAL,
        // }
      }
    } 
    case types.NONCES_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        nonces: initialState.nonces
      }

    default:
      return state
  }
}