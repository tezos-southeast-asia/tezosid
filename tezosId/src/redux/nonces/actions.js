export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  NONCES_REQUEST: 'NONCES_REQUEST',
  NONCES_SUCCESS: 'NONCES_SUCCESS',
  NONCES_FAILURE: 'NONCES_FAILURE',
}

export const fetchNonces = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.NONCES_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchNoncesSuccess = (data) => ({
  type: types.NONCES_SUCCESS,
  data
})

export const fetchNoncesFailure = (error) => ({
  type: types.NONCES_FAILURE,
  error
})
