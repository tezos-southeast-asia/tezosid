import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'

import {
  PER_PAGE,
  DEFAULT_PAGE,
  DEFAULT_TOTAL,
  types,
  fetchNoncesSuccess,
  fetchNoncesFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseNonces = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = DEFAULT_TOTAL }) => {
  const nonces = data.map(({op, seedNonceEvelation}, index) => {
    const { nonce, uuid, blockHash } = seedNonceEvelation || {}
    const { opHash } = op || {}

    return {
      id: opHash,
      key: uuid || index,
      opHash: opHash,
      nonce: nonce,
      blockId:[blockHash]
    }
  })

  return {
    currentPage,
    pageSize,
    total,
    data: nonces
  }
}

function* fetchNoncesSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.nonces,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const totalApiOptions = {
    url: connectionConfig.noncesNum,
  }
  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])
  let formattedData

  if (data) {
    formattedData = parseNonces({ data, currentPage, pageSize, total })
    yield put(fetchNoncesSuccess(formattedData))
  } else {
    yield put(fetchNoncesFailure(error))
  }
}

export default function* noncesRootSaga() {
  yield all([
    takeEvery(types.NONCES_REQUEST, fetchNoncesSaga),
  ])
}
