export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  ORIGINATIONS_REQUEST: 'ORIGINATIONS_REQUEST',
  ORIGINATIONS_SUCCESS: 'ORIGINATIONS_SUCCESS',
  ORIGINATIONS_FAILURE: 'ORIGINATIONS_FAILURE',
}

export const fetchOriginations = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.ORIGINATIONS_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchOriginationsSuccess = (data) => ({
  type: types.ORIGINATIONS_SUCCESS,
  data
})

export const fetchOriginationsFailure = (error) => ({
  type: types.ORIGINATIONS_FAILURE,
  error
})
