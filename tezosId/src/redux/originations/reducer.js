import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  originations: {
    data: null,
    total: DEFAULT_TOTAL,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function originationsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ORIGINATIONS_REQUEST:
      return {
        ...state,
        isLoading: true,
        originations: {
          data: null,
          total: state.originations.total,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.ORIGINATIONS_SUCCESS: { 
      return {
        ...state,
        isLoading: false,
        isError: false,
        originations : action.data
        // originations: {
        //   ...originations,
        //   total: TOTAL_ORIGINATIONS,
        // }
      }
    } 
    case types.ORIGINATIONS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        originations: initialState.originations
      }

    default:
      return state
  }
}