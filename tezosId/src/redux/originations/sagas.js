import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import { dateFormat } from "../../helpers/format";

import {
  PER_PAGE,
  DEFAULT_PAGE,
  types,
  fetchOriginationsSuccess,
  fetchOriginationsFailure,
  DEFAULT_TOTAL
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseOriginations = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = DEFAULT_TOTAL }) => {
  //const originations = data.map(({ hash, source, fee, blockId, inblock, timestamp }) => {
  const originations = data.map(({ op, origination }, index) => {
    const { opHash, blockLevel, blockTimestamp, blockHash } = op || {}
    const { source, fee, uuid, balance, operationResultOriginatedContracts, operationResultStatus } = origination || {}
    return {
      id: opHash,
      key: uuid || index,
      opHash: opHash,
      originateSource: source,
      fee,
      blockId: [blockLevel, blockHash],
      timestamp: dateFormat(blockTimestamp),
      newBalance: balance,
      newAccount: operationResultOriginatedContracts[0],
      status: operationResultStatus !== 'applied' ? 'status-color' : ''
    }
  })

  return {
    currentPage,
    pageSize,
    total,
    data: originations
  }
}

function* fetchOriginationsSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.originations,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const totalApiOptions = {
    url: connectionConfig.originationsNum,
  }
  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])
  //const { data, error } = yield call(fetchApi, apiOptions)
  let formattedData

  if (data) {
    formattedData = parseOriginations({ data, currentPage, pageSize, total })
    yield put(fetchOriginationsSuccess(formattedData))
  } else {
    yield put(fetchOriginationsFailure(error))
  }
}

export default function* originationsRootSaga() {
  yield all([
    takeEvery(types.ORIGINATIONS_REQUEST, fetchOriginationsSaga),
  ])
}
