export const types = {
  VOTING_PERIODS_INIT: 'VOTING_PERIODS_INIT',
  VOTING_PERIODS_SUCCESS: 'VOTING_PERIODS_SUCCESS',
}

export const initVotingPeriods = () => ({
  type: types.VOTING_PERIODS_INIT
})

export const fetchVotingPeriodsSuccess = (data) => ({
  type: types.VOTING_PERIODS_SUCCESS,
  data
})