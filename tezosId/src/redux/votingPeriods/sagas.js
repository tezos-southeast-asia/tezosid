import { all, call, put, takeEvery } from 'redux-saga/effects'

import {
  types,
  fetchVotingPeriodsSuccess,
} from './actions'
import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import { fetchApi } from '../../helpers/api'

// TODO: unit tests
const {
  votingQuorum: votingQuorumUrl,
  votingPeriod: votingPeriodUrl,
  currentProposal: currentProposalUrl,
  voting: votingUrl,
  voterListing: voterListingUrl,
  proposalList: proposalListUrl
} = connectionConfig

const parseVotes = (votes) => {
  const { yay, nay, pass } = votes || {}
  const totalVotes = Number(yay + nay + pass);
  const voteTypes = [ 'yay', 'nay', 'pass' ]

  if (!totalVotes) return null

  const formattedVotes = voteTypes.map((voteType) => {
    const voteCount = votes[voteType]
    const votePercentage = ((voteCount / totalVotes) * 100).toFixed(2)
    return {
      key: voteType,
      ballots: `votingPeriods.${voteType}`,
      votes: votes[voteType],
      votePercentage: `${votePercentage} %`
    }
  })

  return formattedVotes
}

const getSuperMajority = (votes) => {
  const { yay, nay } = votes || {}
  const total = Number(yay + nay)
  let majority = 0

  if (total) {
    majority = ((yay / total) * 100).toFixed(2)
  }

  return majority
}

const getParticipation = ({ voters = [], votes = [] }) => {
  let participation = 0
  let totalRolls = 0
  let votedRolls = 0

  if (voters && voters.length > 0 && votes && votes.length > 0) {
    totalRolls = voters.reduce((accumulator, currentValue) => accumulator + currentValue.rolls, 0)
    votedRolls = votes.reduce((accumulator, currentValue) => accumulator + currentValue.votes, 0)
    participation = ((votedRolls / totalRolls) * 100).toFixed(2);
  }

  return {
    totalRolls,
    votedRolls,
    participation
  }
}

const getProposals = (proposals) => {
  let formattedProposals = null

  if (proposals && proposals.length > 0) {
    formattedProposals = proposals.map(proposal => ({
      protocol_hash: proposal
    }))
  } 

  return formattedProposals
}

function* fetchVotingPeriodsInitSaga() {
  const [
    { data: quorum },
    { data: votingPeriod },
    { data: currentProposal },
    { data: votes },
    { data: voters },
    { data: proposals }
  ] = yield all([
    call(fetchApi, { url: votingQuorumUrl }),
    call(fetchApi, { url: votingPeriodUrl }),
    call(fetchApi, { url: currentProposalUrl }),
    call(fetchApi, { url: votingUrl }),
    call(fetchApi, { url: voterListingUrl }),
    call(fetchApi, { url: proposalListUrl }),
  ])

  let isShowChart = false

  if (votingPeriod === "testing_vote" || votingPeriod === "promotion_vote") {
    isShowChart = true
  }
  const formattedVotes = parseVotes(votes)
  const participationData = getParticipation({ voters, votes: formattedVotes })
  const formattedProposals = getProposals(proposals)

  yield put(fetchVotingPeriodsSuccess({
    quorum: Number(quorum || 0) / 100,
    votingPeriod,
    currentProposal,
    votes: formattedVotes,
    voters,
    majority: getSuperMajority(votes),
    ...participationData,
    proposals: formattedProposals,
    isShowChart
  }))
}

export default function* profileRootSaga() {
  yield all([
    takeEvery(types.VOTING_PERIODS_INIT, fetchVotingPeriodsInitSaga),
  ])
}
