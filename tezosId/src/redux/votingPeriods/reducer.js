import { types } from './actions'

const initialState = {
  votes: null,
  currentProposal: null,
  votingPeriod: null,
  voters: null,
  quorum: 0,
  majority: 0,
  participation: 0,
  totalRoll: 0,
  votedRoll: 0,
  proposals: null,
  isLoading: false,
  isShowChart: false,
}

export default function votingPeriodsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.VOTING_PERIODS_INIT:
      return {
        ...state,
        isLoading: true,
      }
    case types.VOTING_PERIODS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        ...action.data,
      }
    default:
      return state
  }
}