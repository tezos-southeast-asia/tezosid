import { all, call, put, takeEvery } from 'redux-saga/effects'
import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import {
    types,
    fetchActionDataSuccess,
    fetchActionDataFailure
} from './actions'
import { fetchApi } from '../../helpers/api'

function* fetchChartSaga({ actionType }) {
    const url = connectionConfig.chart + actionType + '.json';

    const apiOptions = {
        url
    }
    const { data, error } = yield call(fetchApi, apiOptions)

    if (data) {
        yield put(fetchActionDataSuccess({ data: data, type: actionType }))
    } else {
        yield put(fetchActionDataFailure({ error, type: actionType }))
    }
}


export default function* chartRootSaga() {
    yield all([
        takeEvery(types.CHART_DATA_REQUEST, fetchChartSaga)
    ])
}
