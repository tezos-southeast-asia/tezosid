import { combineReducers } from 'redux'
import bakerBlockRewards from './bakerBlockRewards'
import priorities from './priorities'
import topBakers from './topBakers'
import totalActivation from './totalActivation'
import totalBlocks from './totalBlocks'
import totalEndorsement from './totalEndorsement'
import totalOps from './totalOps'
import totalTransFee from './totalTransFee'
import totalTransaction from './totalTransaction'
import transAvgOp from './transAvgOp'
import bakeTime from './bakeTime'

export default combineReducers({
  bakerBlockRewards,
  priorities,
  topBakers,
  totalActivation,
  totalBlocks,
  totalEndorsement,
  totalOps,
  totalTransFee,
  totalTransaction,
  transAvgOp,
  bakeTime
})