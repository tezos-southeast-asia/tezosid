import { types } from '../actions'

const TYPE = 'transAvgOp'
const initialState =  {
  isLoading: false,
  isError: false,
  data: [],
}

export default function transAvgOpReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.CHART_DATA_REQUEST: {
      if (action.actionType === TYPE) {
        return {
          ...state,
          isLoading: true,
          isError: false,
          data: [],
        }
      } else {
        return state
      }
    }
    case types.CHART_DATA_SUCCESS[TYPE]: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      }
    }
    case types.CHART_DATA_FAILURE[TYPE]: {
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: [],
      }
    }
    default:
      return state
  }
}