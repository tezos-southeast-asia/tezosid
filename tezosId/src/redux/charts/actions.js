export const types = {
  CHART_DATA_REQUEST: 'CHART_DATA_REQUEST',
  CHART_DATA_SUCCESS: {
    bakerBlockRewards: 'CHART_DATA_SUCCESS_BAKER_BLOCK_REWARDS',
    priorities: 'CHART_DATA_SUCCESS_PRIORITIES',
    topBakers: 'CHART_DATA_SUCCESS_TOP_BAKERS',
    totalActivation: 'CHART_DATA_SUCCESS_TOTAL_ACTIVATION',
    totalBlocks: 'CHART_DATA_SUCCESS_TOTAL_BLOCKS',
    totalEndorsement: 'CHART_DATA_SUCCESS_TOTAL_ENDORSEMENT',
    totalOps: 'CHART_DATA_SUCCESS_TOTAL_OPS',
    totalTransFee: 'CHART_DATA_SUCCESS_TOTAL_TRANS_FEE',
    totalTransaction: 'CHART_DATA_SUCCESS_TOTAL_TRANSACTION',
    transAvgOp: 'CHART_DATA_SUCCESS_TRANS_AVG_OP',
    bakeTime:'CHART_DATA_SUCCESS_BAKE_TIME'
  },
  CHART_DATA_FAILURE: {
    bakerBlockRewards: 'CHART_DATA_FAILURE_BAKER_BLOCK_REWARDS',
    priorities: 'CHART_DATA_FAILURE_PRIORITIES',
    topBakers: 'CHART_DATA_FAILURE_TOP_BAKERS',
    totalActivation: 'CHART_DATA_FAILURE_TOTAL_ACTIVATION',
    totalBlocks: 'CHART_DATA_FAILURE_TOTAL_BLOCKS',
    totalEndorsement: 'CHART_DATA_FAILURE_TOTAL_ENDORSEMENT',
    totalOps: 'CHART_DATA_FAILURE_TOTAL_OPS',
    totalTransFee: 'CHART_DATA_FAILURE_TOTAL_TRANS_FEE',
    totalTransaction: 'CHART_DATA_FAILURE_TOTAL_TRANSACTION',
    transAvgOp: 'CHART_DATA_FAILURE_TRANS_AVG_OP',
    bakeTime:'CHART_DATA_FAILURE_BAKE_TIME'
  }
}

export const fetchActionData = ({ type }) => ({
  type: types.CHART_DATA_REQUEST,
  actionType: type,
})

export const fetchActionDataSuccess = ({ type, data }) => ({
  type: types.CHART_DATA_SUCCESS[type],
  data 
})

export const fetchActionDataFailure = ({ type, error }) => ({
  type: types.CHART_DATA_FAILURE[type],
  error
})
