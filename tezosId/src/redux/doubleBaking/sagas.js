import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'

import { dateFormat } from "../../helpers/format";

import {
  PER_PAGE,
  DEFAULT_PAGE,
  DEFAULT_TOTAL,
  types,
  fetchDoubleBakingSuccess,
  fetchDoubleBakingFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseDoubleBaking = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = DEFAULT_TOTAL }) => {
  const doubleBaking = data.map(({ op, bh1s }) => {
    const { opHash, uuid, blockLevel } = op || {}
    const { timestamp, level } = bh1s || {}

    return {
      id: opHash,
      key: uuid,
      opHash: opHash,
      blockId: [blockLevel],
      timestamp: dateFormat(timestamp),
      // baker: denouncer,
      // bakerReward: gain_rewards,
      // offender: offender,
      denouncedLevel: [level],
      // lostDeposit: lost_deposit,
      // lostRewards: lost_rewards,
      // lostFees: lost_fees
    }
  })

  return {
    currentPage,
    pageSize,
    total: total,
    data: doubleBaking
  }
}

function* fetchDoubleBakingSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.doubleBakings,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }

  const totalApiOptions = {
    url: connectionConfig.doubleBakingsNum,
  }

  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])

  let formattedData

  if (data) {
    formattedData = parseDoubleBaking({ data, currentPage, pageSize, total })
    yield put(fetchDoubleBakingSuccess(formattedData))
  } else {
    yield put(fetchDoubleBakingFailure(error))
  }
}

export default function* doubleBakingsRootSaga() {
  yield all([
    takeEvery(types.DOUBLEBAKING_REQUEST, fetchDoubleBakingSaga),
  ])
}
