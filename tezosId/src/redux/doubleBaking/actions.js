export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  DOUBLEBAKING_REQUEST: 'DOUBLEBAKING_REQUEST',
  DOUBLEBAKING_SUCCESS: 'DOUBLEBAKING_SUCCESS',
  DOUBLEBAKING_FAILURE: 'DOUBLEBAKING_FAILURE',
}

export const fetchDoubleBaking = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.DOUBLEBAKING_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchDoubleBakingSuccess = (data) => ({
  type: types.DOUBLEBAKING_SUCCESS,
  data
})

export const fetchDoubleBakingFailure = (error) => ({
  type: types.DOUBLEBAKING_FAILURE,
  error
})
