import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  doubleBaking: {
    data: null,
    total: DEFAULT_TOTAL,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function doubleBakingReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.DOUBLEBAKING_REQUEST:
      return {
        ...state,
        isLoading: true,
        doubleBaking: {
          data: null,
          total: state.doubleBaking.total,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.DOUBLEBAKING_SUCCESS: {
      const doubleBaking = action.data
      return {
        ...state,
        isLoading: false,
        isError: false,
        doubleBaking: {
          ...doubleBaking,
        }
      }
    }
    case types.DOUBLEBAKING_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        doubleBaking: initialState.doubleBaking
      }

    default:
      return state
  }
}