import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'

import {
    types,
    PER_PAGE,
    DEFAULT_PAGE,
    TOTAL_PROPOSERS_VOTERS,
    fetchProposersVotersSuccess,
    fetchProposersVotersFailure
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseProposersVoters = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = TOTAL_PROPOSERS_VOTERS }) => {
    let rowKey = 1;
    const proposers = data.map(({ op, ballot }) => {
        const { period, source } = ballot;
        const { opHash } = op;
        const proposer = {
            key: rowKey,
            period: period,
            opHash,
            ballot:ballot['ballot'],
            //count: count,
            //votes: "0",
            source,
        };
        rowKey++;
        return proposer;
    })
    return {
        currentPage,
        pageSize,
        total,
        data: proposers
    }
}

function* fetchProposersVotersSaga(provider) {
    const {
        hash,
        currentPage,
        pageSize,
    } = provider

    const apiOptions = {
        url: connectionConfig.proposalVoters + hash,
        params: {
            p: currentPage - 1,
            n: pageSize,
        }
    }

    const totalApiOptions = {
        url: connectionConfig.proposalVotersNum + hash
    }
    const [{ data, error }, { data: total }] = yield all([
        call(fetchApi, apiOptions),
        call(fetchApi, totalApiOptions),
    ])
    let formattedData;

    if (data) {
        const {currentPage, pageSize} = provider;
        formattedData = parseProposersVoters({ data, currentPage, pageSize, total })
        yield put(fetchProposersVotersSuccess(formattedData))
    } else {
        yield put(fetchProposersVotersFailure(error))
    }
}

export default function* proposersRootSaga() {
    yield all([
        takeEvery(types.PROPOSERS_VOTERS_REQUEST, fetchProposersVotersSaga),
    ])
}
