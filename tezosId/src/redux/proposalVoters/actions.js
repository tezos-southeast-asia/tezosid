export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const TOTAL_PROPOSERS_VOTERS = 0
export const HASH = ''
export const types = {
  PROPOSERS_VOTERS_REQUEST: 'PROPOSERS_VOTERS_REQUEST',
  PROPOSERS_VOTERS_SUCCESS: 'PROPOSERS_VOTERS_SUCCESS',
  PROPOSERS_VOTERS_FAILURE: 'PROPOSERS_VOTERS_FAILURE',
}

export const fetchProposersVoters = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, hash }) => ({
  type: types.PROPOSERS_VOTERS_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize),
  hash,
})

export const fetchProposersVotersSuccess = (data) => ({
  type: types.PROPOSERS_VOTERS_SUCCESS,
  data
})

export const fetchProposersVotersFailure = (error) => ({
  type: types.PROPOSERS_VOTERS_FAILURE,
  error
})