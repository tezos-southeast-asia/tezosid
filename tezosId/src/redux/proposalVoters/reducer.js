import { types, PER_PAGE, DEFAULT_PAGE, TOTAL_PROPOSERS_VOTERS } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  voters: {
    data: null,
    total: TOTAL_PROPOSERS_VOTERS,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function proposersVotersReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.PROPOSERS_VOTERS_REQUEST:
      return {
        ...state,
        isLoading: true,
        voters: {
          data: null,
          total: TOTAL_PROPOSERS_VOTERS,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.PROPOSERS_VOTERS_SUCCESS: {
      const voters = action.data
      return {
        ...state,
        isLoading: false,
        isError: false,
        voters: {
          ...voters,
        }
      }
    }
    case types.PROPOSERS_VOTERS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        voters: initialState.voters
      }

    default:
      return state
  }
}