import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  activations: {
    data: null,
    total: DEFAULT_TOTAL,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function activationsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ACTIVATIONS_REQUEST:
      return {  
        ...state,
        isLoading: true,
        activations: {
          data: null,
          total: state.activations.total,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.ACTIVATIONS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        activations : action.data
        // endorsements: {
        //   ...endorsements,
        //   total: DEFAULT_TOTAL,
        // }
      }
    } 
    case types.ACTIVATIONS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        activations: initialState.activations
      }

    default:
      return state
  }
}