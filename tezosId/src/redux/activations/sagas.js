import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import { dateFormat } from "../../helpers/format";

import {
  PER_PAGE,
  DEFAULT_PAGE,
  DEFAULT_TOTAL,
  types,
  fetchActivationsSuccess,
  fetchActivationsFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseActivations = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = DEFAULT_TOTAL }) => {
  const activations = data.map(({ op, activateAccount }, index) => {
    const { blockTimestamp, blockHash, opHash } = op || {}
    const { secret, uuid, pkh } = activateAccount || {}

    return {
      id: opHash,
      key: uuid || index,
      blockId: [blockHash],
      timestamp: dateFormat(blockTimestamp),
      secret,
      opHash: opHash,
      hash: pkh
    }
  })

  return {
    currentPage,
    pageSize,
    total,
    data: activations
  }
}

function* fetchActivationsSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.activations,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }

  const totalApiOptions = {
    url: connectionConfig.activationsNum,
  }

  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])
  let formattedData

  if (data) {
    formattedData = parseActivations({ data, currentPage, pageSize, total })
    yield put(fetchActivationsSuccess(formattedData))
  } else {
    yield put(fetchActivationsFailure(error))
  }
}

export default function* activationsRootSaga() {
  yield all([
    takeEvery(types.ACTIVATIONS_REQUEST, fetchActivationsSaga),
  ])
}
