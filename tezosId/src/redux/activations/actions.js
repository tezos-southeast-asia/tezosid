export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  ACTIVATIONS_REQUEST: 'ACTIVATIONS_REQUEST',
  ACTIVATIONS_SUCCESS: 'ACTIVATIONS_SUCCESS',
  ACTIVATIONS_FAILURE: 'ACTIVATIONS_FAILURE',
}

export const fetchActivations = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.ACTIVATIONS_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchActivationsSuccess = (data) => ({
  type: types.ACTIVATIONS_SUCCESS,
  data
})

export const fetchActivationsFailure = (error) => ({
  type: types.ACTIVATIONS_FAILURE,
  error
})
