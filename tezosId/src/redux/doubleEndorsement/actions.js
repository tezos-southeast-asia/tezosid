export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  DOUBLE_ENDORSEMENT_REQUEST: 'DOUBLE_ENDORSEMENT_REQUEST',
  DOUBLE_ENDORSEMENT_SUCCESS: 'DOUBLE_ENDORSEMENT_SUCCESS',
  DOUBLE_ENDORSEMENT_FAILURE: 'DOUBLE_ENDORSEMENT_FAILURE',
}

export const fetchDoubleEndorsement = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.DOUBLE_ENDORSEMENT_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchDoubleEndorsementSuccess = (data) => ({
  type: types.DOUBLE_ENDORSEMENT_SUCCESS,
  data
})

export const fetchDoubleEndorsementFailure = (error) => ({
  type: types.DOUBLE_ENDORSEMENT_FAILURE,
  error
})
