import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  doubleEndorsement: {
    data: null,
    total: DEFAULT_TOTAL,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function doubleEndorsementReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.DOUBLE_ENDORSEMENT_REQUEST:
      return {
        ...state,
        isLoading: true,
        doubleEndorsement: {
          data: null,
          total: state.doubleEndorsement.total,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.DOUBLE_ENDORSEMENT_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        doubleEndorsement: action.data
      }
    } 
    case types.DOUBLE_ENDORSEMENT_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        doubleEndorsement: initialState.doubleEndorsement
      }

    default:
      return state
  }
}