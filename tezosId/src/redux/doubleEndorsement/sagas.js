import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'

import {
  PER_PAGE,
  DEFAULT_PAGE,
  DEFAULT_TOTAL,
  types,
  fetchDoubleEndorsementSuccess,
  fetchDoubleEndorsementFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'
import { dateFormat } from "../../helpers/format";

const parseDoubleEndorsement = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = DEFAULT_TOTAL }) => {
  const doubleEndorsement = data.map(({ op, op1s }) => {
    const { opHash, uuid, blockLevel, blockTimestamp } = op;
    const { level } = op1s;
    return {
      id: opHash,
      key: uuid,
      opHash,
      blockId:blockLevel,
      timestamp:dateFormat(blockTimestamp),
      denouncedLevel:level
    }
  })

  return {
    currentPage,
    pageSize,
    total,
    data: doubleEndorsement
  }
}

function* fetchDoubleEndorsementsSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.doubleEndorsements,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }

  const totalApiOptions = {
    url: connectionConfig.doubleEndorsementsNum,
  }

  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])
  let formattedData

  if (data) {
    formattedData = parseDoubleEndorsement({ data, currentPage, pageSize, total })
    yield put(fetchDoubleEndorsementSuccess(formattedData))
  } else {
    yield put(fetchDoubleEndorsementFailure(error))
  }
}

export default function* doubleEndorsementsRootSaga() {
  yield all([
    takeEvery(types.DOUBLE_ENDORSEMENT_REQUEST, fetchDoubleEndorsementsSaga),
  ])
}
