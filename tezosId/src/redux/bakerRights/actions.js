export const DEFAULT_PAGE_SIZE = 10
export const DEFAULT_PAGE = 1
export const TOTAL = 20480
export const DEFAULT_TAB = 'upcoming'
export const MAX_PRIORITY = 4

export const types = {
  BAKER_RIGHTS_UPDATE_TAB: 'BAKER_RIGHTS_UPDATE_TAB',
  BAKER_RIGHTS_LEVEL_SUCCESS: 'BAKER_RIGHTS_LEVEL_SUCCESS',
  BAKER_RIGHTS_LEVEL_FAILURE: 'BAKER_RIGHTS_LEVEL_FAILURE',
  BAKER_RIGHTS_RECORDS_REQUEST: 'BAKER_RIGHTS_RECORDS_REQUEST',
  BAKER_RIGHTS_RECORDS_SUCCESS: {
    upcoming: 'BAKER_RIGHTS_RECORDS_UPCOMING_SUCCESS',
    past: 'BAKER_RIGHTS_RECORDS_PAST_SUCCESS',
  },
  BAKER_RIGHTS_RECORDS_FAILURE: {
    upcoming: 'BAKER_RIGHTS_RECORDS_UPCOMING_FAILURE',
    past: 'BAKER_RIGHTS_RECORDS_PAST_FAILURE',
  },
  BAKER_RIGHTS_LEVEL_START_BACKGROUND_SYNC: 'BAKER_RIGHTS_LEVEL_START_BACKGROUND_SYNC',
  BAKER_RIGHTS_LEVEL_STOP_BACKGROUND_SYNC: 'BAKER_RIGHTS_LEVEL_STOP_BACKGROUND_SYNC'
}

export const vaildTabs = Object.keys(types.BAKER_RIGHTS_RECORDS_SUCCESS)

export const updateBakerRightsTab = ({ tab = DEFAULT_TAB }) => ({
  type: types.BAKER_RIGHTS_UPDATE_TAB,
  tab,
})

export const startLevelBackgroundSync = () => ({
  type: types.BAKER_RIGHTS_LEVEL_START_BACKGROUND_SYNC,
})

export const stopLevelBackgroundSync = () => ({
  type: types.BAKER_RIGHTS_LEVEL_STOP_BACKGROUND_SYNC,
})

export const fetchBakerRightsLevelSuccess = (level) => ({
  type: types.BAKER_RIGHTS_LEVEL_SUCCESS,
  level
})

export const fetchBakerRightsLevelFailure = (error) => ({
  type: types.BAKER_RIGHTS_LEVEL_FAILURE,
  error
})

export const fetchBakerRightsRecords = ({ level, tab = DEFAULT_TAB, currentPage = DEFAULT_PAGE, pageSize = DEFAULT_PAGE_SIZE }) => ({
  type: types.BAKER_RIGHTS_RECORDS_REQUEST,
  level,
  tab,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize),
})

export const fetchBakerRightsRecordsSuccess = ({ data, tab }) => ({
  type: types.BAKER_RIGHTS_RECORDS_SUCCESS[tab],
  data
})

export const fetchBakerRightsRecordsFailure = ({ error, tab }) => ({
  type: types.BAKER_RIGHTS_RECORDS_FAILURE[tab],
  error
})