import { types } from '../actions'

const initialState = ''

export default function bakerRightsLevelReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.BAKER_RIGHTS_LEVEL_SUCCESS:
      return action.level
    default:
      return state
  }
}