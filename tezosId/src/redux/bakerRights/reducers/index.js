import { combineReducers } from 'redux'
import level from './level'
import tabs from './tabs'
import upcoming from './upcoming'
import past from './past'

export default combineReducers({
  level,
  tabs,
  upcoming,
  past,
})