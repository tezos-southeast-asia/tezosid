import { types, DEFAULT_PAGE, DEFAULT_PAGE_SIZE, TOTAL } from '../actions'

const TYPE = 'past'
const initialState =  {
  isLoading: false,
  isError: false,
  data: [],
  total: TOTAL,
  currentPage: DEFAULT_PAGE,
  pageSize: DEFAULT_PAGE_SIZE,
}

export default function pastReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.BAKER_RIGHTS_RECORDS_REQUEST:
    case types.BAKER_RIGHTS_REQUEST: {
      if (action.tab === TYPE) {
        return {
          ...state,
          currentPage: action.currentPage,
          pageSize: action.pageSize,
          isLoading: true,
          isError: false,
          data: [],
        }
      } else {
        return state
      }
    }
    case types.BAKER_RIGHTS_RECORDS_SUCCESS[TYPE]:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      }
    case types.BAKER_RIGHTS_RECORDS_FAILURE[TYPE]:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: [],
      }
    default:
      return state
  }
}