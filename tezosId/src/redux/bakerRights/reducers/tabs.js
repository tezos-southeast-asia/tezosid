import { types, DEFAULT_TAB } from '../actions'

const initialState = {
  defaultActiveKey: DEFAULT_TAB,
  activeKey: DEFAULT_TAB,
}

export default function bakerRightTabsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.BAKER_RIGHTS_UPDATE_TAB:
      return {
        ...state,
        activeKey: action.tab,
      }
    default:
      return state
  }
}