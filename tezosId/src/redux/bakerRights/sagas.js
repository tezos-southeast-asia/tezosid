import { all, call, put, takeEvery, fork, take, cancel, cancelled } from 'redux-saga/effects'
import { delay } from 'redux-saga'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import {
  types,
  fetchBakerRightsLevelSuccess,
  fetchBakerRightsLevelFailure,
  fetchBakerRightsRecordsSuccess,
  fetchBakerRightsRecordsFailure,
  MAX_PRIORITY,
  DEFAULT_TAB,
} from './actions'
import { fetchApi } from '../../helpers/api'

function* fetchBakerRightsLevelSaga() {
  const apiOptions = {
    url: connectionConfig.baker_rights,
    params: {
      max_priority: MAX_PRIORITY,
      all: '',
    }
  }
  const { data, error } = yield call(fetchApi, apiOptions)
  const level = data && data[0] && data[0].level
  const resError = !level ? 'No level' : error

  if (level) {
    yield put(fetchBakerRightsLevelSuccess(level))
  } else {
    yield put(fetchBakerRightsLevelFailure(resError))
  }

  return level
}

const parseBakerRightsRecords = ({ records, tab }) => {
  let inValidCount = 0
  const formattedData = records && records.map(({ data }) => {
    const level = data[0] && data[0].level

    if (data.length < 4 || !level) {
      inValidCount++
      return null
    }

    return {
      key: `${tab}-${level}`,
      level: [level],
      first: data[0].delegate,
      second: data[1].delegate,
      third: data[2].delegate,
      fourth: data[3].delegate,
    }
  })

  return {
    data: formattedData,
    error: inValidCount === records.length ? 'Failed to fetch Baker Rights Records' : null
  }
}

function* fetchBakerRightsRecordsSaga({ level, tab, currentPage, pageSize }) {
  const multiplier = tab === DEFAULT_TAB ? 1 : -1
  const offset = tab === DEFAULT_TAB ? 0 : -1
  let apiOptions = []
  let start = 0
  let itemLevel = ''

  while (start < pageSize) {
    itemLevel = level + multiplier * start + pageSize * (currentPage - 1) + offset
    apiOptions[start++] = {
      url: connectionConfig.baker_rights,
      params: {
          max_priority: MAX_PRIORITY,
          level: itemLevel,
          all: '', 
      }
    }
  }
  const records = yield all(apiOptions.map((apiOption) => call(fetchApi, apiOption)))
  const { data, error } = parseBakerRightsRecords({ records, tab })
  if (!error) {
    yield put(fetchBakerRightsRecordsSuccess({ data, tab }))
  } else {
    yield put(fetchBakerRightsRecordsFailure({ error, tab })) 
  }
}

function* bgLevelSync() {
  const delayTime = 60000

  try {
    while (true) {
      yield call(fetchBakerRightsLevelSaga)
      yield delay(delayTime)
    }
  } finally {
    yield cancelled()
  }
}

export default function* bakerRightsRootSaga() {
  yield all([
    takeEvery(types.BAKER_RIGHTS_RECORDS_REQUEST, fetchBakerRightsRecordsSaga),
  ])

  while (yield take(types.BAKER_RIGHTS_LEVEL_START_BACKGROUND_SYNC)) {
    // starts the task in the background
    const bgLevelSyncTask = yield fork(bgLevelSync)

    // wait for the user stop action
    yield take(types.BAKER_RIGHTS_LEVEL_STOP_BACKGROUND_SYNC)
    // cancel the background task
    // this will cause the forked bgSync task to jump into its finally block
    yield cancel(bgLevelSyncTask)
  }
}
