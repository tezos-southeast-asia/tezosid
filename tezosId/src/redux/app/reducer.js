import actions, { getView } from "./actions";

const initState = {
  collapsed: window.innerWidth > 1220 ? false : true,
  view: getView(window.innerWidth),
  height: window.innerHeight,
  isOpenDrawer: false,
  isOpenSearchBox: true
};

export default function appReducer(state = initState, action) {
  switch (action.type) {
    case actions.COLLPSE_CHANGE:
      return {
        ...state,
        collapsed: !state.collapsed
      };
    case actions.COLLPSE_OPEN_DRAWER:
      return {
        ...state,
        isOpenDrawer: !state.isOpenDrawer
      };
    case actions.CLOSE_DRAWER:
      return {
        ...state,
        isOpenDrawer: false
      };
    case actions.TOGGLE_ALL:
      if (state.view !== action.view || action.height !== state.height) {
        const height = action.height ? action.height : state.height;
        return {
          ...state,
          collapsed: action.collapsed,
          view: action.view,
          height
        };
      }
      break;
    case actions.TOGGLE_SEARCH_BOX:
      return {
        ...state,
        isOpenSearchBox: !state.isOpenSearchBox
      };
    case actions.CLOSE_SEARCH_BOX:
      return {
        ...state,
        isOpenSearchBox: false
      };
    default:
      return state;
  }
  return state;
}
