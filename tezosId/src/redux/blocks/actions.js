export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  BLOCKS_REQUEST: 'BLOCKS_REQUEST',
  BLOCKS_SUCCESS: 'BLOCKS_SUCCESS',
  BLOCKS_FAILURE: 'BLOCKS_FAILURE',
}

export const fetchBlocks = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.BLOCKS_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchBlocksSuccess = (data) => ({
  type: types.BLOCKS_SUCCESS,
  data
})

export const fetchBlocksFailure = (error) => ({
  type: types.BLOCKS_FAILURE,
  error
})
