import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  blocks: {
    data: null,
    total: DEFAULT_TOTAL,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function blocksReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.BLOCKS_REQUEST:
      return {
        ...state,
        isLoading: true,
        blocks: {
          data: null,
          total: state.blocks.total,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.BLOCKS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        blocks: action.data,
      }
    case types.BLOCKS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        blocks: initialState.blocks
      }

    default:
      return state
  }
}