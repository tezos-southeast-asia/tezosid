import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import { dateFormat } from "../../helpers/format";

import {
  PER_PAGE,
  DEFAULT_PAGE,
  DEFAULT_TOTAL,
  types,
  fetchBlocksSuccess,
  fetchBlocksFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseBlocks = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = DEFAULT_TOTAL }) => {
  const blocks = data.map((item, index) => {
    const block = item.block
    const baker = item.blocksAlpha && item.blocksAlpha.baker
    const { blockStat } = item
    const blockEmpty = !block
    const { timestamp, hash, level } = block || {}
    let { opsCount } = blockStat;
    opsCount = opsCount !== null ? opsCount : 'Calculating';

    return {
      id: hash,
      key: hash || index,
      level: blockEmpty ? "-" : [level, hash],
      timestamp: blockEmpty ? "-" : dateFormat(timestamp),
      hash: blockEmpty ? "-" : hash,
      hashId: blockEmpty ? "-" : hash,
      operations: blockEmpty ? "-" : opsCount,
      baker: baker || "-"
    }
  })

  return {
    currentPage,
    pageSize,
    total,
    data: blocks
  }
}

function* fetchBlocksSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.blocks,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const totalApiOptions = {
    url: connectionConfig.blocksNum,
  }
  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions)
  ])

  let formattedData

  if (data) {
    formattedData = parseBlocks({ data, currentPage, pageSize, total })
    yield put(fetchBlocksSuccess(formattedData))
  } else {
    yield put(fetchBlocksFailure(error))
  }
}

export default function* blocksRootSaga() {
  yield all([
    takeEvery(types.BLOCKS_REQUEST, fetchBlocksSaga),
  ])
}
