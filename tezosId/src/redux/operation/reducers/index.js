import { combineReducers } from 'redux'
import overview from './overview'
import operations from './operations'
import tabs from './tabs'
import opTabs from './opTabs'

export default combineReducers({
  overview,
  tabs,
  opTabs,
  operations,
})