export const types = {
  OPERATION_INIT: 'OPERATION_INIT',
  OPERATION_MAIN_TAB_UPDATE: 'OPERATION_MAIN_TAB_UPDATE',
  OPERATION_OVERVIEW_REQUEST: 'OPERATION_OVERVIEW_REQUEST',
  OPERATION_OVERVIEW_SUCCESS: 'OPERATION_OVERVIEW_SUCCESS',
  OPERATION_OVERVIEW_FAILURE: 'OPERATION_OVERVIEW_FAILURE',
  OPERATION_ACTION_NUMBER_REQUEST: 'OPERATION_ACTION_NUMBER_REQUEST',
  OPERATION_ACTION_NUMBER_SUCCESS: {
    transactions: 'OPERATION_ACTION_NUMBER_SUCCESS_TRANSACTIONS',
    delegations: 'OPERATION_ACTION_NUMBER_SUCCESS_DELEGATIONS',
    originations: 'OPERATION_ACTION_NUMBER_SUCCESS_ORIGINATIONS',
    endorsements: 'OPERATION_ACTION_NUMBER_SUCCESS_ENDORSEMENTS',
    activations: 'OPERATION_ACTION_NUMBER_SUCCESS_ACTIVATIONS',
    reveals: 'OPERATION_ACTION_NUMBER_SUCCESS_REVEALS',
    proposals: 'OPERATION_ACTION_NUMBER_SUCCESS_PROPOSALS',
    ballots: 'OPERATION_ACTION_NUMBER_SUCCESS_BALLOTS',
    nonces: 'OPERATION_ACTION_NUMBER_SUCCESS_NONCES',
    doubleBakings: 'OPERATION_ACTION_NUMBER_SUCCESS_DOUBLE_BAKINGS',
    doubleEndorsements:'OPERATION_ACTION_NUMBER_SUCCESS_DOUBLE_ENDORSEMENTS',
  },
  OPERATION_ACTION_NUMBER_FAILURE: {
    transactions: 'OPERATION_ACTION_NUMBER_FAILURE_TRANSACTIONS',
    delegations: 'OPERATION_ACTION_NUMBER_FAILURE_DELEGATIONS',
    originations: 'OPERATION_ACTION_NUMBER_FAILURE_ORIGINATIONS',
    endorsements: 'OPERATION_ACTION_NUMBER_FAILURE_ENDORSEMENTS',
    activations: 'OPERATION_ACTION_NUMBER_FAILURE_ACTIVATIONS',
    reveals: 'OPERATION_ACTION_NUMBER_FAILURE_REVEALS',
    proposals: 'OPERATION_ACTION_NUMBER_FAILURE_PROPOSALS',
    ballots: 'OPERATION_ACTION_NUMBER_FAILURE_BALLOTS',
    nonces: 'OPERATION_ACTION_NUMBER_FAILURE_NONCES',
    doubleBakings: 'OPERATION_ACTION_NUMBER_FAILURE_DOUBLE_BAKINGS',
    doubleEndorsements:'OPERATION_ACTION_NUMBER_FAILURE_DOUBLE_ENDORSEMENTS',
  },
  OPERATION_ACTION_REQUEST: 'OPERATION_ACTION_REQUEST',
  OPERATION_ACTION_SUCCESS: {
    transactions: 'OPERATION_ACTION_SUCCESS_TRANSACTIONS',
    delegations: 'OPERATION_ACTION_SUCCESS_DELEGATIONS',
    originations: 'OPERATION_ACTION_SUCCESS_ORIGINATIONS',
    endorsements: 'OPERATION_ACTION_SUCCESS_ENDORSEMENTS',
    activations: 'OPERATION_ACTION_SUCCESS_ACTIVATIONS',
    reveals: 'OPERATION_ACTION_SUCCESS_REVEALS',
    proposals: 'OPERATION_ACTION_SUCCESS_PROPOSALS',
    ballots: 'OPERATION_ACTION_SUCCESS_BALLOTS',
    nonces: 'OPERATION_ACTION_SUCCESS_NONCES',
    doubleBakings: 'OPERATION_ACTION_SUCCESS_DOUBLE_BAKINGS',
    doubleEndorsements:'OPERATION_ACTION_SUCCESS_DOUBLE_ENDORSEMENTS',
  },
  OPERATION_ACTION_FAILURE: {
    transactions: 'OPERATION_ACTION_FAILURE_TRANSACTIONS',
    delegations: 'OPERATION_ACTION_FAILURE_DELEGATIONS',
    originations: 'OPERATION_ACTION_FAILURE_ORIGINATIONS',
    endorsements: 'OPERATION_ACTION_FAILURE_ENDORSEMENTS',
    activations: 'OPERATION_ACTION_FAILURE_ACTIVATIONS',
    reveals: 'OPERATION_ACTION_FAILURE_REVEALS',
    proposals: 'OPERATION_ACTION_FAILURE_PROPOSALS',
    ballots: 'OPERATION_ACTION_FAILURE_BALLOTS',
    nonces: 'OPERATION_ACTION_FAILURE_NONCES',
    doubleBakings: 'OPERATION_ACTION_FAILURE_DOUBLE_BAKINGS',
    doubleEndorsements:'OPERATION_ACTION_FAILURE_DOUBLE_ENDORSEMENTS',
  },
  OPERATION_TOTAL_NUMBER_UPDATE: 'OPERATION_TOTAL_NUMBER_UPDATE',
  OPERATION_UPDATE_KEY_ARRAY: "OPERATION_UPDATE_KEY_ARRAY",
}

export const DEFAULT_PAGE_SIZE = 10
export const DEFAULT_PAGE = 1
export const vaildOperationTabs = Object.keys(types.OPERATION_ACTION_SUCCESS)
export const vaildMainTabs = ['overview', 'operations']
export const DEFAULT_TAB = vaildMainTabs[0]
export const DEFAULT_OPERATION_TAB = vaildOperationTabs[0]

export const initOperation = (props) => ({
  ...props,
  type: types.OPERATION_INIT,
})

export const updateMainTab = ({ tab = DEFAULT_TAB, hash, op = DEFAULT_OPERATION_TAB, p = DEFAULT_PAGE, n = DEFAULT_PAGE_SIZE }) => ({
  type: types.OPERATION_MAIN_TAB_UPDATE,
  tab,
  hash,
  op,
  currentPage: p,
  pageSize: n,
})

export const fetchOperationOverview = ({ hash }) => ({
  type: types.OPERATION_OVERVIEW_REQUEST,
  hash
})

export const fetchOperationOverviewSuccess = ({ data }) => ({
  type: types.OPERATION_OVERVIEW_SUCCESS,
  data,
})

export const fetchOperationOverviewFailure = (error) => ({
  type: types.OPERATION_OVERVIEW_FAILURE,
  error
})

export const fetchActionNumber = ({ hash, type = DEFAULT_TAB }) => ({
  type: types.OPERATION_ACTION_NUMBER_REQUEST,
  hash,
  actionType: type,
})

export const fetchActionNumberSuccess = ({ type, number }) => ({
  type: types.OPERATION_ACTION_NUMBER_SUCCESS[type],
  number
})

export const fetchActionNumberFailure = ({ type, error }) => ({
  type: types.OPERATION_ACTION_NUMBER_FAILURE[type],
  error
})

export const fetchOperationAction = ({ hash, tab = DEFAULT_OPERATION_TAB, currentPage = DEFAULT_PAGE, pageSize = DEFAULT_PAGE_SIZE }) => ({
  type: types.OPERATION_ACTION_REQUEST,
  hash,
  actionType: tab,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize),
})

export const fetchOperationActionSuccess = ({ type, data }) => ({
  type: types.OPERATION_ACTION_SUCCESS[type],
  data
})

export const fetchOperationActionFailure = ({ type, error }) => ({
  type: types.OPERATION_ACTION_FAILURE[type],
  error
})

export const updateOperationsTotalNumber = ({ operations }) => ({
  type: types.OPERATION_TOTAL_NUMBER_UPDATE,
  operations,
})

export const updateKeyArray = (key) => ({
  type: types.OPERATION_UPDATE_KEY_ARRAY,
  key
})
