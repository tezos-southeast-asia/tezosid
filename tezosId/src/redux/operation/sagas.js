import { all, call, put, takeEvery, select } from 'redux-saga/effects'
import React from "react";
import { Link } from "react-router-dom";

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import {
  types,
  DEFAULT_OPERATION_TAB,
  DEFAULT_PAGE,
  DEFAULT_PAGE_SIZE,
  vaildOperationTabs,
  updateMainTab,
  fetchOperationOverview,
  fetchOperationOverviewSuccess,
  fetchOperationOverviewFailure,
  fetchActionNumberSuccess,
  fetchActionNumberFailure,
  fetchOperationAction,
  fetchOperationActionSuccess,
  fetchOperationActionFailure,
  updateOperationsTotalNumber,
} from './actions'
import { fetchApi } from '../../helpers/api'
import { dateFormat } from "../../helpers/format";

const parseOperationOverview = ({ data = {} }) => {
  const titles = [
    "table.blockHash",
    "table.levelInfo",
    "table.timeStampInfo",
    "table.branch",
    "table.publicKey",
    "table.signatureInfo"
  ];
  const keys = [
    "blockHash",
    "blockLevel",
    "blockTimestamp",
    "branch",
    "public_key",
    "signature"
  ];
  const tableData = keys.map((key, index) => {
    let cellData = data[key]
    let tableKey = key

    if (cellData === undefined) {
      cellData = ""
    }

    switch (key) {
      case 'blockTimestamp':
        cellData = dateFormat(cellData)
        break
      case 'blockHash':
        cellData = [ cellData, cellData ]
        tableKey = 'blockId'
        break
      case 'blockLevel':
        cellData = [ cellData, cellData ]
        tableKey = 'level'
        break
      default:
    }

    return {
      id: tableKey,
      key: tableKey,
      info: titles[index],
      value: cellData,
    }
  })

  return tableData;
}

function* fetchOperationOverviewSaga({ hash }) {
  const apiOptions = {
    url: connectionConfig.operation + hash,
  }

  const { data, error } = yield call(fetchApi, apiOptions)

  if (data) {
    yield put(fetchOperationOverviewSuccess({ data: parseOperationOverview({ data }) }))
  } else {
    yield put(fetchOperationOverviewFailure(error))
  }
}

function* fetchActionNumberSaga({ hash, actionType }) {
  const apiOptions = {
    url: connectionConfig.operationNum[actionType] + hash,
  }

  const { data, error } = yield call(fetchApi, apiOptions)

  if (!error) {
    yield put(fetchActionNumberSuccess({ number: data, type: actionType }))
  } else {
    yield put(fetchActionNumberFailure({ error, type: actionType }))
  }
}

const parsers = {
  transactions: (data, hash) => data.map(({ tx, op, internalTx }, index) => {
    const { fee, amount, source, destination, blockTimestamp, uuid, blockLevel, operationResultStatus } = tx || {}
    const { opHash, blockHash } = op || {};
    if (internalTx.length > 0) {
      const internalTransaction = internalTx.map(({ destination, amount, source, resultStatus }, index) => {
        return {
          id: opHash,
          key: index,
          opHash: opHash,
          amount,
          level: [blockLevel, blockHash],
          fromElement: [source],
          toElement: [destination],
          timestamp: dateFormat(blockTimestamp),
          fee,
          status: resultStatus !== 'applied' ? 'status-color' : ''
        }
      })
      const transaction = {
        key: uuid || index,
        timestamp: dateFormat(blockTimestamp),
        fromElement: [source, hash],
        toElement: [destination, hash],
        blockId: blockLevel,
        fee,
        amount,
        status: operationResultStatus !== 'applied' ? 'status-color' : '',
        internalTx: internalTransaction
      }
      return transaction
    } else {
      return {
        key: uuid || index,
        timestamp: dateFormat(blockTimestamp),
        fromElement: [source, hash],
        toElement: [destination, hash],
        blockId: blockLevel,
        fee,
        amount,
        status: operationResultStatus !== 'applied' ? 'status-color icon-visible' : 'icon-visible',
        internalTx: []
      }
    }
  }),

  delegations: (data) => data.map(({ op, delegation }) => {
    const { blockHash, blockTimestamp } = op || {}
    const { delegate, fee, uuid, source, operationResultStatus } = delegation || {}

    return {
      key: uuid,
      timestamp: dateFormat(blockTimestamp),
      blockId: [blockHash],
      delegateSource: source || '-',
      delegateDestination: delegate || '-',
      fee,
      status: operationResultStatus !== 'applied' ? 'status-color' : ''
    }

  }),

  originations: (data) => data.map(({ op, origination }, index) => {
    const { blockHash, blockTimestamp } = op || {}
    const { source, fee, uuid, balance, operationResultOriginatedContracts, operationResultStatus } = origination || {}

    return {
      key: uuid || index,
      timestamp: dateFormat(blockTimestamp),
      blockId: [blockHash],
      originateSource: source,
      fee,
      newBalance: balance,
      newAccount: operationResultOriginatedContracts[0],
      status: operationResultStatus !== 'applied' ? 'status-color' : ''
    }
  }),

  endorsements: (data) => data.map(({ op, endor }, index) => {
    const { blockHash } = op || {}
    const { slots, uuid } = endor || {}
    let slot = '';
    for (let i = 0; i < slots.length; i++) {
      slot += slots[i];
      if (i !== slots.length - 1) {
        slot += ', '
      }
    }

    return {
      key: uuid || index,
      blockId: [blockHash],
      slot: slot
    }
  }),
  activations: (data) => data.map(({ op, activateAccount }, index) => {
    const { blockTimestamp, blockHash, opHash } = op || {}
    const { secret, uuid, pkh } = activateAccount || {}

    return {
      id: opHash,
      key: uuid || index,
      blockId: [blockHash],
      timestamp: dateFormat(blockTimestamp),
      secret,
      opHash: opHash,
      hash: pkh
    }
  }),
  reveals: (data) => data.map(({ op, reveal }, index) => {
    const { blockTimestamp, blockHash, opHash } = op || {}
    const { publicKey, uuid, counter, source, fee, gasLimit, storageLimit, operationResultStatus } = reveal || {}
    return {
      id: opHash,
      key: uuid || index,
      blockId: [blockHash],
      timestamp: dateFormat(blockTimestamp),
      publicKey,
      counter,
      source,
      fee,
      gasLimit,
      storageLimit,
      opHash,
      status: operationResultStatus !== 'applied' ? 'status-color' : ''
    }
  }),
  proposals: (data) => data.map(({ op, proposal }, index) => {
    const { proposals, source, period, uuid } = proposal || {}
    const { opHash } = op || {}

    return {
      id: opHash,
      key: uuid || index,
      hash: proposals[0],
      proposalSource: source,
      period: period
    }
  }),
  ballots: (data) => data.map(({ op, ballot }, index) => {
    const { period, proposal, source, uuid } = ballot || {}
    const { opHash } = op || {}

    return {
      id: opHash,
      key: uuid || index,
      hash: proposal,
      proposalSource: source,
      period: period,
      ballot: ballot["ballot"]
    }
  }),
  nonces: (data) => data.map(({ op, seedNonceEvelation }, index) => {
    const { nonce, uuid } = seedNonceEvelation || {}
    const { opHash } = op || {}

    return {
      id: opHash,
      key: uuid || index,
      nonce: nonce
    }
  }),

  doubleBakings: (data) => {
    const temp = [];
    data.map(({ bh1s, bh2s }) => {


      // const bh1s = data[0].bh1s;
      // const bh2s = data[0].bh2s;
      let blockAddress = "/blocks/";
      const keys = ['level', 'proto', 'predecessor', 'timestamp', 'validationPass', 'operationsHash', 'fitness', 'context', 'priority', 'proofOfWorkNonce', 'signature']
      const titles = {
        operationsHash: "table.hash",
        level: "table.doubleBakeLevel",
        timestamp: "table.timeStamp",
        predecessor: "table.predecessor",
        opHash: "table.opHash",
        proto: "table.proto",
        validationPass: "table.validationPass",
        protocolCount: "table.protocolCount",
        fitness: "table.fitness",
        context: "table.context",
        proofOfWorkNonce: "table.proofOfWorkNonce",
        priority: "table.priority",
        signature: "table.signature"
      };

      for (let i = 0; i < keys.length; i++) {
        const title = titles[keys[i]];
        let bh1sentry = bh1s[keys[i]];
        let bh2sentry = bh2s[keys[i]]

        if (keys[i] === 'fitness') {
          bh1sentry = bh1sentry.join(", ")
          bh2sentry = bh2sentry.join(", ")
        }

        if (keys[i] === 'timestamp') {
          bh1sentry = dateFormat(bh1sentry);
          bh2sentry = dateFormat(bh2sentry);
        }

        if (keys[i] === "predecessor") {
          bh1sentry = <Link to={blockAddress + bh1sentry}>{bh1sentry}</Link>;
          bh2sentry = <Link to={blockAddress + bh2sentry}>{bh2sentry}</Link>;
        }

        temp.push({
          id: i,
          key: i,
          info: title,
          bh1s: bh1sentry,
          bh2s: bh2sentry
        })
      }
      return bh1s;
    })
    return temp;
  },
  doubleEndorsements: (data) => {
    const temp = [];
    //let blockAddress = "/blocks/";
    data.map(({ op1s, op2s }) => {

      const keys = ['level', 'branch', 'signature']
      const titles = {
        level: "table.doubleEndorsementLevel",
        branch: "table.branch",
        signature: "table.signature"
      };

      for (let i = 0; i < keys.length; i++) {
        const title = titles[keys[i]];
        let op1sentry = op1s[keys[i]];
        let op2sentry = op2s[keys[i]]

        // if(keys[i]==='branch'){
        //   op1sentry = <Link to={blockAddress + op1sentry}>{op1sentry}</Link>;
        //   op2sentry = <Link to={blockAddress + op2sentry}>{op2sentry}</Link>;
        // }

        if (keys[i] === 'timestamp') {
          op1sentry = dateFormat(op1sentry);
          op2sentry = dateFormat(op2sentry);
        }

        temp.push({
          id: i,
          key: i,
          info: title,
          op1s: op1sentry,
          op2s: op2sentry
        })
      }
      return op1s;
    })
    return temp;
  },
}

function* fetchOperationActionSaga({ hash, actionType = DEFAULT_OPERATION_TAB, currentPage = DEFAULT_PAGE, pageSize = DEFAULT_PAGE_SIZE }) {
  const url = connectionConfig.operationActions[actionType];

  if (!url) {
    return yield put(fetchOperationActionFailure({ type: actionType }))
  }

  const apiOptions = {
    url: url + hash,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const [{ data, error }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchActionNumberSaga, {
      hash,
      actionType,
    })
  ])

  const parser = parsers[actionType]

  if (data) {
    yield put(fetchOperationActionSuccess({ data: parser ? parser(data, hash) : data, type: actionType }))
  } else {
    yield put(fetchOperationActionFailure({ error, type: actionType }))
  }
}

function* fetchOperationSaga({ tab, hash, op, currentPage, pageSize }) {
  switch (tab) {
    case 'operations':
      yield put(fetchOperationAction({ hash, tab: op, currentPage, pageSize }))
      break
    default:
      yield put(fetchOperationOverview({ hash }))
  }
}

const getOperations = (state) => state.Operation.operations
function* initOperationSaga(props) {
  const { hash } = props

  yield put(updateMainTab(props))
  yield all(vaildOperationTabs.map(actionType => call(fetchActionNumberSaga, {
    hash,
    actionType,
  })))

  const operations = yield select(getOperations)
  yield put(updateOperationsTotalNumber({ operations }))
}

export default function* operationRootSaga() {
  yield all([
    takeEvery(types.OPERATION_OVERVIEW_REQUEST, fetchOperationOverviewSaga),
    takeEvery(types.OPERATION_ACTION_NUMBER_REQUEST, fetchActionNumberSaga),
    takeEvery(types.OPERATION_ACTION_REQUEST, fetchOperationActionSaga),
    takeEvery(types.OPERATION_INIT, initOperationSaga),
    takeEvery(types.OPERATION_MAIN_TAB_UPDATE, fetchOperationSaga),
  ])
}
