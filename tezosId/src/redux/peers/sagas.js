import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import * as utils from "../../helpers/format";

import {
  PER_PAGE,
  DEFAULT_PAGE,
  types,
  fetchPeersSuccess,
  fetchPeersFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parsePeers = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => {
  const peers = data.map(({ peerid, countrycode, established_ip, state, total_sent, total_recv, seen_time }) => {
    return {
      id: peerid,
      key: peerid,
      peerId: peerid,
      country: countrycode,
      ip: established_ip,
      state: state,
      lastSeen: utils.dateFormat(seen_time),
      totalSent: total_sent,
      totalReceived: total_recv
    }
  })

  return {
    currentPage,
    pageSize,
    data: peers
  }
}

function* fetchPeersSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.networkpeers,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const { data, error } = yield call(fetchApi, apiOptions)
  let formattedData

  if (data) {
    formattedData = parsePeers({ data, currentPage, pageSize })
    yield put(fetchPeersSuccess(formattedData))
  } else {
    yield put(fetchPeersFailure(error))
  }
}

export default function* peersRootSaga() {
  yield all([
    takeEvery(types.PEERS_REQUEST, fetchPeersSaga),
  ])
}
