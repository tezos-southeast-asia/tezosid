export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const TOTAL_PEERS = 200
export const types = {
  PEERS_REQUEST: 'PEERS_REQUEST',
  PEERS_SUCCESS: 'PEERS_SUCCESS',
  PEERS_FAILURE: 'PEERS_FAILURE',
}

export const fetchPeers = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.PEERS_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchPeersSuccess = (data) => ({
  type: types.PEERS_SUCCESS,
  data
})

export const fetchPeersFailure = (error) => ({
  type: types.PEERS_FAILURE,
  error
})
