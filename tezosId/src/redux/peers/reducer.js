import { types, PER_PAGE, DEFAULT_PAGE, TOTAL_PEERS } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  peers: {
    data: null,
    total: TOTAL_PEERS,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function peersReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.PEERS_REQUEST:
      return {
        ...state,
        isLoading: true,
        peers: {
          data: null,
          total: TOTAL_PEERS,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.PEERS_SUCCESS: {
      const peers = action.data
      return {
        ...state,
        isLoading: false,
        isError: false,
        peers: {
          ...peers,
          total: TOTAL_PEERS,
        }
      }
    } 
    case types.PEERS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        peers: initialState.peers
      }

    default:
      return state
  }
}