export const types = {
    ADD_EMAIL_NOTIFICATION: 'ADD_EMAILNOTIFICATION',
    ADD_MOBILE_NOTIFICATION: 'ADD_MOBILENOTIFICATION',
    REMOVE_EMAIL_NOTIFICATION: 'REMOVE_EMAILNOTIFICATION',
    REMOVE_MOBILE_NOTIFICATION: 'REMOVE_MOBILENOTIFICATION',
    UPDATE_ALIAS_NOTIFICATION: 'UPDATE_ALIAS_NOTIFICATION',
    HAS_ERROR: "HAS_ERROR",
    IS_LOADING: "IS_LOADING",
    CHECK_MOBILE_NOTIFICATION_STATUS: 'CHECK_MOBILENOTIFICATIONSTATUS',
    UPDATE_EMAIL_SUB_HASH_LIST: 'UPDATE_EMAIL_SUB_HASH_LIST',
    UPDATE_EMAIL_SUB_FROM_LIST: 'UPDATE_EMAIL_SUB_FROM_LIST',
    ADD_EMAIL_SUBS_TO_LIST: 'ADD_EMAIL_SUBS_TO_LIST',
    IS_LOADING_BUTTON: 'IS_LOADING_BUTTON'
}

/* Redux saga actions */
// export function checkForMobileNotiStatus(userUid, accountHash) {
//     return { type: types.CHECK_MOBILE_NOTIFICATION_STATUS, userUid, accountHash }
// }

export function updateNotificationAlias(userEmail, accountHash, alias) {
    return { type: types.UPDATE_ALIAS_NOTIFICATION, userEmail, accountHash, alias }
}

export function addEmailNotification(userEmail, accountHash, userUid, alias) {
    return { type: types.ADD_EMAIL_NOTIFICATION, userEmail, accountHash, userUid, alias }
}

export function removeEmailNotification(userEmail, accountHash, userUid) {
    return { type: types.REMOVE_EMAIL_NOTIFICATION, userEmail, accountHash, userUid }
}

// export function checkForMobileNotiStatus(userUid, accountHash) {
//     return { type: types.CHECK_MOBILE_NOTIFICATION_STATUS, userUid, accountHash }
// }

// export function addMobileNotification(userUid, accountHash) {
//     return { type: types.ADD_EMAIL_NOTIFICATION, userUid, accountHash }
// }

// export function removeMobileNotification(userUid, accountHash) {
//     return { type: types.REMOVE_EMAIL_NOTIFICATION, userUid, accountHash }
// }



/* Actions for reducer */
export function loadingHandle(bool) {
    return { type: types.IS_LOADING, isLoading: bool };
}

export function errorHandle(e) {
    return { type: types.HAS_ERROR, errorMsg: e };
}

export function loadingButtonHandle(isLoading, accountHash) {
    return { type: types.IS_LOADING_BUTTON, isLoading, accountHash };
}

export function addEmailSubsToList(emailSubs) {
    return { type: types.ADD_EMAIL_SUBS_TO_LIST, emailSubs }
}

export function updateEmailSubList(emailSub) {
    return { type: types.UPDATE_EMAIL_SUB_FROM_LIST, emailSub }
}

export function mapEmailListSnapshot(emailList) {
    if (emailList.length !== 0) {
        const mapEmailList = emailList.map(doc => {
            return doc.id;
        });
        return mapEmailList;
    } else {
        return [];
    }
}
