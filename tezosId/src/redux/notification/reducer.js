import { types } from './actions'

const initState = {
    emailSubscription: []
}

export default function notificationListReducer(state = initState, action = {}) {
    let emailSubArr = [];
    switch (action.type) {
        case types.IS_LOADING_BUTTON:
            emailSubArr = state.emailSubscription.map(obj => {
                if (obj.accountHash === action.accountHash) {
                    obj.isLoading = action.isLoading;
                }
                return obj;
            });
            return {
                ...state,
                emailSubscription: emailSubArr
            }
        case types.HAS_ERROR:
            return {
                ...state,
            }

        case types.ADD_EMAIL_SUBS_TO_LIST:
            return {
                ...state,
                //emailSubscription: [...state.emailSubscription, action.accountHashObj]
                emailSubscription: action.emailSubs
            }

        case types.UPDATE_EMAIL_SUB_FROM_LIST:
            emailSubArr = state.emailSubscription.map(obj => {
                if (obj.accountHash === action.emailSub.accountHash) {
                    obj.isLoading = action.emailSub.isLoading;
                    obj.subscriptionTxt = action.emailSub.subscriptionTxt;
                }
                return obj;
            });
            return {
                ...state,
                emailSubscription: emailSubArr,
            }
        default:
            return state
    }
}