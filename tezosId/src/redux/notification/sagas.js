    import rsf from '../auth/rsf'
    import { all, call, put, takeEvery } from 'redux-saga/effects';
    import { firebaseApp } from '../auth/rsf';
    import {
        types,
        errorHandle,
        updateEmailSubList,
        loadingButtonHandle,
        loadingHandle,
    } from './actions';

    const db = firebaseApp && firebaseApp.firestore();


    function* addEmailNotificationSaga(provider) {
        try {
            yield put(loadingButtonHandle(true, provider.accountHash));
            const query = db.collection("notification").doc(provider.accountHash).collection("emails").doc(provider.userEmail);
            yield call(rsf.firestore.setDocument, query, {
                alias: provider.alias,
            });

            const userQuery = db.collection("users").doc(provider.userUid).collection("bookmarks").doc(provider.accountHash);
            yield call(rsf.firestore.updateDocument, userQuery, {
                emailSub: true,
            });

            yield put(updateEmailSubList({ accountHash: provider.accountHash, isLoading: false, subscriptionTxt: "Unsubscribe" }));
        } catch (e) {
            yield put(errorHandle(e));
        }
    }

    function* removeEmailNotificationSaga(provider) {
        try {
            yield put(loadingButtonHandle(true, provider.accountHash));
            const query = db.collection("notification").doc(provider.accountHash).collection("emails").doc(provider.userEmail);
            yield call(rsf.firestore.deleteDocument, query);

            if(provider.userUid!==undefined){
                const userQuery = db.collection("users").doc(provider.userUid).collection("bookmarks").doc(provider.accountHash);
                yield call(rsf.firestore.updateDocument, userQuery, {
                    emailSub: false,
                });
            }  
            yield put(updateEmailSubList({ accountHash: provider.accountHash, isLoading: false, subscriptionTxt: "Subscribe" }));
        } catch (e) {
            yield put(errorHandle(e));
        }
    }

    function* updateNotificationAliasSaga(provider) {
        try {
            yield put(loadingHandle(true));
            const query = db.collection("notification").doc(provider.accountHash).collection("emails").doc(provider.userEmail);
            yield call(rsf.firestore.updateDocument, query, {
                alias: provider.alias,
            });
        } catch (e) {
            yield put(errorHandle(e));
        }
    }


    export default function* notificationListRootSaga() {
        yield all([
            takeEvery(types.ADD_EMAIL_NOTIFICATION, addEmailNotificationSaga),
            takeEvery(types.REMOVE_EMAIL_NOTIFICATION, removeEmailNotificationSaga),
            takeEvery(types.UPDATE_ALIAS_NOTIFICATION,updateNotificationAliasSaga)
        ])
    }