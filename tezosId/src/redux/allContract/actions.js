export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  ALLCONTRACT_REQUEST: 'ALLCONTRACT_REQUEST',
  ALLCONTRACT_SUCCESS: 'ALLCONTRACT_SUCCESS',
  ALLCONTRACT_LIST_SUCCESS: 'ALLCONTRACT_LIST_SUCCESS',
  ALLCONTRACT_FAILURE: 'ALLCONTRACT_FAILURE',
}

export const fetchAllContract = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.ALLCONTRACT_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchAllContractSuccess = (data) => ({
  type: types.ALLCONTRACT_SUCCESS,
  data
})

export const fetchAllContractListSuccess = (data) => ({
  type: types.ALLCONTRACT_LIST_SUCCESS,
  data
})

export const fetchAllContractFailure = (error) => ({
  type: types.ALLCONTRACT_FAILURE,
  error
})
