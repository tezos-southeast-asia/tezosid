import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  allContract: {
    data: null,
    total: DEFAULT_TOTAL,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function allContractReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ALLCONTRACT_REQUEST:
      return {
        ...state,
        isLoading: true,
        allContract: {
          data: null,
          total: state.allContract.total,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.ALLCONTRACT_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        allContract: action.data
      }
    }
    case types.ALLCONTRACT_LIST_SUCCESS:
      return {
        ...state,
        isLoading: true,
        isError: false,
        allContract: action.data
      }
    case types.ALLCONTRACT_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        allContract: initialState.allContract
      }

    default:
      return state
  }
}
