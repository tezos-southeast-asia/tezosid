import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'

import {
  PER_PAGE,
  DEFAULT_PAGE,
  DEFAULT_TOTAL,
  types,
  fetchAllContractSuccess,
  fetchAllContractListSuccess,
  fetchAllContractFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseAllContract = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = DEFAULT_TOTAL, addData }) => {
  const allContract = data.map(({ account, id }, index) => {
    const extraData = (addData && addData[index] && addData[index].data) || {}
    const { balance, delegate } = extraData

    return {
      id: id,
      key: id,
      hash: account,
      balance: addData ? balance : '',
      delegate: addData ? (delegate ? delegate : 'table.forbidden') : '',
    }
  })

  return {
    currentPage,
    pageSize,
    total,
    data: allContract
  }
}

function* fetchAllContractSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.allContract,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const totalApiOptions = {
    url: connectionConfig.allContractNum,
  }
  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])
  let formattedData

  if (data) {
    formattedData = parseAllContract({ data, currentPage, pageSize, total })
    yield put(fetchAllContractListSuccess(formattedData))

    const addData = yield all(data.map(({ account }) => {
      const apiOptions = {
        url: connectionConfig.additionalAccInfo + account
      }

      return call(fetchApi, apiOptions);
    }))

    formattedData = parseAllContract({ data, currentPage, pageSize, total, addData })
    yield put(fetchAllContractSuccess(formattedData))
  } else {
    yield put(fetchAllContractFailure(error))
  }
}

export default function* allContractRootSaga() {
  yield all([
    takeEvery(types.ALLCONTRACT_REQUEST, fetchAllContractSaga),
  ])
}
