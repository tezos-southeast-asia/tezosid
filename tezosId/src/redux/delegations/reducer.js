import { types, PER_PAGE, DEFAULT_PAGE, TOTAL_DELEGATIONS } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  delegations: {
    data: null,
    total: TOTAL_DELEGATIONS,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function delegationsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.DELEGATIONS_REQUEST:
      return {
        ...state,
        isLoading: true,
        delegations: {
          data: null,
          total: state.delegations.total,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.DELEGATIONS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        delegations: action.data
      }
    }
    case types.DELEGATIONS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        delegations: initialState.delegations
      }

    default:
      return state
  }
}