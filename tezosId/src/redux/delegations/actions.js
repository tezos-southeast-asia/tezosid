export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const TOTAL_DELEGATIONS = 0
export const types = {
  DELEGATIONS_REQUEST: 'DELEGATIONS_REQUEST',
  DELEGATIONS_SUCCESS: 'DELEGATIONS_SUCCESS',
  DELEGATIONS_FAILURE: 'DELEGATIONS_FAILURE',
}

export const fetchDelegations = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.DELEGATIONS_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchDelegationsSuccess = (data) => ({
  type: types.DELEGATIONS_SUCCESS,
  data
})

export const fetchDelegationsFailure = (error) => ({
  type: types.DELEGATIONS_FAILURE,
  error
})
