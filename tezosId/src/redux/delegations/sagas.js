import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import { dateFormat } from "../../helpers/format";

import {
  PER_PAGE,
  DEFAULT_PAGE,
  TOTAL_DELEGATIONS,
  types,
  fetchDelegationsSuccess,
  fetchDelegationsFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseDelegations = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = TOTAL_DELEGATIONS }) => {
  const delegations = data.map((item) => {
    const { op, delegation } = item
    const { opHash, blockLevel, blockHash, blockTimestamp } = op || {}
    const { delegate, fee, uuid, source, operationResultStatus } = delegation || {}

    return {
      id: opHash,
      key: uuid,
      opHash: opHash,
      delegateSource: source || '-',
      delegateDestination: delegate || '-',
      fee: fee,
      blockId: [blockLevel, blockHash],
      timestamp: dateFormat(blockTimestamp),
      status: operationResultStatus !== 'applied' ? 'status-color' : ''
    }
  })

  return {
    currentPage,
    pageSize,
    total,
    data: delegations
  }
}

function* fetchDelegationsSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.delegations,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const totalApiOptions = {
    url: connectionConfig.delegationsNum,
  }
  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])
  let formattedData

  if (data) {
    formattedData = parseDelegations({ data, currentPage, pageSize, total })
    yield put(fetchDelegationsSuccess(formattedData))
  } else {
    yield put(fetchDelegationsFailure(error))
  }
}

export default function* delegationsRootSaga() {
  yield all([
    takeEvery(types.DELEGATIONS_REQUEST, fetchDelegationsSaga),
  ])
}
