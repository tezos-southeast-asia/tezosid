export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  SNAPSHOTS_REQUEST: 'SNAPSHOTS_REQUEST',
  SNAPSHOTS_SUCCESS: 'SNAPSHOTS_SUCCESS',
  SNAPSHOTS_FAILURE: 'SNAPSHOTS_FAILURE',
}

export const fetchSnapshots = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.SNAPSHOTS_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchSnapshotsSuccess = (data) => ({
  type: types.SNAPSHOTS_SUCCESS,
  data
})

export const fetchSnapshotsFailure = (error) => ({
  type: types.SNAPSHOTS_FAILURE,
  error
})
