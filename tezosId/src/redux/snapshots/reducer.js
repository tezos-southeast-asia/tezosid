import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
    isLoading: false,
    isError: false,
    snapshots: {
        data: null,
        total: DEFAULT_TOTAL,
        currentPage: DEFAULT_PAGE,
        pageSize: PER_PAGE,
    }
}

export default function snapshotsReducer(state = initialState, action = {}) {
    switch (action.type) {
        case types.SNAPSHOTS_REQUEST:
            return {
                ...state,
                isLoading: true,
                snapshots: {
                    data: null,
                    total: state.snapshots.total,
                    currentPage: action.currentPage,
                    pageSize: action.pageSize || PER_PAGE,
                }
            }
        case types.SNAPSHOTS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                snapshots: action.data,
            }
        case types.SNAPSHOTS_FAILURE:
            return {
                ...state,
                isLoading: false,
                isError: true,
                snapshots: initialState.snapshots
            }

        default:
            return state
    }
}