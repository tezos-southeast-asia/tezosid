import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import {
    PER_PAGE,
    DEFAULT_PAGE,
    DEFAULT_TOTAL,
    types,
    fetchSnapshotsSuccess,
    fetchSnapshotsFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

import { blockConfigs } from '../../customApp/configs/blockConfigs';
import { DEFAULT_PAGE_SIZE } from '../block/actions';

const {
  blocksPerCycle,
  blocksPerRollSnapshot,
  minCycle,
} = blockConfigs

export const parseSnapshots = ({ snapshots = [], cycles = [], } = {}) => snapshots && snapshots.map(({ data }, index) => {
  const cycle = Number.isInteger(cycles[index] && cycles[index].cycle) ? cycles[index].cycle : ''
  const snapshotIndex = Number.isInteger(data) ? data : ''
  let level = ''

  if (snapshotIndex !== '') {
    level = (cycle - minCycle) * blocksPerCycle + (data + 1) * blocksPerRollSnapshot;
  }

  return {
    key: cycle || `cycle-${index}`,
    level,
    cycle,
    index: snapshotIndex,
  }
})

export const getCycles = ({ currentCycle, currentBlockLevel, currentPage = DEFAULT_PAGE, pageSize = DEFAULT_PAGE_SIZE } = {}) => {
  if (!Number.isInteger(currentCycle) || !Number.isInteger(currentBlockLevel)) {
    return {
      total: DEFAULT_TOTAL,
      cycles: [],
      indexApis: [],
    }
  }

  const firstIndex = (currentPage - 1) * pageSize
  const maxCycle = currentCycle + minCycle
  const maxCycleCurrentPage = maxCycle - firstIndex
  const nextMaxCycleCurrentPage = Math.max(maxCycleCurrentPage - pageSize, minCycle - 1)
  const total = Math.max(maxCycle - minCycle + 1, DEFAULT_TOTAL)
  let cycleIndex
  let blockLevel
  let cycles = []

  for (cycleIndex = maxCycleCurrentPage; cycleIndex > nextMaxCycleCurrentPage; cycleIndex--) {
    if (cycleIndex >= currentCycle) {
      blockLevel = currentBlockLevel
    } else if (cycleIndex >= minCycle) {
      blockLevel = cycleIndex * blocksPerCycle + 1
    }

    cycles.push({
      cycle: cycleIndex,
      apiOptions: {
        url: connectionConfig.snapshot.replace("<level>", blockLevel).replace("<cycle>", cycleIndex),
      }
    })
  }

  return {
    total,
    cycles,
  }
}

function* fetchSnapshotsSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const blockApiOptions = {
    url: connectionConfig.blocks,
    params: {
      p: 0,
      n: 1,
    }
  }
  const { data, error: blockError } = yield call(fetchApi, blockApiOptions);
  const latestBlockData = data && data[0] && data[0].blocksAlpha

  if (blockError || !latestBlockData) {
    return yield put(fetchSnapshotsFailure(blockError))
  }

  const { level: currentBlockLevel,  cycle: currentCycle } = latestBlockData
  const { total, cycles = [] } = getCycles({ currentCycle, currentBlockLevel, currentPage, pageSize })

  const snapshots = yield all(cycles.map(({ apiOptions }) => call(fetchApi, apiOptions)));
  const formattedSnapshots = parseSnapshots({ snapshots, cycles })

  yield put(fetchSnapshotsSuccess({
    data: formattedSnapshots,
    total,
    currentPage,
    pageSize,
  }))
}

export default function* snapshotsRootSaga() {
  yield all([
    takeEvery(types.SNAPSHOTS_REQUEST, fetchSnapshotsSaga),
  ])
}
