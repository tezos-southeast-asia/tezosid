import Auth from './auth/reducer';
import App from './app/reducer';
import ThemeSwitcher from './themeSwitcher/reducer';
import LanguageSwitcher from './languageSwitcher/reducer';
import VotingPeriods from './votingPeriods/reducer';
import VoterList from './voterList/reducer';
import Bookmarklist from './bookmarklist/reducer';
import Peers from './peers/reducer'
import Blocks from './blocks/reducer';
import Transactions from './transactions/reducer';
import Endorsements from './endorsements/reducer';
import Delegations from './delegations/reducer';
import Originations from './originations/reducer';
import Account from './account/reducers';
import Notification from './notification/reducer';
import BakerRights from './bakerRights/reducers';
import Proposers from './proposers/reducer';
import ProposalVoters from './proposalVoters/reducer';
import DoubleEndorsement from './doubleEndorsement/reducer';
import DoubleBaking from './doubleBaking/reducer';
import Protocols from './protocol/reducer';
import Operation from './operation/reducers';
import Block from './block/reducers';
import AllAccount from './allAccount/reducer';
import AllContract from './allContract/reducer';
import Chart from './charts/reducers';
import Nonces from './nonces/reducer';
import Activations from './activations/reducer';
import Snapshots from './snapshots/reducer';

export default {
  Auth,
  App,
  ThemeSwitcher,
  LanguageSwitcher,
  VotingPeriods,
  VoterList,
  Bookmarklist,
  Peers,
  Blocks,
  Transactions,
  Endorsements,
  Delegations,
  Originations,
  Account,
  Notification,
  BakerRights,
  Proposers,
  ProposalVoters,
  DoubleEndorsement,
  DoubleBaking,
  Protocols,
  Operation,
  Block,
  AllAccount,
  AllContract,
  Chart,
  Nonces,
  Activations,
  Snapshots
};
