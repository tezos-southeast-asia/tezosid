import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'

import {
  PER_PAGE,
  DEFAULT_PAGE,
  DEFAULT_TOTAL,
  types,
  fetchAllAccountSuccess,
  fetchAllAccountListSuccess,
  fetchAllAccountFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseAllAccount = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = DEFAULT_TOTAL, addData }) => {
  const allAccount = data.map(({ account, id }, index) => {
    const extraData = (addData && addData[index] && addData[index].data) || {}
    const { balance, counter } = extraData

    return {
      id: id,
      key: id,
      hash: account,
      balance: addData ? balance : "",
      counter: addData ? counter : "",
    }
  })

  return {
    currentPage,
    pageSize,
    total,
    data: allAccount
  }
}

function* fetchAllAccountSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.allAccount,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const totalApiOptions = {
    url: connectionConfig.allAccountNum,
  }
  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])
  let formattedData

  if (data) {
    formattedData = parseAllAccount({ data, currentPage, pageSize, total })
    yield put(fetchAllAccountListSuccess(formattedData))

    const addData = yield all(data.map(({ account }) => {
      const apiOptions = {
        url: connectionConfig.additionalAccInfo + account
      }

      return call(fetchApi, apiOptions);
    }))

    formattedData = parseAllAccount({ data, currentPage, pageSize, total, addData })
    yield put(fetchAllAccountSuccess(formattedData))
  } else {
    yield put(fetchAllAccountFailure(error))
  }
}

export default function* allAccountRootSaga() {
  yield all([
    takeEvery(types.ALLACCOUNT_REQUEST, fetchAllAccountSaga),
  ])
}
