export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  ALLACCOUNT_REQUEST: 'ALLACCOUNT_REQUEST',
  ALLACCOUNT_SUCCESS: 'ALLACCOUNT_SUCCESS',
  ALLACCOUNT_LIST_SUCCESS: 'ALLACCOUNT_LIST_SUCCESS',
  ALLACCOUNT_FAILURE: 'ALLACCOUNT_FAILURE',
}

export const fetchAllAccount = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.ALLACCOUNT_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchAllAccountSuccess = (data) => ({
  type: types.ALLACCOUNT_SUCCESS,
  data
})

export const fetchAllAccountListSuccess = (data) => ({
  type: types.ALLACCOUNT_LIST_SUCCESS,
  data
})

export const fetchAllAccountFailure = (error) => ({
  type: types.ALLACCOUNT_FAILURE,
  error
})
