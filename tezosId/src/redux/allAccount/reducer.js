import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  allAccount: {
    data: null,
    total: DEFAULT_TOTAL,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  }
}

export default function allAccountReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ALLACCOUNT_REQUEST:
      return {
        ...state,
        isLoading: true,
        allAccount: {
          data: null,
          total: state.allAccount.total,         //change to ...state.allAccount.total if there is pagination problem
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
        }
      }
    case types.ALLACCOUNT_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        allAccount: action.data
      }
    }
    case types.ALLACCOUNT_LIST_SUCCESS: {
      return {
        ...state,
        isLoading: true,
        isError: false,
        allAccount: action.data
      }
    }
    case types.ALLACCOUNT_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        allAccount: initialState.allAccount
      }

    default:
      return state
  }
}
