import { all, call, put, takeEvery } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import { dateFormat } from "../../helpers/format";

import {
  PER_PAGE,
  DEFAULT_PAGE,
  DEFAULT_TOTAL,
  types,
  fetchTransactionsSuccess,
  fetchTransactionsFailure,
} from './actions'
import { fetchApi } from '../../helpers/api'

const parseTransactions = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = DEFAULT_TOTAL }) => {
  const keyArr = [];
  const transactions = data.map(({ tx, op, internalTx }, index) => {
    const { opHash, } = op || {}
    const { amount, source, destination, fee, blockTimestamp, blockLevel, blockHash, uuid, operationResultStatus } = tx || {}
    if (internalTx.length) {
      const internalTransaction = internalTx.map(({ destination, amount, source, resultStatus }, index) => {
        return {
          id: opHash,
          key: index,
          opHash: opHash,
          amount,
          level: [blockLevel, blockHash],
          fromElement: [source],
          toElement: [destination],
          timestamp: dateFormat(blockTimestamp),
          fee,
          status: resultStatus !== 'applied' ? 'status-color' : ''
        }
      })
      const transaction = {
        id: opHash,
        key: uuid || index,
        opHash,
        amount,
        timestamp: dateFormat(blockTimestamp),
        fromElement: [source],
        fromId: source,
        toId: destination,
        toElement: [destination],
        fee,
        level: [blockLevel, blockHash],
        status: operationResultStatus !== 'applied' ? 'status-color' : '',
        internalTx: internalTransaction
      }
      keyArr.push(uuid);
      return transaction
    } else {
      return {
        id: opHash,
        key: uuid || index,
        opHash,
        amount,
        timestamp: dateFormat(blockTimestamp),
        fromElement: [source],
        fromId: source,
        toId: destination,
        toElement: [destination],
        fee,
        level: [blockLevel, blockHash],
        status: operationResultStatus !== 'applied' ? 'status-color icon-visible' : 'icon-visible',
        internalTx: []
      }
    }
  })

  return {
    currentPage,
    pageSize,
    total,
    data: transactions,
    keyArr
  }
}

function* fetchTransactionsSaga({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) {
  const apiOptions = {
    url: connectionConfig.transactions,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const totalApiOptions = {
    url: connectionConfig.transactionsNum,
  }
  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])
  let formattedData

  if (data) {
    formattedData = parseTransactions({ data, currentPage, pageSize, total })
    yield put(fetchTransactionsSuccess(formattedData))
  } else {
    yield put(fetchTransactionsFailure(error))
  }
}

export default function* transactionsRootSaga() {
  yield all([
    takeEvery(types.TRANSACTIONS_REQUEST, fetchTransactionsSaga),
  ])
}
