import { types, PER_PAGE, DEFAULT_PAGE, DEFAULT_TOTAL } from './actions'

const initialState = {
  isLoading: false,
  isError: false,
  transactions: {
    data: null,
    total: DEFAULT_TOTAL,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
    keyArr: [],
  }
}

export default function transactionsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.TRANSACTIONS_REQUEST:
      return {
        ...state,
        isLoading: true,
        transactions: {
          data: null,
          total: state.transactions.total,
          currentPage: action.currentPage,
          pageSize: action.pageSize || PER_PAGE,
          keyArr: [],
        }
      }
    case types.TRANSACTIONS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        transactions: action.data,
      }
    case types.TRANSACTIONS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        transactions: initialState.transactions
      }
    case types.UPDATE_KEY_ARRAY:
      return {
        ...state,
        transactions: {
          ...state.transactions,
          keyArr:action.key
        }
      }
    default:
      return state
  }
}