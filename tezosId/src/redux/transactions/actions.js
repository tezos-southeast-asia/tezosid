export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const DEFAULT_TOTAL = 0
export const types = {
  TRANSACTIONS_REQUEST: 'TRANSACTIONS_REQUEST',
  TRANSACTIONS_SUCCESS: 'TRANSACTIONS_SUCCESS',
  TRANSACTIONS_FAILURE: 'TRANSACTIONS_FAILURE',
  UPDATE_KEY_ARRAY: "UPDATE_KEY_ARRAY",
}

export const fetchTransactions = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE }) => ({
  type: types.TRANSACTIONS_REQUEST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize)
})

export const fetchTransactionsSuccess = (data) => ({
  type: types.TRANSACTIONS_SUCCESS,
  data
})

export const fetchTransactionsFailure = (error) => ({
  type: types.TRANSACTIONS_FAILURE,
  error
})

export const updateKeyArray = (key) => ({
  type: types.UPDATE_KEY_ARRAY,
  key
})