import { combineReducers } from 'redux'
import upcomingBaking from './upcomingBaking'
import upcomingEndorsement from './upcomingEndorsement'

export default combineReducers({
    upcomingBaking,
    upcomingEndorsement
})