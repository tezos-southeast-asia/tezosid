import { types } from '../../actions'

const initialState = {
    data: [],
    isLoading: false,
    isError: false,
    nextBaking: {},
    prevBaking: {}
}

export default function upcomingBakingReducer(state = initialState, action = {}) {
    switch (action.type) {
        case types.ACCOUNT_NEXT_BAKING_REQUEST:
            return {
                ...state,
                isLoading: true,
                isError: false,
            }
        case types.ACCOUNT_NEXT_BAKING_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                nextBaking:action.nextBaking,
                prevBaking:action.prevBaking
            }
        case types.ACCOUNT_NEXT_BAKING_FAILURE:
                return {
                    ...state,
                    isLoading: false,
                    isError: true
                }
        default:
            return state
    }
}