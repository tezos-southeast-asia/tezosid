import { types } from '../../actions'

const initialState = {
    data: [],
    isLoading: false,
    isError: false,
    nextEndorsement: {},
    prevEndorsement: {}
}

export default function upcomingEndorsementReducer(state = initialState, action = {}) {
    switch (action.type) {
        case types.ACCOUNT_NEXT_ENDORSEMENT_REQUEST:
            return {
                ...state,
                isLoading: true,
                isError: false,
            }
        case types.ACCOUNT_NEXT_ENDORSEMENT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                nextEndorsement: action.nextEndorsement,
                prevEndorsement: action.prevEndorsement
            }
        case types.ACCOUNT_NEXT_ENDORSEMENT_FAILURE:
            return {
                ...state,
                isLoading: false,
                isError: true
            }
        default:
            return state
    }
}