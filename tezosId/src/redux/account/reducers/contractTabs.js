import { types, DEFAULT_CONTRACT_TAB } from '../actions'

const initialState = {
  defaultActiveKey: DEFAULT_CONTRACT_TAB,
  activeKey: DEFAULT_CONTRACT_TAB,
}

export default function operationsTabsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ACCOUNT_CONTRACT_REQUEST:
      return {
        ...state,
        activeKey: action.actionType,
      }
    default:
      return state
  }
}