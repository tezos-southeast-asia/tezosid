import { types, DEFAULT_TAB } from '../actions'

const initialState = {
  defaultActiveKey: DEFAULT_TAB,
  activeKey: DEFAULT_TAB,
}

export default function operationsTabsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ACCOUNT_MAIN_TAB_UPDATE:
      return {
        ...state,
        activeKey: action.tab,
      }
    default:
      return state
  }
}