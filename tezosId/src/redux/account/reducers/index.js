import { combineReducers } from 'redux'
import accountDetails from './accountDetails'
import bakerDetails from './bakerDetails'
import operations from './operations'
import tabs from './tabs'
import contracts from './contracts'
import opTabs from './opTabs'
import contractTabs from './contractTabs'
import baking from './baking'
import bakingTabs from './bakingTabs'

export default combineReducers({
  bakerDetails,
  accountDetails,
  tabs,
  operations,
  contracts,
  baking,
  opTabs,
  contractTabs,
  bakingTabs
})