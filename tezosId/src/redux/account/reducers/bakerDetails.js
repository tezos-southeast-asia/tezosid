import { types } from '../actions'

const initialState = null

export default function bakerDetailsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ACCOUNT_BAKER_DETAIL_REQUEST:
      return null
    case types.ACCOUNT_DETAIL_REQUEST: 
      return null
    case types.ACCOUNT_BAKER_DETAIL_SUCCESS:
      return action.data
    case types.ACCOUNT_BAKER_DETAIL_FAILURE:
    default:
      return state
  }
}