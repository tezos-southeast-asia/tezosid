import { types } from '../actions'
import { getBakerName } from '../../../helpers/utility'

const initialState = {
  name: '',
  hash: '',
  tableData: [],
  details: null,
  isContract: false,
  alias: '',
  contract: {},
  total: 0,
  isBaker: false,
  modalState: false
}

export default function accountDetailsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ACCOUNT_DETAIL_REQUEST: {
      const { hash } = action
      const bakerInfo = getBakerName(hash)
      const { baker, replace } = bakerInfo
      return {
        name: replace ? baker : '',
        hash,
        tableData: [],
        details: null,
        isContract: false,
        isBaker: false,
        total: 0,
        modalState: false
      }
    }
    case types.ACCOUNT_DETAIL_SUCCESS:
      return {
        ...state,
        details: action.data.details,
        tableData: state.tableData.length > 0 ? action.data.tableData.concat(state.tableData) : state.tableData.concat(action.data.tableData),
        isContract: action.data.isContract
      }
    case types.ACCOUNT_DELEGATE_STATUS_SUCCESS:
      return {
        ...state,
        tableData: state.tableData.concat(action.data)
      }

    case types.ACCOUNT_ACTION_NUMBER_SUCCESS.originations:
      return {
        ...state,
        tableData: state.tableData.map((item) => {
          const { key } = item

          if (key === 'originated') {
            return {
              ...item,
              value: action.number > 0,
            }
          } else {
            return item
          }
        }),
      }
    case types.GET_ALIAS_STATE:
      return {
        ...state,
        alias: action.alias
      }
    case types.ACCOUNT_OPERATION_TOTAL_NUMBER_UPDATE:
      return {
        ...state,
        total: Object.keys(action.operations).reduce((accumulator, type) => accumulator + action.operations[type].total, 0)
      }
    case types.ACCOUNT_IS_BAKER_SUCCESS:
      return {
        ...state,
        isBaker: action.isBaker
      }
    case types.ACCOUNT_UPDATE_NOTIFICATION_MODAL_STATE:
      return {
        ...state,
        modalState: action.state
      }
    case types.ACCOUNT_DETAIL_FAILURE:
    default:
      return state
  }
}