import { types } from '../../actions'

const TYPE = 'contractCode'
const initialState =  {
  isLoading: false,
  isError: false,
}

export default function contractsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ACCOUNT_CONTRACT_REQUEST: {
      if (action.actionType === TYPE) {
        return {
          ...state,
          isLoading: true,
          isError: false,
        }
      } else {
        return state
      }
    }
    case types.ACCOUNT_CONTRACT_SUCCESS[TYPE]: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        ...action.data,
      }
    }
    case types.ACCOUNT_CONTRACT_FAILURE[TYPE]: {
      return {
        ...state,
        isLoading: false,
        isError: true
      }
    }
    default:
      return state
  }
}