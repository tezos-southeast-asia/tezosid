import { combineReducers } from 'redux'
import contractCode from './contracts'
import contractStorage from './contractStorage'

export default combineReducers({
    contractCode,
    contractStorage
})