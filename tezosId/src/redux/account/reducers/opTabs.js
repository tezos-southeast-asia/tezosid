import { types, DEFAULT_OPERATION_TAB } from '../actions'

const initialState = {
  defaultActiveKey: DEFAULT_OPERATION_TAB,
  activeKey: DEFAULT_OPERATION_TAB,
}

export default function operationsTabsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ACCOUNT_ACTION_REQUEST:
      return {
        ...state,
        activeKey: action.actionType,
      }
    default:
      return state
  }
}