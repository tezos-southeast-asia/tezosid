import { types, DEFAULT_PAGE, DEFAULT_PAGE_SIZE } from '../../actions'

const TYPE = 'proposals'
const initialState =  {
  isLoading: false,
  isError: false,
  data: [],
  total: 0,
  currentPage: DEFAULT_PAGE,
  pageSize: DEFAULT_PAGE_SIZE,
}

export default function proposalsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ACCOUNT_ACTION_NUMBER_SUCCESS[TYPE]:
      return {
        ...state,
        total: action.number,
      }
    case types.ACCOUNT_ACTION_NUMBER_FAILURE[TYPE]:
      return {
        ...state,
        total: 0,
      }
    case types.ACCOUNT_ACTION_REQUEST: {
      if (action.actionType === TYPE) {
        return {
          ...state,
          currentPage: action.currentPage,
          pageSize: action.pageSize,
          isLoading: true,
          isError: false,
          data: [],
        }
      } else {
        return state
      }
    }
    case types.ACCOUNT_ACTION_SUCCESS[TYPE]: {
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      }
    }
    case types.ACCOUNT_ACTION_FAILURE[TYPE]: {
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: [],
      }
    }
    default:
      return state
  }
}