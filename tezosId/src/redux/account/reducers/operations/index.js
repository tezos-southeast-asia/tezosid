import { combineReducers } from 'redux'
import transactions from './transactions'
import endorsements from './endorsements'
import originations from './originations'
import delegations from './delegations'
import activations from './activations'
import reveals from './reveals'
import proposals from './proposals'
import ballots from './ballots'


export default combineReducers({
    transactions,
    endorsements,
    originations,
    delegations,
    activations,
    proposals,
    ballots,
    reveals
})