import { types, DEFAULT_BAKING_TAB } from '../actions'

const initialState = {
  defaultActiveKey: DEFAULT_BAKING_TAB,
  activeKey: DEFAULT_BAKING_TAB,
}

export default function bakingsTabsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.ACCOUNT_BAKING_REQUEST:
      return {
        ...state,
        activeKey: action.actionType,
      }
    default:
      return state
  }
}