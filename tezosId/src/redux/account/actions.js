export const types = {
  ACCOUNT_OPERATIONS_INIT: 'ACCOUNT_OPERATIONS_INIT',
  ACCOUNT_CONTRACT_INIT: 'ACCOUNT_CONTRACT_INIT',
  ACCOUNT_BAKING_INIT: 'ACCOUNT_BAKING_INIT',
  ACCOUNT_MAIN_TAB_UPDATE: 'ACCOUNT_MAIN_TAB_UPDATE',
  ACCOUNT_DETAIL_REQUEST: 'ACCOUNT_DETAIL_REQUEST',
  ACCOUNT_DETAIL_SUCCESS: 'ACCOUNT_DETAIL_SUCCESS',
  ACCOUNT_DETAIL_FAILURE: 'ACCOUNT_DETAIL_FAILURE',
  ACCOUNT_BAKER_DETAIL_REQUEST: 'ACCOUNT_BAKER_DETAIL_REQUEST',
  ACCOUNT_BAKER_DETAIL_SUCCESS: 'ACCOUNT_BAKER_DETAIL_SUCCESS',
  ACCOUNT_BAKER_DETAIL_FAILURE: 'ACCOUNT_BAKER_DETAIL_FAILURE',
  ACCOUNT_ACTION_NUMBER_REQUEST: 'ACCOUNT_ACTION_NUMBER_REQUEST',
  ACCOUNT_ACTION_NUMBER_SUCCESS: {
    transactions: 'ACCOUNT_ACTION_NUMBER_SUCCESS_TRANSACTIONS',
    delegations: 'ACCOUNT_ACTION_NUMBER_SUCCESS_DELEGATIONS',
    originations: 'ACCOUNT_ACTION_NUMBER_SUCCESS_ORIGINATIONS',
    endorsements: 'ACCOUNT_ACTION_NUMBER_SUCCESS_ENDORSEMENTS',
    activations: 'ACCOUNT_ACTION_NUMBER_SUCCESS_ACTIVATIONS',
    reveals: 'ACCOUNT_ACTION_NUMBER_SUCCESS_REVEALS',
    proposals: 'ACCOUNT_ACTION_NUMBER_SUCCESS_PROPOSALS',
    ballots: 'ACCOUNT_ACTION_NUMBER_SUCCESS_BALLOTS',
  },
  ACCOUNT_ACTION_NUMBER_FAILURE: {
    transactions: 'ACCOUNT_ACTION_NUMBER_FAILURE_TRANSACTIONS',
    delegations: 'ACCOUNT_ACTION_NUMBER_FAILURE_DELEGATIONS',
    originations: 'ACCOUNT_ACTION_NUMBER_FAILURE_ORIGINATIONS',
    endorsements: 'ACCOUNT_ACTION_NUMBER_FAILURE_ENDORSEMENTS',
    activations: 'ACCOUNT_ACTION_NUMBER_FAILURE_ACTIVATIONS',
    reveals: 'ACCOUNT_ACTION_NUMBER_FAILURE_REVEALS',
    proposals: 'ACCOUNT_ACTION_NUMBER_FAILURE_PROPOSALS',
    ballots: 'ACCOUNT_ACTION_NUMBER_FAILURE_BALLOTS',
  },
  ACCOUNT_ACTION_REQUEST: 'ACCOUNT_ACTION_REQUEST',
  ACCOUNT_ACTION_SUCCESS: {
    transactions: 'ACCOUNT_ACTION_SUCCESS_TRANSACTIONS',
    delegations: 'ACCOUNT_ACTION_SUCCESS_DELEGATIONS',
    originations: 'ACCOUNT_ACTION_SUCCESS_ORIGINATIONS',
    endorsements: 'ACCOUNT_ACTION_SUCCESS_ENDORSEMENTS',
    activations: 'ACCOUNT_ACTION_SUCCESS_ACTIVATIONS',
    reveals: 'ACCOUNT_ACTION_SUCCESS_REVEALS',
    proposals: 'ACCOUNT_ACTION_SUCCESS_PROPOSALS',
    ballots: 'ACCOUNT_ACTION_SUCCESS_BALLOTS'
  },
  ACCOUNT_ACTION_FAILURE: {
    transactions: 'ACCOUNT_ACTION_FAILURE_TRANSACTIONS',
    delegations: 'ACCOUNT_ACTION_FAILURE_DELEGATIONS',
    originations: 'ACCOUNT_ACTION_FAILURE_ORIGINATIONS',
    endorsements: 'ACCOUNT_ACTION_FAILURE_ENDORSEMENTS',
    activations: 'ACCOUNT_ACTION_FAILURE_ACTIVATIONS',
    reveals: 'ACCOUNT_ACTION_FAILURE_REVEALS',
    proposals: 'ACCOUNT_ACTION_FAILURE_PROPOSALS',
    ballots: 'ACCOUNT_ACTION_FAILURE_BALLOTS'
  },
  ACCOUNT_CONTRACT_REQUEST: 'ACCOUNT_CONTRACT_REQUEST',
  ACCOUNT_CONTRACT_SUCCESS: {
    contractCode: 'ACCOUNT_CONTRACT_SUCCESS_CONTRACTCODE',
    contractStorage: 'ACCOUNT_CONTRACT_SUCCESS_CONTRACTSTORAGE'
  },
  ACCOUNT_CONTRACT_FAILURE: {
    contractCode: 'ACCOUNT_CONTRACT_FAILURE_CONTRACTCODE',
    contractStorage: 'ACCOUNT_CONTRACT_FAILURE_CONTRACTSTORAGE'
  },
  ACCOUNT_BAKING_REQUEST: 'ACCOUNT_BAKING_REQUEST',
  ACCOUNT_BAKING_SUCCESS: {
    upcomingBaking: 'ACCOUNT_BAKING_SUCCESS_UPCOMING_BAKING',
    upcomingEndorsement: 'ACCOUNT_BAKING_SUCCESS_UPCOMING_ENDORSEMENT'
  },
  ACCOUNT_BAKING_FAILURE: {
    upcomingBaking: 'ACCOUNT_BAKING_FAILURE_UPCOMING_BAKING',
    upcomingEndorsement: 'ACCOUNT_BAKING_FAILURE_UPCOMING_ENDORSEMENT'
  },
  ACCOUNT_NEXT_BAKING_REQUEST: 'ACCOUNT_NEXT_BAKING_REQUEST',
  ACCOUNT_NEXT_BAKING_SUCCESS: 'ACCOUNT_NEXT_BAKING_SUCCESS',
  ACCOUNT_NEXT_BAKING_FAILURE: 'ACCOUNT_NEXT_BAKING_FAILURE',
  ACCOUNT_NEXT_ENDORSEMENT_REQUEST: 'ACCOUNT_NEXT_ENDORSEMENT_REQUEST',
  ACCOUNT_NEXT_ENDORSEMENT_SUCCESS: 'ACCOUNT_NEXT_ENDORSEMENT_SUCCESS',
  ACCOUNT_NEXT_ENDORSEMENT_FAILURE: 'ACCOUNT_NEXT_ENDORSEMENT_FAILURE',
  ACCOUNT_IS_BAKER_SUCCESS: 'ACCOUNT_IS_BAKER_SUCCESS',
  GET_ALIAS: 'GET_ALIAS',
  GET_ALIAS_STATE: 'GET_ALIAS_STATE',
  FORMAT_TRANSACTION_DETAIL: 'FORMAT_TRANSACTION_DETAIL',
  FORMAT_TRANSACTION_DETAIL_STATE: 'FORMAT_TRANSACTION_DETAIL_STATE',
  ACCOUNT_DELEGATE_STATUS_SUCCESS: 'ACCOUNT_DELEGATE_STATUS_SUCCESS',
  ACCOUNT_OPERATION_TOTAL_NUMBER_UPDATE: 'ACCOUNT_OPERATION_TOTAL_NUMBER_UPDATE',
  ACCOUNT_UPDATE_KEY_ARRAY: 'ACCOUNT_UPDATE_KEY_ARRAY',
  ACCOUNT_UPDATE_NOTIFICATION_MODAL_STATE: 'ACCOUNT_UPDATE_NOTIFICATION_MODAL_STATE'
}
export const DEFAULT_PAGE_SIZE = 10
export const DEFAULT_PAGE = 1
export const vaildOperationTabs = Object.keys(types.ACCOUNT_ACTION_SUCCESS)
export const validContractTabs = Object.keys(types.ACCOUNT_CONTRACT_SUCCESS)
export const validBakingTabs = Object.keys(types.ACCOUNT_BAKING_SUCCESS)
export const vaildMainTabs = ['operations', 'contract', 'baking']
// export const DEFAULT_TAB = 'transactions'
export const DEFAULT_TAB = vaildMainTabs[0]
export const DEFAULT_OPERATION_TAB = vaildOperationTabs[0]
export const DEFAULT_CONTRACT_TAB = validContractTabs[0]
export const DEFAULT_BAKING_TAB = validBakingTabs[0]
// export const vaildTabs = Object.keys(types.ACCOUNT_ACTION_SUCCESS)

export const initOperation = (props) => ({
  ...props,
  type: types.ACCOUNT_OPERATIONS_INIT,
})

export const initContract = (props) => ({
  ...props,
  type: types.ACCOUNT_CONTRACT_INIT,
})

export const initBaking = (props) => ({
  ...props,
  type: types.ACCOUNT_BAKING_INIT,
})

export const updateMainTab = ({ tab = DEFAULT_TAB, hash, op = DEFAULT_OPERATION_TAB, p = DEFAULT_PAGE, n = DEFAULT_PAGE_SIZE, contractOp = DEFAULT_CONTRACT_TAB, bakingOp = DEFAULT_BAKING_TAB }) => ({
  type: types.ACCOUNT_MAIN_TAB_UPDATE,
  tab,
  hash,
  op,
  currentPage: p,
  pageSize: n,
  contractOp,
  bakingOp
})

export const fetchAccountDetails = ({ hash }) => ({
  type: types.ACCOUNT_DETAIL_REQUEST,
  hash
})

export const fetchAccountDetailsSuccess = (data) => ({
  type: types.ACCOUNT_DETAIL_SUCCESS,
  data
})

export const fetchAccountDetailsFailure = (error) => ({
  type: types.ACCOUNT_DETAIL_FAILURE,
  error
})

export const fetchBakerDetails = ({ hash }) => ({
  type: types.ACCOUNT_BAKER_DETAIL_REQUEST,
  hash
})

export const fetchBakerDetailsSuccess = (data) => ({
  type: types.ACCOUNT_BAKER_DETAIL_SUCCESS,
  data
})

export const fetchBakerDetailsFailure = (error) => ({
  type: types.ACCOUNT_BAKER_DETAIL_FAILURE,
  error
})

export const fetchActionNumber = ({ hash, type = DEFAULT_TAB }) => ({
  type: types.ACCOUNT_ACTION_NUMBER_REQUEST,
  hash,
  actionType: type,
})

export const fetchActionNumberSuccess = ({ type, number }) => ({
  type: types.ACCOUNT_ACTION_NUMBER_SUCCESS[type],
  number
})

export const fetchActionNumberFailure = ({ type, error }) => ({
  type: types.ACCOUNT_ACTION_NUMBER_FAILURE[type],
  error
})

export const fetchAccountAction = ({ hash, tab = DEFAULT_OPERATION_TAB, currentPage = DEFAULT_PAGE, pageSize = DEFAULT_PAGE_SIZE }) => ({
  type: types.ACCOUNT_ACTION_REQUEST,
  hash,
  actionType: tab,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize),
})

export const fetchActionSuccess = ({ type, data }) => ({
  type: types.ACCOUNT_ACTION_SUCCESS[type],
  data
})

export const fetchActionFailure = ({ type, error }) => ({
  type: types.ACCOUNT_ACTION_FAILURE[type],
  error
})

export const fetchBakingAction = ({ hash, tab = DEFAULT_BAKING_TAB, currentPage = DEFAULT_PAGE, pageSize = DEFAULT_PAGE_SIZE }) => ({
  type: types.ACCOUNT_BAKING_REQUEST,
  hash,
  actionType: tab,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize),
})

export const fetchBakingActionSuccess = ({ type, data }) => ({
  type: types.ACCOUNT_BAKING_SUCCESS[type],
  data
})

export const fetchBakingActionFailure = ({ type, error }) => ({
  type: types.ACCOUNT_BAKING_FAILURE[type],
  error
})

export const fetchNextBakingAction = ({ hash }) => ({
  type: types.ACCOUNT_NEXT_BAKING_REQUEST,
  hash,
})

export const fetchNextBakingActionSuccess = ({ nextBaking, prevBaking }) => ({
  type: types.ACCOUNT_NEXT_BAKING_SUCCESS,
  nextBaking,
  prevBaking
})

export const fetchNextBakingActionFailure = ({ error }) => ({
  type: types.ACCOUNT_NEXT_BAKING_FAILURE,
  error
})

export const fetchNextEndorsementAction = ({ hash }) => ({
  type: types.ACCOUNT_NEXT_ENDORSEMENT_REQUEST,
  hash,
})

export const fetchNextEndorsementActionSuccess = ({ nextEndorsement, prevEndorsement }) => ({
  type: types.ACCOUNT_NEXT_ENDORSEMENT_SUCCESS,
  nextEndorsement,
  prevEndorsement
})

export const fetchNextEndorsementActionFailure = ({ error }) => ({
  type: types.ACCOUNT_NEXT_ENDORSEMENT_FAILURE,
  error
})

export const formatTransactionDetails = ({ transactions, bookmarkList }) => ({
  type: types.FORMAT_TRANSACTION_DETAIL,
  transactions,
  bookmarkList
})

export const formatTransactionDetailsState = ({ data }) => ({
  type: types.FORMAT_TRANSACTION_DETAIL_STATE,
  data,

})

export const getAlias = ({ bookmarklist, hash }) => ({
  type: types.GET_ALIAS,
  bookmarklist,
  hash
})

export const getAliasState = ({ alias }) => ({
  type: types.GET_ALIAS_STATE,
  alias
})

export const fetchDelegateStatusSuccess = ({ data }) => ({
  type: types.ACCOUNT_DELEGATE_STATUS_SUCCESS,
  data
})

export const fetchContractAction = ({ hash, tab = DEFAULT_CONTRACT_TAB }) => ({
  type: types.ACCOUNT_CONTRACT_REQUEST,
  hash,
  actionType: tab,
})

export const fetchContractSuccess = ({ type, data }) => ({
  type: types.ACCOUNT_CONTRACT_SUCCESS[type],
  data
})

export const fetchContractFailure = ({ type, error }) => ({
  type: types.ACCOUNT_CONTRACT_FAILURE[type],
  error
})

export const updateOperationsTotalNumber = ({ operations }) => ({
  type: types.ACCOUNT_OPERATION_TOTAL_NUMBER_UPDATE,
  operations,
})

export const fetchBakerStatusSuccess = ({ isBaker }) => ({
  type: types.ACCOUNT_IS_BAKER_SUCCESS,
  isBaker
})

export const updateKeyArray = (key) => ({
  type: types.ACCOUNT_UPDATE_KEY_ARRAY,
  key
})

export const updateModalState = ({ state }) => ({
  type: types.ACCOUNT_UPDATE_NOTIFICATION_MODAL_STATE,
  state
})
