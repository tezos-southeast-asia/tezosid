import { all, call, put, takeEvery, select } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import {
  types,
  DEFAULT_OPERATION_TAB,
  DEFAULT_CONTRACT_TAB,
  DEFAULT_PAGE,
  DEFAULT_PAGE_SIZE,
  vaildOperationTabs,
  updateOperationsTotalNumber,
  updateMainTab,
  fetchContractAction,
  fetchContractSuccess,
  fetchContractFailure,
  fetchAccountDetailsSuccess,
  fetchAccountDetailsFailure,
  fetchBakerDetails,
  fetchBakerDetailsSuccess,
  fetchBakerDetailsFailure,
  fetchActionNumberSuccess,
  fetchActionNumberFailure,
  fetchAccountAction,
  fetchActionSuccess,
  fetchActionFailure,
  formatTransactionDetailsState,
  getAliasState,
  fetchDelegateStatusSuccess,
  fetchNextBakingActionSuccess,
  fetchNextBakingActionFailure,
  fetchNextEndorsementActionFailure,
  fetchNextEndorsementActionSuccess,
  fetchNextBakingAction,
  fetchNextEndorsementAction,
  fetchBakingAction,
  fetchBakerStatusSuccess
} from './actions'
import { fetchApi } from '../../helpers/api';
import { dateFormat } from "../../helpers/format";

const parseAccountDetails = ({ data = {}, isContract }) => {
  const { counter, delegate } = data
  // const { setable: delegateSetable, value: delegateValue } = delegate
  const tableKeyPrefix = 'table.'

  let tableData = [
    // {
    //   info: `${tableKeyPrefix}manager`,
    //   key: 'manager',
    //   value: manager,
    // },
    {
      info: `${tableKeyPrefix}originated`,
      key: 'originated',
      value: false,
    },
    // {
    //   info: `${tableKeyPrefix}spendable`,
    //   key: 'spendable',
    //   value: spendable,
    // },
    // {
    //   info: `${tableKeyPrefix}delegatable`,
    //   key: 'delegatable',
    //   value: delegateSetable,
    // },
    // {
    //   info: `${tableKeyPrefix}delegate`,
    //   key: 'delegate',
    //   value: delegateValue,
    // },
    {
      info: `${tableKeyPrefix}counter`,
      key: 'counter',
      value: counter,
    },
  ];

  if (isContract) {
    tableData.push({
      info: `${tableKeyPrefix}delegatedContracts`,
      key: 'delegatedContracts',
      value: [ delegate ],
    })
  }

  return {
    isContract,
    tableData,
    details: data
  }
}

const parseBalanceDetails = ({ data = {} }) => {
  const { frozen_balance_by_cycle } = data

  let currentDeposit = 0;
  let pendingRewards = 0;

  frozen_balance_by_cycle.map((item) => {
    currentDeposit += parseInt(item.deposit);
    pendingRewards += parseInt(item.rewards);
    return item;
  })

  data = {
    ...data,
    'currentDeposit': currentDeposit,
    'pendingRewards': pendingRewards
  }
  return data;
}


function* fetchAccountSaga({ hash }) {
  const apiOptions = {
    url: connectionConfig.accountDetails_KT + hash,
  }
  const isContract = hash.substring(0, 2).toUpperCase() === 'KT'
  let deactivatedStatus = '';
  if (!isContract) {
    yield put(fetchBakerDetails({ hash }))
  } else {
    deactivatedStatus = true;
  }

  const [{ data, error }] = yield all([
    call(fetchApi, apiOptions),
  ])
  let formattedData

  if (data) {
    formattedData = parseAccountDetails({ data, hash, isContract, deactivatedStatus })

    yield put(fetchAccountDetailsSuccess(formattedData))
    if (deactivatedStatus !== '') {
      const deactivatedData = 'inactive';
      const tableData = { info: `table.deactivatedStatus`, key: 'deactivatedStatus', value: deactivatedData }
      yield put(fetchDelegateStatusSuccess({ data: tableData }));
    }
    if (isContract) {
      yield put(fetchBakerStatusSuccess({ isBaker: false }));
    }
  } else {
    yield put(fetchAccountDetailsFailure(error))
  }
}

function* fetchBakerSaga({ hash }) {
  const apiOptions = {
    url: connectionConfig.accountDetails_TZ + hash,
  }
  const { data, error } = yield call(fetchApi, apiOptions)

  let formattedData;

  if (data) {
    const { deactivated, delegated_contracts: delegatedContracts = [] } = data;
    const deactivatedStatus = deactivated !== undefined ? deactivated : true;
    const deactivatedData = deactivatedStatus === true ? 'inactive' : 'active';
    const tableData = [
      {
        info: 'table.deactivatedStatus',
        key: 'deactivatedStatus',
        value: deactivatedData
      },
      {
        info: 'table.delegatedContracts',
        key: 'delegatedContracts',
        value: delegatedContracts.filter(delegatedContract => delegatedContract !== hash)
      }
    ]
    formattedData = parseBalanceDetails({ data })
    yield put(fetchDelegateStatusSuccess({ data: tableData }))
    yield put(fetchBakerDetailsSuccess(formattedData))
    yield put(fetchBakerStatusSuccess({ isBaker: true }));
  } else {
    const deactivatedData = 'inactive';
    const tableData = { info: `table.deactivatedStatus`, key: 'deactivatedStatus', value: deactivatedData }
    yield put(fetchDelegateStatusSuccess({ data: tableData }))
    yield put(fetchBakerStatusSuccess({ isBaker: false }));
    yield put(fetchBakerDetailsFailure(error))
  }
}

function* transverseArgs(args) {
  let temp = [];
  for (let i = 0; i < args.length; i++) {
    let input = "";
    let list = false;
    if (args[i].prim) {
      input = args[i].prim;
      if (input === "list") {
        list = true;
      }
    }
    if (args[i].annots) {
      for (let x = 0; x < args[i].annots.length; x++) {
        input = input + " " + args[i].annots[x]
      }
    }

    if (input !== "") temp.push(input + ";");
    if (list) temp.push("[")
    if (args[i].args) {
      temp.push("{")
      temp = [...temp, ...yield transverseArgs(args[i].args)]
      temp.push("}")
    }
    if (Array.isArray(args[i])) {
      temp = [...temp, ...yield transverseArgs(args[i])]
    }
    if (list) temp.push("]")
  }
  return temp;
}

function transverseStorage(args) {
  let temp = [];
  for (let i = 0; i < args.length; i++) {
    let pair = false;
    if (args[i].prim) {
      temp.push(args[i].prim);
      if (args[i].prim === "Pair") {
        pair = true
        temp.push("(")
      }
    }

    if (args[i].annots) {
      let input = "";
      for (let x = 0; x < args[i].annots.length; x++) {
        input = input + " " + args[i].annots[x]
      }
      temp.push(input);
    }
    if (!args[i].prim && !args[i].args && !Array.isArray(args[i]) && typeof args[i] === "object") {
      let tempArray = [];
      Object.keys(args[i]).forEach((key) => {
        if (args[i][key].args) {
          let currentArray = tempArray;
          let arrayReturn = transverseStorage(args[i][key].args);
          tempArray = [...currentArray, ...arrayReturn];
        }
        else if (Array.isArray(args[i][key])) {
          let currentArray = tempArray;
          let arrayReturn = transverseStorage(args[i][key]);
          tempArray = [...currentArray, "[", ...arrayReturn, "]"];
        } else {
          tempArray.push(args[i][key])
        }
      })
      let currentArray = temp;
      temp = [...currentArray, ...tempArray];
    }
    if (args[i].args) {
      let currentArray = temp;
      let arrayReturn = transverseStorage(args[i].args);
      temp = [...currentArray, ...arrayReturn];
    }
    if (Array.isArray(args[i])) {
      temp.push("[")
      const currentArray = temp;
      let arrayReturn = transverseStorage(args[i]);
      temp = [...currentArray, ...arrayReturn];
      temp.push("]")
    }
    if (pair) temp.push(")")
  }
  return temp;
}

function* fetchActionNumberSaga({ hash, actionType }) {
  const url = connectionConfig.accountNum[actionType]

  if (!url) {
    return yield put(fetchActionNumberFailure({ type: actionType }))
  }

  const apiOptions = {
    url: connectionConfig.accountNum[actionType] + hash,
  }
  const { data, error } = yield call(fetchApi, apiOptions)
  if (data) {
    yield put(fetchActionNumberSuccess({ number: data, type: actionType }))
  } else {
    yield put(fetchActionNumberFailure({ error, type: actionType }))
  }
}

const getActionType = ({ to, hash }) => {
  let type = '';
  let color = null;

  if (to === hash) {
    color = '#01ab03';
    type = 'Received';
  } else {
    type = 'Sent';
    color = '#dd0303';
  }

  return {
    color,
    type,
  }
}

const parsers = {
  transactions: (data, hash) => data.map(({ tx, op, internalTx }, index) => {
    const { opHash, blockLevel, blockHash } = op || {}
    const { fee, amount, blockTimestamp, source, destination, uuid, operationResultStatus } = tx || {}

    if (internalTx.length > 0) {
      const keyArr = [];
      const internalTransaction = internalTx.map(({ destination, amount, source, resultStatus }, index) => {
        return {
          id: opHash,
          key: index,
          opHash: opHash,
          amount,
          level: [blockLevel, blockHash],
          fromElement: [source],
          toElement: [destination],
          timestamp: dateFormat(blockTimestamp),
          fee,
          status: resultStatus !== 'applied' ? 'status-color' : ''
        }
      })
      const transaction = {
        id: opHash,
        key: uuid || index,
        level: [blockLevel, blockHash],
        fee: fee,
        amount: amount,
        timestamp: blockTimestamp,
        fromElement: [source, hash],
        toElement: [destination, hash],
        action: getActionType({ to: destination, hash }),
        opHash: opHash,
        status: operationResultStatus !== 'applied' ? 'status-color' : '',
        internalTx: internalTransaction
      }
      keyArr.push(uuid);
      return transaction
    } else {
      return {
        id: opHash,
        key: uuid || index,
        level: [blockLevel, blockHash],
        fee: fee,
        amount: amount,
        timestamp: blockTimestamp,
        fromElement: [source, hash],
        toElement: [destination, hash],
        action: getActionType({ to: destination, hash }),
        opHash: opHash,
        status: operationResultStatus !== 'applied' ? 'status-color icon-visible' : 'icon-visible',
        internalTx: []
      }
    }
  }),

  delegations: (data) => data.map(({ op, delegation }) => {
    const { opHash, blockHash, blockTimestamp } = op || {}
    const { fee, delegate, uuid, source, operationResultStatus } = delegation || {}

    return {
      id: opHash,
      key: uuid,
      blockId: [blockHash],
      timestamp: blockTimestamp,
      delegateSource: source || '-',
      delegateDestination: delegate || '-',
      fee: fee,
      opHash: opHash,
      status: operationResultStatus !== 'applied' ? 'status-color' : ''
    }

  }),

  originations: (data) => data.map(({ op, origination }, index) => {
    const { blockHash, blockTimestamp, opHash } = op || {}
    const { source, fee, uuid, balance, operationResultOriginatedContracts, operationResultStatus } = origination || {}

    return {
      id: opHash,
      key: uuid || index,
      blockId: [blockHash],
      timestamp: blockTimestamp,
      originateSource: source,
      fee: fee,
      opHash: opHash,
      newBalance: balance,
      newAccount: operationResultOriginatedContracts[0],
      status: operationResultStatus !== 'applied' ? 'status-color' : ''
    }
  }),

  endorsements: (data) => data.map(({ op, endor }, index) => {
    const { blockTimestamp, blockHash, opHash } = op || {}
    const { slots, uuid } = endor || {}
    let slot = '';
    for (let i = 0; i < slots.length; i++) {
      slot += slots[i];
      if (i !== slots.length - 1) {
        slot += ', '
      }
    }

    return {
      id: opHash,
      key: uuid || index,
      blockId: [blockHash],
      timestamp: blockTimestamp,
      slot: slot,
      opHash: opHash
    }
  }),
  activations: (data) => data.map(({ op, activateAccount }, index) => {
    const { blockTimestamp, blockHash, opHash } = op || {}
    const { secret, uuid, pkh } = activateAccount || {}

    return {
      id: opHash,
      key: uuid || index,
      blockId: [blockHash],
      timestamp: blockTimestamp,
      secret,
      opHash: opHash,
      hash: pkh
    }
  }),
  reveals: (data) => data.map(({ op, reveal }, index) => {
    const { blockTimestamp, blockHash, opHash } = op || {}
    const { publicKey, uuid, counter, source, fee, gasLimit, storageLimit, operationResultStatus } = reveal || {}
    return {
      id: opHash,
      key: uuid || index,
      blockId: [blockHash],
      timestamp: blockTimestamp,
      publicKey,
      counter,
      source,
      fee,
      gasLimit,
      storageLimit,
      opHash,
      status: operationResultStatus !== 'applied' ? 'status-color' : ''
    }
  }),
  proposals: (data) => data.map(({ op, proposal }, index) => {
    const { proposals, source, period, uuid } = proposal || {}
    const { opHash } = op || {}

    return {
      id: opHash,
      key: uuid || index,
      opHash,
      hash: proposals[0],
      proposalSource: source,
      period: period
    }
  }),
  ballots: (data) => data.map(({ op, ballot }, index) => {
    const { period, proposal, source, ballot: ballotNum, uuid } = ballot || {}
    const { opHash } = op || {}

    return {
      id: opHash,
      key: uuid || index,
      opHash,
      hash: proposal,
      proposalSource: source,
      period: period,
      ballot: ballotNum
    }
  }),
}

function* fetchActionSaga({ hash, actionType = DEFAULT_OPERATION_TAB, currentPage = DEFAULT_PAGE, pageSize = DEFAULT_PAGE_SIZE }) {
  const url = connectionConfig.accountActions[actionType]
  if (!url) {
    return yield put(fetchActionFailure({ type: actionType }))
  }

  const apiOptions = {
    url: url + hash,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const { data, error } = yield call(fetchApi, apiOptions)

  const parser = parsers[actionType]
  if (data) {
    yield put(fetchActionSuccess({ data: parser ? parser(data, hash) : data, type: actionType }))
  } else {
    yield put(fetchActionFailure({ error, type: actionType }))
  }
}

function* fetchNextBakingSaga({ hash }) {
  let currentCycle = 0;
  let currentBlock = 0;
  const blockApiOptions = {
    url: connectionConfig.blocks + '?p=0&n=1'
  }
  const { data: cycle } = yield call(fetchApi, blockApiOptions);
  if (cycle) {
    cycle.map(({ blocksAlpha }) => {
      const { cycle, level } = blocksAlpha;
      currentCycle = cycle;
      currentBlock = level;
      return blocksAlpha
    })
  }
  const prevCycle = currentCycle - 1;
  const nextCycle = currentCycle + 1;
  const currentCycleUrl = connectionConfig.baker_rights + `?cycle=${currentCycle}&delegate=${hash}&max_priority=1`;
  const nextCycleUrl = connectionConfig.baker_rights + `?cycle=${nextCycle}&delegate=${hash}&max_priority=1`;
  const prevCycleUrl = connectionConfig.baker_rights + `?cycle=${prevCycle}&delegate=${hash}&max_priority=1`;

  const apiOptions = {
    url: currentCycleUrl
  }
  const { data, error } = yield call(fetchApi, apiOptions)

  if (data.length > 0) {
    const nextBakingIndex = data.findIndex(item => item.level > currentBlock)
    let prevBaking = {};
    let nextBaking = {};
    if (nextBakingIndex !== -1) {
      nextBaking = {
        cycle: currentCycle,
        level: data[nextBakingIndex].level,
        estTime: dateFormat(data[nextBakingIndex].estimated_time),
        priority: data[nextBakingIndex].priority
      }
      if (nextBakingIndex === 0) {
        const apiOptions = {
          url: prevCycleUrl
        }
        const { data, error } = yield call(fetchApi, apiOptions)
        if (data.length > 0) {
          const blockApiOptions = {
            url: connectionConfig.blocks + data[data.length - 1].level
          }
          const { data: blockData } = yield call(fetchApi, blockApiOptions)
          const { block } = blockData;
          prevBaking = {
            cycle: prevCycle,
            level: data[data.length - 1].level,
            priority: data[data.length - 1].priority,
            estTime: dateFormat(block.timestamp)
          }
        } else if (data.length === 0) {
          prevBaking = {
            cycle: '-',
            level: '-',
            priority: '-',
            estTime: '-'
          }
        } else {
          yield put(fetchNextBakingActionFailure({ error }))
        }
      } else {
        const blockApiOptions = {
          url: connectionConfig.blocks + data[nextBakingIndex - 1].level
        }
        const { data: blockData } = yield call(fetchApi, blockApiOptions)
        const { block } = blockData;
        prevBaking = {
          cycle: currentCycle,
          level: data[nextBakingIndex - 1].level,
          priority: data[nextBakingIndex - 1].priority,
          estTime: dateFormat(block.timestamp)
        }
      }

      yield put(fetchNextBakingActionSuccess({ nextBaking: nextBaking, prevBaking: prevBaking }))
    } else {
      let nextBaking = {};
      let prevBaking = {};
      const apiOptions = {
        url: nextCycleUrl
      }
      const { data, error } = yield call(fetchApi, apiOptions)
      if (data.length > 0) {
        const nextBakingIndex = data.findIndex(item => item.level > currentBlock)
        nextBaking = {
          cycle: nextCycle,
          level: data[nextBakingIndex].level,
          estTime: dateFormat(data[nextBakingIndex].estimated_time),
          priority: data[nextBakingIndex].priority
        }
        if (nextBakingIndex !== 0) {
          const blockApiOptions = {
            url: connectionConfig.blocks + data[nextBakingIndex - 1].level
          }
          const { data: blockData } = yield call(fetchApi, blockApiOptions)
          const { block } = blockData;
          prevBaking = {
            cycle: nextCycle,
            level: data[nextBakingIndex - 1].level,
            priority: data[nextBakingIndex - 1].priority,
            estTime: dateFormat(block.timestamp)
          }
        } else {
          const apiOptions = {
            url: currentCycleUrl
          }
          const { data, error } = yield call(fetchApi, apiOptions)
          if (data) {
            const blockApiOptions = {
              url: connectionConfig.blocks + data[nextBakingIndex - 1].level
            }
            const { data: blockData } = yield call(fetchApi, blockApiOptions)
            const { block } = blockData;
            prevBaking = {
              cycle: currentCycle,
              level: data[nextBakingIndex - 1].level,
              priority: data[nextBakingIndex - 1].priority,
              estTime: dateFormat(block.timestamp)
            }
          }
          else {
            yield put(fetchNextBakingActionFailure({ error }))
          }
        }

        yield put(fetchNextBakingActionSuccess({ nextBaking: nextBaking, prevBaking: prevBaking }))
      } else if (data.length === 0) {
        let nextBaking = {};
        let prevBaking = {};

        const nextCycleApiOptions = {
          url: nextCycleUrl
        }
        const { data: nextCycleData } = yield call(fetchApi, nextCycleApiOptions)

        if (nextCycleData.length > 0) {
          const nextBakingIndex = nextCycleData.findIndex(item => item.level > currentBlock)
          nextBaking = {
            cycle: nextCycle,
            level: nextCycleData[nextBakingIndex].level,
            estTime: dateFormat(nextCycleData[nextBakingIndex].estimated_time),
            priority: nextCycleData[nextBakingIndex].priority
          }
        }
        else {
          nextBaking = {
            cycle: '-',
            level: '-',
            estTime: '-',
            priority: '-'
          }
        }
        const apiOptions = {
          url: prevCycleUrl
        }
        const { data } = yield call(fetchApi, apiOptions)
        if (data.length > 0) {
          const blockApiOptions = {
            url: connectionConfig.blocks + data[data.length - 1].level
          }
          const { data: blockData } = yield call(fetchApi, blockApiOptions)
          const { block } = blockData;
          prevBaking = {
            cycle: prevCycle,
            level: data[data.length - 1].level,
            priority: data[data.length - 1].priority,
            estTime: dateFormat(block.timestamp)
          }
        } else {
          prevBaking = {
            cycle: '-',
            level: '-',
            priority: '-',
            estTime: '-'
          }
        }

        yield put(fetchNextBakingActionSuccess({ nextBaking: nextBaking, prevBaking: prevBaking }))
      } else {
        yield put(fetchNextBakingActionFailure({ error }))
      }
    }
  } else {
    const nextBaking = {
      cycle: '-',
      level: '-',
      estTime: '-',
      priority: '-'
    }
    const prevBaking = {
      cycle: '-',
      level: '-',
      priority: '-',
      estTime: '-'
    }
    yield put(fetchNextBakingActionSuccess({ nextBaking: nextBaking, prevBaking: prevBaking }))
    yield put(fetchNextBakingActionFailure({ error }))
  }
}

function* fetchNextEndorsementSaga({ hash }) {
  let currentCycle = 0;
  let currentBlock = 0;
  const blockApiOptions = {
    url: connectionConfig.blocks + '?p=0&n=1'
  }
  const { data: cycle } = yield call(fetchApi, blockApiOptions);
  if (cycle) {
    cycle.map(({ blocksAlpha }) => {
      const { cycle, level } = blocksAlpha;
      currentCycle = cycle;
      currentBlock = level;
      return blocksAlpha
    })
  }

  const prevCycle = currentCycle - 1;
  const nextCycle = currentCycle + 1;
  const currentCycleUrl = connectionConfig.endorsing_rights + `?cycle=${currentCycle}&delegate=${hash}`;
  const nextCycleUrl = connectionConfig.endorsing_rights + `?cycle=${nextCycle}&delegate=${hash}`;
  const prevCycleUrl = connectionConfig.endorsing_rights + `?cycle=${prevCycle}&delegate=${hash}`;

  const apiOptions = {
    url: currentCycleUrl
  }
  const { data, error } = yield call(fetchApi, apiOptions)

  if (data.length > 0) {
    const nextEndorsementIndex = data.findIndex(item => item.level > currentBlock)
    if (nextEndorsementIndex !== -1) {
      let nextEndorsement = {};
      let prevEndorsement = {};
      nextEndorsement = {
        cycle: currentCycle,
        level: data[nextEndorsementIndex].level,
        estTime: dateFormat(data[nextEndorsementIndex].estimated_time),
        slots: data[nextEndorsementIndex].slots
      }
      if (nextEndorsementIndex === 0) {
        const apiOptions = {
          url: prevCycleUrl
        }
        const { data, error } = yield call(fetchApi, apiOptions)
        if (data.length > 0) {
          const blockApiOptions = {
            url: connectionConfig.blocks + data[data.length - 1].level
          }
          const { data: blockData } = yield call(fetchApi, blockApiOptions)
          const { block } = blockData;
          prevEndorsement = {
            cycle: prevCycle,
            level: data[data.length - 1].level,
            slots: data[data.length - 1].slots,
            estTime: dateFormat(block.timestamp)
          }
        } else if (data.length === 0) {
          prevEndorsement = {
            cycle: '-',
            level: '-',
            slots: '-',
            estTime: '-'
          }
        } else {
          yield put(fetchNextEndorsementActionFailure({ error }))
        }

      } else {
        const blockApiOptions = {
          url: connectionConfig.blocks + data[nextEndorsementIndex - 1].level
        }
        const { data: blockData } = yield call(fetchApi, blockApiOptions)
        const { block } = blockData;
        prevEndorsement = {
          cycle: currentCycle,
          level: data[nextEndorsementIndex - 1].level,
          slots: data[nextEndorsementIndex - 1].slots,
          estTime: dateFormat(block.timestamp)
        }
      }
      yield put(fetchNextEndorsementActionSuccess({ nextEndorsement, prevEndorsement }))
    } else {
      let nextEndorsement = {};
      let prevEndorsement = {};
      const apiOptions = {
        url: nextCycleUrl
      }
      const { data, error } = yield call(fetchApi, apiOptions)
      if (data.length > 0) {
        const nextEndorsementIndex = data.findIndex(item => item.level > currentBlock)
        nextEndorsement = {
          cycle: nextCycle,
          level: data[nextEndorsementIndex].level,
          estTime: dateFormat(data[nextEndorsementIndex].estimated_time),
          slots: data[nextEndorsementIndex].slots
        }
        if (nextEndorsementIndex !== 0) {
          const blockApiOptions = {
            url: connectionConfig.blocks + data[nextEndorsementIndex - 1].level
          }
          const { data: blockData } = yield call(fetchApi, blockApiOptions)
          const { block } = blockData;
          prevEndorsement = {
            cycle: nextCycle,
            level: data[nextEndorsementIndex - 1].level,
            estTime: dateFormat(block.timestamp),
            slots: data[nextEndorsementIndex - 1].slots
          }
        } else {
          const apiOptions = {
            url: prevCycleUrl
          }
          const { data, error } = yield call(fetchApi, apiOptions)
          if (data) {
            const blockApiOptions = {
              url: connectionConfig.blocks + data[nextEndorsementIndex - 1].level
            }
            const { data: blockData } = yield call(fetchApi, blockApiOptions)
            const { block } = blockData;
            prevEndorsement = {
              cycle: prevCycle,
              level: data[data.length - 1].level,
              estTime: dateFormat(block.timestamp),
              slots: data[data.length - 1].slots
            }
          } else {
            yield put(fetchNextEndorsementActionFailure({ error }))
          }
        }
        yield put(fetchNextEndorsementActionSuccess({ nextEndorsement, prevEndorsement }))
      } else if (data.length === 0) {
        let nextEndorsement = {};
        let prevEndorsement = {};

        const nextCycleApiOptions = {
          url: nextCycleUrl
        }
        const { data: nextCycleData } = yield call(fetchApi, nextCycleApiOptions)
        if (nextCycleData.length > 0) {
          const nextEndorsementIndex = nextCycleData.findIndex(item => item.level > currentBlock)
          nextEndorsement = {
            cycle: nextCycle,
            level: nextCycleData[nextEndorsementIndex].level,
            estTime: dateFormat(nextCycleData[nextEndorsementIndex].estimated_time),
            slots: nextCycleData[nextEndorsementIndex].slots
          }
        } else {
          nextEndorsement = {
            cycle: '-',
            level: '-',
            estTime: '-',
            priority: '-'
          }
        }
        const apiOptions = {
          url: prevCycleUrl
        }
        const { data } = yield call(fetchApi, apiOptions)
        if (data.length > 0) {
          const blockApiOptions = {
            url: connectionConfig.blocks + data[data.length - 1].level
          }
          const { data: blockData } = yield call(fetchApi, blockApiOptions)
          const { block } = blockData;
          prevEndorsement = {
            cycle: prevCycle,
            level: data[data.length - 1].level,
            estTime: dateFormat(block.timestamp),
            slots: data[data.length - 1].slots
          }
        } else {
          prevEndorsement = {
            cycle: '-',
            level: '-',
            estTime: '-',
            slots: '-'
          }
        }

        yield put(fetchNextEndorsementActionSuccess({ nextEndorsement, prevEndorsement }))
      } else {
        yield put(fetchNextEndorsementActionFailure({ error }))
      }
    }
  } else {
    const nextEndorsement = {
      cycle: '-',
      level: '-',
      estTime: '-',
      priority: '-'
    }
    const prevEndorsement = {
      cycle: '-',
      level: '-',
      estTime: '-',
      slots: '-'
    }
    yield put(fetchNextEndorsementActionSuccess({ nextEndorsement, prevEndorsement }))
    yield put(fetchNextEndorsementActionFailure({ error }))
  }
}

function* formatTransactionSaga({ transactions, bookmarkList }) {
  transactions.map((transaction) => {

    for (let bookmark of bookmarkList) {
      if (bookmark.hash === transaction.fromElement[0]) {
        transaction.fromElementAlias = bookmark.alias;

      }
      if (bookmark.hash === transaction.toElement[0]) {
        transaction.toElementAlias = bookmark.alias;
      }
    }

    return transaction;
  })
  yield put(formatTransactionDetailsState({ data: transactions }));
}

function* getAliasSaga({ bookmarklist, hash }) {
  let alias = "";
  if (bookmarklist !== undefined) {
    Object.keys(bookmarklist).map((key) => {
      if (bookmarklist[key].hash === hash) {
        if (bookmarklist[key].alias !== hash) {
          alias = bookmarklist[key].alias;
        }
      }
      return key;
    })
  }
  yield put(getAliasState({ alias: alias }));
}

function* fetchContractSaga({ hash, actionType = DEFAULT_CONTRACT_TAB }) {
  let smartContractAPIOptions = {
    url: connectionConfig.smartContract + hash,
  }
  const { data, error } = yield call(fetchApi, smartContractAPIOptions)
  if (data) {
    const { code, storage } = data.script;
    const [parameter, storageCode, contractCode] = code;
    if (actionType === 'contractStorage') {
      let currentStorage = [];
      currentStorage = yield transverseStorage([storage])
      yield put(fetchContractSuccess({ data: { storage: currentStorage }, type: actionType }))
    } else {
      let formattedParameter = [parameter.prim];
      formattedParameter.push(yield transverseArgs(parameter.args))
      let formattedStorage = [storageCode.prim];
      formattedStorage.push(yield transverseArgs(storageCode.args))
      let formattedContract = [contractCode.prim];
      formattedContract.push(yield transverseArgs(contractCode.args));
      yield put(fetchContractSuccess({ data: { formattedContract, formattedParameter, formattedStorage }, type: actionType }))
    }
  } else {
    yield put(fetchContractFailure({ error, type: actionType }))
  }
}

// function* fetchBakingSaga({ hash, actionType = DEFAULT_OPERATION_TAB, currentPage = DEFAULT_PAGE, pageSize = DEFAULT_PAGE_SIZE }) {
//   // const url = connectionConfig.accountActions[actionType]
//   // if (!url) {
//   //   return yield put(fetchActionFailure({ type: actionType }))
//   // }

//   // const apiOptions = {
//   //   url: url + hash,
//   //   params: {
//   //     p: currentPage - 1,
//   //     n: pageSize,
//   //   }
//   // }
//   // const { data, error } = yield call(fetchApi, apiOptions)

//   // const parser = parsers[actionType]
//   // if (data) {
//   //   yield put(fetchActionSuccess({ data: parser ? parser(data, hash) : data, type: actionType }))
//   // } else {
//   //   yield put(fetchActionFailure({ error, type: actionType }))
//   // }
// }

function* fetchOperationSaga({ tab, hash, op, currentPage, pageSize, contractOp, bakingOp }) {
  switch (tab) {
    case 'baking':
      yield put(fetchBakingAction({ hash, tab: bakingOp, currentPage, pageSize }))
      switch (bakingOp) {
        case 'upcomingBaking':
          yield put(fetchNextBakingAction({ hash }))
          break
        case 'upcomingEndorsement':
          yield put(fetchNextEndorsementAction({ hash }))
          break
        default:
      }
      break
    case 'contract':
      yield put(fetchContractAction({ hash, tab: contractOp }))
      break
    default:
      yield put(fetchAccountAction({ hash, tab: op, currentPage, pageSize }))

  }
}

const getOperations = (state) => state.Account.operations
function* initOperationSaga(props) {
  const { hash } = props

  yield put(updateMainTab(props))
  yield all(vaildOperationTabs.map(actionType => call(fetchActionNumberSaga, {
    hash,
    actionType,
  })))

  const operations = yield select(getOperations)
  yield put(updateOperationsTotalNumber({ operations }))
}

function* initContractSaga(props) {
  const { op } = props

  yield put(updateMainTab({ props, contractOp: op }))
}

function* initBakingSaga(props) {
  const { op, hash } = props

  switch (op) {
    case 'upcomingBaking':
      yield put(fetchNextBakingAction({ hash }))
      break
    case 'upcomingEndorsement':
      yield put(fetchNextEndorsementAction({ hash }))
      break
    default:
  }
  yield put(updateMainTab({ props, bakingOp: op }))

}

export default function* accountRootSaga() {
  yield all([
    takeEvery(types.ACCOUNT_DETAIL_REQUEST, fetchAccountSaga),
    takeEvery(types.ACCOUNT_BAKER_DETAIL_REQUEST, fetchBakerSaga),
    takeEvery(types.ACCOUNT_ACTION_NUMBER_REQUEST, fetchActionNumberSaga),
    takeEvery(types.ACCOUNT_ACTION_REQUEST, fetchActionSaga),
    takeEvery(types.FORMAT_TRANSACTION_DETAIL, formatTransactionSaga),
    takeEvery(types.GET_ALIAS, getAliasSaga),
    takeEvery(types.ACCOUNT_OPERATIONS_INIT, initOperationSaga),
    takeEvery(types.ACCOUNT_MAIN_TAB_UPDATE, fetchOperationSaga),
    takeEvery(types.ACCOUNT_CONTRACT_REQUEST, fetchContractSaga),
    takeEvery(types.ACCOUNT_CONTRACT_INIT, initContractSaga),
    takeEvery(types.ACCOUNT_BAKING_INIT, initBakingSaga),
    takeEvery(types.ACCOUNT_NEXT_BAKING_REQUEST, fetchNextBakingSaga),
    takeEvery(types.ACCOUNT_NEXT_ENDORSEMENT_REQUEST, fetchNextEndorsementSaga)
    //takeEvery(types.ACCOUNT_BAKING_REQUEST, fetchBakingSaga),
  ])
}
