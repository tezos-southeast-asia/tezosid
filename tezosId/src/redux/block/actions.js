export const types = {
  BLOCK_INIT: 'BLOCK_INIT',
  BLOCK_MAIN_TAB_UPDATE: 'BLOCK_MAIN_TAB_UPDATE',
  BLOCK_OVERVIEW_REQUEST: 'BLOCK_OVERVIEW_REQUEST',
  BLOCK_OVERVIEW_SUCCESS: 'BLOCK_OVERVIEW_SUCCESS',
  BLOCK_OVERVIEW_FAILURE: 'BLOCK_OVERVIEW_FAILURE',
  BLOCK_ACTION_NUMBER_REQUEST: 'BLOCK_ACTION_NUMBER_REQUEST',
  BLOCK_ACTION_NUMBER_SUCCESS: {
    transactions: 'BLOCK_ACTION_NUMBER_SUCCESS_TRANSACTIONS',
    delegations: 'BLOCK_ACTION_NUMBER_SUCCESS_DELEGATIONS',
    originations: 'BLOCK_ACTION_NUMBER_SUCCESS_ORIGINATIONS',
    endorsements: 'BLOCK_ACTION_NUMBER_SUCCESS_ENDORSEMENTS',
    activations: 'BLOCK_ACTION_NUMBER_SUCCESS_ACTIVATIONS',
    reveals: 'BLOCK_ACTION_NUMBER_SUCCESS_REVEALS',
    proposals: 'BLOCK_ACTION_NUMBER_SUCCESS_PROPOSALS',
    ballots: 'BLOCK_ACTION_NUMBER_SUCCESS_BALLOTS',
    nonces: 'BLOCK_ACTION_NUMBER_SUCCESS_NONCES',
    doubleBakings: 'BLOCK_ACTION_NUMBER_SUCCESS_DOUBLE_BAKINGS',
    doubleEndorsements:'BLOCK_ACTION_NUMBER_SUCCESS_DOUBLE_ENDORSEMENTS',
  },
  BLOCK_ACTION_NUMBER_FAILURE: {
    transactions: 'BLOCK_ACTION_NUMBER_FAILURE_TRANSACTIONS',
    delegations: 'BLOCK_ACTION_NUMBER_FAILURE_DELEGATIONS',
    originations: 'BLOCK_ACTION_NUMBER_FAILURE_ORIGINATIONS',
    endorsements: 'BLOCK_ACTION_NUMBER_FAILURE_ENDORSEMENTS',
    activations: 'BLOCK_ACTION_NUMBER_FAILURE_ACTIVATIONS',
    reveals: 'BLOCK_ACTION_NUMBER_FAILURE_REVEALS',
    proposals: 'BLOCK_ACTION_NUMBER_FAILURE_PROPOSALS',
    ballots: 'BLOCK_ACTION_NUMBER_FAILURE_BALLOTS',
    nonces: 'BLOCK_ACTION_NUMBER_FAILURE_NONCES',
    doubleBakings: 'BLOCK_ACTION_NUMBER_FAILURE_DOUBLE_BAKINGS',
    doubleEndorsements:'BLOCK_ACTION_NUMBER_FAILURE_DOUBLE_ENDORSEMENTS',
  },
  BLOCK_ACTION_REQUEST: 'BLOCK_ACTION_REQUEST',
  BLOCK_ACTION_SUCCESS: {
    transactions: 'BLOCK_ACTION_SUCCESS_TRANSACTIONS',
    delegations: 'BLOCK_ACTION_SUCCESS_DELEGATIONS',
    originations: 'BLOCK_ACTION_SUCCESS_ORIGINATIONS',
    endorsements: 'BLOCK_ACTION_SUCCESS_ENDORSEMENTS',
    activations: 'BLOCK_ACTION_SUCCESS_ACTIVATIONS',
    reveals: 'BLOCK_ACTION_SUCCESS_REVEALS',
    proposals: 'BLOCK_ACTION_SUCCESS_PROPOSALS',
    ballots: 'BLOCK_ACTION_SUCCESS_BALLOTS',
    nonces: 'BLOCK_ACTION_SUCCESS_NONCES',
    doubleBakings: 'BLOCK_ACTION_SUCCESS_DOUBLE_BAKINGS',
    doubleEndorsements:'BLOCK_ACTION_SUCCESS_DOUBLE_ENDORSEMENTS',
  },
  BLOCK_ACTION_FAILURE: {
    transactions: 'BLOCK_ACTION_FAILURE_TRANSACTIONS',
    delegations: 'BLOCK_ACTION_FAILURE_DELEGATIONS',
    originations: 'BLOCK_ACTION_FAILURE_ORIGINATIONS',
    endorsements: 'BLOCK_ACTION_FAILURE_ENDORSEMENTS',
    activations: 'BLOCK_ACTION_FAILURE_ACTIVATIONS',
    reveals: 'BLOCK_ACTION_FAILURE_REVEALS',
    proposals: 'BLOCK_ACTION_FAILURE_PROPOSALS',
    ballots: 'BLOCK_ACTION_FAILURE_BALLOTS',
    nonces: 'BLOCK_ACTION_FAILURE_NONCES',
    doubleBakings: 'BLOCK_ACTION_FAILURE_DOUBLE_BAKINGS',
    doubleEndorsements:'BLOCK_ACTION_FAILURE_DOUBLE_ENDORSEMENTS',
  },
  BLOCK_TOTAL_NUMBER_UPDATE: 'BLOCK_TOTAL_NUMBER_UPDATE',
  BLOCK_UPDATE_KEY_ARRAY: "BLOCK_UPDATE_KEY_ARRAY",
}

export const DEFAULT_PAGE_SIZE = 10
export const DEFAULT_PAGE = 1
export const vaildOperationTabs = Object.keys(types.BLOCK_ACTION_SUCCESS)
export const vaildMainTabs = ['overview', 'operations']
export const DEFAULT_TAB = vaildMainTabs[0]
export const DEFAULT_OPERATION_TAB = vaildOperationTabs[0]

export const initBlock = (props) => ({
  ...props,
  type: types.BLOCK_INIT,
})

export const updateMainTab = ({ tab = DEFAULT_TAB, hash, op = DEFAULT_OPERATION_TAB, p = DEFAULT_PAGE, n = DEFAULT_PAGE_SIZE }) => ({
  type: types.BLOCK_MAIN_TAB_UPDATE,
  tab,
  hash,
  op,
  currentPage: p,
  pageSize: n,
})

export const fetchBlockOverview = ({ hash }) => ({
  type: types.BLOCK_OVERVIEW_REQUEST,
  hash
})

export const fetchBlockOverviewSuccess = ({ data }) => ({
  type: types.BLOCK_OVERVIEW_SUCCESS,
  data,
})

export const fetchBlockOverviewFailure = ({ error, isFutureBaking }) => ({
  type: types.BLOCK_OVERVIEW_FAILURE,
  error,
  isFutureBaking,
})

export const fetchActionNumber = ({ hash, type = DEFAULT_TAB }) => ({
  type: types.BLOCK_ACTION_NUMBER_REQUEST,
  hash,
  actionType: type,
})

export const fetchActionNumberSuccess = ({ type, number }) => ({
  type: types.BLOCK_ACTION_NUMBER_SUCCESS[type],
  number
})

export const fetchActionNumberFailure = ({ type, error }) => ({
  type: types.BLOCK_ACTION_NUMBER_FAILURE[type],
  error
})

export const fetchOperationAction = ({ hash, tab = DEFAULT_OPERATION_TAB, currentPage = DEFAULT_PAGE, pageSize = DEFAULT_PAGE_SIZE }) => ({
  type: types.BLOCK_ACTION_REQUEST,
  hash,
  actionType: tab,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize),
})

export const fetchOperationActionSuccess = ({ type, data }) => ({
  type: types.BLOCK_ACTION_SUCCESS[type],
  data
})

export const fetchOperationActionFailure = ({ type, error }) => ({
  type: types.BLOCK_ACTION_FAILURE[type],
  error
})

export const updateOperationsTotalNumber = ({ operations }) => ({
  type: types.BLOCK_TOTAL_NUMBER_UPDATE,
  operations,
})

export const updateKeyArray = (key) => ({
  type: types.BLOCK_UPDATE_KEY_ARRAY,
  key
})
