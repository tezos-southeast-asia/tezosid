import { all, call, put, takeEvery, select } from 'redux-saga/effects'

import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import {
  types,
  DEFAULT_OPERATION_TAB,
  DEFAULT_PAGE,
  DEFAULT_PAGE_SIZE,
  vaildOperationTabs,
  updateMainTab,
  fetchBlockOverview,
  fetchBlockOverviewSuccess,
  fetchBlockOverviewFailure,
  fetchActionNumberSuccess,
  fetchActionNumberFailure,
  fetchOperationAction,
  fetchOperationActionSuccess,
  fetchOperationActionFailure,
  updateOperationsTotalNumber,
} from './actions'
import { fetchApi } from '../../helpers/api'
import { dateFormat } from "../../helpers/format";
import { blockConfigs } from '../../customApp/configs/blockConfigs';

const { blocksPerCycle } = blockConfigs

const parseBlockOverview = ({ data = {}, successorBlockHash }) => {
  const titles = [
    "table.levelInfo",
    "table.hashInfo",
    "table.successor",
    "table.predecessor",
    "table.timeStampInfo",
    "table.bakerInfo",
    "table.operationsCount",
    "table.netID",
    "table.protocolCount",
    "table.fitnessInfo",
    "table.seedNonceHash",
    "table.proofOfWorkNonce",
    "table.priority",
    "table.cyclePosition",
    "table.cycle",
    "table.fee",
    "table.volume"
  ];
  const keys = [
    "textLevel",
    "textBlockHash",
    "successor",
    "predecessor",
    "timestamp",
    "linkBaker",
    "operationsCount",
    "netID",
    "protocol",
    "fitness",
    "seedNonceHash",
    "proofOfWorkNonce",
    "priority",
    "cyclePosition",
    "cycle",
    "fee",
    "volume"
  ];
  const { blocksAlpha, block, blockStat } = data || {}
  const baker = blocksAlpha ? blocksAlpha.baker : ""
  const emptyBaker = !blocksAlpha;
  const emptyBlock = !block;
  let { opDelegationsFee, opOriginationsFee, opRevealsFee, opTxsFee, opTxsVolume, opsCount } = blockStat;

  if (emptyBlock) {
    return []
  }

  const blockInfo = {
    textLevel: block.level,
    blockNum: baker,
    textBlockHash: block.hash,
    predecessor: emptyBlock ? "" : block.predecessor,
    timestamp: emptyBlock ? "" : block.timestamp,
    linkBaker: emptyBaker ? "" : baker,
    operationsCount: opsCount !== null ? opsCount : 'calculating',
    netID: emptyBlock ? "" : block.chainId,
    protocol: emptyBlock ? "" : block.proto,
    fitness: emptyBlock ? "" : block.fitness.join(", "),
    seedNonceHash: emptyBaker ? "" : block.seedNonceHash,
    proofOfWorkNonce: emptyBaker ? "" : block.proofOfWorkNonce,
    priority: emptyBaker ? "" : block.priority,
    cyclePosition: blocksAlpha.cyclePosition,
    cycle: blocksAlpha.cycle,
    fee: opDelegationsFee !== null && opOriginationsFee !== null && opRevealsFee !== null && opTxsFee !== null ? opDelegationsFee + opOriginationsFee + opRevealsFee + opTxsFee : 'calculating',
    volume: opTxsVolume !== null ? opTxsVolume : 'calculating',
    successor: successorBlockHash
  };
  const tableData = keys.map((key, index) => {
    let entry = blockInfo[key];

    if (key === "timestamp") {
      entry = dateFormat(blockInfo[key]);
    }
    if (key === 'cyclePosition') {
      entry = entry + ' / '+(blocksPerCycle-1);
    }

    return { id: key, key, info: titles[index], value: entry };
  })
  return tableData;
}

const parsers = {
  internalTransactions: (data) => data.map(({ internalTx, op, tx }, index) => {
    const { destination, amount, source, resultStatus } = internalTx || {};
    const { blockTimestamp, fee } = tx || {};
    const { opHash, blockLevel, uuid, blockHash } = op || {};

    return {
      id: opHash,
      key: uuid || index,
      opHash: opHash,
      amount,
      level: [blockLevel, blockHash],
      fromElement: [source],
      toElement: [destination],
      timestamp: dateFormat(blockTimestamp),
      fee,
      status: resultStatus
    }
  }),

  transactions: (data) => data.map(({ tx, op, internalTx }, index) => {
    const { blockTimestamp, amount, source, destination, fee, operationResultStatus } = tx || {};
    const { opHash, blockLevel, uuid, blockHash } = op || {};
    if (internalTx.length > 0) {
      const internalTransaction = internalTx.map(({ destination, amount, source, resultStatus }, index) => {
        return {
          id: opHash,
          key: index,
          opHash: opHash,
          amount,
          level: [blockLevel, blockHash],
          fromElement: [source],
          toElement: [destination],
          timestamp: dateFormat(blockTimestamp),
          fee,
          status: resultStatus !== 'applied' ? 'status-color' : ''
        }
      })
      const transaction = {
        id: opHash,
        key: uuid || index,
        opHash: opHash,
        amount,
        level: [blockLevel, blockHash],
        fromElement: [source],
        toElement: [destination],
        timestamp: dateFormat(blockTimestamp),
        fee,
        status: operationResultStatus !== 'applied' ? 'status-color' : '',
        internalTx: internalTransaction
      }
      return transaction
    }
    else {
      return {
        id: opHash,
        key: uuid || index,
        opHash: opHash,
        amount,
        level: [blockLevel, blockHash],
        fromElement: [source],
        toElement: [destination],
        timestamp: dateFormat(blockTimestamp),
        fee,
        status: operationResultStatus !== 'applied' ? 'status-color icon-visible' : 'icon-visible',
        internalTx: []
      }
    }

  }),

  delegations: (data) => data.map(({ op, delegation }) => {
    const { opHash, blockLevel, blockTimestamp } = op || {};
    const { fee, delegate, uuid, source, operationResultStatus } = delegation || {};

    return {
      id: opHash,
      key: uuid,
      opHash: opHash,
      blockId: [blockLevel],
      timestamp: dateFormat(blockTimestamp),
      delegateSource: source || '-',
      delegateDestination: delegate || '-',
      fee: fee,
      status: operationResultStatus !== 'applied' ? 'status-color' : ''
    }

  }),

  originations: (data) => data.map(({ op, origination }, index) => {
    const { blockLevel, blockTimestamp, opHash } = op || {};
    const { source, fee, uuid, balance, operationResultOriginatedContracts, operationResultStatus } = origination || {};

    return {
      id: opHash,
      key: uuid || index,
      opHash: opHash,
      blockId: [blockLevel],
      timestamp: dateFormat(blockTimestamp),
      fee: fee,
      originateSource: source,
      newBalance: balance,
      newAccount: operationResultOriginatedContracts[0],
      status: operationResultStatus !== 'applied' ? 'status-color' : ''
    }
  }),

  endorsements: (data) => data.map(({ op, endor }, index) => {
    const { blockTimestamp, slots } = endor || {};
    const { opHash, blockLevel, uuid } = op || {};
    let slot = '';
    for (let i = 0; i < slots.length; i++) {
      slot += slots[i];
      if (i !== slots.length - 1) {
        slot += ', '
      }
    }

    return {
      id: opHash,
      key: uuid || index,
      opHash: opHash,
      blockId: [blockLevel],
      timestamp: dateFormat(blockTimestamp),
      slot: slot
    }
  }),
  activations: (data) => data.map(({ op, activateAccount }, index) => {
    const { secret, uuid, pkh } = activateAccount || {}
    const { blockTimestamp, blockHash, opHash } = op || {}

    return {
      id: opHash,
      key: uuid || index,
      blockId: [blockHash],
      timestamp: blockTimestamp,
      secret,
      opHash: opHash,
      hash: pkh
    }
  }),
  reveals: (data) => data.map(({ op, reveal }, index) => {
    const { publicKey, uuid, counter, source, fee, gasLimit, storageLimit, operationResultStatus } = reveal || {}
    const { blockTimestamp, blockHash, opHash } = op || {}
    return {
      id: opHash,
      key: uuid || index,
      blockId: [blockHash],
      timestamp: blockTimestamp,
      publicKey,
      counter,
      source,
      fee,
      gasLimit,
      storageLimit,
      opHash,
      status: operationResultStatus !== 'applied' ? 'status-color' : ''
    }
  }),
  proposals: (data) => data.map(({ op, proposal }, index) => {
    const { proposals, source, period, uuid } = proposal || {}
    const { opHash } = op || {}

    return {
      id: opHash,
      key: uuid || index,
      opHash: opHash,
      hash: proposals[0],
      proposalSource: source,
      period: period
    }
  }),
  ballots: (data) => data.map(({ op, ballot }, index) => {
    const { period, proposal, source, uuid } = ballot || {}
    const { opHash } = op || {}

    return {
      id: opHash,
      key: uuid || index,
      opHash: opHash,
      hash: proposal,
      proposalSource: source,
      period: period,
      ballot: ballot["ballot"]
    }
  }),
  nonces: (data) => data.map(({ op, seedNonceEvelation }, index) => {
    const { nonce, uuid } = seedNonceEvelation || {}
    const { opHash } = op || {}

    return {
      id: opHash,
      key: uuid || index,
      opHash: opHash,
      nonce: nonce
    }
  }),

  doubleBakings: (data) => data.map(({ op, bh1s }, index) => {
    const { opHash, uuid } = op || {}
    const { timestamp, level } = bh1s || {}

    return {
      id: opHash,
      key: uuid || index,
      opHash: opHash,
      timestamp: dateFormat(timestamp),
      denouncedLevel: [level],
    }
  }),

  doubleEndorsements: (data) => data.map(({ op }, index) => {
    const { opHash, uuid, blockTimestamp } = op;

    return {
      id: opHash,
      key: uuid || index,
      opHash,
      timestamp: dateFormat(blockTimestamp)
    }
  }),
}

function* fetchBlockOverviewSaga({ hash }) {
  const apiOptions = {
    url: connectionConfig.blocks + hash,
  }

  const { data, error } = yield call(fetchApi, apiOptions)

  if (data) {
    const successorApiOptions = {
      url: connectionConfig.blocks + parseInt(data.block.level + 1),
    }
    const successorBlock = yield call(fetchApi, successorApiOptions)
    const successorBlockHash = successorBlock.data !== null ? successorBlock.data.block.hash : '-';

    yield put(fetchBlockOverviewSuccess({ data: parseBlockOverview({ data, successorBlockHash }) }))
  } else {
    if (Number.isInteger(Number(hash))) {
      const latestBlockApiOptions = {
        url: connectionConfig.blocks,
        params: {
          p: 0,
          n: 1,
        }
      }
      const { data: latestBlock } = yield call(fetchApi, latestBlockApiOptions)
      const latestBlockLevel = latestBlock && latestBlock[0] && latestBlock[0].blocksAlpha && latestBlock[0].blocksAlpha.level

      if (Number.isInteger(latestBlockLevel) && Number(hash) > latestBlockLevel) {
        yield put(fetchBlockOverviewFailure({ isFutureBaking: true }))
      }
    } else {
      yield put(fetchBlockOverviewFailure({ error }))
    }
  }
}

function* fetchActionNumberSaga({ hash, actionType }) {
  const apiOptions = {
    url: connectionConfig.blockOperationNum[actionType] + hash,
  }

  const { data, error } = yield call(fetchApi, apiOptions)

  if (!error) {
    yield put(fetchActionNumberSuccess({ number: data, type: actionType }))
  } else {
    yield put(fetchActionNumberFailure({ error, type: actionType }))
  }
}

function* fetchOperationActionSaga({ hash, actionType = DEFAULT_OPERATION_TAB, currentPage = DEFAULT_PAGE, pageSize = DEFAULT_PAGE_SIZE }) {
  const url = connectionConfig.blockOperation[actionType];

  if (!url) {
    return yield put(fetchOperationActionFailure({ type: actionType }))
  }

  const apiOptions = {
    url: url + hash,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const [{ data, error }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchActionNumberSaga, {
      hash,
      actionType,
    })
  ])

  const parser = parsers[actionType]

  if (data) {
    yield put(fetchOperationActionSuccess({ data: parser ? parser(data, hash) : data, type: actionType }))
  } else {
    yield put(fetchOperationActionFailure({ error, type: actionType }))
  }
}

function* fetchOperationSaga({ tab, hash, op, currentPage, pageSize }) {
  switch (tab) {
    case 'operations':
      yield put(fetchOperationAction({ hash, tab: op, currentPage, pageSize }))
      break
    default:
      yield put(fetchBlockOverview({ hash }))
  }
}

const getOperations = (state) => state.Block.operations
function* initBlockSaga(props) {
  const { hash } = props

  yield put(updateMainTab(props))
  yield all(vaildOperationTabs.map(actionType => call(fetchActionNumberSaga, {
    hash,
    actionType,
  })))

  const operations = yield select(getOperations)
  yield put(updateOperationsTotalNumber({ operations }))
}

export default function* blockRootSaga() {
  yield all([
    takeEvery(types.BLOCK_OVERVIEW_REQUEST, fetchBlockOverviewSaga),
    takeEvery(types.BLOCK_ACTION_NUMBER_REQUEST, fetchActionNumberSaga),
    takeEvery(types.BLOCK_ACTION_REQUEST, fetchOperationActionSaga),
    takeEvery(types.BLOCK_INIT, initBlockSaga),
    takeEvery(types.BLOCK_MAIN_TAB_UPDATE, fetchOperationSaga),
  ])
}
