import { types } from '../actions'

const initialState = {
  data: [],
  total: 0,
  isLoading: false,
  isFutureBaking: false,
}

export default function operationDetailsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.BLOCK_MAIN_TAB_UPDATE:
    case types.BLOCK_OVERVIEW_REQUEST:
      return {
        ...state,
        data: [],
        isLoading: true,
      }
    case types.BLOCK_OVERVIEW_SUCCESS:
      return {
        ...state,
        data: action.data,
        isLoading: false
      }
    case types.BLOCK_TOTAL_NUMBER_UPDATE:
      return {
        ...state,
        total: Object.keys(action.operations).reduce((accumulator, type) => accumulator + action.operations[type].total, 0)
      }
    case types.BLOCK_OVERVIEW_FAILURE:
        return {
          ...state,
          data: [],
          isLoading: false,
          isFutureBaking: action.isFutureBaking,
        }
    default:
      return state
  }
}
