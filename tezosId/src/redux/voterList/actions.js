export const PER_PAGE = 10
export const DEFAULT_PAGE = 1
export const TOTAL_PROPOSERS_VOTERS = 0
export const CURRENTPROPOSER = ''

export const types = {
  VOTER_LIST_INIT: 'VOTER_LIST_INIT',
  VOTER_LIST_SUCCESS: 'VOTER_LIST_SUCCESS',
  VOTER_LIST_FAILURE: 'VOTER_LIST_FAILURE',
  FETCH_VOTER_LIST: 'FETCH_VOTER_LIST'
}

export const initVoterList = () => ({
  type: types.VOTER_LIST_INIT
})

export const fetchVoterListSuccess = (data) => ({
  type: types.VOTER_LIST_SUCCESS,
  data
})

export const fetchVoterListFailure = (error) => ({
  type: types.VOTER_LIST_FAILURE,
  error
})

export const fetchVoterList = ({ currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, currentProposal }) => ({
  type: types.FETCH_VOTER_LIST,
  currentPage: Number(currentPage),
  pageSize: Number(pageSize),
  currentProposal
})