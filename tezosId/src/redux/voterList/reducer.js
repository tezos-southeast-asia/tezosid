import { types, PER_PAGE, DEFAULT_PAGE, TOTAL_PROPOSERS_VOTERS } from './actions'

const initialState = {
  votes: null,
  currentProposal: null,
  votingPeriod: null,
  voters:
  {
    data: null,
    total: TOTAL_PROPOSERS_VOTERS,
    currentPage: DEFAULT_PAGE,
    pageSize: PER_PAGE,
  },
  proposals: null,
  isLoading: false,
  isError: false
}

export default function votingListReducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.VOTER_LIST_INIT:
      return {
        ...state,
        isLoading: true,
      }
    case types.FETCH_VOTER_LIST:
      return {
        ...state,
        isLoading: true
      }
    case types.VOTER_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        ...action.data,
      }
    case types.VOTER_LIST_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
        voters: initialState.voters
      }
    default:
      return state
  }
}