import { all, call, put, takeEvery } from 'redux-saga/effects'

import {
  types,
  fetchVoterListSuccess, 
  PER_PAGE,
  DEFAULT_PAGE,
  TOTAL_PROPOSERS_VOTERS,
  fetchVoterListFailure
} from './actions'
import { connectionConfig } from '../../customApp/configs/connectionConfigs'
import { fetchApi } from '../../helpers/api'

// TODO: unit tests
const {
  votingPeriod: votingPeriodUrl,
  currentProposal: currentProposalUrl,
  voterListing: voterListingUrl,
  votersBallot: votersBallotUrl,
} = connectionConfig

function* fetchVoterListInitSaga() {
  const [
    { data: votingPeriod },
    { data: currentProposal },

  ] = yield all([
    call(fetchApi, { url: votingPeriodUrl }),
    call(fetchApi, { url: currentProposalUrl }),
    call(fetchApi, { url: voterListingUrl }),
    call(fetchApi, { url: votersBallotUrl }),
  ])

  // const formattedVoters = ballots.map(({ pkh, ballot }) => {
  //   const votes = voters.find(({ pkh: voterHash }) => voterHash === pkh) || {}

  //   return {
  //     key: pkh,
  //     id: pkh,
  //     sourceText: getBakerName(pkh).baker,
  //     source: pkh,
  //     ballot: ballot,
  //     votes: votes.rolls || null
  //   }
  // })

  yield put(fetchVoterListSuccess({
    votingPeriod,
    currentProposal,
    //voters: new SortTableData(formattedVoters)
  }))
}

function* fetchProposersVoterListSaga(provider) {
  const {
    currentProposal,
    currentPage,
    pageSize,
  } = provider
  
  const apiOptions = {
    url: connectionConfig.proposalVoters + currentProposal,
    params: {
      p: currentPage - 1,
      n: pageSize,
    }
  }
  const totalApiOptions = {
    url: connectionConfig.proposalVotersNum + currentProposal
  }
  const [{ data, error }, { data: total }] = yield all([
    call(fetchApi, apiOptions),
    call(fetchApi, totalApiOptions),
  ])

  let formattedData;

  if (data) {
    const {currentPage, pageSize} = provider;
    formattedData = parseProposersVoters({ data, currentPage, pageSize, total })
    yield put(fetchVoterListSuccess({voters:formattedData}))
  } else {
    yield put(fetchVoterListFailure(error))
  }
}

const parseProposersVoters = ({ data = [], currentPage = DEFAULT_PAGE, pageSize = PER_PAGE, total = TOTAL_PROPOSERS_VOTERS }) => {
  let rowKey = 1;
  const proposers = data.map(({ op, ballot }) => {
    const { period, source } = ballot;
    const { opHash } = op;
    const proposer = {
      key: rowKey,
      period: period,
      opHash,
      ballot: ballot['ballot'],
      //count: count,
      //votes: "0",
      source,
    };
    rowKey++;
    return proposer;
  })
  return {
    currentPage,
    pageSize,
    total,
    data: proposers
  }
}

export default function* profileRootSaga() {
  yield all([
    takeEvery(types.VOTER_LIST_INIT, fetchVoterListInitSaga),
    takeEvery(types.FETCH_VOTER_LIST, fetchProposersVoterListSaga)
  ])
}
