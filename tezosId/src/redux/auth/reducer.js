import { types } from './actions'

const initialState = {
  loading: false,
  isLoggedIn: false,
  user: null,
  loginError: null,
  logoutError: null,
  signUpError: null,
  pwdResetEmailMsg: null,
  verificationEmailMsg: null,
  updatePwdMsg: null,
  updateProfileMsg: null,
}

export default function loginReducer(state = initialState, action = {}) {
  switch (action.type) {

    /* Reducer: Login */
    case types.LOGIN.EMAIL_PWD_REQUEST:
      return {
        ...state,
        loading: true,
      }
    case types.LOGIN.REQUEST:
      return {
        ...state,
        loading: true,
      }
    case types.LOGIN.SUCCESS:
      return {
        ...state,
        loading: false,
        isLoggedIn: true,
        user: action.user,
        loginError: null
      }
    case types.LOGIN.FAILURE:
      return {
        ...state,
        user: null,
        loading: false,
        loginError: action.error
      }

    /* Reducer: Logout */
    case types.LOGOUT.SUCCESS:
      return initialState
    case types.LOGOUT.FAILURE:
      return {
        ...state,
        logoutError: action.error
      }

    /* Reducer: Email Password Sign Up */
    case types.SIGNUP.REQUEST:
      return {
        ...state,
        loading: true,
      }
    case types.SIGNUP.SUCCESS:
      return {
        ...state,
        loading: false,
        isLoggedIn: true,
        user: action.user,
        loginError: null
      }
    case types.SIGNUP.FAILURE:
      return {
        ...state,
        user: null,
        loading: false,
        signUpError: action.error
      }

    /* Reducer: Reset Password */
    case types.EMAIL_PWD_RESET.REQUEST:
      return {
        ...state,
        loading: true,
      }
    case types.EMAIL_PWD_RESET.SUCCESS:
      return {
        ...state,
        loading: false,
        pwdResetEmailMsg: action.success
      }
    case types.EMAIL_PWD_RESET.FAILURE:
      return {
        ...state,
        loading: false,
        pwdResetEmailMsg: action.error
      }

    /* Reducer: Verification Email */
    case types.EMAIL_VERIFICATION.REQUEST:
      return {
        ...state,
        loading: true,
      }
    case types.EMAIL_VERIFICATION.SUCCESS:
      return {
        ...state,
        loading: false,
        verificationEmailMsg: action.success
      }
    case types.EMAIL_VERIFICATION.FAILURE:
      return {
        ...state,
        loading: false,
        verificationEmailMsg: action.error
      }

    /* Reducer: Update Password*/
    case types.UPDATE_PWD.REQUEST:
      return {
        ...state,
        loading: true,
      }
    case types.UPDATE_PWD.SUCCESS:
      return {
        ...state,
        loading: false,
        updatePwdMsg: action.success
      }
    case types.UPDATE_PWD.FAILURE:
      return {
        ...state,
        loading: false,
        updatePwdMsg: action.error
      }

    /* Reducer: Update Profile*/
    case types.UPDATE_PROFILE.REQUEST:
      return {
        ...state,
        loading: true,
      }
    case types.UPDATE_PROFILE.SUCCESS:
      return {
        ...state,
        loading: false,
        updateProfileMsg: action.success
      }
    case types.UPDATE_PROFILE.FAILURE:
      return {
        ...state,
        loading: false,
        updateProfileMsg: action.error
      }
    default:
      return state
  }
}