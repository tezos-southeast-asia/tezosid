import firebase from 'firebase/app';
import 'firebase/auth';
import { all, call, fork, put, take, takeEvery } from 'redux-saga/effects'

import {
  types,
  loginSuccess,
  loginFailure,
  logoutSuccess,
  logoutFailure,
  signUpSuccess,
  signUpFailure,
  pwdResetEmailFailure,
  pwdResetEmailSuccess,
  verificationEmailSuccess,
  verificationEmailFailure,
  updatePwdFailure,
  updatePwdSuccess,
  updateProfileSuccess,
  updateProfileFailure,
} from './actions'

import rsf from './rsf'

const authProviders = {
  google: new firebase.auth.GoogleAuthProvider(),
  twitter: new firebase.auth.TwitterAuthProvider()
}

/* Saga: Logins */
function* emailPwdLoginSaga(user) {
  try {
    const data = yield call(rsf.auth.signInWithEmailAndPassword, user.email, user.pwd);
    yield put(loginSuccess(data));
  }
  catch (error) {
    yield put(loginFailure(error));
  }
}

function* loginSaga(data) {
  const { provider } = data
  try {
    yield call(rsf.auth.signInWithPopup, authProviders[provider])
    // successful login will trigger the loginStatusWatcher, which will update the state
  } catch (error) {
    yield put(loginFailure(error))
  }
}

/* Saga: Logout */
function* logoutSaga() {
  try {
    yield call(rsf.auth.signOut)
    // successful logout will trigger the loginStatusWatcher, which will update the state
  } catch (error) {
    yield put(logoutFailure(error))
  }
}

/* Saga: Email Password Sign Up */
function* emailPwdSignUpSaga(newUser) {
  try {
    if (newUser.pwd !== undefined && newUser.pwd === newUser.confirmPwd) {
      const user = yield call(rsf.auth.createUserWithEmailAndPassword, newUser.email, newUser.pwd);
      yield put(signUpSuccess(user));
    } else {
      yield put(signUpFailure({ message: "Password doesn't match." }));
    }
  }
  catch (error) {
    yield put(signUpFailure(error));
  }
}

/* Saga: Password Reset Email */
function* passwordResetEmailSaga(data) {
  try {
    yield call(rsf.auth.sendPasswordResetEmail, data.email);
    yield put(pwdResetEmailSuccess({ message: "Reset password link has been sent to your inbox." }));
  }
  catch (error) {
    yield put(pwdResetEmailFailure(error));
  }
}

/* Saga: Verification Email */
function* verificationEmailSaga() {
  try {
    yield call(rsf.auth.sendEmailVerification);
    yield put(verificationEmailSuccess({ message: "Verification email has been sent has to your inbox." }));
  }
  catch (error) {
    yield put(verificationEmailFailure(error));
  }
}


/* Saga: Update Password */
function* updatePwdSaga(data) {
  try {
    yield call(rsf.auth.updatePassword, data.pwd);
    yield put(updatePwdSuccess({ message: "Password update successfully." }));
  }
  catch (error) {
    yield put(updatePwdFailure(error));
  }
}

/* Saga: Update Profile */
function* updateProfileSaga(date) {
  try {
    yield call(rsf.auth.updateProfile, {
      displayName: date.displayName,
      photoURL: null
    });
    yield put(updateProfileSuccess({ message: "Profile name update successfully." }));
  }
  catch (error) {
    yield put(updateProfileFailure(error));
  }
}

function* loginStatusWatcher() {
  try {
    // events on this channel fire when the user logs in or logs out
    const channel = yield call(rsf.auth.channel)

    while (true) {
      const { user } = yield take(channel)

      if (user) yield put(loginSuccess(user))
      else yield put(logoutSuccess())
    }
  } catch (e) {
    yield put(logoutSuccess())
  }
}

export default function* loginRootSaga() {
  yield fork(loginStatusWatcher)
  yield all([
    takeEvery(types.LOGIN.REQUEST, loginSaga),
    takeEvery(types.LOGIN.EMAIL_PWD_REQUEST, emailPwdLoginSaga),
    takeEvery(types.LOGOUT.REQUEST, logoutSaga),
    takeEvery(types.SIGNUP.REQUEST, emailPwdSignUpSaga),
    takeEvery(types.EMAIL_PWD_RESET.REQUEST, passwordResetEmailSaga),
    takeEvery(types.EMAIL_VERIFICATION.REQUEST, verificationEmailSaga),
    takeEvery(types.UPDATE_PWD.REQUEST, updatePwdSaga),
    takeEvery(types.UPDATE_PROFILE.REQUEST, updateProfileSaga)
  ])
}
