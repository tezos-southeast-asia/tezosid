import { firebaseApp } from './rsf';

export const types = {
  LOGIN: {
    EMAIL_PWD_REQUEST: 'EMAIL_PWD_LOGIN.REQUEST',
    REQUEST: 'LOGIN.REQUEST',
    SUCCESS: 'LOGIN.SUCCESS',
    FAILURE: 'LOGIN.FAILURE',
  },
  LOGOUT: {
    REQUEST: 'LOGOUT.REQUEST',
    SUCCESS: 'LOGOUT.SUCCESS',
    FAILURE: 'LOGOUT.FAILURE',
  },
  SIGNUP: {
    REQUEST: 'SIGNUP.REQUEST',
    SUCCESS: 'SIGNUP.SUCCESS',
    FAILURE: 'SIGNUP.FAILURE',
  },
  EMAIL_PWD_RESET: {
    REQUEST: 'PWD_RESET_EMAIL.REQUEST',
    SUCCESS: 'PWD_RESET_EMAIL.SUCCESS',
    FAILURE: 'PWD_RESET_EMAIL.FAILURE',
  },
  EMAIL_VERIFICATION: {
    REQUEST: 'EMAIL_VERIFICATION.REQUEST',
    SUCCESS: 'EMAIL_VERIFICATION.SUCCESS',
    FAILURE: 'EMAIL_VERIFICATION.FAILURE',
  },
  UPDATE_PWD: {
    REQUEST: 'UPDATE_PWD.REQUEST',
    SUCCESS: 'UPDATE_PWD.SUCCESS',
    FAILURE: 'UPDATE_PWD.FAILURE',
  },
  UPDATE_PROFILE: {
    REQUEST: 'UPDATE_PROFILE.REQUEST',
    SUCCESS: 'UPDATE_PROFILE.SUCCESS',
    FAILURE: 'UPDATE_PROFILE.FAILURE',
  }
}

/* Actions: Logins */
export const emailPwdLogin = (email, pwd) => ({
  type: types.LOGIN.EMAIL_PWD_REQUEST,
  email: email,
  pwd: pwd
})

export const login = (provider) => ({
  type: types.LOGIN.REQUEST,
  provider
})

export const loginSuccess = user => ({
  type: types.LOGIN.SUCCESS,
  user,
})

export const loginFailure = error => ({
  type: types.LOGIN.FAILURE,
  error,
})


/* Actions: Logout */
export const logout = () => ({
  type: types.LOGOUT.REQUEST,
})

export const logoutSuccess = (user) => ({
  type: types.LOGOUT.SUCCESS,
  user
})

export const logoutFailure = error => ({
  type: types.LOGOUT.FAILURE,
  error,
})


/* Actions: Email Password Sign Up */
export const emailPwdSignup = (email, pwd, confirmPwd) => ({
  type: types.SIGNUP.REQUEST,
  email: email,
  pwd: pwd,
  confirmPwd: confirmPwd
})

export const signUpSuccess = (user) => ({
  type: types.SIGNUP.SUCCESS,
  user
})

export const signUpFailure = error => ({
  type: types.SIGNUP.FAILURE,
  error,
})


/* Actions: Email Password Reset */
export const pwdResetEmail = (email) => ({
  type: types.EMAIL_PWD_RESET.REQUEST,
  email,
})

export const pwdResetEmailSuccess = success => ({
  type: types.EMAIL_PWD_RESET.SUCCESS,
  success
})

export const pwdResetEmailFailure = error => ({
  type: types.EMAIL_PWD_RESET.FAILURE,
  error,
})

/* Actions: Email verification */
export const verificationEmail = () => ({
  type: types.EMAIL_VERIFICATION.REQUEST,
})

export const verificationEmailSuccess = success => ({
  type: types.EMAIL_VERIFICATION.SUCCESS,
  success
})

export const verificationEmailFailure = error => ({
  type: types.EMAIL_VERIFICATION.FAILURE,
  error,
})


/* Actions: Update Password */
export const updatePwd = (pwd, confirmPwd) => ({
  type: types.UPDATE_PWD.REQUEST,
  pwd,
  confirmPwd
})

export const updatePwdSuccess = (success) => ({
  type: types.UPDATE_PWD.SUCCESS,
  success
})

export const updatePwdFailure = error => ({
  type: types.UPDATE_PWD.FAILURE,
  error,
})


/* Actions: Update User Profile */
export const updateProfile = (displayName) => ({
  type: types.UPDATE_PROFILE.REQUEST,
  displayName
})

export const updateProfileSuccess = (success) => ({
  type: types.UPDATE_PROFILE.SUCCESS,
  success
})

export const updateProfileFailure = error => ({
  type: types.UPDATE_PROFILE.FAILURE,
  error,
})

// checkAuth is only for boot process
export const checkAuth = (store) =>
  new Promise(resolve => {
    try {
      firebaseApp.auth().onAuthStateChanged((user) => {
        if (user) {
          return resolve(store.dispatch(loginSuccess(user)))
        } else {
          return resolve(store.dispatch(logoutSuccess()))
        }
      })
    } catch (e) {
      return resolve(store.dispatch(logoutSuccess()))
    }
  })
