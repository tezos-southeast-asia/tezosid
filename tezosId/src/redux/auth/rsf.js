import firebase from 'firebase/app'
import '@firebase/firestore'
import ReduxSagaFirebase from 'redux-saga-firebase'

import authConfigs, { isEnableFirebase } from '../../customApp/configs/authConfigs'

export const firebaseApp = isEnableFirebase ? firebase.initializeApp(authConfigs.firebase) : null

let rsf

try {
  const firestore = firebaseApp && firebaseApp.firestore();
  const settings = {timestampsInSnapshots: true};
  firestore && firestore.settings(settings);

  rsf = new ReduxSagaFirebase(firebaseApp)
} catch (e) {
  console.error('[FirebaseApp Error]', e)
}

export default rsf
