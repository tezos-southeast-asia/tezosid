import Amount from '../../../../customApp/components/amount';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('<Amount />', () => {
  it('renders with default props', () => {
    const wrapper = shallow(<Amount />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders with > 0', () => {
    const wrapper = shallow(<Amount amount={20} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders with > 10k', () => {
    const wrapper = shallow(<Amount amount={20000} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders with > 10m', () => {
    const wrapper = shallow(<Amount amount={20000000} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
