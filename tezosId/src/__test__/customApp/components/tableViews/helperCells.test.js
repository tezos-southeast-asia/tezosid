import {
  LinkCell,
  NumberCell,
} from '../../../../customApp/components/tableViews/helperCells';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('<LinkCell />', () => {
  it('renders empty string without value', () => {
    const wrapper = shallow(<LinkCell />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders a link with value = 0', () => {
    const wrapper = shallow(<LinkCell value={0} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders empty string with value = null', () => {
    const wrapper = shallow(<LinkCell value={null} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders empty string with value as -', () => {
    const wrapper = shallow(<LinkCell value="-" />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders a link with baker name when value has baker name', () => {
    const wrapper = shallow(<LinkCell value="tz1eEnQhbwf6trb8Q8mPb2RaPkNk2rN7BKi8" />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders a link when value is a normal hash', () => {
    const wrapper = shallow(<LinkCell value="BMABSiaZxdPSV1ivc3V4cezDiCiiPxSXQ9cRmkNvHRFxJH6Qj6P" />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
})


describe('<NumberCell />', () => {
  it('renders empty string without value', () => {
    const wrapper = shallow(<NumberCell />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders number 0 with value = 0', () => {
    const wrapper = shallow(<NumberCell value={0} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders number with value as number', () => {
    const wrapper = shallow(<NumberCell value={100} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders empty div with value = null', () => {
    const wrapper = shallow(<NumberCell value={null} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders translation wording with value as text', () => {
    const wrapper = shallow(<NumberCell value="calculating" />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
})
