import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import SigninContainer from '../../../../customApp/containers/signin';
import createComponentWithIntlRouter from '../../../utils/createComponentWithIntlRouter';
import createComponentWithRouter from '../../../utils/createComponentWithRouter';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('<Signin />', () => {
  it('renders with default props', () => {
    const mockData = {
      Auth: {}
    }
    const store = mockStore(mockData);
    const wrapper = createComponentWithIntlRouter(<SigninContainer store={store} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('renders with isLoggedIn', () => {
    const mockData = {
      Auth: {
        isLoggedIn: true
      }
    }
    const store = mockStore(mockData);
    const wrapper = createComponentWithRouter(<SigninContainer store={store} />);
    expect(
      wrapper.find('Router').prop('history').location.pathname
    ).toEqual('/')
  });

  it('renders with spin', () => {
    const mockData = {
      Auth: {
        loading: true
      }
    }
    const store = mockStore(mockData);
    const wrapper = createComponentWithIntlRouter(<SigninContainer store={store} />);
    expect(wrapper).toMatchSnapshot();
  });
});
