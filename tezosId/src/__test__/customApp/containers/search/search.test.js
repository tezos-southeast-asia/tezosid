import React from 'react';
import { Redirect } from "react-router-dom";
import { shallow } from 'enzyme';

import SearchContainer from '../../../../customApp/containers/search';

describe('<Search />', () => {
  const redirectTests = [
    {
      title: 'redirect to notfound with default props',
      props: {},
      to: '/notfound'
    },
    {
      title: 'redirect to notfound with undefined type of hash',
      props: {
        location: { pathname: '/search', search: 'hash=test' }
      },
      to: '/notfound'
    },
    {
      title: 'redirect to blocks with block hash',
      props: {
        location: { pathname: '/search', search: 'hash=BMZvSvhe2stQpBZMxApYvqXwBMrSjg9hp2XQwE58qJD6eLxgbke' }
      },
      to: '/blocks/BMZvSvhe2stQpBZMxApYvqXwBMrSjg9hp2XQwE58qJD6eLxgbke'
    },
    {
      title: 'redirect to accounts with account hash',
      props: {
        location: { pathname: '/search', search: 'hash=tz1R6Ej25VSerE3MkSoEEeBjKHCDTFbpKuSX' }
      },
      to: '/accounts/tz1R6Ej25VSerE3MkSoEEeBjKHCDTFbpKuSX'
    },
    {
      title: 'redirect to opertaions with operation hash',
      props: {
        location: { pathname: '/search', search: 'hash=ooukzM8ZMRoBNexPcrDvvPyxvjfds6c6j722ytUna5L7syvjXkR' }
      },
      to: '/operations/ooukzM8ZMRoBNexPcrDvvPyxvjfds6c6j722ytUna5L7syvjXkR'
    },
  ]

  redirectTests.forEach(({
    title,
    props,
    to
  }) => {
    it(title, () => {
      const wrapper = shallow(<SearchContainer {...props} />)
      const redirectComp = wrapper.find(Redirect)

      expect(redirectComp).toHaveLength(1)
      expect(redirectComp.prop('to')).toEqual(to)
    })
  })
})