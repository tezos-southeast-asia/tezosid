import axios from 'axios'

import { fetchApi } from '../../helpers/api'

jest.mock('axios')

describe('Helper - api - fetchApi', () => {
  const apiOptions = {
    url: 'https://test'
  }
  const mockResponse = { data: 'TEST DATA' }
  const mockError = 'TEST Error'

  afterEach(() => {
    axios.request.mockReset()
  })

  describe('When no url in apiOptions', () => {
    it('Should return error', async () => {
      const res = await fetchApi()

      expect(res.error).toBe('No API URL')
    })
  })

  describe('When apiOptions includes url', () => {
    it('Should return data for successful request', async () => {
      axios.request.mockResolvedValue(mockResponse);

      const res = await fetchApi(apiOptions)
      expect(res.data).toBe(mockResponse.data)
    })

    it('Should return error for failed request', async () => {
      axios.request.mockRejectedValue(mockError)
      const res = await fetchApi(apiOptions)
      expect(res.error).toBe(mockError)
    })
  })

  describe('When apiOptions includes url & other options', () => {
    it('Should return data for successful request', async () => {
      axios.request.mockResolvedValue(mockResponse);

      const options = {
        ...apiOptions,
        method: 'post',
        params: {
          p: 0
        },
        data: {
          n: 0
        }
      }
      const res = await fetchApi(options)
      const requestOptions = axios.request.mock.calls[0][0]

      expect(requestOptions.params).toBe(options.params)
      expect(requestOptions.method).toBe(options.method)
      expect(requestOptions.data).toBe(options.data)
      expect(res.data).toBe(mockResponse.data)
    })
  })
})
