import {
  parseSnapshots,
  getCycles,
} from '../../../redux/snapshots/sagas'

describe('Redux - snapshots', () => {
  describe('parseSnapshots', () => {
    describe('When no snapshots & cycles', () => {
      it('Should return an empty array', () => {
        const snapshots = parseSnapshots()

        expect(snapshots).toEqual([])
      })
    })

    describe('When snapshot index is not an Integer', () => {
      let snapshots

      beforeAll(() => {
        snapshots = parseSnapshots({
          snapshots: [{
            data: 'test'
          }],
          cycles: [{
            cycle: 8
          }],
        })
      })

      it('Should return snapshot with index as an empty string', () => {
        expect(snapshots[0].index).toEqual('')
      })

      it('Should return snapshot with level as an empty string', () => {
        expect(snapshots[0].level).toEqual('')
      })
    })

    describe('When snapshot cycle is not an Integer', () => {
      let snapshots

      beforeAll(() => {
        snapshots = parseSnapshots({
          snapshots: [{
            data: 8
          }],
          cycles: [{
            cycle: 'test'
          }],
        })
      })

      it('Should return snapshot with cycle as an empty string', () => {
        expect(snapshots[0].cycle).toEqual('')
      })

      it('Should return snapshot with key as array index', () => {
        expect(snapshots[0].key).toEqual('cycle-0')
      })
    })

    describe('When snapshot has cycle & snapshot index', () => {
      let snapshots

      beforeAll(() => {
        snapshots = parseSnapshots({
          snapshots: [{
            data: 8
          }],
          cycles: [{
            cycle: 173
          }],
        })
      })

      it('Should return snapshot with cycle', () => {
        expect(snapshots[0].cycle).toEqual(173)
        expect(snapshots[0].key).toEqual(173)
      })

      it('Should return snapshot with index as snapshot index', () => {
        expect(snapshots[0].index).toEqual(8)
      })

      it('Should return snapshot with correct level', () => {
        expect(snapshots[0].level).toEqual(682240)
      })
    })
  })

  describe('getCycles', () => {
    describe('When currentCycle or currentBlockLevel is not an Integer', () => {
      it('Should return default array', () => {
        const cycles = getCycles()

        expect(cycles).toEqual({
          total: 0,
          cycles: [],
          indexApis: [],
        })
      })
    })

    describe('When currentCycle & currentBlockLevel exist', () => {
      const props = {
        currentCycle: 169,
        currentBlockLevel: 692964,
      }
      let cycles

      beforeAll(() => {
        cycles = getCycles(props)
      })

      it('Should return correct total based on currentCycle', () => {
        expect(cycles.total).toEqual(170)
      })

      it('Should return correct cycle & apoOptions', () => {
        const cycleList = cycles.cycles
        const cycleLength = cycleList.length
        const maxCycle = props.currentCycle + 7

        expect(cycleLength).toEqual(10)
        expect(cycleList[0].cycle).toEqual(maxCycle)
        expect(cycleList[0].apiOptions.url).toEqual('https://api.tezos.id/mainnet/chains/main/blocks/692964/context/raw/json/cycle/176/roll_snapshot')
        expect(cycleList[cycleLength -1].cycle).toEqual(maxCycle - 9)
        expect(cycleList[cycleLength -1].apiOptions.url).toEqual('https://api.tezos.id/mainnet/chains/main/blocks/684033/context/raw/json/cycle/167/roll_snapshot')
      })
    })

    describe('When currentCycle, currentBlockLevel, pageSize & currentPage exist', () => {
      const props = {
        currentCycle: 169,
        currentBlockLevel: 692964,
        currentPage: 5,
        pageSize: 20,
      }
      let cycles

      beforeAll(() => {
        cycles = getCycles(props)
      })

      it('Should return correct total based on currentCycle', () => {
        expect(cycles.total).toEqual(170)
      })

      it('Should return correct cycle & apoOptions', () => {
        const cycleList = cycles.cycles
        const cycleLength = cycleList.length
        const {
          currentCycle,
          currentPage,
          pageSize,
        } = props
        const firstIndex = (currentPage - 1) * pageSize - 7

        expect(cycleLength).toEqual(pageSize)
        expect(cycleList[0].cycle).toEqual(currentCycle - firstIndex)
        expect(cycleList[0].apiOptions.url).toEqual('https://api.tezos.id/mainnet/chains/main/blocks/393217/context/raw/json/cycle/96/roll_snapshot')
        expect(cycleList[cycleLength -1].cycle).toEqual(currentCycle - firstIndex - pageSize + 1)
        expect(cycleList[cycleLength -1].apiOptions.url).toEqual('https://api.tezos.id/mainnet/chains/main/blocks/315393/context/raw/json/cycle/77/roll_snapshot')
      })
    })
  })
})