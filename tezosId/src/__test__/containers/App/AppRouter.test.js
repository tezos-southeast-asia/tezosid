import AppRouter from '../../../containers/App/AppRouter';
import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';

describe('<AppRouter />', () => {
  it('renders with default props', () => {
    const wrapper = shallow(<AppRouter />);
    expect(shallowToJson(wrapper)).toMatchSnapshot();
  });
});
