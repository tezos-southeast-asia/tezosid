import * as React from 'react';
import renderer from 'react-test-renderer';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from "react-router-dom";
const messages = require('../../languageProvider/locales/en-US'); 

const createComponentWithIntlRouter = (children, props) => {
  const intlProps = {
    locale: 'en',
    messages,
    ...props
  }
  return renderer.create(<IntlProvider {...intlProps}>
    <MemoryRouter>{children}</MemoryRouter>
  </IntlProvider>);
};

export default createComponentWithIntlRouter;