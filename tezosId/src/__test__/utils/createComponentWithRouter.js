
import * as React from 'react';
import { MemoryRouter } from "react-router-dom";
import { mount } from 'enzyme';

const createComponentWithIntlRouter = (children, initialEntries = ['/']) => {
  return mount(<MemoryRouter initialEntries={initialEntries}>{children}</MemoryRouter>);
};

export default createComponentWithIntlRouter;