import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from "react-router-redux";
import { Route } from "react-router-dom";

import { store, history } from './redux/store';
import App from './containers/App/App';

const DashApp = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Route
        path=""
        component={App}
      />
    </ConnectedRouter>
  </Provider>
);

export default DashApp;
