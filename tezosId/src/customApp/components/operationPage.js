import React, { Component } from "react";
import { Helmet } from "react-helmet";
import queryString from "query-string";
import { FormattedMessage } from "react-intl";
import { Alert } from "antd";

import { connectionConfig } from "../configs/connectionConfigs";
import {
  infoColumns,
  transactionsColumns
} from "../configs/tableColumnsConfigs";
import Box from "../../components/utility/box";
import LayoutWrapper from "../../components/utility/layoutWrapper";
import PageHeader from "../../components/utility/pageHeader";
import Table from "./tableViews/table";
import LinkTabs, { TabPane, LinkTab } from "./linkTabs";
import Tag from "./tag";

class OperationPage extends Component {
  static defaultProps = {
    vaildMainTabs: [],
    vaildOperationTabs: [],
    operationColumns: [],
    tabs: {},
    operations: {},
    pageName: '',
    pageRoute: '',
    location: {},
    match: {},
    history: {},
    initData: () => { },
    updateMainTab: () => { },
    fetchOperationAction: () => { },
    fetchActionNumber: () => { },
  }

  verifyTab = (tab) => {
    const { vaildMainTabs } = this.props
    const isVaildTab = tab ? vaildMainTabs.indexOf(tab) !== -1 : true

    return isVaildTab
  }

  verifyOperationTab = (opTab) => {
    const { vaildOperationTabs } = this.props
    const isVaildTab = opTab ? vaildOperationTabs.indexOf(opTab) !== -1 : true

    return isVaildTab
  }

  componentDidUpdate(prevProps) {
    const {
      location,
      match,
      history,
      updateMainTab,
      initData,
      fetchOperationAction,
    } = this.props
    const currentHash = match.params.id
    const prevHash = prevProps.match.params.id
    const { p, tab, n, op } = queryString.parse(location.search)
    const { p: prevP, tab: prevTab, n: prevN, op: prevOp } = queryString.parse(prevProps.location.search)
    const isVaildTab = this.verifyTab(tab)
    const isVaildOperationTab = this.verifyOperationTab(op)

    if (currentHash && prevHash && currentHash !== prevHash) {
      return initData({ hash: currentHash })
    }

    if (isVaildTab) {
      if (tab !== prevTab) {
        updateMainTab({ hash: currentHash, p, tab, n, op })
      }

      if (tab === 'operations') {
        if (isVaildOperationTab) {
          if (p !== prevP || op !== prevOp || n !== prevN) {
            fetchOperationAction({ hash: currentHash, tab: op, currentPage: p, pageSize: n })
          }
        } else {
          history.push({
            ...location,
            search: {
              tab: 'operations'
            }
          })
        }
      }
    } else {
      history.push({ ...location, search: '' })
    }
  }

  componentDidMount() {
    const {
      match,
      location,
      history,
      initData,
    } = this.props
    const hash = match.params.id
    const searchQueries = queryString.parse(location.search)
    const { tab, op } = searchQueries
    const isVaildTab = this.verifyTab(tab)
    const isVaildOperationTab = this.verifyOperationTab(op)

    if (isVaildTab) {
      if (tab === 'operations') {
        if (isVaildOperationTab) {
          initData({ hash, ...searchQueries })
        } else {
          history.push({
            ...location,
            search: {
              tab: 'operations'
            }
          })
        }
      } else {
        initData({ hash, ...searchQueries })
      }
    } else {
      history.push({ ...location, search: '' })
    }
  }

  onExpand = (expanded, record) => {
    const { updateKeyArray, operations } = this.props;
    const { keyArr } = operations['transactions']
    if (!expanded) {
      keyArr.splice(keyArr.indexOf(record.key), 1);
    } else {
      keyArr.push(record.key);
    }
    updateKeyArray(keyArr);
  }

  getOperationTabPanes = () => {
    const {
      location,
      operations,
      operationColumns,
    } = this.props
    const tabs = Object.keys(operationColumns)
    const tabPanes = tabs.map(type => {
      const columns = operationColumns[type]
      const data = operations[type]
      const newSearch = queryString.stringify({
        tab: "operations",
        op: type
      })

      const { data: tableData, total, isLoading, isError, currentPage, pageSize, keyArr } = data
      if (type === 'transactions') {
        return (
          <TabPane
            tab={
              <LinkTab
                to={{
                  ...location,
                  search: `?${newSearch}`
                }}
              >
                <FormattedMessage id={`topbar.${type}`} />
                <Tag>{total}</Tag>
              </LinkTab>
            }
            key={type}
          >
              <Table
                totalPage={total}
                currentPage={currentPage}
                pageSize={pageSize}
                pageTitle=""
                dataSource={tableData}
                columns={columns}
                fail={isError}
                loading={isLoading}
                isLink={true}
                expandedRowKeys={keyArr}
                onExpand={this.onExpand}
                expandedRowRender={data => {
                  const { internalTx } = data || []
                  return (
                    <Table
                      dataSource={internalTx}
                      columns={transactionsColumns}
                      pagination={false}
                    />
                  )
                }}
              />
          </TabPane>
        )
      }
      else {
        return (
          <TabPane
            tab={
              <LinkTab
                to={{
                  ...location,
                  search: `?${newSearch}`
                }}
              >
                <FormattedMessage id={`topbar.${type}`} />
                <Tag>{total}</Tag>
              </LinkTab>
            }
            key={type}
          >
            <Table
              totalPage={total}
              currentPage={currentPage}
              pageSize={pageSize}
              pageTitle=""
              dataSource={tableData}
              columns={columns}
              fail={isError}
              loading={isLoading}
              isLink={true}
            />
          </TabPane>
        )
      }

    })

    return tabPanes
  }

  getMainTabContent = (tab) => {
    switch (tab) {
      case "overview": {
        const { overview } = this.props;
        const { data, isLoading, isFutureBaking } = overview;

        return isFutureBaking ? (
            <Alert message={<FormattedMessage id="operation.waitingForBaking" />} type="warning" showIcon />
          ) : (
            <Table
              pagination={false}
              dataSource={data}
              columns={infoColumns}
              loading={isLoading}
            />
          )
      }
      case "operations": {
        const {
          opTabs,
        } = this.props

        return (
          <LinkTabs
            {...opTabs}
          >
            {this.getOperationTabPanes()}
          </LinkTabs>
        )
      }

      default:
        return null
    }
  }

  getMainTabPanes = () => {
    const {
      location,
      overview,
      vaildMainTabs,
    } = this.props
    const { total } = overview

    const mainTabPanes = vaildMainTabs.map((tab) => (
      <TabPane
        tab={
          <LinkTab
            to={{
              ...location,
              search: `?tab=${tab}`
            }}
          >
            <FormattedMessage id={`table.${tab}`} />
            {tab === 'operations' ? <Tag>{total}</Tag> : null}
          </LinkTab>
        }
        key={tab}
      >
        {this.getMainTabContent(tab)}
      </TabPane>
    ))

    return mainTabPanes
  }

  render() {
    const hashNum = this.props.match.params.id;
    const mainTabPanes = this.getMainTabPanes()
    const {
      tabs,
      pageName,
      pageRoute,
    } = this.props;

    return (
      <LayoutWrapper>
        <Helmet>
          <link rel="canonical" href={`${connectionConfig.tezosIdWeb}${pageRoute}/${hashNum}`} />
          <link rel="alternate" href={`${connectionConfig.tezosIdWeb}${hashNum}`} />
        </Helmet>
        <PageHeader>
          <FormattedMessage id={`${pageName}.title`} /><div className="hash-number">#{hashNum}</div>
        </PageHeader>
        <Box>
          <LinkTabs
            {...tabs}
          >
            {mainTabPanes}
          </LinkTabs>
        </Box>
      </LayoutWrapper>
    );
  }
}

export default OperationPage
