import React from "react";
import { Helmet } from "react-helmet";
import { withRouter } from "react-router";

import { domain, common } from "../../customApp/configs/metaConfigs";

const CommonMeta = ({ location }) => {
  const { title } = common

  return (
    <Helmet>
      {/* following meta tags are able to be overriden in different pages */}
      <title>{title}</title>
      <meta property="og:title" content={title} />
      <meta name="twitter:title" content={title} />
      <meta property="og:url" content={`${domain}${location.pathname.substring(1)}`} />
    </Helmet>
  )
}

export default withRouter(CommonMeta)
