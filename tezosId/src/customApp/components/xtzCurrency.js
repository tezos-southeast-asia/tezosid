import React from "react";

function xtzCurrency({ unit }) {
  return unit && <span className="xtz-currency">{unit}</span>;
}

export default xtzCurrency;
