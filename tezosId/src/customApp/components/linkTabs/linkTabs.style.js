import styled from 'styled-components';

const LinkTabsWrapper = ComponentName => styled(ComponentName)`
  &.ant-tabs.ant-link-tabs {
    .ant-tabs-nav {
      .ant-tabs-tab {
        padding: 0;
      }
      .ant-link-tab {
        padding: 12px 16px;
        display: block;
        text-decoration: none;

        &:focus {
          text-decoration: none;
        }
      }
    }   
  }
`;

export default LinkTabsWrapper;
