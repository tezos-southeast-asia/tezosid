import React from "react";
import classnames from "classnames";

import Tabs, { TabPane } from "../../../components/uielements/tabs";
import LinkTabWrapper from './linkTabs.style';
import LinkTab from "./linkTab";

const LinkTabs = (props) => {
  const { children, className, ...otherProps } = props
  return (
    <Tabs
      {...otherProps}
      className={classnames("ant-link-tabs", className)}
    >
      {children}
    </Tabs>
  )
}

export default LinkTabWrapper(LinkTabs);
export { TabPane, Tabs, LinkTab };