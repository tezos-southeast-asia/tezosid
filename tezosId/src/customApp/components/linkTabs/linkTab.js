import React from "react";
import classnames from "classnames";
import { Link } from "react-router-dom";

const LinkTab = (props) => {
  const { to, children, className, ...otherProps } = props
  return (
    <Link
      to={to}
      className={classnames("ant-link-tab", className)}
      {...otherProps}
    >
      {children}
    </Link>
  )
}


export default LinkTab;
