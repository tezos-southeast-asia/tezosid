import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import queryString from "query-string";

const defaultHandleClick = (e) => e.preventDefault()

const PageItem = withRouter(({ page, originalElement, location }) => {
  const { props } = originalElement
  const { children, onClick, ...restProps } = props
  const { p, ...rest } = queryString.parse(location.search)
  const newSearch = queryString.stringify({
    ...rest,
    p: page
  })

  if (page === 0) {
    return originalElement
  }

  return (
    <Link
      to={{
        ...location,
        search: `?${newSearch}`
      }}
      onClick={onClick || defaultHandleClick}
      {...restProps}
    >
      {children}
    </Link>
  )
})

const tablePaginationItem = (page, type, originalElement) => {
  return <PageItem page={page} originalElement={originalElement} />
}

export default tablePaginationItem
