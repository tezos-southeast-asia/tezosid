import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

import LayoutContentWrapper from "../../../components/utility/layoutWrapper";
import Box from "../../../components/utility/box";
import Table from "./table";

import Alert from "../../../components/feedback/alert";

const ErrorTable = () => (
  <Alert
    message={<FormattedMessage id="table.alertTitle" />}
    description={<FormattedMessage id="table.alertDesc" />}
    type="error"
    showIcon
  />
)

const ContentTable = ({
  tableTitle,
  tableSubtitle,
  pagination,
  columns,
  dataSource,
  loading,
  size,
  totalPage,
  pageOnChange,
  currentPage,
  pageSize,
  isLink,
  expandedRowRender,
  expandedRowKeys,
  onExpand,
}) => (
  <Box
    title={tableTitle}
    subtitle={tableSubtitle}
  >
    <Table
      pagination={pagination}
      columns={columns}
      dataSource={dataSource}
      loading={loading}
      size={size}
      totalPage={totalPage}
      pageOnChange={pageOnChange}
      currentPage={currentPage}
      pageSize={pageSize}
      isLink={isLink}
      expandedRowRender={expandedRowRender}
      expandedRowKeys={expandedRowKeys}
      onExpand={onExpand}
    />
  </Box>
)
export default class TablePage extends Component {
  render() {
    const { fail, pageTitle, pageDesc, className, ...restProps } = this.props
    const children = fail ? <ErrorTable /> : <ContentTable {...restProps} />

    return (
      <LayoutContentWrapper
        pageTitle={pageTitle}
        pageDesc={pageDesc}
        className={className}
      >
        { children }
      </LayoutContentWrapper>
    )
  }
}
