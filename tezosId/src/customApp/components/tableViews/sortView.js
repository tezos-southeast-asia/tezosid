import React, { Component } from "react";
import TableWrapper from "./antTable.style";
import Card from "../../containers/blockChain/card/card.style";
import Input from "../../../components/uielements/input";
import dataClass from "./data";
import { FormattedMessage } from "react-intl";

export default class extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      dataList: [],
      sortedFiltered: false,
      input: "",
      searchTerm: ""
    };
  }

  onChange(pagination, filters, sorter) {
    let dataList = this.state.sortedFiltered
      ? this.state.dataList
      : this.props.dataList;
    if (
      sorter &&
      sorter.columnKey &&
      sorter.order &&
      sorter.columnKey !== "source"
    ) {
      if (sorter.order === "ascend") {
        dataList.getSortAsc(sorter.columnKey);
      } else {
        dataList.getSortDesc(sorter.columnKey);
      }
      this.setState({ dataList: dataList, sortedFiltered: true });
    }
  }
  handleChange(event) {
    this.setState({ input: event.target.value });
  }
  componentDidUpdate() {
    if (this.state.searchTerm !== this.state.input) {
      let reset = false;
      let dataSet = this.props.dataList.datas;
      let newDataSet = [];
      if (this.state.input === "") {
        for (let i = 0; i < dataSet.length; i++) {
          newDataSet.push(dataSet[i]);
        }
        reset = true;
      } else {
        for (let i = 0; i < dataSet.length; i++) {
          if (
            dataSet[i].sourceText
              .toUpperCase()
              .includes(this.state.input.toUpperCase()) ||
            dataSet[i].ballot
              .toUpperCase()
              .includes(this.state.input.toUpperCase())
          ) {
            newDataSet.push(dataSet[i]);
          }
        }
      }
      this.setState({
        dataList: new dataClass(newDataSet),
        searchTerm: this.state.input,
        sortedFiltered: reset ? false : true
      });
    }
  }
  render() {
    const {
      title,
      loading,
      pagination = false,
    } = this.props
  
    return (
      <Card
        title={title}
        extra={
          <FormattedMessage id="voterList.filter">
            {placeholder => (
              <Input
                onChange={this.handleChange}
                placeholder={placeholder}
                style={{ width: "100%" }}
                type="text"
                value={this.state.input}
              />
            )}
          </FormattedMessage>
        }
      >
        <TableWrapper
          pagination={pagination}
          loading={loading}
          columns={this.props.columns}
          onChange={this.onChange}
          dataSource={
            this.state.sortedFiltered
              ? this.state.dataList.getAll()
              : this.props.dataList.getAll()
          }
          className="isoSortingTable"
        />
      </Card>
    );
  }
}
