import React, { Component } from "react";
import queryString from "query-string";
import { withRouter } from "react-router";

import Table from "../../../components/uielements/table";
import Pagniation from "../../../components/uielements/pagination";
import tablePaginationItem from "./tablePaginationItem";
import TableWrapper from "./antTable.style";

class TableComp extends Component {
  static defaultProps = {
    pagination: {
      showSizeChanger: true,
      pageSizeOptions: ["10", "20"],
      showQuickJumper: true,
      position: "both",
      size: "small",
    },
    isLink: false,
    pageOnChange: () => { }
  }

  componentDidMount() {
    const { isLink } = this.props

    isLink && this.getData()
  }

  componentDidUpdate(prevProps) {
    const { isLink, location } = this.props

    if (!isLink) return

    const { p: currentPage, n: pageSize } = queryString.parse(location.search);
    const { p: prevCurrentPage, n: prevPageSize } = queryString.parse(prevProps.location.search);

    if (currentPage !== prevCurrentPage || pageSize !== prevPageSize) {
      this.getData()
    }
  }

  getData = () => {
    const { location, pageOnChange, isLink } = this.props
    const { p, n } = queryString.parse(location.search)

    isLink && pageOnChange && pageOnChange({ currentPage: p, pageSize: n })
  }

  handlePageSizeChange = (page, pageSize) => {
    const { history, location } = this.props
    const { p, ...rest } = queryString.parse(location.search)
    const newSearch = queryString.stringify({
      ...rest,
      p: page,
      n: pageSize
    })

    history.push({
      ...location,
      search: `?${newSearch}`
    })
  }

  render() {
    const {
      dataSource,
      columns,
      loading,
      isLink,
      pagination,
      totalPage,
      pageOnChange,
      currentPage,
      pageSize,
      expandedRowRender,
      expandedRowKeys,
      onExpand,
    } = this.props
    let paginationProps = pagination ? { ...pagination } : false

    if (paginationProps) {
      paginationProps.total = totalPage;
      paginationProps.onChange = pageOnChange;
      paginationProps.onShowSizeChange = pageOnChange;
      paginationProps.current = currentPage;
      paginationProps.pageSize = pageSize;

      if (isLink) {
        paginationProps.className = "ant-table-pagination"
        paginationProps.itemRender = tablePaginationItem
        paginationProps.onChange = this.handlePageSizeChange
        paginationProps.onShowSizeChange = this.handlePageSizeChange
      }
    }
    const size = this.props.size || "default";

    return (
      <TableWrapper>
        {isLink && <Pagniation
          {...paginationProps}
        />
        }
        <Table
          pagination={isLink ? false : paginationProps}
          columns={columns}
          loading={loading}
          dataSource={dataSource}
          className="isoSimpleTable"
          size={size}
          rowClassName={(record) => record.status}
          expandedRowRender={expandedRowRender}
          expandedRowKeys={expandedRowKeys || []}
          onExpand={onExpand}
        />
        {isLink && <Pagniation
          {...paginationProps}
        />
        }
      </TableWrapper>
    );
  }
}

export default withRouter(TableComp)