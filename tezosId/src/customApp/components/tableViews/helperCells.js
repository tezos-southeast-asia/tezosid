import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from 'react-intl';

import ImageCellView from "../../../components/tables/imageCell";
import Amount from "../amount";
import DeleteCell from "../../../components/tables/deleteCell";
import EditableCell from "../../../components/tables/editableCell";
import FilterDropdown from "../../../components/tables/filterDropdown";
import Tag from "../tag";
import CopyToClipboard from "../copyToClipboard";
import { getBakerName } from '../../../helpers/utility';
import { dateFormat, getCurrentTimeDiff } from "../../../helpers/format";
import { columns } from '../../configs/tableColumnsConfigs'

const ShortTextCellWithCopyToClipboard = ({ value, type = "text", alias }) => {
  const TAILER = "...";
  const SUB_STRING_END_INDEX = 7;

  let Wrapper = "span";
  let wrapperProps = {};
  let displayValue = value;

  if (typeof value === "number") {
    displayValue = value.toString();
  } else if (typeof value !== "string") {
    displayValue = "-";
  }

  if (displayValue === "-") {
    return TextCell(displayValue);
  }
  let bakerName = getBakerName(displayValue);
  if (bakerName.replace) {
    displayValue = bakerName.baker;
  } else {
    displayValue = displayValue.substring(0, SUB_STRING_END_INDEX) + TAILER;
  }
  if (alias !== '-' && alias !== undefined) {
    if (alias !== value) {

      displayValue = alias.substring(0, 12);
      if (displayValue.length >= 12) {
        displayValue = displayValue + TAILER;
      }

    }
  }

  if (type === "link") {
    Wrapper = Link;
    wrapperProps = {
      to: `/${value}`
    };
  }

  return (
    <CopyToClipboard text={value}>
      <Wrapper {...wrapperProps}>{displayValue}</Wrapper>
    </CopyToClipboard>
  );
};
const DateCell = data => <p>{data.toLocaleString()}</p>;
const ImageCell = src => <ImageCellView src={src} />;
const ExternalLinkCell = (link, href) => <a href={href ? href : "#"}>{link}</a>;
const LinkCell = ({ value = '' } = {}) => {
  const displayValue = (value === null) ? '' : value.toString();
  const address = `/${value}`;
  const bakerDisplayValue = getBakerName(displayValue).baker;

  if (displayValue === "-") {
    return TextCell(value);
  } else if (bakerDisplayValue) {
    return <Link to={address}>{bakerDisplayValue}</Link>;
  } else {
    return (displayValue !== '') && <Link to={address}>{displayValue}</Link>;
  }
}
const TextCell = text => <div>{text}</div>;
const AmountCell = amount => <Amount amount={amount} />
const ValueCell = ({ object }) => {
  const { key, value } = object
  const column = columns[key] || {
    render: (object, key) => renderCell(object, "TextCell", key)
  } 
  const { render } = column
  const cellData = {
    [key]: value
  }

  return render(cellData, key);
}
const NumberCell = ({ value }) => {
  const number = Number(value)

  if (Number.isNaN(number)) {
    return value && <FormattedMessage id={`table.${value}`} />
  } else {
    return TextCell(value)
  }
}

const renderCell = (object, type, key) => {
  let value = object && object[key];

  switch (type) {
    case "ImageCell":
      return ImageCell(value);

    case "DateCell":
      return DateCell(value);

    case "LinkCell":
      return <LinkCell value={value} />;

    case "VoterLinkCell": {
      let Address = `/proposal-voters/${value}`;
      let displayValue = value.toString();
      if (displayValue === "-") {
        return TextCell(value);
      } else {
        return value && <CopyToClipboard text={value}><Link to={Address}>{displayValue.substring(0, 10) + '...'}</Link></CopyToClipboard>;
      }
    }


    case "ShortLinkCell":
      return (
        value && <ShortTextCellWithCopyToClipboard value={value} type="link" />
      );
    
    case "MultipleShortLinkCell":
        return (
          Array.isArray(value) && value.map(data => data && <ShortTextCellWithCopyToClipboard key={data} value={data} type="link" />)
        );

    case "ShortCopyCell":
      return (
        <ShortTextCellWithCopyToClipboard value={value} type="-" />
      )

    case "ShortFromToLinkCell": {
      const listedValue = value[0];
      const valueCurrent = value[1];
      let alias = "-";
      if (key === "fromElement") {
        if (object.fromElementAlias !== undefined) {
          alias = object.fromElementAlias
        }
      }
      else if (key === "toElement") {
        if (object.toElementAlias !== undefined) {
          alias = object.toElementAlias
        }

      }
      if (listedValue !== valueCurrent) {
        return (
          <ShortTextCellWithCopyToClipboard value={listedValue} type="link" alias={alias} />
        );
      } else {
        return <ShortTextCellWithCopyToClipboard value={listedValue} alias={alias} />;
      }
    }

    case "ShortDualLinkCell": {
      const valueFrom = value[0];
      const valueTo = value[1];

      return (
        <div>
          <ShortTextCellWithCopyToClipboard value={valueFrom} type="link" />
          {" → "}
          <ShortTextCellWithCopyToClipboard value={valueTo} type="link" />
        </div>
      );
    }

    case "HashConversionCell": {
      const valueIdNum = value[0]; //level/id
      const valueHash = value[1]; //hash
      let AddressIdNum = `/${valueHash}`;
      if (valueHash) {
        return <Link to={AddressIdNum}>{valueIdNum}</Link>;
      } else return TextCell(valueIdNum);
    }

    case "ShortHashConversionCell": {
      //For full block hash: in this case only [0]
      const valueIdNum = value[0];

      return (
        <ShortTextCellWithCopyToClipboard value={valueIdNum} type="link" />
      );
    }
    case "ExternalLinkCell":
      return ExternalLinkCell(value, object.link);

    case "AmountCell": {
      if (Number.isNaN(Number(value))) {
        return value ? <FormattedMessage id={`table.${value}`} /> : '';
      } else {
        return AmountCell(value);
      }
    }

    case "TranslationCell":
      return <FormattedMessage id={value} />

    case "TagCell": {
      const { color, type } = value
      return (
        <Tag color={color} style={{ width: 90, textAlign: "center" }}>
          {type}
        </Tag>
      )
    }

    case "DurationCell": {
      const timestamp = value
      const duration = getCurrentTimeDiff({ timestamp, format: 'H:m:s' })

      return (
        <Fragment>
          <div className="table-timestamp">{dateFormat(timestamp)}</div>
          <div className="table-duration">
            <FormattedMessage id="table.duration" values={duration} />
          </div>
        </Fragment>
      )
    }

    case 'ValueCell':
      return ValueCell({ object, type, key })

    case 'StatusCell':
      return value === 'active' ? (
        <div className="ant-table-status__active">
          <FormattedMessage id="table.active" />
        </div>
      ) : (
        <div className="ant-table-status__inactive">
          <FormattedMessage id="table.inactive" />
        </div>
      )

    case 'NumberCell':
      return <NumberCell value={value} />

    default: {
      const typeOfValue = typeof value;

      if (typeOfValue === "boolean") value = value.toString();

      return TextCell(value);
    }
  }
};

export {
  DateCell,
  ImageCell,
  ExternalLinkCell,
  LinkCell,
  TextCell,
  AmountCell,
  EditableCell,
  DeleteCell,
  FilterDropdown,
  ValueCell,
  NumberCell,
  renderCell,
};
