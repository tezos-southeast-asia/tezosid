import React, { Component } from "react";
import PropTypes from "prop-types";
import { message } from "antd";
import { CopyToClipboard } from "react-copy-to-clipboard";
import classnames from "classnames";
import { injectIntl } from "react-intl";

import Button from "../../../components/uielements/button";
import CopyToClipboardWrapper from "./copyToClipboard.style";

class CopyToClipboardComp extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    children: PropTypes.element.isRequired,
    className: PropTypes.string
  };

  static defaultProps = {
    text: "",
    children: null,
    className: ""
  };

  showCopiedInfo = text => {
    const { intl } = this.props
    const messageText = intl.formatMessage({ id: "copyToClipboard.copiedToClipboard"}, { text })
    message.config({ maxCount: 1, duration: 1 });
    message.info(messageText);
  };

  render() {
    const { children, text, className } = this.props;

    return (
      <CopyToClipboardWrapper className={classnames("copy-to-clipboard-wrapper", className)}>
        <CopyToClipboard text={text} onCopy={this.showCopiedInfo}>
          <Button icon="copy" />
        </CopyToClipboard>
        {children}
      </CopyToClipboardWrapper>
    );
  }
}

export default injectIntl(CopyToClipboardComp);
