import styled from 'styled-components';
import WithDirection from '../../../settings/withDirection';

const CopyToClipboardWrapper = styled.div`
  display: inline-block;

  .ant-btn.ant-btn-icon-only {
    height: auto;
    border: none;
    box-shadow: none;
    font-size: 12px;
  }
`;

export default WithDirection(CopyToClipboardWrapper);
