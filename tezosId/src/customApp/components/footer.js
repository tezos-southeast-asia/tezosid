import React, { Component } from "react";
import { Layout } from "antd";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";

import LanguageSwitcher from "../containers/languageSwitcher";

const { Footer } = Layout;

class TezosidFooter extends Component {
  render() {
    const year = new Date().getFullYear();

    return (
      <Footer>
        <Link to="/newsletter">
          <FormattedMessage id="footer.subscription" />
        </Link>
        <LanguageSwitcher />
        <div className="copyright">{`${year} © Tezos.ID`}</div>
      </Footer>
    );
  }
}

export default TezosidFooter;
