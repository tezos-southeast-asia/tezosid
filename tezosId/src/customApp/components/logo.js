import React from "react";
import { Link } from "react-router-dom";

export default ({ width = 150 }) => (
  <div className="isoLogoWrapper">
    <Link to="/">
      <img alt="TezosID Logo" src={require("../../image/TezosID_header_logo_w_name.svg")} style={{ width, textIndent: "-9999px" }} />
    </Link>
  </div>
);
