import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";

import XtzCurrency from "./xtzCurrency"

class Amount extends PureComponent {
  static propTypes = {
    amount: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    className: PropTypes.string,
    style: PropTypes.object
  }

  static defaultProps = {
    className: '',
    style: {}
  }

  render() {
    const { className, style } = this.props
    let amount = this.props.amount

    if (amount && typeof amount !== "number") amount = Number(amount)
    
    if (!Number.isInteger(amount)) return ""

    let amountComp = null;
    let amount1k = amount / 1000;
    let amount1000k = amount / 1000000;

    if (amount1000k >= 10) {
      amountComp = <Fragment>{amount1000k.toLocaleString()}<XtzCurrency unit="ꜩ"/></Fragment>;
    } else if (amount1k >= 10) {
      amountComp = <Fragment>{amount1k.toLocaleString()}<XtzCurrency unit="mꜩ"/></Fragment>;
    } else if (amount === 0) {
      amountComp = <Fragment>0<XtzCurrency unit="μꜩ"/></Fragment>;
    } else {
      amountComp = <Fragment>{amount.toLocaleString()}<XtzCurrency unit="μꜩ"/></Fragment>;
    }

    return (
      <span
        className={className}
        style={style}
      >
        {amountComp}
      </span>
    );
  }
}

export default Amount;
