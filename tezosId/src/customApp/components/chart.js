import React, { Component } from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

const generateChart = (data, xValue, yValue, chartType) => {
    let chart;

    if (chartType === 'pieChart') {
        chart = am4core.create("chartDiv", am4charts.PieChart);
    } else if (chartType === 'xyChart') {
        chart = am4core.create("chartDiv", am4charts.XYChart);
    }

    chart.data = data

    if (chartType === 'pieChart') {
        let pieSeries = chart.series.push(new am4charts.PieSeries());

        pieSeries.dataFields.value = xValue;
        pieSeries.dataFields.category = yValue;
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.strokeWidth = 2;
        pieSeries.slices.template.strokeOpacity = 1;

        // This creates initial animation
        pieSeries.hiddenState.properties.opacity = 1;
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.hiddenState.properties.startAngle = -90;
        return pieSeries;
    } else if (chartType === 'xyChart') {
        let dateAxis1 = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis1.renderer.grid.template.location = 0;

        let valueAxis1 = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis1.tooltip.disabled = true;
        valueAxis1.renderer.minWidth = 35;

        let series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.dateX = xValue;
        series1.dataFields.valueY = yValue;

        series1.tooltipText = "{valueY.value}";
        chart.cursor = new am4charts.XYCursor();

        let scrollbarX1 = new am4charts.XYChartScrollbar();
        scrollbarX1.series.push(series1);
        chart.scrollbarX = scrollbarX1;
        return chart;
    }
}

export class chart extends Component {

    componentDidMount() {
        let { data, xValue, yValue, chartType } = this.props;
        generateChart(data, xValue, yValue, chartType)
    }

    render() {
        return (
            <div id='chartDiv' style={{ width: "100%", height: "500px" }}>

            </div>
        )
    }
}

export default chart
