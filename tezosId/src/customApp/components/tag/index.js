
import React from "react";

import Tags from "../../../components/uielements/tag";
import TagWrapper from "../../../components/uielements/styles/tag.style";

const Tag = props => (
  <TagWrapper>
    <Tags color="#108ee9" {...props}>{props.children}</Tags>
  </TagWrapper>
);

export default Tag
