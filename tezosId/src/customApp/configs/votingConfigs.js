export const votingSteps = [
  {
    step: "proposal",
  },
  {
    step: "testing_vote",
  },
  {
    step: "testing",
  },
  {
    step: "promotion_vote",
  },
]