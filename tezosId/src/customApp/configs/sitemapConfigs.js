import { connectionConfig } from './connectionConfigs'

const { tezosEnv } = connectionConfig

/* each sitemap path could include
 *  - path
    - changeFreq
    - priority
    - isNoSitemap
 */

const sitemapEnvPaths = {
  mainnet: [
    {
      path: 'accounts/tz1TDSmoZXwVevLTEvKCTHWpomG76oC9S2fJ',
    },
    {
      path: 'accounts/tz1Tnjaxk6tbAeC2TmMApPh8UsrEVQvhHvx5',
    },
    {
      path: 'accounts/tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q',
    },
    {
      path: 'accounts/tz3e75hU4EhDU3ukyJueh5v6UvEHzGwkg3yC',
    },
    {
      path: 'accounts/tz1TzaNn7wSQSP5gYPXCnNzBCpyMiidCq1PX',
    },
    {
      path: 'accounts/tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
    },
    {
      path: 'accounts/tz1NortRftucvAkD1J58L32EhSVrQEWJCEnB',
    },
    {
      path: 'accounts/tz1c3Wh8gNMMsYwZd67JndQpYxdaaPUV27E7',
    },
    {
      path: 'accounts/tz1Xek93iSXXckyQ6aYLVS5Rr2tge2en7ZxS',
    },
    {
      path: 'accounts/tz1VmiY38m3y95HqQLjMwqnMS7sdMfGomzKi',
    },
    {
      path: 'accounts/tz1Ldzz6k1BHdhuKvAtMRX7h5kJSMHESMHLC',
    },
    {
      path: 'accounts/tz3adcvQaKXTCg12zbninqo3q8ptKKtDFTLv',
    },
    {
      path: 'accounts/tz1Pwgj6j55akKCyvTwwr9X4np1RskSXpQY4',
    },
    {
      path: 'accounts/tz1d6Fx42mYgVFnHUW8T8A7WBfJ6nD9pVok8',
    },
    {
      path: 'accounts/tz1MXFrtZoaXckE41bjUCSjAjAap3AFDSr3N',
    },
    {
      path: 'accounts/tz1aqcYgG6NuViML5vdWhohHJBYxcDVLNUsE',
    },
    {
      path: 'accounts/tz1Vm5cfHncKGBo7YvZfHc4mmudY4qpWzvSB',
    },
    {
      path: 'accounts/tz1MDKr36woXfVtrtXfV1ppPJERxPcm2wU6V',
    },
    {
      path: 'accounts/tz1NpWrAyDL9k2Lmnyxcgr9xuJakbBxdq7FB',
    },
    {
      path: 'accounts/tz3bEQoFCZEEfZMskefZ8q8e4eiHH1pssRax',
    },
    {
      path: 'accounts/tz1fUjvVhJrHLZCbhPNvDRckxApqbkievJHN',
    },
    {
      path: 'accounts/tz1NkFRjmkqqcGkAhqe78fdgemDNKXvL7Bod',
    },
    {
      path: 'accounts/tz1aRhFErGMgL57DYHMT1vYwv7PzsJN1chrk',
    },
    {
      path: 'accounts/tz1abTjX2tjtMdaq5VCzkDtBnMSCFPW2oRPa',
    },
    {
      path: 'accounts/tz1isXamBXpTUgbByQ6gXgZQg4GWNW7r6rKE',
    },
    {
      path: 'accounts/tz1KfEsrtDaA1sX7vdM4qmEPWuSytuqCDp5j',
    },
    {
      path: 'accounts/tz1c7pVR4w3KSQarJdBeh4NS2WxMUFBy1rHQ',
    },
    {
      path: 'accounts/tz1NRGxXV9h6SdNaZLcgmjuLx3hyy2f8YoGN',
    },
    {
      path: 'accounts/tz1MJx9vhaNRSimcuXPK2rW4fLccQnDAnVKJ',
    },
    {
      path: 'accounts/tz1hAYfexyzPGG6RhZZMpDvAHifubsbb6kgn',
    },
    {
      path: 'accounts/tz1Lc87p6zRaDwzJs9kHvdpm7XzeWE8QTwVB',
    },
    {
      path: 'accounts/tz1Scdr2HsZiQjc7bHMeBbmDRXYVvdhjJbBh',
    },
    {
      path: 'accounts/tz1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj',
    },
    {
      path: 'accounts/tz1bakeKFwqmtLBzghw8CFnqFvRxLj849Vfg',
    },
    {
      path: 'accounts/tz1UUgPwikRHW1mEyVZfGYy6QaxrY6Y7WaG5',
    },
    {
      path: 'accounts/tz1RCFbB9GpALpsZtu6J58sb74dm8qe6XBzv',
    },
    {
      path: 'accounts/tz1MkaTVYdM9QvXxKvYEwG9BPN2XxriTs7Jh',
    },
    {
      path: 'accounts/tz1b9MYGrbN1NAxphLEsPPNT9JC7aNFc5nA4',
    },
    {
      path: 'accounts/tz1V4qCyvPKZ5UeqdH14HN42rxvNPQfc9UZg',
    },
    {
      path: 'accounts/tz1bkKTY9Y3rTsHbpr2fbGUCRm736LLquQfM',
    },
    {
      path: 'accounts/tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
    },
    {
      path: 'accounts/tz1UJvHTgpVzcKWhTazGxVcn5wsHru5Gietg',
    },
    {
      path: 'accounts/tz1LH4L6XYT2JNPhvWYC4Zq3XEiGgEwzNRvo',
    },
    {
      path: 'accounts/tz1PPUo28B8BroqmVCMMNDudG4ShA2bzicrU',
    },
    {
      path: 'accounts/tz1Zhv3RkfU2pHrmaiDyxp7kFZpZrUCu1CiF',
    },
    {
      path: 'accounts/tz1X1fpAZtwQk94QXUgZwfgsvkQgyc2KHp9d',
    },
    {
      path: 'accounts/tz1RV1MBbZMR68tacosb7Mwj6LkbPSUS1er1',
    },
    {
      path: 'accounts/tz1TnYeuR4HhUTapQiXvzcjmcpvZYwR1B5S3',
    },
    {
      path: 'accounts/tz1hThMBD8jQjFt78heuCnKxJnJtQo9Ao25X',
    },
    {
      path: 'accounts/tz1R6Ej25VSerE3MkSoEEeBjKHCDTFbpKuSX',
    },
    {
      path: 'accounts/tz1Vtimi84kLh9RANfRVX2JvYtP4NPCT1aFm',
    },
    {
      path: 'accounts/tz1S1Aew75hMrPUymqenKfHo8FspppXKpW7h',
    },
    {
      path: 'accounts/tz1ZQppA6UerMz5CJtGvZmmB6z8L9syq7ixu',
    },
    {
      path: 'accounts/tz1PesW5khQNhy4revu2ETvMtWPtuVyH2XkZ',
    },
    {
      path: 'accounts/tz1iJ4qgGTzyhaYEzd1RnC6duEkLBd1nzexh',
    },
    {
      path: 'accounts/tz1PK8bNrfPbc73BtHH1A63jfU5UtbRFkvU4',
    },
    {
      path: 'accounts/tz1Z1WwoqgRFbLE3YNdYRpCx44NSfiMJzeAG',
    },
    {
      path: 'accounts/tz1ZuxNqUk7odhMC4Bfx2NXXeej9ReXKewa8',
    },
    {
      path: 'accounts/tz1QLXqnfN51dkjeghXvKHkJfhvGiM5gK4tc',
    },
    {
      path: 'accounts/tz1cX93Q3KsiTADpCC4f12TBvAmS5tw7CW19',
    },
    {
      path: 'accounts/tz1Vyuu4EJ5Nym4JcrfRLnp3hpaq1DSEp1Ke',
    },
    {
      path: 'accounts/tz1bkg7rynMXVcjomoe3diB4URfv8GU2GAcw',
    },
    {
      path: 'accounts/tz1TcH4Nb3aHNDJ7CGZhU7jgAK1BkSP4Lxds',
    },
    {
      path: 'accounts/tz1ei4WtWEMEJekSv8qDnu9PExG6Q8HgRGr3',
    },
    {
      path: 'accounts/tz1irJKkXS2DBWkU1NnmFQx1c1L7pbGg4yhk',
    },
    {
      path: 'accounts/tz1YKh8T79LAtWxX29N5VedCSmaZGw9LNVxQ',
    },
    {
      path: 'accounts/tz1gk3TDbU7cJuiBRMhwQXVvgDnjsxuWhcEA',
    },
    {
      path: 'accounts/tz1Vd1rXpV8hTHbFXCXN3c3qzCsgcU5BZw1e',
    },
    {
      path: 'accounts/tz1bHzftcTKZMTZgLLtnrXydCm6UEqf4ivca',
    },
    {
      path: 'accounts/tz1VceyYUpq1gk5dtp6jXQRtCtY8hm5DKt72',
    },
    {
      path: 'accounts/tz1aiYKXmSRckyJ9EybKmpVry373rfyngJU8',
    },
    {
      path: 'accounts/tz1KzSC1J9aBxKp7u8TUnpN8L7S65PBRkgdF',
    },
    {
      path: 'accounts/tz1L3vFD8mFzBaS8yLHFsd7qDJY1t276Dh8i',
    },
    {
      path: 'accounts/tz1e2meErj7eEfXwqr7bDK6N1YatmLaugfMp',
    },
    {
      path: 'accounts/tz1YdCPrYbksK7HCoYKDyzgfXwY16Fy9rrGa',
    },
    {
      path: 'accounts/tz1U638Z3xWRiXDDKx2S125MCCaAeGDdA896',
    },
    {
      path: 'accounts/tz1WpeqFaBG9Jm73Dmgqamy8eF8NWLz9JCoY',
    },
    {
      path: 'accounts/tz1iMAHAVpkCVegF9FLGWUpQQeiAHh4ffdLQ',
    },
    {
      path: 'accounts/tz1Lhf4J9Qxoe3DZ2nfe8FGDnvVj7oKjnMY6',
    },
    {
      path: 'accounts/tz1WJpSCYdQE9merfftanA8eFsqUPTpwAt8a',
    },
    {
      path: 'accounts/tz1KtvGSYU5hdKD288a1koTBURWYuADJGrLE',
    },
    {
      path: 'accounts/tz1Ryat7ZuRapjGUgsPym9DuMGrTYYyDBJoq',
    },
    {
      path: 'accounts/tz1MQJPGNMijnXnVoBENFz9rUhaPt3S7rWoz',
    },
    {
      path: 'accounts/tz1VHFxUuBhwopxC9YC9gm5s2MHBHLyCtvN1',
    },
    {
      path: 'accounts/tz1fZ767VDbqx4DeKiFswPSHh513f51mKEUZ',
    },
    {
      path: 'accounts/tz1cZfFQpcYhwDp7y1njZXDsZqCrn2NqmVof',
    },
    {
      path: 'accounts/tz1VYQpZvjVhv1CdcENuCNWJQXu1TWBJ8KTD',
    },
    {
      path: 'accounts/tz1LLNkQK4UQV6QcFShiXJ2vT2ELw449MzAA',
    },
    {
      path: 'accounts/tz1bLwpPfr3xqy1gWBF4sGvv8bLJyPHR11kx',
    },
    {
      path: 'accounts/tz1PeZx7FXy7QRuMREGXGxeipb24RsMMzUNe',
    },
    {
      path: 'accounts/tz1SYq214SCBy9naR6cvycQsYcUGpBqQAE8d',
    },
    {
      path: 'accounts/tz1iLbZZ9uoRuVJCrZ9ZwiJMpfzhy3c67mav',
    },
    {
      path: 'accounts/tz1Yh1nLXfwqVpP8btykuRguu61n3veVmADa',
    },
    {
      path: 'accounts/tz2PdGc7U5tiyqPgTSgqCDct94qd6ovQwP6u',
    },
    {
      path: 'accounts/tz1SohptP53wDPZhzTWzDUFAUcWF6DMBpaJV',
    },
    {
      path: 'accounts/tz1eEnQhbwf6trb8Q8mPb2RaPkNk2rN7BKi8',
    },
    {
      path: 'accounts/tz1WBfwbT66FC6BTLexc2BoyCCBM9LG7pnVW',
    },
    {
      path: 'accounts/tz1Z3KCf8CLGAYfvVWPEr562jDDyWkwNF7sT',
    },
    {
      path: 'accounts/tz1ZRWFLgT9sz8iFi1VYWPfRYeUvUSFAaDao',
    },
    {
      path: 'accounts/tz1iZEKy4LaAjnTmn2RuGDf2iqdAQKnRi8kY',
    },
    {
      path: 'accounts/tz1V2FYediKBAEaTpXXJBSjuQpjkyCzrTSiE',
    },
    {
      path: 'accounts/tz1eZwq8b5cvE2bPKokatLkVMzkxz24z3Don',
    },
    {
      path: 'accounts/tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
    },
    {
      path: 'accounts/tz1LmaFsWRkjr7QMCx5PtV6xTUz3AmEpKQiF',
    },
    {
      path: 'accounts/tz1Zcxkfa5jKrRbBThG765GP29bUCU3C4ok5',
    },
    {
      path: 'accounts/tz1go7f6mEQfT2xX2LuHAqgnRGN6c2zHPf5c',
    },
    {
      path: 'accounts/tz1YTyvABUyhE7JHpxMVBVqjZnZM4ofMrWKE',
    },
    {
      path: 'accounts/tz1XXayQohB8XRXN7kMoHbf2NFwNiH3oMRQQ',
    },
    {
      path: 'accounts/tz1W1f1JrE7VsqgpUpj1iiDobqP5TixgZhDk',
    },
    {
      path: 'accounts/tz1YQCVjyfVwtiPCDPR8JP4TA9G9H5Y3dkup',
    },
    {
      path: 'accounts/tz1djEKk6j1FqigTgbRsunbnY9BB7qsn1aAQ',
    },
    {
      path: 'accounts/tz1P9GwUWeAyPoT3ZwBc6gWCTP9exEPaVU3w',
    },
    {
      path: 'accounts/tz1Nn14BBsDULrPXtkM9UQeXaE4iqJhmqmK5',
    },
    {
      path: 'accounts/tz1TaLYBeGZD3yKVHQGBM857CcNnFFNceLYh',
    },
  ]
}

export const sitemapPaths = sitemapEnvPaths[tezosEnv] || []

