import { connectionConfig } from "./connectionConfigs"

const tezosEnv = connectionConfig.tezosEnv
const authConfigs = {
  mainnet: {
    firebase: {
      apiKey: '',
      authDomain: '',
      databaseURL: '',
      projectId: '',
      storageBucket: '',
      messagingSenderId: '',
    }
  },
  babylonnet: {
    firebase: {
      apiKey: '',
      authDomain: '',
      databaseURL: '',
      projectId: '',
      storageBucket: '',
      messagingSenderId: '',
    }
  },
  carthagenet: {
    firebase: {
      apiKey: '',
      authDomain: '',
      databaseURL: '',
      projectId: '',
      storageBucket: '',
      messagingSenderId: '',
    }
  },
}

const authEnvConfigs = authConfigs[tezosEnv]
const { projectId, apiKey, authDomain } = authEnvConfigs.firebase
export const isEnableFirebase = projectId && apiKey
export const isEnableFirebaseAuth = isEnableFirebase && authDomain

export default authEnvConfigs;
