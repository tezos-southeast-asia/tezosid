export const devlogList = [
  {
    date: "2019/11/21",
    entries: [
      "Added Notification Button For Telegram"
    ]
  },
  {
    date: "2019/11/8",
    entries: [
      "Added Internal Transactions"
    ]
  },
  {
    date: "2019/11/5",
    entries: [
      "Added Developer API Page"
    ]
  },
  {
    date: "2019/10/31",
    entries: [
      "Added Next and Last Baking",
      "Added Next and Last Endorsement",
      "Added Snapshot Page for BABYLONNET"
    ]
  },
  {
    date: "2019/10/23",
    entries: [
      "Added Snapshot Page",
      "Added Nonces Page"
    ]
  },
  {
    date: "2019/10/22",
    entries: [
      "Added Activation Page",
      "Launched BABYLONNET"
    ]
  },
  {
    date: "2019/10/18",
    entries: [
      "Added Smart Contract Viewer Page"
    ]
  },
  {
    date: "2019/10/15",
    entries: [
      "Added API Documentation"
    ]
  },
  {
    date: "2019/10/8",
    entries: [
      "Added Top Baker Chart",
      "Added Operations Per Block Chart",
      "Added Voting Process Explaination",
      "Added Current Deposits and Pending Rewards To Account Page"
    ]
  },
  {
    date: "2019/10/2",
    entries: [
      "Added Bakers Average Priorities Chart",
      "Average Baking Time Chart"
    ]
  },
  {
    date: "2019/9/30",
    entries: [
      "Added All Account Page",
      "Added All Contract Page"
    ]
  },
  {
    date: "2019/9/25",
    entries: [
      "Added Fee and Volume in Block Information Page"
    ]
  },
  {
    date: "2019/9/20",
    entries: [
      "Added Double Endorsement Page",
      "Added Double Endorsement tab on operation and block page"
    ]
  },
  {
    date: "2019/9/18",
    entries: [
      "Added Double Baking Page",
      "Added Double Baking tab on operation and block page",
      "Operations Chart",
      "Transactions Fee Chart"
    ]
  },
  {
    date: "2019/9/7",
    entries: [
      "migrate homepage/blocks page/transactions page to the new api which provide more precise data",
      "support level search",
      "provide total number of data for blocks page/transactions page",
    ]
  },
  {
    date: "2019/9/6",
    entries: [
      "add follow/unfollow account feature to account page",
      "add display monitored accounts on My Page",
      "add email notification for monitored account activity"
    ]
  },
  {
    date: "2019/9/1",
    entries: [
      "SEO pagination/tabs for backer rights page",
      "SEO pagination/tabs for account page",
      "Update Sitemap & add different title for account pages",
    ]
  },
  {
    date: "2019/8/13",
    entries: [
      "SEO pagination for blocks/transactions/endorsements/delegations/originations pages and support its url query",
    ]
  },
  {
    date: "2019/8/6",
    entries: [
      "add blocks, baking rewards, transactions, endorsements, activations charts",
    ]
  },
  {
    date: "2019/8/5",
    entries: [
      "SEO improvement",
    ]
  },
  {
    date: "2019/6/28",
    entries: [
      "tezos.id is open source now!",
    ]
  },
  {
    date: "2019/6/19",
    entries: [
      "refine the newsletter page",
      "minor enhancements"
    ]
  },
  {
    date: "2019/6/12",
    entries: [
      "the search bar of mobile devies is default on",
      "miner bug fixed",
    ]
  },
  {
    date: "2019/6/7",
    entries: [
      "unfiy the style of Tezos.ID",
    ]
  },
  {
    date: "2019/5/28",
    entries: [
      "update the translations of Tezos.ID",
      "replace tz account with baker name",
      "bug fix for language display when switching language"
    ]
  },
  {
    date: "2019/5/17",
    entries: [
      "support for multiple languages",
      "add copy to clipboard feature for the short hashs!",
      "user profile online"
    ]
  },
  {
    date: "2019/5/10",
    entries: [
      "Add page to unsubscribe newsletter",
      "Login / Logout by Google account",
      "Fixed broken ꜩ symbol on mobile",
      "Unify the link of hash",
      "Minor bug fix"
    ]
  },
  {
    date: "2019/5/2",
    entries: [
      "Add '24h Price Change' info on homepage",
      "Auto reload  'LAST 10 TEZOS BLOCKS', 'LAST 10 TEZOS TRANSACTIONS' and 'LATEST BLOCK' every 1 minute",
      "Fine tuning the search bar",
      "Optimise code and several pages "
    ]
  },
  {
    date: "2019/4/26",
    entries: [
      "Redesign the new search bar",
      "'Baker Rights', 'Delegation Services', 'Accounts' and 'Contracts' pages online",
      "Add description for network peer page"
    ]
  },
  {
    date: "2019/4/23",
    entries: [
      "Email newsletter subscription",
      "New, friendly and detailed Tezos account page online",
      "'Nonces' page online"
    ]
  },
  {
    date: "2019/4/19",
    entries: [
      "Standardize URIs",
      "Change icon image for easily identify on browser tab",
      "Support http://tezos.id/{hash} search for account/block/operation hash",
      "Minor changes and bug fix"
    ]
  },
  {
    date: "2019/4/15",
    entries: [
      "Voting page revamp and support testing period",
      "'Proposal' and 'protocol' page online",
      "Robot icon for each account",
      "Minor change and bug fix"
    ]
  },
  {
    date: "2019/4/11",
    entries: ["Minor bugs fixed", "About us page online"]
  },
  {
    date: "2019/4/10",
    entries: [
      "Search bar for hash of blocks/operations/accounts",
      "Various operations pages: Endorsements, Delegations, Originations, Activations",
      "Operation and Block UI refinement",
      "Devlog page online"
    ]
  },
  {
    date: "2019/4/7",
    entries: [
      "(Point Header) User experience improvement",
      "Enhanced the performance of Tezos.ID homepage ~100%",
      "Show an icon when page is loading"
    ]
  }
];
