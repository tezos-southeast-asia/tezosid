import { connectionConfig } from "./connectionConfigs"

const tezosEnv = connectionConfig.tezosEnv
const bakerNameConfigs = {
  mainnet: [
    {
      "rank": 1,
      "baker_name": "Tezos Capital Legacy",
      "delegation_code": "tz1TDSmoZXwVevLTEvKCTHWpomG76oC9S2fJ",
      "fee": "15",
      "baker_efficiency": "98.8718906783",
      "available_capacity": "5061945.8993",
      "accepting_delegation": "true",
      "expected_roi": "6.42"
    },
    {
      "rank": 2,
      "baker_name": "Cryptium Labs",
      "delegation_code": "tz1eEnQhbwf6trb8Q8mPb2RaPkNk2rN7BKi8",
      "fee": "10",
      "baker_efficiency": "100.035555093",
      "available_capacity": "2056319.6865",
      "accepting_delegation": "true",
      "expected_roi": "6.80"
    },
    {
      "rank": 3,
      "baker_name": "Happy Tezos",
      "delegation_code": "tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q",
      "fee": "15",
      "baker_efficiency": "100.095812119",
      "available_capacity": "1774113.6935",
      "accepting_delegation": "true",
      "expected_roi": "6.40"
    },
    {
      "rank": 4,
      "baker_name": "At James",
      "delegation_code": "tz3e75hU4EhDU3ukyJueh5v6UvEHzGwkg3yC",
      "fee": "12",
      "baker_efficiency": "99.8931599117",
      "available_capacity": "82775.4704",
      "accepting_delegation": "true",
      "expected_roi": "6.63"
    },
    {
      "rank": 5,
      "baker_name": "Flippin' tacos",
      "delegation_code": "tz1TzaNn7wSQSP5gYPXCnNzBCpyMiidCq1PX",
      "fee": "15",
      "baker_efficiency": "100.383369239",
      "available_capacity": "5372368.9668",
      "accepting_delegation": "true",
      "expected_roi": "6.41"
    },
    {
      "rank": 6,
      "baker_name": "P2P Validator",
      "delegation_code": "tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb",
      "fee": "9.95",
      "baker_efficiency": "100.492481794",
      "available_capacity": "1031892.87483",
      "accepting_delegation": "true",
      "expected_roi": "6.82"
    },
    {
      "rank": 7,
      "baker_name": "Crypto Delegate",
      "delegation_code": "tz1Tnjaxk6tbAeC2TmMApPh8UsrEVQvhHvx5",
      "fee": "28",
      "baker_efficiency": "99.6201303074",
      "available_capacity": "5325409.8089",
      "accepting_delegation": "true",
      "expected_roi": "5.42"
    },
    {
      "rank": 8,
      "baker_name": "Lucid Mining",
      "delegation_code": "tz1VmiY38m3y95HqQLjMwqnMS7sdMfGomzKi",
      "fee": "10",
      "baker_efficiency": "99.707585153",
      "available_capacity": "1857939.13405",
      "accepting_delegation": "true",
      "expected_roi": "6.71"
    },
    {
      "rank": 9,
      "baker_name": "XTZ Delegate",
      "delegation_code": "tz1Xek93iSXXckyQ6aYLVS5Rr2tge2en7ZxS",
      "fee": "10",
      "baker_efficiency": "95.1341350601",
      "available_capacity": "135213.589336",
      "accepting_delegation": "true",
      "expected_roi": "6.36"
    },
    {
      "rank": 10,
      "baker_name": "TezosBC",
      "delegation_code": "tz1QGZ3dD2YpRKZ4APeso6EXTeyCUUkw6MQC",
      "fee": "30",
      "baker_efficiency": "95.1494396811",
      "available_capacity": "415000.84695",
      "accepting_delegation": "true",
      "expected_roi": "4.49"
    },
    {
      "rank": 11,
      "baker_name": "Bake'n'Rolls",
      "delegation_code": "tz1NortRftucvAkD1J58L32EhSVrQEWJCEnB",
      "fee": "9",
      "baker_efficiency": "99.8329937123",
      "available_capacity": "1136361.14334",
      "accepting_delegation": "true",
      "expected_roi": "6.85"
    },
    {
      "rank": 12,
      "baker_name": "Tezzigator",
      "delegation_code": "tz3adcvQaKXTCg12zbninqo3q8ptKKtDFTLv",
      "fee": "10",
      "baker_efficiency": "101.386481802",
      "available_capacity": "502957.44785",
      "accepting_delegation": "true",
      "expected_roi": "n/a"
    },
    {
      "rank": 13,
      "baker_name": "PayTezos",
      "delegation_code": "tz1Ldzz6k1BHdhuKvAtMRX7h5kJSMHESMHLC",
      "fee": "10",
      "baker_efficiency": "99.4210701863",
      "available_capacity": "3845008.244",
      "accepting_delegation": "true",
      "expected_roi": "6.77"
    },
    {
      "rank": 14,
      "baker_name": "Tezos Tacos",
      "delegation_code": "tz1bkKTY9Y3rTsHbpr2fbGUCRm736LLquQfM",
      "fee": "10",
      "baker_efficiency": "100.56412809",
      "available_capacity": "19800.43547",
      "accepting_delegation": "true",
      "expected_roi": "6.81"
    },
    {
      "rank": 15,
      "baker_name": "Tezos Vote",
      "delegation_code": "tz1bHzftcTKZMTZgLLtnrXydCm6UEqf4ivca",
      "fee": "5",
      "baker_efficiency": "100.255437727",
      "available_capacity": "38938.18091",
      "accepting_delegation": "false",
      "expected_roi": "7.15"
    },
    {
      "rank": 16,
      "baker_name": "Everstake",
      "delegation_code": "tz1MXFrtZoaXckE41bjUCSjAjAap3AFDSr3N",
      "fee": "10",
      "baker_efficiency": "100.106787195",
      "available_capacity": "510056.25619",
      "accepting_delegation": "true",
      "expected_roi": "6.82"
    },
    {
      "rank": 17,
      "baker_name": "Tezos France",
      "delegation_code": "tz1U638Z3xWRiXDDKx2S125MCCaAeGDdA896",
      "fee": "11",
      "baker_efficiency": "97.1690258118",
      "available_capacity": "263470.636531",
      "accepting_delegation": "true",
      "expected_roi": "6.66"
    },
    {
      "rank": 18,
      "baker_name": "LetzBake!",
      "delegation_code": "tz1aqcYgG6NuViML5vdWhohHJBYxcDVLNUsE",
      "fee": "10",
      "baker_efficiency": "98.0582524272",
      "available_capacity": "33574.84558",
      "accepting_delegation": "true",
      "expected_roi": "6.77"
    },
    {
      "rank": 19,
      "baker_name": "Tezos Istanbul",
      "delegation_code": "tz1Vm5cfHncKGBo7YvZfHc4mmudY4qpWzvSB",
      "fee": "5",
      "baker_efficiency": "100.046906119",
      "available_capacity": "-70918.0299",
      "accepting_delegation": "false",
      "expected_roi": "7.18"
    },
    {
      "rank": 20,
      "baker_name": "Tz Bakery",
      "delegation_code": "tz1cX93Q3KsiTADpCC4f12TBvAmS5tw7CW19",
      "fee": "10",
      "baker_efficiency": "100.553288996",
      "available_capacity": "24740.466772",
      "accepting_delegation": "true",
      "expected_roi": "6.78"
    },
    {
      "rank": 21,
      "baker_name": "Tz Dutch",
      "delegation_code": "tz1PesW5khQNhy4revu2ETvMtWPtuVyH2XkZ",
      "fee": "10",
      "baker_efficiency": "100.450707611",
      "available_capacity": "708336.22078",
      "accepting_delegation": "false",
      "expected_roi": "6.80"
    },
    {
      "rank": 22,
      "baker_name": "Tezos Panda",
      "delegation_code": "tz1PeZx7FXy7QRuMREGXGxeipb24RsMMzUNe",
      "fee": "10",
      "baker_efficiency": "100.435057001",
      "available_capacity": "885721.4204",
      "accepting_delegation": "true",
      "expected_roi": "6.79"
    },
    {
      "rank": 23,
      "baker_name": "Coinbase",
      "delegation_code": "tz1irJKkXS2DBWkU1NnmFQx1c1L7pbGg4yhk",
      "fee": "25",
      "baker_efficiency": "100.386100386",
      "available_capacity": "2128566.62905",
      "accepting_delegation": "true",
      "expected_roi": "n/a"
    },
    {
      "rank": 24,
      "baker_name": "Staking Facilities",
      "delegation_code": "tz1WpeqFaBG9Jm73Dmgqamy8eF8NWLz9JCoY",
      "fee": "12.5",
      "baker_efficiency": "100.332469289",
      "available_capacity": "879307.54687",
      "accepting_delegation": "true",
      "expected_roi": "6.57"
    },
    {
      "rank": 25,
      "baker_name": "Shake 'n Bake",
      "delegation_code": "tz1V4qCyvPKZ5UeqdH14HN42rxvNPQfc9UZg",
      "fee": "10",
      "baker_efficiency": "100.29134533",
      "available_capacity": "142692.949129",
      "accepting_delegation": "true",
      "expected_roi": "6.81"
    },
    {
      "rank": 26,
      "baker_name": "TZ Bake",
      "delegation_code": "tz1Zhv3RkfU2pHrmaiDyxp7kFZpZrUCu1CiF",
      "fee": "14",
      "baker_efficiency": "100.272542028",
      "available_capacity": "147838.18404",
      "accepting_delegation": "true",
      "expected_roi": "6.49"
    },
    {
      "rank": 27,
      "baker_name": "EON",
      "delegation_code": "tz1PK8bNrfPbc73BtHH1A63jfU5UtbRFkvU4",
      "fee": "5",
      "baker_efficiency": "100.266666667",
      "available_capacity": "139942.25748",
      "accepting_delegation": "true",
      "expected_roi": "7.13"
    },
    {
      "rank": 28,
      "baker_name": "Gate.io",
      "delegation_code": "tz1NpWrAyDL9k2Lmnyxcgr9xuJakbBxdq7FB",
      "fee": "33",
      "baker_efficiency": "100.263898268",
      "available_capacity": "15582093.1364",
      "accepting_delegation": "true",
      "expected_roi": "5.05"
    },
    {
      "rank": 29,
      "baker_name": "Ø Crypto Pool",
      "delegation_code": "tz1KtvGSYU5hdKD288a1koTBURWYuADJGrLE",
      "fee": "10",
      "baker_efficiency": "100.259126591",
      "available_capacity": "114782.58677",
      "accepting_delegation": "true",
      "expected_roi": "6.79"
    },
    {
      "rank": 30,
      "baker_name": "Tezos Capital",
      "delegation_code": "tz2PdGc7U5tiyqPgTSgqCDct94qd6ovQwP6u",
      "fee": "20",
      "baker_efficiency": "100.23403544",
      "available_capacity": "3385320.447",
      "accepting_delegation": "true",
      "expected_roi": "6.02"
    },
    {
      "rank": 31,
      "baker_name": "TezoSteam",
      "delegation_code": "tz1LLNkQK4UQV6QcFShiXJ2vT2ELw449MzAA",
      "fee": "14",
      "baker_efficiency": "100.200264993",
      "available_capacity": "668762.51131",
      "accepting_delegation": "true",
      "expected_roi": "6.50"
    },
    {
      "rank": 32,
      "baker_name": "Fresh Tezos",
      "delegation_code": "tz1QLXqnfN51dkjeghXvKHkJfhvGiM5gK4tc",
      "fee": "10",
      "baker_efficiency": "100.1917458",
      "available_capacity": "22404.383622",
      "accepting_delegation": "true",
      "expected_roi": "6.71"
    },
    {
      "rank": 33,
      "baker_name": "Tz Node",
      "delegation_code": "tz1Vd1rXpV8hTHbFXCXN3c3qzCsgcU5BZw1e",
      "fee": "5.51",
      "baker_efficiency": "100.184605241",
      "available_capacity": "8660.09203",
      "accepting_delegation": "true",
      "expected_roi": "7.11"
    },
    {
      "rank": 34,
      "baker_name": "Blockpower",
      "delegation_code": "tz1LmaFsWRkjr7QMCx5PtV6xTUz3AmEpKQiF",
      "fee": "10",
      "baker_efficiency": "100.153851299",
      "available_capacity": "1069362.78743",
      "accepting_delegation": "true",
      "expected_roi": "6.76"
    },
    {
      "rank": 35,
      "baker_name": "Airfoil",
      "delegation_code": "tz1gk3TDbU7cJuiBRMhwQXVvgDnjsxuWhcEA",
      "fee": "5",
      "baker_efficiency": "100.104650402",
      "available_capacity": "1251759.36717",
      "accepting_delegation": "true",
      "expected_roi": "7.21"
    },
    {
      "rank": 36,
      "baker_name": "Staked",
      "delegation_code": "tz1RCFbB9GpALpsZtu6J58sb74dm8qe6XBzv",
      "fee": "10",
      "baker_efficiency": "100.096441758",
      "available_capacity": "1937485.19584",
      "accepting_delegation": "true",
      "expected_roi": "6.80"
    },
    {
      "rank": 37,
      "baker_name": "Tez Whale",
      "delegation_code": "tz1isXamBXpTUgbByQ6gXgZQg4GWNW7r6rKE",
      "fee": "8",
      "baker_efficiency": "100.07256354",
      "available_capacity": "3937440.8371",
      "accepting_delegation": "true",
      "expected_roi": "6.94"
    },
    {
      "rank": 38,
      "baker_name": "BAKER-IL",
      "delegation_code": "tz1cYufsxHXJcvANhvS55h3aY32a9BAFB494",
      "fee": "5",
      "baker_efficiency": "100.040907231",
      "available_capacity": "13570.12142",
      "accepting_delegation": "false",
      "expected_roi": "7.12"
    },
    {
      "rank": 39,
      "baker_name": "My Tezos Baking",
      "delegation_code": "tz1d6Fx42mYgVFnHUW8T8A7WBfJ6nD9pVok8",
      "fee": "14",
      "baker_efficiency": "100.024425989",
      "available_capacity": "373741.77476",
      "accepting_delegation": "true",
      "expected_roi": "6.44"
    },
    {
      "rank": 40,
      "baker_name": "Figment",
      "delegation_code": "tz1Scdr2HsZiQjc7bHMeBbmDRXYVvdhjJbBh",
      "fee": "15",
      "baker_efficiency": "100.021358394",
      "available_capacity": "-32830.783371",
      "accepting_delegation": "false",
      "expected_roi": "6.39"
    },
    {
      "rank": 41,
      "baker_name": "Tezos HODL",
      "delegation_code": "tz1WnfXMPaNTBmH7DBPwqCWs9cPDJdkGBTZ8",
      "fee": "8",
      "baker_efficiency": "100.003662601",
      "available_capacity": "-23923.61324",
      "accepting_delegation": "false",
      "expected_roi": "6.92"
    },
    {
      "rank": 42,
      "baker_name": "AirGap",
      "delegation_code": "tz1MJx9vhaNRSimcuXPK2rW4fLccQnDAnVKJ",
      "fee": "10",
      "baker_efficiency": "100",
      "available_capacity": "72228.4554441",
      "accepting_delegation": "true",
      "expected_roi": "n/a"
    },
    {
      "rank": 43,
      "baker_name": "Tezetetic",
      "delegation_code": "tz1VceyYUpq1gk5dtp6jXQRtCtY8hm5DKt72",
      "fee": "5",
      "baker_efficiency": "100",
      "available_capacity": "3706.251144",
      "accepting_delegation": "false",
      "expected_roi": "7.25"
    },
    {
      "rank": 44,
      "baker_name": "Validators.com",
      "delegation_code": "tz1Pwgj6j55akKCyvTwwr9X4np1RskSXpQY4",
      "fee": "20",
      "baker_efficiency": "100",
      "available_capacity": "225200.396324",
      "accepting_delegation": "true",
      "expected_roi": "n/a"
    },
    {
      "rank": 45,
      "baker_name": "Hayek Lab",
      "delegation_code": "tz1SohptP53wDPZhzTWzDUFAUcWF6DMBpaJV",
      "fee": "15",
      "baker_efficiency": "99.9510739273",
      "available_capacity": "760833.80848",
      "accepting_delegation": "true",
      "expected_roi": "6.35"
    },
    {
      "rank": 46,
      "baker_name": "View Nodes",
      "delegation_code": "tz1aiYKXmSRckyJ9EybKmpVry373rfyngJU8",
      "fee": "15",
      "baker_efficiency": "99.9393449252",
      "available_capacity": "166516.819046",
      "accepting_delegation": "true",
      "expected_roi": "6.49"
    },
    {
      "rank": 47,
      "baker_name": "Tez-Baking",
      "delegation_code": "tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc",
      "fee": "15",
      "baker_efficiency": "99.8281901957",
      "available_capacity": "1116408.01661",
      "accepting_delegation": "true",
      "expected_roi": "6.36"
    },
    {
      "rank": 48,
      "baker_name": "XTZ Master",
      "delegation_code": "tz1KfEsrtDaA1sX7vdM4qmEPWuSytuqCDp5j",
      "fee": "12.5",
      "baker_efficiency": "99.8064021726",
      "available_capacity": "2032407.50012",
      "accepting_delegation": "true",
      "expected_roi": "6.60"
    },
    {
      "rank": 49,
      "baker_name": "Tez Baker",
      "delegation_code": "tz1Lhf4J9Qxoe3DZ2nfe8FGDnvVj7oKjnMY6",
      "fee": "14",
      "baker_efficiency": "99.8058581822",
      "available_capacity": "955561.69218",
      "accepting_delegation": "true",
      "expected_roi": "6.48"
    },
    {
      "rank": 50,
      "baker_name": "Tezos Hot Bakery",
      "delegation_code": "tz1bLwpPfr3xqy1gWBF4sGvv8bLJyPHR11kx",
      "fee": "10",
      "baker_efficiency": "99.7692662667",
      "available_capacity": "158647.850766",
      "accepting_delegation": "true",
      "expected_roi": "6.75"
    },
    {
      "rank": 51,
      "baker_name": "XTZ Antipodes",
      "delegation_code": "tz1TcH4Nb3aHNDJ7CGZhU7jgAK1BkSP4Lxds",
      "fee": "8",
      "baker_efficiency": "99.6322026972",
      "available_capacity": "79282.756662",
      "accepting_delegation": "true",
      "expected_roi": "6.92"
    },
    {
      "rank": 52,
      "baker_name": "Tezos Chef",
      "delegation_code": "tz1hThMBD8jQjFt78heuCnKxJnJtQo9Ao25X",
      "fee": "15",
      "baker_efficiency": "99.6272053405",
      "available_capacity": "75519.629473",
      "accepting_delegation": "true",
      "expected_roi": "6.41"
    },
    {
      "rank": 53,
      "baker_name": "Tezos Mars",
      "delegation_code": "tz1S1Aew75hMrPUymqenKfHo8FspppXKpW7h",
      "fee": "5",
      "baker_efficiency": "99.6216646754",
      "available_capacity": "33532.547677",
      "accepting_delegation": "true",
      "expected_roi": "7.14"
    },
    {
      "rank": 54,
      "baker_name": "888 XTZ",
      "delegation_code": "tz1WBfwbT66FC6BTLexc2BoyCCBM9LG7pnVW",
      "fee": "8.88",
      "baker_efficiency": "99.5813862928",
      "available_capacity": "315043.54348",
      "accepting_delegation": "true",
      "expected_roi": "6.84"
    },
    {
      "rank": 55,
      "baker_name": "Illuminarean",
      "delegation_code": "tz1Ryat7ZuRapjGUgsPym9DuMGrTYYyDBJoq",
      "fee": "15",
      "baker_efficiency": "99.4930414285",
      "available_capacity": "307758.04741",
      "accepting_delegation": "true",
      "expected_roi": "6.40"
    },
    {
      "rank": 56,
      "baker_name": "Stack Tezos",
      "delegation_code": "tz1MDKr36woXfVtrtXfV1ppPJERxPcm2wU6V",
      "fee": "10",
      "baker_efficiency": "99.4330738926",
      "available_capacity": "56739.92689",
      "accepting_delegation": "true",
      "expected_roi": "6.69"
    },
    {
      "rank": 57,
      "baker_name": "Tezry",
      "delegation_code": "tz1UJvHTgpVzcKWhTazGxVcn5wsHru5Gietg",
      "fee": "8",
      "baker_efficiency": "99.3551306709",
      "available_capacity": "481128.92649",
      "accepting_delegation": "true",
      "expected_roi": "6.85"
    },
    {
      "rank": 58,
      "baker_name": "TezosSEAsia",
      "delegation_code": "tz1R6Ej25VSerE3MkSoEEeBjKHCDTFbpKuSX",
      "fee": "7",
      "baker_efficiency": "99.3033702467",
      "available_capacity": "149494.67212",
      "accepting_delegation": "true",
      "expected_roi": "7.02"
    },
    {
      "rank": 59,
      "baker_name": "POS Bakerz",
      "delegation_code": "tz1Vyuu4EJ5Nym4JcrfRLnp3hpaq1DSEp1Ke",
      "fee": "2",
      "baker_efficiency": "99.1886409736",
      "available_capacity": "-26842.728148",
      "accepting_delegation": "false",
      "expected_roi": "7.37"
    },
    {
      "rank": 60,
      "baker_name": "TezTech Labs",
      "delegation_code": "tz1Vtimi84kLh9RANfRVX2JvYtP4NPCT1aFm",
      "fee": "15",
      "baker_efficiency": "99.1113434396",
      "available_capacity": "10179354.6617",
      "accepting_delegation": "true",
      "expected_roi": "6.39"
    },
    {
      "rank": 61,
      "baker_name": "Tezos Suisse",
      "delegation_code": "tz1hAYfexyzPGG6RhZZMpDvAHifubsbb6kgn",
      "fee": "7.5",
      "baker_efficiency": "98.9739961083",
      "available_capacity": "13420.693193",
      "accepting_delegation": "true",
      "expected_roi": "6.95"
    },
    {
      "rank": 62,
      "baker_name": "Tz Bank",
      "delegation_code": "tz1bkg7rynMXVcjomoe3diB4URfv8GU2GAcw",
      "fee": "10",
      "baker_efficiency": "98.9608666814",
      "available_capacity": "177225.761808",
      "accepting_delegation": "true",
      "expected_roi": "6.84"
    },
    {
      "rank": 63,
      "baker_name": "Stir Delegate",
      "delegation_code": "tz1LH4L6XYT2JNPhvWYC4Zq3XEiGgEwzNRvo",
      "fee": "14",
      "baker_efficiency": "98.9454937433",
      "available_capacity": "858053.54787",
      "accepting_delegation": "true",
      "expected_roi": "6.48"
    },
    {
      "rank": 64,
      "baker_name": "Stake House",
      "delegation_code": "tz1V2FYediKBAEaTpXXJBSjuQpjkyCzrTSiE",
      "fee": "7",
      "baker_efficiency": "98.9438460734",
      "available_capacity": "161157.902632",
      "accepting_delegation": "true",
      "expected_roi": "6.95"
    },
    {
      "rank": 65,
      "baker_name": "Bakemon",
      "delegation_code": "tz1Z1WwoqgRFbLE3YNdYRpCx44NSfiMJzeAG",
      "fee": "15",
      "baker_efficiency": "98.8606380427",
      "available_capacity": "545492.186667",
      "accepting_delegation": "true",
      "expected_roi": "6.39"
    },
    {
      "rank": 66,
      "baker_name": "Tz Envoy",
      "delegation_code": "tz1iJ4qgGTzyhaYEzd1RnC6duEkLBd1nzexh",
      "fee": "10",
      "baker_efficiency": "98.8553675088",
      "available_capacity": "47671.45519",
      "accepting_delegation": "true",
      "expected_roi": "6.80"
    },
    {
      "rank": 67,
      "baker_name": "Tezocracy",
      "delegation_code": "tz1TaLYBeGZD3yKVHQGBM857CcNnFFNceLYh",
      "fee": "10",
      "baker_efficiency": "98.8408730689",
      "available_capacity": "2783854.47265",
      "accepting_delegation": "true",
      "expected_roi": "6.61"
    },
    {
      "rank": 68,
      "baker_name": "Tez Patisserie",
      "delegation_code": "tz1UUgPwikRHW1mEyVZfGYy6QaxrY6Y7WaG5",
      "fee": "10",
      "baker_efficiency": "98.7678996323",
      "available_capacity": "3355821.85846",
      "accepting_delegation": "true",
      "expected_roi": "6.74"
    },
    {
      "rank": 69,
      "baker_name": "You Loaf We Bake",
      "delegation_code": "tz1eZwq8b5cvE2bPKokatLkVMzkxz24z3Don",
      "fee": "15",
      "baker_efficiency": "98.6612082319",
      "available_capacity": "88944.756726",
      "accepting_delegation": "true",
      "expected_roi": "6.33"
    },
    {
      "rank": 70,
      "baker_name": "Tezos Kitchen",
      "delegation_code": "tz1Nn14BBsDULrPXtkM9UQeXaE4iqJhmqmK5",
      "fee": "8",
      "baker_efficiency": "98.6078338839",
      "available_capacity": "6877.619145",
      "accepting_delegation": "true",
      "expected_roi": "6.98"
    },
    {
      "rank": 71,
      "baker_name": "Tezzigator Legacy",
      "delegation_code": "tz1iZEKy4LaAjnTmn2RuGDf2iqdAQKnRi8kY",
      "fee": "25",
      "baker_efficiency": "98.5905211886",
      "available_capacity": "2517406.74939",
      "accepting_delegation": "false",
      "expected_roi": "5.65"
    },
    {
      "rank": 72,
      "baker_name": "Stakery",
      "delegation_code": "tz1go7f6mEQfT2xX2LuHAqgnRGN6c2zHPf5c",
      "fee": "11",
      "baker_efficiency": "98.5497237569",
      "available_capacity": "164153.105947",
      "accepting_delegation": "true",
      "expected_roi": "6.49"
    },
    {
      "rank": 73,
      "baker_name": "Tez Milk",
      "delegation_code": "tz1bakeKFwqmtLBzghw8CFnqFvRxLj849Vfg",
      "fee": "6.5",
      "baker_efficiency": "98.4633853541",
      "available_capacity": "26563.672471",
      "accepting_delegation": "true",
      "expected_roi": "7.03"
    },
    {
      "rank": 74,
      "baker_name": "Bake ꜩ For Me",
      "delegation_code": "tz1NRGxXV9h6SdNaZLcgmjuLx3hyy2f8YoGN",
      "fee": "12.5",
      "baker_efficiency": "98.4588441331",
      "available_capacity": "20991.620619",
      "accepting_delegation": "true",
      "expected_roi": "6.53"
    },
    {
      "rank": 75,
      "baker_name": "Bake Tz",
      "delegation_code": "tz1ei4WtWEMEJekSv8qDnu9PExG6Q8HgRGr3",
      "fee": "3.9",
      "baker_efficiency": "98.4391704084",
      "available_capacity": "-988.364926",
      "accepting_delegation": "false",
      "expected_roi": "7.22"
    },
    {
      "rank": 76,
      "baker_name": "XTZ Land",
      "delegation_code": "tz1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj",
      "fee": "4.5",
      "baker_efficiency": "97.8541652429",
      "available_capacity": "13729.014209",
      "accepting_delegation": "true",
      "expected_roi": "7.15"
    },
    {
      "rank": 77,
      "baker_name": "Tezos Delegate EU",
      "delegation_code": "tz1YTyvABUyhE7JHpxMVBVqjZnZM4ofMrWKE",
      "fee": "10",
      "baker_efficiency": "97.6767247485",
      "available_capacity": "81255.606615",
      "accepting_delegation": "true",
      "expected_roi": "6.66"
    },
    {
      "rank": 78,
      "baker_name": "Ceibo XTZ",
      "delegation_code": "tz3bEQoFCZEEfZMskefZ8q8e4eiHH1pssRax",
      "fee": "5",
      "baker_efficiency": "97.0383275261",
      "available_capacity": "13859.388436",
      "accepting_delegation": "false",
      "expected_roi": "7.12"
    },
    {
      "rank": 79,
      "baker_name": "XTZ Bakery",
      "delegation_code": "tz1P9GwUWeAyPoT3ZwBc6gWCTP9exEPaVU3w",
      "fee": "7",
      "baker_efficiency": "96.724667349",
      "available_capacity": "223731.560862",
      "accepting_delegation": "true",
      "expected_roi": "6.97"
    },
    {
      "rank": 80,
      "baker_name": "TezosBr",
      "delegation_code": "tz1YKh8T79LAtWxX29N5VedCSmaZGw9LNVxQ",
      "fee": "9",
      "baker_efficiency": "96.6985386975",
      "available_capacity": "37924.874555",
      "accepting_delegation": "true",
      "expected_roi": "6.67"
    },
    {
      "rank": 81,
      "baker_name": "TEZ Rocket",
      "delegation_code": "tz1abTjX2tjtMdaq5VCzkDtBnMSCFPW2oRPa",
      "fee": "20",
      "baker_efficiency": "96.5162200282",
      "available_capacity": "477535.5164",
      "accepting_delegation": "true",
      "expected_roi": "6.03"
    },
    {
      "rank": 82,
      "baker_name": "Infinity Stones",
      "delegation_code": "tz1awXW7wuXy21c66vBudMXQVAPgRnqqwgTH",
      "fee": "10",
      "baker_efficiency": "96.494948235",
      "available_capacity": "241771.337372",
      "accepting_delegation": "true",
      "expected_roi": "6.70"
    },
    {
      "rank": 83,
      "baker_name": "Tezos Ninja",
      "delegation_code": "tz1djEKk6j1FqigTgbRsunbnY9BB7qsn1aAQ",
      "fee": "10",
      "baker_efficiency": "96.4526427811",
      "available_capacity": "89472.648198",
      "accepting_delegation": "true",
      "expected_roi": "6.60"
    },
    {
      "rank": 84,
      "baker_name": "Tezos Bakery",
      "delegation_code": "tz1fZ767VDbqx4DeKiFswPSHh513f51mKEUZ",
      "fee": "10",
      "baker_efficiency": "96.1313012896",
      "available_capacity": "52292.725142",
      "accepting_delegation": "true",
      "expected_roi": "6.77"
    },
    {
      "rank": 85,
      "baker_name": "Norn Delegate",
      "delegation_code": "tz1YdCPrYbksK7HCoYKDyzgfXwY16Fy9rrGa",
      "fee": "5",
      "baker_efficiency": "95.9299693113",
      "available_capacity": "18380.641581",
      "accepting_delegation": "true",
      "expected_roi": "7.16"
    },
    {
      "rank": 86,
      "baker_name": "Tezos Cat",
      "delegation_code": "tz1XXayQohB8XRXN7kMoHbf2NFwNiH3oMRQQ",
      "fee": "5",
      "baker_efficiency": "95.6926658906",
      "available_capacity": "27986.184183",
      "accepting_delegation": "true",
      "expected_roi": "7.12"
    },
    {
      "rank": 87,
      "baker_name": "Tez Mania",
      "delegation_code": "tz1MQJPGNMijnXnVoBENFz9rUhaPt3S7rWoz",
      "fee": "4.5",
      "baker_efficiency": "95.3191489362",
      "available_capacity": "80762.222048",
      "accepting_delegation": "true",
      "expected_roi": "6.46"
    },
    {
      "rank": 88,
      "baker_name": "Hot Stake",
      "delegation_code": "tz1Z3KCf8CLGAYfvVWPEr562jDDyWkwNF7sT",
      "fee": "10",
      "baker_efficiency": "95.0987564009",
      "available_capacity": "21315.170117",
      "accepting_delegation": "true",
      "expected_roi": "5.92"
    },
    {
      "rank": 89,
      "baker_name": "Baking Tacos",
      "delegation_code": "tz1RV1MBbZMR68tacosb7Mwj6LkbPSUS1er1",
      "fee": "8",
      "baker_efficiency": "94.9511400651",
      "available_capacity": "68801.16593",
      "accepting_delegation": "true",
      "expected_roi": "6.91"
    },
    {
      "rank": 90,
      "baker_name": "Elite Tezos",
      "delegation_code": "tz1W1f1JrE7VsqgpUpj1iiDobqP5TixgZhDk",
      "fee": "8",
      "baker_efficiency": "94.7368421053",
      "available_capacity": "67629.979747",
      "accepting_delegation": "true",
      "expected_roi": "n/a"
    },
    {
      "rank": 91,
      "baker_name": "TzBakeOven",
      "delegation_code": "tz1Yh1nLXfwqVpP8btykuRguu61n3veVmADa",
      "fee": "8",
      "baker_efficiency": "94.6479953961",
      "available_capacity": "13284.204085",
      "accepting_delegation": "true",
      "expected_roi": "6.17"
    },
    {
      "rank": 92,
      "baker_name": "Tezos Moon",
      "delegation_code": "tz1TgvirXEwVJPg1cCrxT9zFubdzw7Ng6Ke9",
      "fee": "10",
      "baker_efficiency": "94.6356515547",
      "available_capacity": "462122.575696",
      "accepting_delegation": "true",
      "expected_roi": "6.39"
    },
    {
      "rank": 93,
      "baker_name": "Wetez Wallet",
      "delegation_code": "tz1iMAHAVpkCVegF9FLGWUpQQeiAHh4ffdLQ",
      "fee": "10",
      "baker_efficiency": "94.3741690908",
      "available_capacity": "1239897.92759",
      "accepting_delegation": "true",
      "expected_roi": "6.77"
    },
    {
      "rank": 94,
      "baker_name": "Tezzieland",
      "delegation_code": "tz1fUjvVhJrHLZCbhPNvDRckxApqbkievJHN",
      "fee": "12.5",
      "baker_efficiency": "93.9909297052",
      "available_capacity": "40541.823598",
      "accepting_delegation": "true",
      "expected_roi": "6.31"
    },
    {
      "rank": 95,
      "baker_name": "First Block",
      "delegation_code": "tz1ZQppA6UerMz5CJtGvZmmB6z8L9syq7ixu",
      "fee": "10",
      "baker_efficiency": "93.9451654338",
      "available_capacity": "-3714.426823",
      "accepting_delegation": "false",
      "expected_roi": "6.81"
    },
    {
      "rank": 96,
      "baker_name": "Hyperblocks",
      "delegation_code": "tz1VHFxUuBhwopxC9YC9gm5s2MHBHLyCtvN1",
      "fee": "15",
      "baker_efficiency": "92.1667511105",
      "available_capacity": "1782724.63719",
      "accepting_delegation": "true",
      "expected_roi": "6.41"
    },
    {
      "rank": 97,
      "baker_name": "Tezz City",
      "delegation_code": "tz1Zcxkfa5jKrRbBThG765GP29bUCU3C4ok5",
      "fee": "10",
      "baker_efficiency": "91.3764510779",
      "available_capacity": "165517.383733",
      "accepting_delegation": "true",
      "expected_roi": "6.67"
    },
    {
      "rank": 98,
      "baker_name": "BakeMyStake",
      "delegation_code": "tz1NkFRjmkqqcGkAhqe78fdgemDNKXvL7Bod",
      "fee": "8",
      "baker_efficiency": "90.5660377358",
      "available_capacity": "108916.288433",
      "accepting_delegation": "true",
      "expected_roi": "5.67"
    },
    {
      "rank": 99,
      "baker_name": "XTZ Baker",
      "delegation_code": "tz1ZKi4VrDMEQpypn2NTn9pPbZL3xLRkETLA",
      "fee": "5",
      "baker_efficiency": "90.4054825814",
      "available_capacity": "106032.521966",
      "accepting_delegation": "true",
      "expected_roi": "4.24"
    },
    {
      "rank": 100,
      "baker_name": "Tezos Tokyo",
      "delegation_code": "tz1VYQpZvjVhv1CdcENuCNWJQXu1TWBJ8KTD",
      "fee": "20",
      "baker_efficiency": "89.3183758078",
      "available_capacity": "79958.533114",
      "accepting_delegation": "true",
      "expected_roi": "5.72"
    },
    {
      "rank": 101,
      "baker_name": "TezosRUs",
      "delegation_code": "tz1b9MYGrbN1NAxphLEsPPNT9JC7aNFc5nA4",
      "fee": "10",
      "baker_efficiency": "88.3333333333",
      "available_capacity": "188733.127414",
      "accepting_delegation": "true",
      "expected_roi": "6.21"
    },
    {
      "rank": 102,
      "baker_name": "ownBLOCK",
      "delegation_code": "tz1X1fpAZtwQk94QXUgZwfgsvkQgyc2KHp9d",
      "fee": "10",
      "baker_efficiency": "88.3013879709",
      "available_capacity": "1233061.74287",
      "accepting_delegation": "true",
      "expected_roi": "6.59"
    },
    {
      "rank": 103,
      "baker_name": "Zednode",
      "delegation_code": "tz1L3vFD8mFzBaS8yLHFsd7qDJY1t276Dh8i",
      "fee": "10",
      "baker_efficiency": "86.5215287857",
      "available_capacity": "-15798.901379",
      "accepting_delegation": "false",
      "expected_roi": "6.45"
    },
    {
      "rank": 104,
      "baker_name": "STB",
      "delegation_code": "tz1e2meErj7eEfXwqr7bDK6N1YatmLaugfMp",
      "fee": "5",
      "baker_efficiency": "85.5072463768",
      "available_capacity": "397382.11332",
      "accepting_delegation": "true",
      "expected_roi": "7.14"
    },
    {
      "rank": 105,
      "baker_name": "01No.de",
      "delegation_code": "tz1W1qbXXJrtLWwaaoXhe8LYGZqmNnpKaH9q",
      "fee": "10",
      "baker_efficiency": "82.8703703704",
      "available_capacity": "87911.9791098",
      "accepting_delegation": "true",
      "expected_roi": "4.06"
    },
    {
      "rank": 106,
      "baker_name": "Baked Tezos",
      "delegation_code": "tz1aRhFErGMgL57DYHMT1vYwv7PzsJN1chrk",
      "fee": "13",
      "baker_efficiency": "79.1213389121",
      "available_capacity": "172822.323127",
      "accepting_delegation": "true",
      "expected_roi": "6.94"
    },
    {
      "rank": 107,
      "baker_name": "AirBie",
      "delegation_code": "tz1iLbZZ9uoRuVJCrZ9ZwiJMpfzhy3c67mav",
      "fee": "5",
      "baker_efficiency": "75.1715618995",
      "available_capacity": "52663.177401",
      "accepting_delegation": "true",
      "expected_roi": "7.28"
    },
    {
      "rank": 108,
      "baker_name": "Tezos Japan",
      "delegation_code": "tz1b7YSEeNRqgmjuX4d4aiai2sQTF4A7WBZf",
      "fee": "15",
      "baker_efficiency": "0",
      "available_capacity": "76537.9887205",
      "accepting_delegation": "true",
      "expected_roi": "n/a"
    },
    {
      "rank": 109,
      "baker_name": "Tezos Korea",
      "delegation_code": "tz1PPUo28B8BroqmVCMMNDudG4ShA2bzicrU",
      "fee": "12",
      "baker_efficiency": "0",
      "available_capacity": "943467.97153",
      "accepting_delegation": "true",
      "expected_roi": "6.62"
    },
    {
      "rank": 110,
      "baker_name": "Tezos Rio",
      "delegation_code": "tz1cZfFQpcYhwDp7y1njZXDsZqCrn2NqmVof",
      "fee": "4.99",
      "baker_efficiency": "0",
      "available_capacity": "41041.43372",
      "accepting_delegation": "true",
      "expected_roi": "n/a"
    },
    {
      "rank": 111,
      "baker_name": "Hodl.farm",
      "delegation_code": "tz1gg5bjopPcr9agjamyu9BbXKLibNc2rbAq",
      "fee": "5",
      "baker_efficiency": "0",
      "available_capacity": "60930",
      "accepting_delegation": "true",
      "expected_roi": "n/a"
    }
  ],
  // TODO: need to update for babylonnet
  babylonnet: [
  ],
  // TODO: need to update for carthagenet
  carthagenet:[

  ]
}

export const bakerNameConfig = bakerNameConfigs[tezosEnv] || []
  