export const roadmapList = [
  {
    title: "Speak your language",
    content:
      " Don't speak English? Multilanguage support comes to Tezos.ID where we are planing to support additional languages such as Traditional Chinese, Simplified Chinese, Japanese, Korean and more, thanks to the contribution and hard work from our colleagues around the various community organizations. Have a language you need? Please submit a bug report and tell us more.",
    ETA: "Done. check the footer to select your language!"
  },
  {
    title: "Your personal Tezos.ID",
    content:
      "Worried about tracking multiple accounts and missing important activity on one? No worries, from now you track account activity from your own dashboard.",
    ETA: "ETA 28th Jun."
  },
  {
    title: "Big brother is watching!",
    content:
      "Ability to receive notifications for commonly tasked activities such as account activity, balance changes, initial baker activity, transactions, etc..",
    ETA: "ETA 14th Jul."
  },
  {
    title: "Know your (and others') contracts",
    content:
      "Explore further in depth about the status, code or more of the contracts you are interested in.",
    ETA: "ETA 28th Jul."
  },
  {
    title: "Find more valuable intel for you",
    content:
      "Provide within the user dashboard a series of customized reports, statistical analysis and, even better, data trends",
    ETA: "ETA 12th Aug."
  },
  {
    title: "Build your own application",
    content:
      "Release of our underlying API for you to build tomorrow's applications upon",
    ETA: "ETA 26th Aug."
  }
];
