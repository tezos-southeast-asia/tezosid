import { connectionConfig } from './connectionConfigs'

const { tezosIdWeb } = connectionConfig
const logoUrl = `${tezosIdWeb}images/TezosID_logo.png`
const name = "Tezos.ID"

export const websiteLdJson = {
  "@context": "https://schema.org",
  "@type": "WebSite",
  name,
  url: tezosIdWeb,
  image: logoUrl,
  potentialAction: {
    "@type": "SearchAction",
    target: `${tezosIdWeb}search?hash={search_hash}&fr=gs`,
    "query-input": "required name=search_hash"
  }
}

export const domain = tezosIdWeb

export const common = {
  title: "Tezos.ID - Tezos Blockchain & Identity Explorer",
  description: "Tezos Blockchain Explorer, Wallet and Transaction Monitoring tool" ,
  logoUrl,
  author: name
}
