import { connectionConfig, isTestNet } from "./connectionConfigs";
import { isEnableFirebaseAuth } from "./authConfigs";
import { getMenuItemKeys } from "../../helpers/utility";

const menu = [
  {
    title: "topbar.blockDetail",
    icon: "ion-link",
    items: [
      {
        type: "item",
        title: "topbar.blocks",
        link: "/blocks",
        icon: "ion-cube"
      },
      {
        type: "item",
        title: "topbar.bakerRights",
        link: "/baker-rights",
        icon: "ion-thumbsup"
      },
      // {
      //   title: "topbar.heads",
      //   link: "/heads",
      //   icon: "ion-cube"
      // },
      {
        title: "topbar.snapshot",
        link: "/snapshots",
        icon: "ion-camera"
      },
      {
        type: "divider"
      },
      {
        type: "item",
        title: "topbar.transactions",
        link: "/transactions",
        icon: "ion-arrow-swap"
      },
      {
        type: "item",
        title: "topbar.endorsements",
        link: "/endorsements",
        icon: "ion-checkmark-circled"
      },
      {
        type: "item",
        title: "topbar.delegations",
        link: "/delegations",
        icon: "ion-android-contacts"
      },
      {
        type: "item",
        title: "topbar.originations",
        link: "/originations",
        icon: "ion-home"
      },
      {
        type: "item",
        title: "topbar.doubleBakings",
        link: "/double-baking",
        icon: "ion-thumbsdown"
      },
      {
        type: "item",
        title: "topbar.doubleEndorsements",
        link: "/double-endorsement",
        icon: "ion-thumbsdown"
      },
      {
        type: "item",
        title: "topbar.injectOperation",
        link: "/inject-operation",
        icon: "ion-cloud"
      },
      {
        type: "item",
        title: "topbar.activations",
        link: "/activations",
        icon: "ion-play"
      },
      {
        type: "item",
        title: "topbar.nonces",
        link: "/nonces",
        icon: "ion-ios-barcode"
      }
    ]
  },
  {
    title: "topbar.allAccounts",
    icon: "ion-ios-list",
    items: [
      {
        type: "item",
        title: "topbar.allAccount",
        link: "/accounts",
        icon: "ion-ios-people"
      },
      {
        type: "item",
        title: "topbar.allContracts",
        link: "/contracts",
        icon: "ion-ios-copy"
      }
    ]
  },
  {
    title: "topbar.network",
    icon: "ion-ios-world-outline",
    items: [
      {
        type: "item",
        title: "topbar.peers",
        link: "/peers",
        icon: "ion-ios-people"
      }
    ]
  },
  {
    title: "topbar.charts",
    icon: "ion-stats-bars",
    items: [
      {
        type: "item",
        title: "topbar.blockschart",
        link: "/blocks-chart",
        icon: "ion-cube"
      },
      {
        type: "item",
        title: "topbar.blocksrewardchart",
        link: "/blocks-reward-chart",
        icon: "ion-cube"
      },
      {
        type: "item",
        title: "topbar.bakeTimeChart",
        link: "/baketime-chart",
        icon: "ion-ios-alarm"
      },
      {
        type: "divider"
      },
      {
        type: "item",
        title: "topbar.transchart",
        link: "/transactions-chart",
        icon: "ion-arrow-swap"
      },
      {
        type: "item",
        title: "topbar.transOpPerBlock",
        link: "/transaction-op-chart",
        icon: "ion-arrow-swap"
      },
      {
        type: "item",
        title: "topbar.transfeechart",
        link: "/transaction-fee-chart",
        icon: "ion-cash"
      },
      {
        type: "item",
        title: "topbar.operationschart",
        link: "/operations-chart",
        icon: "ion-checkmark-circled"
      },
      {
        type: "item",
        title: "topbar.endorsechart",
        link: "/endorsements-chart",
        icon: "ion-thumbsup"
      },
      {
        type: "item",
        title: "topbar.activationchart",
        link: "/activations-chart",
        icon: "ion-ios-body"
      },
      {
        type: "divider"
      },
      {
        type: "item",
        title: "topbar.topBakerChart",
        link: "/topBaker-chart",
        icon: "ion-trophy"
      },
      {
        type: "item",
        title: "topbar.prioritiesChart",
        link: "/priorities-chart",
        icon: "ion-ios-star"
      }
    ],
    isHidden: isTestNet,
  },
  {
    title: "topbar.resources",
    icon: "ion-ios-box",
    items: [
      {
        type: "item",
        title: "topbar.delegationServices",
        link: "/delegation-services",
        icon: "ion-android-people"
      },
      /**
      {
        type: "item",
        title: "topbar.terminologies",
        link: "/terminologies",
        icon: "ion-information-circled"
      },
      **/
      {
        type: "item",
        title: "topbar.roadmap",
        link: "/roadmap",
        icon: "ion-android-time"
      },
      {
        type: "item",
        title: "topbar.devlog",
        link: "/devlog",
        icon: "ion-ios-book"
      },
      {
        type: "item",
        title: "topbar.aboutUs",
        link: "/aboutus",
        icon: "ion-android-people"
      }
    ]
  },
  {
    title: "topbar.protocol",
    icon: "ion-android-document",
    items: [
      {
        type: "item",
        title: "topbar.votingPeriods",
        link: "/voting-periods",
        icon: "ion-android-contact"
      },
      {
        type: "item",
        title: "topbar.proposalList",
        link: "/proposals",
        icon: "ion-android-folder"
      },
      {
        type: "item",
        title: "topbar.protocolList",
        link: "/protocols",
        icon: "ion-android-document"
      },
      {
        type: "item",
        title: "topbar.voterList",
        link: "/voters",
        icon: "ion-clipboard"
      }
    ]
  },
  {
    title: "topbar.login",
    link: "/signin",
    icon: "ion-person",
    isLogoutRequired: true,
    isHidden: !isEnableFirebaseAuth
  },
  {
    title: "topbar.profile",
    link: "/myaccount",
    icon: "ion-person",
    isLoginRequired: true,
    isHidden: !isEnableFirebaseAuth
  },
  {
    title: "topbar.api",
    icon: "ion-arrow-swap",
    items: [
      {
        type: "item",
        title: "topbar.apidoc",
        link: "/dev-apis",
        icon: "ion-android-document"
      },
      {
        type: "item",
        title: "topbar.apimonitor",
        link: "/api-monitoring",
        icon: "ion-stats-bars"
      }
    ]
  },
  {
    title: "topbar.reportIssue",
    href: connectionConfig.issueReportForm,
    target: "_blank",
    icon: "ion-clipboard"
  }
];

const menuKeys = getMenuItemKeys({ menu });

export { menu, menuKeys };
