const tezosEnv = process.env.REACT_APP_TEZOS_ENV || "mainnet";

const sourceConfigs = {
  mainnet: {
    tezosIdWeb: "https://tezos.id/",
    tezosNodeSource: "https://api.tezos.id/mainnet/",
    tezosIdApiSource: "https://api.tezos.id/mooncake/mainnet/",
    coinMarketCapSource: "https://node1.lax.tezos.org.sg:8084/"
  },
  babylonnet: {
    tezosIdWeb: "https://babylonnet.tezos.id/",
    tezosNodeSource: "https://api.tezos.id/babylonnet/",
    tezosIdApiSource: "https://api.tezos.id/mooncake/babylonnet/",
    coinMarketCapSource: "https://node1.lax.tezos.org.sg:8084/"
  },
  carthagenet: {
    tezosIdWeb: "https://carthagenet.tezos.id/",
    tezosNodeSource: "https://carthagenet.tezos.id:8083/",
    tezosIdApiSource: "https://carthagenet.tezos.id:9998/",
    coinMarketCapSource: "https://carthagenet.tezos.id:8084/"
  }
};

const tezosNets = Object.keys(sourceConfigs).map(net => ({
  net,
  domain: sourceConfigs[net].tezosIdWeb
}));

const {
  tezosNodeSource,
  tezosIdApiSource,
  coinMarketCapSource,
  tezosIdWeb
} = sourceConfigs[tezosEnv];

const isTestNet = tezosEnv !== 'mainnet'

exports.isTestNet = isTestNet;
exports.connectionConfig = {
  tezosNets,
  tezosEnv,
  tezosIdWeb,
  voting: tezosNodeSource + "chains/main/blocks/head/votes/ballots",
  votingPeriod:
    tezosNodeSource + "chains/main/blocks/head/votes/current_period_kind",
  currentProposal:
    tezosNodeSource + "chains/main/blocks/head/votes/current_proposal",
  tezPrice:
    coinMarketCapSource +
    "v1/cryptocurrency/listings/latest?start=1&limit=100&convert=USD",
  blocks: tezosIdApiSource + "v1/blocks/",
  blocksNum: tezosIdApiSource + "v1/blocks_num",
  transactions: tezosIdApiSource + "v1/transactions",
  transactionsNum: tezosIdApiSource + "v1/transactions_num",
  endorsements: tezosIdApiSource + "v1/endorsements",
  endorsementsNum: tezosIdApiSource + "v1/endorsements_num",
  delegations: tezosIdApiSource + "v1/delegations",
  delegationsNum: tezosIdApiSource + "v1/delegations_num",
  originations: tezosIdApiSource + "v1/originations",
  originationsNum: tezosIdApiSource + "v1/originations_num",
  activations: tezosIdApiSource + "v1/activate_accounts",
  activationsNum: tezosIdApiSource + "v1/activate_accounts_num",
  networkpeers: tezosIdApiSource + "v0/peers",
  operation: tezosIdApiSource + "v1/operations/",
  operations: tezosIdApiSource + "v0/operations?n=50",
  nonces: tezosIdApiSource + "v1/seed_nonce_revelations",
  noncesNum: tezosIdApiSource + "v1/seed_nonce_revelations_num",
  operationTemp: tezosIdApiSource + "v0/operations?n=1",
  issueReportForm: "https://forms.gle/sKwi1R5QDBXzK975A",
  tempApiLink:
    "https://gitlab.com/tezos-southeast-asia/tezos.id-doc/blob/dev/Api.md",
  blockOperation: {
    transactions: tezosIdApiSource + "v1/transactions?block=",
    delegations: tezosIdApiSource + "v1/delegations?block=",
    originations: tezosIdApiSource + "v1/originations?block=",
    endorsements: tezosIdApiSource + "v1/endorsements?block=",
    proposals: tezosIdApiSource + "v1/proposals?block=",
    ballots: tezosIdApiSource + "v1/ballots?block=",
    activations: tezosIdApiSource + "v1/activate_accounts?block=",
    reveals: tezosIdApiSource + "v1/reveals?block=",
    nonces: tezosIdApiSource + "v1/seed_nonce_revelations?block=",
    doubleBakings: tezosIdApiSource + "v1/double_baking_evidences?block=",
    doubleEndorsements:
      tezosIdApiSource + "v1/double_endorsement_evidences?block="
  },
  blockOperationNum: {
    transactions: tezosIdApiSource + "v1/transactions_num?block=",
    delegations: tezosIdApiSource + "v1/delegations_num?block=",
    originations: tezosIdApiSource + "v1/originations_num?block=",
    endorsements: tezosIdApiSource + "v1/endorsements_num?block=",
    proposals: tezosIdApiSource + "v1/proposals_num?block=",
    ballots: tezosIdApiSource + "v1/ballots_num?block=",
    activations: tezosIdApiSource + "v1/activate_accounts_num?block=",
    reveals: tezosIdApiSource + "v1/reveals_num?block=",
    nonces: tezosIdApiSource + "v1/seed_nonce_revelations_num?block=",
    doubleBakings: tezosIdApiSource + "v1/double_baking_evidences_num?block=",
    doubleEndorsements:
      tezosIdApiSource + "v1/double_endorsement_evidences_num?block="
  },
  protocolStartEnd: tezosIdApiSource + "v1/protocals",
  protocolNum: tezosIdApiSource + "v1/protocals_num",
  voterListing: tezosNodeSource + "chains/main/blocks/head/votes/listings",
  votersBallot: tezosNodeSource + "chains/main/blocks/head/votes/ballot_list",
  votingQuorum:
    tezosNodeSource + "chains/main/blocks/head/votes/current_quorum",
  accountDelegates:
    tezosNodeSource + "chains/main/blocks/head/context/delegates/",
  accountDetails_TZ:
    tezosNodeSource + "chains/main/blocks/head/context/delegates/",
  accountDetails_KT:
    tezosNodeSource + "chains/main/blocks/head/context/contracts/",
  accountNum: {
    transactions: tezosIdApiSource + "v1/transactions_num?account=",
    delegations: tezosIdApiSource + "v1/delegations_num?account=",
    originations: tezosIdApiSource + "v1/originations_num?account=",
    endorsements: tezosIdApiSource + "v1/endorsements_num?account=",
    activations: tezosIdApiSource + "v1/activate_accounts_num?account=",
    reveals: tezosIdApiSource + "v1/reveals_num?account=",
    proposals: tezosIdApiSource + "v1/proposals_num?account=",
    ballots: tezosIdApiSource + "v1/ballots_num?account="
  },
  accountActions: {
    transactions: tezosIdApiSource + "v1/transactions?account=",
    delegations: tezosIdApiSource + "v1/delegations?account=",
    originations: tezosIdApiSource + "v1/originations?account=",
    endorsements: tezosIdApiSource + "v1/endorsements?account=",
    activations: tezosIdApiSource + "v1/activate_accounts?account=",
    reveals: tezosIdApiSource + "v1/reveals?account=",
    proposals: tezosIdApiSource + "v1/proposals?account=",
    ballots: tezosIdApiSource + "v1/ballots?account="
  },
  operationNum: {
    transactions: tezosIdApiSource + "v1/transactions_num?op=",
    delegations: tezosIdApiSource + "v1/delegations_num?op=",
    originations: tezosIdApiSource + "v1/originations_num?op=",
    endorsements: tezosIdApiSource + "v1/endorsements_num?op=",
    proposals: tezosIdApiSource + "v1/proposals_num?op=",
    ballots: tezosIdApiSource + "v1/ballots_num?op=",
    activations: tezosIdApiSource + "v1/activate_accounts_num?op=",
    reveals: tezosIdApiSource + "v1/reveals_num?op=",
    nonces: tezosIdApiSource + "v1/seed_nonce_revelations_num?op=",
    doubleBakings: tezosIdApiSource + "v1/double_baking_evidences_num?op=",
    doubleEndorsements:
      tezosIdApiSource + "v1/double_endorsement_evidences_num?op="
  },
  operationActions: {
    transactions: tezosIdApiSource + "v1/transactions?op=",
    delegations: tezosIdApiSource + "v1/delegations?op=",
    originations: tezosIdApiSource + "v1/originations?op=",
    endorsements: tezosIdApiSource + "v1/endorsements?op=",
    proposals: tezosIdApiSource + "v1/proposals?op=",
    ballots: tezosIdApiSource + "v1/ballots?op=",
    activations: tezosIdApiSource + "v1/activate_accounts?op=",
    reveals: tezosIdApiSource + "v1/reveals?op=",
    nonces: tezosIdApiSource + "v1/seed_nonce_revelations?op=",
    doubleBakings: tezosIdApiSource + "v1/double_baking_evidences?op=",
    doubleEndorsements:
      tezosIdApiSource + "v1/double_endorsement_evidences?op=",
    injectOperation: tezosNodeSource + "injection/operation?chain=main"
  },
  baker_rights:
    tezosNodeSource + "chains/main/blocks/head/helpers/baking_rights",
  deleServices: "https://api.mytezosbaker.com/v1/bakers/",
  transactions_24h: tezosIdApiSource + "v1/24_hours_stat",
  heads: tezosNodeSource + "chains/main/blocks",
  proposalList: tezosNodeSource + "chains/main/blocks/head/votes/proposals",
  proposers: tezosIdApiSource + "v1/proposers",
  proposersNum: tezosIdApiSource + "v1/proposers_num",
  proposalVoters: tezosIdApiSource + "v1/ballots?proposal=",
  proposalVotersNum: tezosIdApiSource + "v1/ballots_num?proposal=",
  doubleBakings: tezosIdApiSource + "v1/double_baking_evidences",
  doubleBakingsNum: tezosIdApiSource + "v1/double_baking_evidences_num",
  doubleEndorsements: tezosIdApiSource + "v1/double_endorsement_evidences",
  doubleEndorsementsNum:
    tezosIdApiSource + "v1/double_endorsement_evidences_num",
  allAccount: tezosIdApiSource + "v1/accounts?prefix=tz",
  allAccountNum: tezosIdApiSource + "v1/accounts_num?prefix=tz",
  additionalAccInfo:
    tezosNodeSource + "chains/main/blocks/head/context/contracts/",
  allContract: tezosIdApiSource + "v1/accounts?prefix=KT1",
  allContractNum: tezosIdApiSource + "v1/accounts_num?prefix=KT1",
  smartContract: tezosNodeSource + "chains/main/blocks/head/context/contracts/",
  snapshot: tezosNodeSource + "chains/main/blocks/<level>/context/raw/json/cycle/<cycle>/roll_snapshot",
  endorsing_rights: tezosNodeSource + "chains/main/blocks/head/helpers/endorsing_rights",
  chart: 'https://tezosid-auth.firebaseio.com/',
};
