import React from "react";
import {
  renderCell,
} from "../components/tableViews/helperCells";
import { FormattedMessage } from "react-intl";

const getTitle = text => {
  return <FormattedMessage id={text} />;
};
const columns = {
  level: {
    title: getTitle("table.level"),
    key: "level",
    width: 50,
    render: object => renderCell(object, "HashConversionCell", "level")
  },

  timestamp: {
    title: getTitle("table.timeStamp"),
    key: "timestamp",
    width: 200,
    render: object => renderCell(object, "TextCell", "timestamp")
  },

  duration: {
    title: getTitle("table.timeStamp"),
    key: "timestamp",
    width: 200,
    render: object => renderCell(object, "DurationCell", "timestamp")
  },

  operations: {
    title: getTitle("table.operations"),
    key: "operations",
    width: 50,
    render: object => renderCell(object, "TextCell", "operations")
  },

  bakerLink: {
    title: getTitle("table.baker"),
    key: "baker",
    width: 20,
    render: object => renderCell(object, "ShortLinkCell", "baker")
  },

  amount: {
    title: getTitle("table.amount"),
    key: "amount",
    width: 50,
    align: "right",
    render: object => renderCell(object, "AmountCell", "amount")
  },

  fromElement: {
    title: getTitle("table.from"),
    key: "fromElement",
    width: 200,
    render: object => renderCell(object, "ShortFromToLinkCell", "fromElement")
  },

  toElement: {
    title: getTitle("table.to"),
    key: "toElement",
    width: 200,
    render: object => renderCell(object, "ShortFromToLinkCell", "toElement")
  },

  peerId: {
    title: getTitle("table.peer_id"),
    key: "peerId",
    width: 50,
    render: object => renderCell(object, "TextCell", "peerId")
  },

  country: {
    title: getTitle("table.country"),
    key: "country",
    width: 50,
    render: object => renderCell(object, "TextCell", "country")
  },

  ip: {
    title: getTitle("table.ip"),
    key: "ip",
    width: 50,
    render: object => renderCell(object, "TextCell", "ip")
  },

  state: {
    title: getTitle("table.state"),
    key: "state",
    width: 50,
    render: object => renderCell(object, "TextCell", "state")
  },

  lastseen: {
    title: getTitle("table.lastSeen"),
    key: "lastSeen",
    width: 50,
    render: object => renderCell(object, "TextCell", "lastSeen")
  },

  totalSent: {
    title: getTitle("table.totalSent"),
    key: "totalSent",
    width: 50,
    render: object => renderCell(object, "TextCell", "totalSent")
  },

  totalReceived: {
    title: getTitle("table.totalReceived"),
    key: "totalReceived",
    width: 50,
    render: object => renderCell(object, "TextCell", "totalReceived")
  },

  ballots: {
    title: getTitle("table.ballots"),
    key: "ballots",
    width: 50,
    render: object => renderCell(object, "TranslationCell", "ballots")
  },

  votes: {
    title: getTitle("table.votes"),
    key: "votes",
    width: 20,
    render: object => renderCell(object, "TextCell", "votes")
  },

  votePercentage: {
    title: getTitle("table.votesPercentage"),
    key: "votePercentage",
    width: 50,
    render: object => renderCell(object, "TextCell", "votePercentage")
  },

  transNum: {
    title: getTitle("table.#op"),
    key: "transNum",
    width: 50,
    render: object => renderCell(object, "TextCell", "transNum")
  },

  age: {
    title: getTitle("table.age"),
    key: "age",
    width: 50,
    render: object => renderCell(object, "TextCell", "age")
  },

  opHash: {
    title: getTitle("table.opHash"),
    key: "opHash",
    width: 50,
    render: object => renderCell(object, "ShortLinkCell", "opHash")
  },

  newAccount: {
    title: getTitle("table.newAccount"),
    key: "newAccount",
    width: 50,
    render: object => renderCell(object, "ShortLinkCell", "newAccount")
  },

  fromTo: {
    title: getTitle("table.fromTo"),
    key: "fromTo",
    width: 50,
    render: object => renderCell(object, "ShortDualLinkCell", "fromTo")
  },

  baker: {
    title: getTitle("table.baker"),
    key: "baker",
    width: 20,
    render: object => renderCell(object, "TextCell", "baker")
  },

  slot: {
    title: getTitle("table.slot"),
    key: "slot",
    width: 20,
    render: object => renderCell(object, "TextCell", "slot")
  },

  blockId: {
    title: getTitle("table.block"),
    key: "blockId",
    width: 20,
    render: object => renderCell(object, "HashConversionCell", "blockId")
  },

  blockHash: {
    title: getTitle("table.block"),
    key: "blockId",
    width: 20,
    render: object => renderCell(object, "ShortHashConversionCell", "blockId")
  },
  blockHashFull: {
    title: getTitle("table.block"),
    key: "blockId",
    width: 50,
    render: object => renderCell(object, "LinkCell", "blockId")
  },
  fee: {
    title: getTitle("table.fee"),
    key: "fee",
    width: 20,
    align: "right",
    render: object => renderCell(object, "AmountCell", "fee")
  },

  delegateSource: {
    title: getTitle("table.delegatingAccount"),
    key: "delegateSource",
    width: 20,
    render: object => renderCell(object, "ShortLinkCell", "delegateSource")
  },

  delegateDestination: {
    title: getTitle("table.delegatedAccount"),
    key: "delegateDestination",
    width: 20,
    render: object => renderCell(object, "ShortLinkCell", "delegateDestination")
  },

  originateSource: {
    title: getTitle("table.origin"),
    key: "originateSource",
    width: 20,
    render: object => renderCell(object, "ShortLinkCell", "originateSource")
  },

  proposalSource: {
    title: getTitle("table.proposalsSource"),
    key: "proposalSource",
    width: 20,
    render: object => renderCell(object, "ShortLinkCell", "proposalSource")
  },

  originated: {
    title: getTitle("table.originated"),
    key: "originated",
    width: 20,
    render: object => renderCell(object, "TextCell", "originated")
  },

  counter: {
    title: getTitle("table.counter"),
    key: "counter",
    width: 20,
    render: object => renderCell(object, "TextCell", "counter")
  },

  gasLimit: {
    title: getTitle("table.gasLimit"),
    key: "gasLimit",
    width: 20,
    render: object => renderCell(object, "TextCell", "gasLimit")
  },

  storageLimit: {
    title: getTitle("table.storageLimit"),
    key: "storageLimit",
    width: 20,
    render: object => renderCell(object, "TextCell", "storageLimit")
  },

  info: {
    title: getTitle("table.information"),
    key: "info",
    width: 20,
    render: object => renderCell(object, "TranslationCell", "info")
  },

  value: {
    title: getTitle("table.values"),
    key: "value",
    width: 20,
    render: object => renderCell(object, "ValueCell", "value")
  },

  bh1s: {
    title: getTitle("table.values"),
    key: "bh1s",
    width: 20,
    render: object => renderCell(object, "TextCell", "bh1s")
  },

  bh2s: {
    title: getTitle("table.values"),
    key: "bh2s",
    width: 20,
    render: object => renderCell(object, "TextCell", "bh2s")
  },

  op1s: {
    title: getTitle("table.values"),
    key: "op1s",
    width: 20,
    render: object => renderCell(object, "TextCell", "op1s")
  },

  op2s: {
    title: getTitle("table.values"),
    key: "op2s",
    width: 20,
    render: object => renderCell(object, "TextCell", "op2s")
  },

  type: {
    title: getTitle("table.type"),
    key: "type",
    width: 20,
    render: object => renderCell(object, "TextCell", "type")
  },

  signature: {
    title: getTitle("table.signature"),
    key: "signature",
    width: 100,
    render: object => renderCell(object, "TextCell", "signature")
  },

  index: {
    title: getTitle("table.index"),
    key: "index",
    width: 100,
    render: object => renderCell(object, "TextCell", "index")
  },

  proto: {
    title: getTitle("table.proto"),
    key: "proto",
    width: 100,
    render: object => renderCell(object, "TextCell", "proto")
  },

  start: {
    title: getTitle("table.startingBlock"),
    key: "start",
    width: 100,
    render: object => renderCell(object, "LinkCell", "start")
  },

  end: {
    title: getTitle("table.endingBlock"),
    key: "end",
    width: 100,
    render: object => renderCell(object, "LinkCell", "end")
  },

  period: {
    title: getTitle("table.votingPeriod"),
    key: "period",
    width: 100,
    render: object => renderCell(object, "TextCell", "period")
  },

  count: {
    title: getTitle("table.counts"),
    key: "count",
    width: 100,
    render: object => renderCell(object, "TextCell", "count")
  },

  source: {
    title: getTitle("table.source"),
    key: "source",
    width: 100,
    render: object => renderCell(object, "ShortLinkCell", "source")
  },

  ballot: {
    title: getTitle("table.ballot"),
    key: "ballot",
    width: 20,
    render: object => renderCell(object, "TextCell", "ballot")
  },

  cycle: {
    title: getTitle("table.cycle"),
    key: "cycle",
    width: 20,
    render: object => renderCell(object, "TextCell", "cycle")
  },

  action: {
    title: getTitle("table.action"),
    key: "action",
    width: 20,
    render: object => renderCell(object, "TagCell", "action")
  },

  hashbars: {
    title: "",
    key: "hashbars",
    width: "100%",
    render: object => renderCell(object, "TextCell", "hashbars")
  },

  noncepercent: {
    title: getTitle("table.noncesRevelation"),
    key: "noncepercent",
    width: 20,
    render: object => renderCell(object, "TextCell", "noncepercent")
  },

  protocolhash: {
    title: getTitle("table.protocol"),
    key: "protocolhash",
    width: 20,
    render: object => renderCell(object, "TextCell", "protocolhash")
  },

  priority: {
    title: getTitle("table.priority"),
    key: "priority",
    width: 20,
    render: object => renderCell(object, "TextCell", "priority")
  },

  volume: {
    title: getTitle("table.volume"),
    key: "volume",
    width: 20,
    render: object => renderCell(object, "AmountCell", "volume")
  },

  fitness: {
    title: getTitle("table.fitness"),
    key: "fitness",
    width: 20,
    render: object => renderCell(object, "TextCell", "fitness")
  },

  first: {
    title: getTitle("table.firstPriority"),
    key: "first",
    width: 20,
    render: object => renderCell(object, "LinkCell", "first")
  },

  second: {
    title: getTitle("table.secondPriority"),
    key: "second",
    width: 20,
    render: object => renderCell(object, "LinkCell", "second")
  },

  third: {
    title: getTitle("table.thirdPriority"),
    key: "third",
    width: 20,
    render: object => renderCell(object, "LinkCell", "third")
  },

  fourth: {
    title: getTitle("table.fourthPriority"),
    key: "fourth",
    width: 20,
    render: object => renderCell(object, "LinkCell", "fourth")
  },

  hash: {
    title: getTitle("table.hash"),
    key: "hash",
    width: 50,
    render: object => renderCell(object, "LinkCell", "hash")
  },

  publicKeyHash: {
    title: getTitle("table.publicKeyHash"),
    key: "hash",
    width: 50,
    render: object => renderCell(object, "ShortLinkCell", "hash")
  },


  proposalHash: {
    title: getTitle("table.hash"),
    key: "hash",
    width: 50,
    render: object => renderCell(object, "VoterLinkCell", "hash")
  },

  textHash: {
    title: getTitle("table.hash"),
    key: "textHash",
    width: 50,
    render: object => renderCell(object, "TextCell", "hash")
  },

  managerhash: {
    title: getTitle("table.manager"),
    key: "managerhash",
    width: 50,
    render: object => renderCell(object, "LinkCell", "hash")
  },

  profileManagerhash: {
    title: getTitle("table.manager"),
    key: "managerhash",
    width: 50,
    render: object => renderCell(object, "LinkCell", "managerhash")
  },

  delegatable: {
    title: getTitle("table.delegatable"),
    key: "delegatable",
    width: 50,
    render: object => renderCell(object, "TextCell", "delegatable")
  },

  contracts: {
    title: getTitle("table.hash"),
    key: "contracts",
    width: 20,
    render: object => renderCell(object, "LinkCell", "contracts")
  },

  manager: {
    title: getTitle("table.manager"),
    key: "manager",
    width: 20,
    render: object => renderCell(object, "LinkCell", "manager")
  },

  delegate: {
    title: getTitle("table.delegate"),
    key: "delegate",
    width: 20,
    render: object => renderCell(object, "LinkCell", "delegate")
  },

  spendable: {
    title: getTitle("table.spendable"),
    key: "spendable",
    width: 20,
    render: object => renderCell(object, "TextCell", "spendable")
  },

  balance: {
    title: getTitle("table.balance"),
    key: "balance",
    width: 20,
    render: object => renderCell(object, "AmountCell", "balance")
  },

  evaluatedBalance: {
    title: getTitle("table.evaluatedBalance"),
    key: "evaluatedBalance",
    width: 20,
    render: object => renderCell(object, "AmountCell", "evaluatedBalance")
  },

  stakingBalance: {
    title: getTitle("table.stakingBalance"),
    key: "stakingBalance",
    width: 20,
    render: object => renderCell(object, "AmountCell", "stakingBalance")
  },

  newBalance: {
    title: getTitle("table.newBalance"),
    key: "newBalance",
    width: 20,
    render: object => renderCell(object, "AmountCell", "newBalance")
  },

  proposal: {
    title: getTitle("table.proposal"),
    key: "protocol_hash",
    width: 20,
    render: object => renderCell(object, "TextCell", "protocol_hash")
  },

  rank: {
    title: getTitle("table.rank"),
    key: "rank",
    width: 20,
    render: object => renderCell(object, "TextCell", "rank")
  },

  delegationCode: {
    title: getTitle("table.baker"),
    key: "delegation_code",
    width: 20,
    render: object => renderCell(object, "ShortLinkCell", "delegation_code")
  },

  bakerEfficiency: {
    title: getTitle("table.efficiency"),
    key: "baker_efficiency",
    width: 20,
    render: object => renderCell(object, "TextCell", "baker_efficiency")
  },

  availableCapacity: {
    title: getTitle("table.availableCapacity"),
    key: "available_capacity",
    width: 20,
    render: object => renderCell(object, "TextCell", "available_capacity")
  },

  acceptingDelegation: {
    title: getTitle("table.acceptingDelegation"),
    key: "accepting_delegation",
    width: 20,
    render: object => renderCell(object, "TextCell", "accepting_delegation")
  },
  secret: {
    title: getTitle("activation.secret"),
    key: "secret",
    width: 20,
    render: object => renderCell(object, "TextCell", "secret")
  },
  publicKey: {
    title: getTitle("activation.publicKey"),
    key: "publicKey",
    width: 20,
    render: object => renderCell(object, "ShortCopyCell", "publicKey")
  },
  nonce: {
    title: getTitle("table.nonce"),
    key: "nonce",
    width: 20,
    render: object => renderCell(object, "TextCell", "nonce")
  },

  bakerReward: {
    title: getTitle("table.bakerReward"),
    key: "bakerReward",
    width: 20,
    render: object => renderCell(object, "AmountCell", "bakerReward")
  },

  offender: {
    title: getTitle("table.offender"),
    key: "offender",
    width: 20,
    render: object => renderCell(object, "ShortLinkCell", "offender")
  },

  denouncedLevel: {
    title: getTitle("table.denouncedLevel"),
    key: "denouncedLevel",
    width: 20,
    render: object => renderCell(object, "LinkCell", "denouncedLevel")
  },

  lostDeposit: {
    title: getTitle("table.lostDeposit"),
    key: "lostDeposit",
    width: 20,
    render: object => renderCell(object, "AmountCell", "lostDeposit")
  },

  lostRewards: {
    title: getTitle("table.lostReward"),
    key: "lostRewards",
    width: 20,
    render: object => renderCell(object, "AmountCell", "lostReward")
  },

  lostFees: {
    title: getTitle("table.lostFees"),
    key: "lostFees",
    width: 20,
    render: object => renderCell(object, "AmountCell", "lostFees")
  },

  allAccountHash: {
    title: getTitle("table.hash"),
    key: "hash",
    width: 50,
    render: object => renderCell(object, "ShortLinkCell", "hash")
  },

  allAccountManager: {
    title: getTitle("table.manager"),
    key: "manager",
    width: 50,
    render: object => renderCell(object, "ShortLinkCell", "manager")
  },

  delegateStatus: {
    title: getTitle("table.delegate"),
    key: "delegate",
    width: 20,
    render: object => renderCell(object, object.delegate === 'table.forbidden' ? 'TranslationCell' : 'ShortLinkCell', "delegate")
  },

  snapshotLevel: {
    title: getTitle("table.level"),
    key: "level",
    width: 50,
    render: object => renderCell(object, "LinkCell", "level")
  },

  deactivatedStatus: {
    key: "deactivatedStatus",
    render: object => renderCell(object, "StatusCell", "deactivatedStatus")
  },

  delegatedContracts: {
    key: "delegatedContracts",
    render: object => renderCell(object, "MultipleShortLinkCell", "delegatedContracts")
  },

  successor: {
    key: "successor",
    render: object => renderCell(object, "LinkCell", "successor")
  },

  predecessor: {
    key: "predecessor",
    render: object => renderCell(object, "LinkCell", "predecessor")
  },

  linkBaker: {
    key: "linkBaker",
    render: object => renderCell(object, "LinkCell", "linkBaker")
  },

  operationsCount: {
    key: "operationsCount",
    render: object => renderCell(object, "NumberCell", "operationsCount")
  }
};

const bakerRightsColumn = [
  { ...columns.level },
  { ...columns.first },
  { ...columns.second },
  { ...columns.third },
  { ...columns.fourth }
];
const proposalListColumns = [
  { ...columns.proposal }
];

const proposalColumns = [
  { ...columns.period },
  { ...columns.proposalHash },
  { ...columns.count },
  { ...columns.votes },
  { ...columns.source }
];

const proposalVotersColumns = [
  { ...columns.period },
  { ...columns.opHash },
  { ...columns.ballot },
  { ...columns.source }
];

const protocolsColumns = [
  { ...columns.proto },
  { ...columns.textHash },
  { ...columns.start },
  { ...columns.end }
];

const blocksColumns = [
  { ...columns.level },
  { ...columns.timestamp },
  { ...columns.hash },
  { ...columns.operations },
  { ...columns.bakerLink }
];
const miniBlocksColumns = [
  { ...columns.level },
  { ...columns.bakerLink },
  { ...columns.transNum },
  { ...columns.age }
];

const infoColumns = [{ ...columns.info }, { ...columns.value }];

const accountOperationColumns = {
  transactions: [
    { ...columns.opHash },
    // { ...columns.timestamp },
    { ...columns.level },
    { ...columns.duration },
    { ...columns.fromElement },
    { ...columns.toElement },
    { ...columns.action },
    { ...columns.fee },
    { ...columns.amount }
  ],
  delegations: [
    { ...columns.opHash },
    { ...columns.duration },
    { ...columns.blockHash },
    { ...columns.delegateSource },
    { ...columns.delegateDestination },
    { ...columns.fee }
  ],
  endorsements: [
    { ...columns.opHash },
    { ...columns.duration },
    { ...columns.blockHash },
    { ...columns.slot }
  ],
  originations: [
    { ...columns.opHash },
    { ...columns.duration },
    { ...columns.blockHash },
    { ...columns.newAccount },
    { ...columns.newBalance },
    { ...columns.originateSource },
    { ...columns.fee }
  ],
  activations: [
    { ...columns.opHash },
    { ...columns.duration },
    { ...columns.blockHash },
    { ...columns.publicKeyHash },
    { ...columns.secret }
  ],
  reveals: [
    { ...columns.opHash },
    { ...columns.duration },
    { ...columns.formattedTimestamp },
    { ...columns.source },
    { ...columns.publicKey },
    { ...columns.blockHash },
    { ...columns.counter },
    { ...columns.gasLimit },
    { ...columns.storageLimit },
    { ...columns.fee }
  ],
  proposals: [
    { ...columns.opHash },
    { ...columns.proposalHash },
    { ...columns.proposalSource },
    { ...columns.period },
  ],
  ballots: [
    { ...columns.opHash },
    { ...columns.period },
    { ...columns.proposalSource },
    { ...columns.proposalHash },
    { ...columns.ballot },
  ],
}

const operationColumns = {
  transactions: [
    { ...columns.blockHashFull },
    { ...columns.fromElement },
    { ...columns.toElement },
    { ...columns.fee },
    { ...columns.amount }
  ],
  delegations: [
    { ...columns.blockHash },
    { ...columns.delegateSource },
    { ...columns.delegateDestination },
    { ...columns.fee }
  ],
  originations: [
    { ...columns.blockHash },
    { ...columns.newAccount },
    { ...columns.newBalance },
    { ...columns.originateSource },
    { ...columns.fee }
  ],
  endorsements: [
    { ...columns.blockId },
    { ...columns.slot }
  ],
  activations: [
    { ...columns.blockHash },
    { ...columns.publicKeyHash },
    { ...columns.secret }
  ],
  reveals: [
    { ...columns.source },
    { ...columns.publicKey },
    { ...columns.blockHash },
    { ...columns.counter },
    { ...columns.gasLimit },
    { ...columns.storageLimit },
    { ...columns.fee }
  ],
  proposals: [
    { ...columns.proposalHash },
    { ...columns.proposalSource },
    { ...columns.period },
  ],
  ballots: [
    { ...columns.period },
    { ...columns.proposalSource },
    { ...columns.proposalHash },
    { ...columns.ballot },
  ],
  nonces: [
    { ...columns.nonce }
  ],
  doubleBakings: [{ ...columns.info }, { ...columns.bh1s }, { ...columns.bh2s }],
  doubleEndorsements: [{ ...columns.info }, { ...columns.op1s }, { ...columns.op2s }],
}

const operationTableColumns = [
  { ...columns.opHash },
  { ...columns.timestamp },
  { ...columns.type },
  { ...columns.signature }
];
const votingColumns = [
  { ...columns.ballots },
  { ...columns.votes },
  { ...columns.votePercentage }
];
const votersColumns = [
  { ...columns.period },
  { ...columns.opHash },
  { ...columns.ballot },
  { ...columns.source }
  // { ...columns.source, sorter: false },
  // { ...columns.ballot, sorter: true },
  // { ...columns.votes, sorter: true }
];

const transactionsColumns = [
  { ...columns.opHash },
  { ...columns.level },
  { ...columns.timestamp },
  { ...columns.amount },
  { ...columns.fromElement },
  { ...columns.toElement },
  { ...columns.fee }
];
const accountTransactionsColumns = [
  { ...columns.opHash },
  // { ...columns.timestamp },
  { ...columns.level },
  { ...columns.duration },
  { ...columns.fromElement },
  { ...columns.toElement },
  { ...columns.action },
  { ...columns.fee },
  { ...columns.amount }
];

const accountProfileColumns = [
  { ...columns.opHash },
  { ...columns.duration },
  { ...columns.fromElement },
  { ...columns.toElement },
  { ...columns.action },
  { ...columns.amount }
];
const miniTransactionsColumns = [
  { ...columns.opHash },
  { ...columns.amount },
  { ...columns.fromTo },
  { ...columns.age }
];
const peersColumns = [
  { ...columns.peerId },
  { ...columns.country },
  { ...columns.ip },
  { ...columns.state },
  { ...columns.lastseen },
  { ...columns.totalSent },
  { ...columns.totalReceived }
];
const endorsementsColumns = [
  { ...columns.opHash },
  { ...columns.timestamp },
  { ...columns.blockId },
  { ...columns.slot }
];
const delegationsColumns = [
  { ...columns.opHash },
  { ...columns.timestamp },
  { ...columns.blockId },
  { ...columns.delegateSource },
  { ...columns.delegateDestination },
  { ...columns.fee }
];
const originationsColumns = [
  { ...columns.opHash },
  { ...columns.timestamp },
  { ...columns.blockId },
  { ...columns.newAccount },
  { ...columns.newBalance },
  { ...columns.originateSource },
  { ...columns.fee }
];
const trackingAccountsColumns = [
  { ...columns.hash },
  { ...columns.balance },
  { ...columns.evaluatedBalance },
  { ...columns.stakingBalance },
  { ...columns.profileManagerhash },
  { ...columns.originated },
  { ...columns.spendable },
  { ...columns.delegatable },
  { ...columns.delegate },
  { ...columns.counter }
];
const contractsColumns = [
  { ...columns.contracts },
  { ...columns.manager },
  { ...columns.delegate },
  { ...columns.spendable },
  { ...columns.balance }
];
const noncesHashColumns = [
  { ...columns.cycle },
  { ...columns.noncepercent },
  { ...columns.hashbars }
];

const headsColumns = [
  { ...columns.level },
  { ...columns.timestamp },
  { ...columns.priority },
  { ...columns.operations },
  { ...columns.fitness },
  { ...columns.bakerLink },
  { ...columns.protocolhash }
];

const accountDelegationsColumns = [
  { ...columns.opHash },
  { ...columns.duration },
  { ...columns.blockHash },
  { ...columns.delegateSource },
  { ...columns.delegateDestination },
  { ...columns.fee }
];

const accountEndorsementsColumns = [
  { ...columns.opHash },
  { ...columns.duration },
  { ...columns.blockHash },
  { ...columns.slot }
];

const accountProposalsColumns = [
  { ...columns.opHash },
  { ...columns.proposalHash },
  { ...columns.proposalSource },
  { ...columns.period },
];

const accountBallotsColumns = [
  { ...columns.opHash },
  { ...columns.period },
  { ...columns.proposalSource },
  { ...columns.proposalHash },
  { ...columns.ballot },
];

const accountOriginationsColumns = [
  { ...columns.opHash },
  { ...columns.duration },
  { ...columns.blockHash },
  { ...columns.newAccount },
  { ...columns.newBalance },
  { ...columns.originateSource },
  { ...columns.fee }
];

const delegationServicesColumns = [
  { ...columns.rank },
  { ...columns.delegationCode },
  { ...columns.fee },
  { ...columns.bakerEfficiency },
  { ...columns.availableCapacity },
  { ...columns.acceptingDelegation },
];
const accountActivationsColumns = [
  { ...columns.opHash },
  { ...columns.duration },
  { ...columns.blockHash },
  { ...columns.publicKeyHash },
  { ...columns.secret }
];

const accountRevealsColumns = [
  { ...columns.opHash },
  { ...columns.duration },
  { ...columns.formattedTimestamp },
  { ...columns.source },
  { ...columns.publicKey },
  { ...columns.blockHash },
  { ...columns.counter },
  { ...columns.gasLimit },
  { ...columns.storageLimit },
  { ...columns.fee }
];

const doubleBakingColumns = [
  { ...columns.opHash },
  { ...columns.blockHashFull },
  { ...columns.timestamp },
  // { ...columns.bakerLink },
  // { ...columns.bakerReward },
  // { ...columns.offender },
  { ...columns.denouncedLevel },
  // { ...columns.lostDeposit },
  // { ...columns.lostReward },
  // { ...columns.lostFees }
];

const doubleEndorsementColumns = [
  { ...columns.opHash },
  { ...columns.blockHashFull },
  { ...columns.timestamp },
  // { ...columns.bakerLink },
  // { ...columns.bakerReward },
  // { ...columns.offender },
  { ...columns.denouncedLevel },
  // { ...columns.lostDeposit },
  // { ...columns.lostReward },
  // { ...columns.lostFees }
];

const allAccountColumns = [
  { ...columns.allAccountHash },
  { ...columns.counter },
  { ...columns.balance },
];

const allContractColumns = [
  { ...columns.allAccountHash },
  { ...columns.delegateStatus },
  { ...columns.balance },
];

const noncesColumns=[
  { ...columns.opHash },
  { ...columns.blockHash },
  { ...columns.nonce }
]
const allActivationsColumns = [
  { ...columns.opHash },
  { ...columns.blockHash },
  { ...columns.publicKeyHash },
  { ...columns.secret }
];

const allSnapshotsColumns = [
  { ...columns.cycle },
  { ...columns.snapshotLevel },
  { ...columns.index }
];

const blockOperationColumns = {
  transactions: [
    ...transactionsColumns,
  ],
  delegations: [
    ...delegationsColumns,
  ],
  originations: [
    ...originationsColumns,
  ],
  endorsements: [
    ...endorsementsColumns
  ],
  activations: [
    { ...columns.opHash },
    { ...columns.blockHash },
    { ...columns.publicKeyHash },
    { ...columns.secret },
  ],
  reveals: [
    { ...columns.opHash },
    { ...columns.source },
    { ...columns.publicKey },
    { ...columns.blockHash },
    { ...columns.counter },
    { ...columns.gasLimit },
    { ...columns.storageLimit },
    { ...columns.fee },
  ],
  proposals: [
    ...accountProposalsColumns,
  ],
  ballots: [
    ...accountBallotsColumns,
  ],
  nonces: [
    { ...columns.opHash },
    { ...columns.nonce },
  ],
  doubleBakings: [
    { ...columns.opHash },
    { ...columns.timestamp },
    { ...columns.denouncedLevel },
  ],
  doubleEndorsements: [
    { ...columns.opHash },
    { ...columns.timestamp }
  ],
};

export {
  columns,
  blocksColumns,
  transactionsColumns,
  peersColumns,
  miniBlocksColumns,
  miniTransactionsColumns,
  accountTransactionsColumns,
  endorsementsColumns,
  delegationsColumns,
  originationsColumns,
  votingColumns,
  votersColumns,
  operationTableColumns,
  protocolsColumns,
  proposalColumns,
  proposalVotersColumns,
  contractsColumns,
  trackingAccountsColumns,
  noncesHashColumns,
  infoColumns,
  bakerRightsColumn,
  headsColumns,
  accountDelegationsColumns,
  accountEndorsementsColumns,
  accountOriginationsColumns,
  proposalListColumns,
  delegationServicesColumns,
  accountProfileColumns,
  accountActivationsColumns,
  accountRevealsColumns,
  accountProposalsColumns,
  accountBallotsColumns,
  doubleBakingColumns,
  doubleEndorsementColumns,
  operationColumns,
  allAccountColumns,
  allContractColumns,
  noncesColumns,
  allActivationsColumns,
  allSnapshotsColumns,
  accountOperationColumns,
  blockOperationColumns,
};
