
import { language } from '../../settings';

const langConfigs = {
  defaultLanguage: language,
  options: [
    {
      languageId: 'english',
      locale: 'en',
      locales: ['en', 'en-US'],
      text: 'English'
    },
    {
      languageId: 'japanese',
      locale: 'ja',
      locales: [ 'ja', 'ja-JP' ],
      text: '日本語'
    },
    {
      languageId: 'korean',
      locale: 'ko-KR',
      locales: [ 'ko', 'ko-KR' ],
      text: '한국어'
    },
    {
      languageId: 'traditionalChinese',
      locale: 'zh-TW',
      locales: [ 'zh-TW', 'zh', 'zh-Hant', 'zh-Hant-TW' ],
      text: '繁體中文'
    },
    {
      languageId: 'simplifiedChinese',
      locale: 'zh-CN',
      locales: [ 'zh-CN', 'zh-Hans', 'zh-Hans-CN' ],
      text: '简体中文'
    }
  ],
  langOptions: {}
};

let langOptions = {}
langConfigs.options.forEach((option) => {
  const { locales } = option

  locales.forEach((locale) => {
    langOptions[locale] = option
  })
})
langConfigs.langOptions = langOptions

export default langConfigs;
