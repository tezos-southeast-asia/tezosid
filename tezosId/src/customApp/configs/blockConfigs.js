import { connectionConfig } from "./connectionConfigs"

const tezosEnv = connectionConfig.tezosEnv
const CYCLES_PER_PERIOD = 8
const blockEnvConfigs = {
  mainnet: {
    blocksPerCycle: 4096,
    cyclesPerPeriod: CYCLES_PER_PERIOD,
    blocksPerPeriod: 4096 * CYCLES_PER_PERIOD,
    blocksPerRollSnapshot: 256,
    minCycle: 7,
  },
  babylonnet:
  {
    blocksPerCycle: 2048,
    cyclesPerPeriod: CYCLES_PER_PERIOD,
    blocksPerPeriod: 2048 * CYCLES_PER_PERIOD,
    blocksPerRollSnapshot: 256,
    minCycle: 5,
  },
  carthagenet: {
    blocksPerCycle: 2048,
    cyclesPerPeriod: CYCLES_PER_PERIOD,
    blocksPerPeriod: 2048 * CYCLES_PER_PERIOD,
    blocksPerRollSnapshot: 256,
    minCycle: 5,
  },
}

export const blockConfigs = blockEnvConfigs[tezosEnv] || {}
