const routes = [
    {
      prefixes: [ "KT", "tz" ],
      path: "/accounts/",
      type: "accounts"
    },
    {
      prefixes: [ "o" ],
      path: "/operations/",
      type: "operations"
    },
    {
      prefixes: [ "B" ],
      path: "/blocks/",
      type: "blocks"
    }
  ]

export {
  routes
}