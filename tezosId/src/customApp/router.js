import asyncComponent from "../helpers/AsyncFunc";
import { isEnableFirebaseAuth } from "../customApp/configs/authConfigs";
import { isTestNet } from "../customApp/configs/connectionConfigs";

// Note that the order of routes is directly related to the prority of routing rules
const routes = [
  {
    path: "",
    exact: true,
    component: asyncComponent(() => import("./containers/home/index")),
    changeFreq: 'always',
    priority: 0.9,
  },
  {
    path: "blocks/:id",
    component: asyncComponent(() =>
      import("./containers/blockChain/block")
    ),
    isNoSitemap: true,
  },
  {
    path: "blocks",
    component: asyncComponent(() => import("./containers/blockChain/blocks")),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "accounts/:id([A-Za-z0-9]*)",
    component: asyncComponent(() => import("./containers/accounts")),
    isNoSitemap: true,
  },
  {
    path: "operations/:id([A-Za-z0-9]*)",
    component: asyncComponent(() => import("./containers/operations/operation")),
    isNoSitemap: true,
  },
  {
    path: "operations",
    component: asyncComponent(() => import("./containers/operations/operation")),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "transactions",
    component: asyncComponent(() =>
      import("./containers/blockChain/transactions")
    ),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "signin",
    component: asyncComponent(() => import("./containers/signin")),
    isHidden: !isEnableFirebaseAuth,
  },
  {
    path: "signup",
    component: asyncComponent(() => import("./containers/signup")),
    isHidden: !isEnableFirebaseAuth,
  },
  {
    path: "signout",
    component: asyncComponent(() => import("./containers/signout")),
    isHidden: !isEnableFirebaseAuth,
    isNoSitemap: true,
  },
  {
    path: "forgotpassword",
    component: asyncComponent(() => import("./containers/forgotpassword")),
    isHidden: !isEnableFirebaseAuth,
    isNoSitemap: true,
  },
  {
    path: "myaccount",
    component: asyncComponent(() => import("./containers/profile")),
    isLoginRequired: true,
    isHidden: !isEnableFirebaseAuth,
    isNoSitemap: true,
  },
  /**
  {
    path: "terminologies",
    component: asyncComponent(() =>
      import("./containers/tezResources/terminologies")
    )
  },**/
  {
    path: "voting-periods",
    component: asyncComponent(() => import("./containers/voting/votingPeriods")),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "peers",
    component: asyncComponent(() => import("./containers/network/peers")),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "roadmap",
    component: asyncComponent(() => import("./containers/tezResources/roadmap")),
    priority: 0.8,
  },
  {
    path: "endorsements",
    component: asyncComponent(() =>
      import("./containers/operations/endorsements")
    ),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "delegations",
    component: asyncComponent(() =>
      import("./containers/operations/delegations")
    ),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "originations",
    component: asyncComponent(() =>
      import("./containers/operations/originations")
    ),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "inject-operation",
    component: asyncComponent(() =>
      import("./containers/operations/injectOperation")
    ),
    priority: 0.8,
  },
  {
    path: "voters",
    component: asyncComponent(() => import("./containers/voting/voterList")),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "proposal-voters/:id",
    component: asyncComponent(() => import("./containers/voting/proposalVoters")),
    isNoSitemap: true,
  },
  {
    path: "devlog",
    component: asyncComponent(() => import("./containers/tezResources/devlog")),
    priority: 0.8,
  },
  {
    path: "newsletter",
    component: asyncComponent(() =>
      import("./containers/newsletter/newsletter")
    ),
    priority: 0.8,
  },
  {
    path: "unsubscribe",
    component: asyncComponent(() =>
      import("./containers/newsletter/unsubscribe")
    ),
    isNoSitemap: true,
  },
  {
    path: "thankyou",
    component: asyncComponent(() => import("./containers/newsletter/thankyou")),
    isNoSitemap: true,
  },
  {
    path: "aboutus",
    component: asyncComponent(() => import("./containers/tezResources/aboutus")),
    priority: 0.8,
  },
  {
    path: "protocols",
    component: asyncComponent(() => import("./containers/protocols/protocols")),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "proposals",
    component: asyncComponent(() => import("./containers/protocols/proposals")),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "notfound",
    exact: true,
    component: asyncComponent(() => import("../containers/Page/404")),
    isNoSitemap: true,
  },
  {
    path: ":id(B[A-Za-z0-9]{50})",
    exact: true,
    component: asyncComponent(() =>
      import("./containers/blockChain/block")
    ),
    isNoSitemap: true,
  },
  {
    path: ":id([KT|tz]{2}[A-Za-z0-9]{34})",
    component: asyncComponent(() => import("./containers/accounts")),
    isNoSitemap: true,
  },
  {
    path: ":id(o[A-Za-z0-9]{50})",
    component: asyncComponent(() => import("./containers/operations/operation")),
    isNoSitemap: true,
  },
  {
    path: ":id([0-9]*)",
    exact: true,
    component: asyncComponent(() =>
      import("./containers/blockChain/block")
    ),
    isNoSitemap: true,
  },
  {
    path: "delegation-services",
    component: asyncComponent(() =>
      import("./containers/tezResources/delegationServices")
    ),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "baker-rights",
    component: asyncComponent(() =>
      import("./containers/bakerRights")
    ),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  // {
  //   path: "heads",
  //   component: asyncComponent(() => import("./containers/blockChain/heads"))
  // },
  {
    path: "search",
    component: asyncComponent(() => import("./containers/search")),
    isNoSitemap: true,
  },
  {
    path: "transactions-chart",
    component: asyncComponent(() => import("./containers/simpleCharts/transChart")),
    priority: 0.8,
    isHidden: isTestNet,
  },
  {
    path: "activations-chart",
    component: asyncComponent(() => import("./containers/simpleCharts/activationChart")),
    priority: 0.8,
    isHidden: isTestNet,
  },
  {
    path: "blocks-chart",
    component: asyncComponent(() => import("./containers/simpleCharts/blocksChart")),
    priority: 0.8,
    isHidden: isTestNet,
  },
  {
    path: "endorsements-chart",
    component: asyncComponent(() => import("./containers/simpleCharts/endorsementChart")),
    priority: 0.8,
    isHidden: isTestNet,
  },
  {
    path: "blocks-reward-chart",
    component: asyncComponent(() => import("./containers/simpleCharts/bakerBlockReward")),
    priority: 0.8,
    isHidden: isTestNet,
  },
  {
    path: "double-baking",
    component: asyncComponent(() => import("./containers/doubleBaking/doubleBaking")),
    priority: 0.8,
  },
  {
    path: "transaction-fee-chart",
    component: asyncComponent(() => import("./containers/simpleCharts/transFeeChart")),
    priority: 0.8,
    isHidden: isTestNet,
  },
  {
    path: "operations-chart",
    component: asyncComponent(() => import("./containers/simpleCharts/opsChart")),
    priority: 0.8,
    isHidden: isTestNet,
  },
  {
    path: "baketime-chart",
    component: asyncComponent(() => import("./containers/simpleCharts/bakeTimeChart")),
    priority: 0.8,
    isHidden: isTestNet,
  },
  {
    path: "priorities-chart",
    component: asyncComponent(() => import("./containers/simpleCharts/prioritiesChart")),
    priority: 0.8,
    isHidden: isTestNet,
  },
  {
    path: "transaction-op-chart",
    component: asyncComponent(() => import("./containers/simpleCharts/transOpPerBlock")),
    priority: 0.8,
    isHidden: isTestNet,
  },
  {
    path: "topBaker-chart",
    component: asyncComponent(() => import("./containers/simpleCharts/topBakerChart")),
    priority: 0.8,
    isHidden: isTestNet,
  },
  {
    path: "double-endorsement",
    component: asyncComponent(() => import("./containers/doubleEndorsement/doubleEndorsement")),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "accounts",
    component: asyncComponent(() => import("./containers/accountList/allAccount")),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "contracts",
    component: asyncComponent(() => import("./containers/accountList/allContract")),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "activations",
    component: asyncComponent(() =>
      import("./containers/operations/activations")
    ),
    changeFreq: 'hourly',
    priority: 0.8,
  }, 
  {
    path: "api-monitoring",
    component: asyncComponent(() =>
      import("./containers/apiMonitoring")
    ),
    priority: 0.8,
  },
  {
    path: "snapshots",
    component: asyncComponent(() =>
      import("./containers/snapshot/snapshot")
    ),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "nonces",
    component: asyncComponent(() => import("./containers/operations/nonces")),
    changeFreq: 'hourly',
    priority: 0.8,
  },
  {
    path: "dev-apis",
    component: asyncComponent(() => import("./containers/api/devAPIs")),
    priority: 0.8,
  }
];

export default routes;
