import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { emailPwdSignup } from "../../../redux/auth/actions";

import Input from "../../../components/uielements/input";
import Button from "../../../components/uielements/button";
import Spin from "../../../components/uielements/spin";
import HelperText from '../../../components/utility/helper-text';
import IntlMessages from "../../../components/utility/intlMessages";
import SignUpStyleWrapper from "./signup.style";

export class SignUp extends Component {
    static defaultProps = {
        isLoggedIn: false,
        loading: false,
        signUpError: null,
        emailPwdSignup: () => { },
    }

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            pwd: '',
            confirmPwd: '',
        };
    }

    updateEmailValue(evt) {
        this.setState({
            email: evt.target.value
        });
    }

    updatePwdValue(evt) {
        this.setState({
            pwd: evt.target.value
        });
    }

    updateConfirmPwdValue(evt) {
        this.setState({
            confirmPwd: evt.target.value
        });
    }

    handleSignUp = () => {
        const { emailPwdSignup } = this.props;
        emailPwdSignup(this.state.email, this.state.pwd, this.state.confirmPwd);
    }

    render() {
        const home = { pathname: "/" };
        const { isLoggedIn, loading, signUpError } = this.props;

        if (isLoggedIn) {
            return <Redirect to={home} />;
        }

        return (
            <SignUpStyleWrapper className="isoSignUpPage">
                <div className="isoSignUpContentWrapper">
                    <div className="isoSignUpContent">
                        <div className="isoLogoWrapper">
                            <Link to="/">
                                <IntlMessages id="page.signUpTitle" />
                            </Link>
                        </div>

                        {signUpError && (
                            <HelperText text={signUpError.message} />
                        )
                        }

                        <div className="isoSignUpForm">
                            <div className="isoInputWrapper">
                                <Input size="large" placeholder="Email" onChange={evt => this.updateEmailValue(evt)} />
                            </div>
                            <div className="isoInputWrapper">
                                <Input size="large" type="password" placeholder="Password" onChange={evt => this.updatePwdValue(evt)} />
                            </div>
                            <div className="isoInputWrapper">
                                <Input
                                    size="large"
                                    type="password"
                                    placeholder="Confirm Password"
                                    onChange={evt => this.updateConfirmPwdValue(evt)}
                                />
                            </div>
                            <div className="isoInputWrapper">
                                <Button type="primary" onClick={this.handleSignUp}>
                                    <IntlMessages id="page.signUpButton" />
                                </Button>
                            </div>
                            <div className="isoInputWrapper isoCenterComponent isoHelperWrapper">
                                <Link to="/signin">
                                    <IntlMessages id="page.signUpAlreadyAccount" />
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                {loading && (
                    <div className="isoSignUpLoadingWrapper">
                        <Spin spinning />
                    </div>
                )
                }
            </SignUpStyleWrapper>
        );
    }
}

export default connect(
    state => ({
        signUpError: state.Auth.signUpError,
        loading: state.Auth.loading,
        isLoggedIn: state.Auth.isLoggedIn
    }),
    { emailPwdSignup }
)(SignUp);
