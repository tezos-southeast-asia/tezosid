import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { allSnapshotsColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchSnapshots } from "../../../redux/snapshots/actions";

class Snapshots extends Component {
  render() {
    const {
      isLoading,
      snapshots = {},
      isError,
      fetchSnapshots
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = snapshots

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="snapshot.title" />}
        dataSource={data}
        columns={allSnapshotsColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchSnapshots}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.Snapshots
})

const mapDispatchToProps = dispatch => {
  return {
    fetchSnapshots: (props) => dispatch(fetchSnapshots(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Snapshots)