import React, { Component } from "react";
import LayoutWrapper from "../../../../components/utility/layoutWrapper";
import IntlMessages from "../../../../components/utility/intlMessages";
import { FormattedMessage } from "react-intl";
import { Menu, Col, Card } from 'antd';
import apiDoc from './devApi.json';

const { SubMenu } = Menu;

export default class APIDoc extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      selectMenuItem: 'overview',
      selectSubMenu: null,
      data: []
    };
  }

  onCollapse = collapsed => {
    this.setState({ collapsed });
  };

  onChangeMenuItem = key => {
    this.setState({
      selectMenuItem: key.keyPath[0],
      selectSubMenu: key.keyPath[1] || null
    })
  }


  componentDidUpdate(_, prevState) {
    if (prevState.selectMenuItem !== this.state.selectMenuItem) {
      const selectedSubMenu = this.state.selectSubMenu;
      const selectedMenuItem = this.state.selectMenuItem;
      this.setState({
        data: selectedSubMenu !== null ? apiDoc[selectedSubMenu][selectedMenuItem] : apiDoc[selectedMenuItem]
      })
    }
  }

  componentDidMount() {
    const activeKey = this.state.selectMenuItem;
    const data = apiDoc[activeKey]
    this.setState({
      data: data
    })
  }

  getMenuBar = () => {
    const menuBar = Object.keys(apiDoc).map((key) => {
      if (apiDoc[key].query) {
        return (
          <Menu.Item key={key}>
            <span>{<IntlMessages id={`topbar.${key}`} />}</span>
          </Menu.Item>
        )
      } else {
        const subMenu = Object.keys(apiDoc[key]).map((subKey) => {
          return (
            <Menu.Item key={subKey}>{<IntlMessages id={`topbar.${subKey}`} />}</Menu.Item>
          )
        })
        return (
          <SubMenu
            key={key}
            title={
              <span>
                <span>{<IntlMessages id={`topbar.${key}`} />}</span>
              </span>
            }
          >
            {subMenu}
          </SubMenu>
        )
      }
    })
    return { menuBar }
  }

  getContent = () => {
    const { data } = this.state;

    const formatNewLine = desc => {
      let newText = desc.split('\n').map((item, i) => {
        if (item.includes('<br/>')) {
          return <div key={i}><p >{item.replace('<br/>', '')}</p><br /></div>
        } else {
          return <p key={i}>{item}</p>
        }
      });
      return (
        <div>
          {newText}
        </div>
      )
    }

    const formattedData = Object.keys(data).map((key) => {
      const getAttribute = Object.keys(data[key]).map((innerKey) => {
        if (innerKey !== 'url') {
          const splitedData = data[key][innerKey].split('\n');
          let newText = splitedData.map((data) => {
            if (data.includes('<b>')) {
              let splitText = data.split('</b>');
              return (
                splitText.map((item) => {
                  if (item.includes('<b>')) {
                    let removedTag = item.substring(3, item.length)

                    if (removedTag.includes('<br/>')) {
                      return (
                        <p key={removedTag}><b>{removedTag.substring(0, removedTag.length - 5)}</b></p>
                      )
                    } else {
                      return (
                        <span key={removedTag}><b>{removedTag}</b></span>
                      )
                    }




                  }
                  else {
                    return (
                      <span style={{ overflowWrap: 'break-word' }} key={item}>{item}</span>
                    )
                  }
                }))
            } else if (innerKey === 'params') {
              return (
                <p key={data}>{data}</p>
              )
            } else {
              return (
                <FormattedMessage id={`devapi.${data}`} key={data}>{formatNewLine}</FormattedMessage>
              )
            }
          })
          return (
            <div key={innerKey}>
              {newText}<br />
            </div>
          )
        } else {
          return (
            <div key={innerKey} style={{ overflowWrap: 'break-word' }}>
              <h2>{data[key][innerKey]}</h2><hr /><br />
            </div>
          )
        }
      })
      return getAttribute
    })
    return (
      <div>
        {formattedData}
      </div>
    )
  }

  render() {
    const { menuBar } = this.getMenuBar()
    const content = this.getContent()
    return (
      <LayoutWrapper
        pageTitle={<IntlMessages id="devapi.title" />}
      >
        <Card>

          <Col lg={3} md={24} sm={24} xs={24}>
            <Menu defaultSelectedKeys={['overview']} mode="inline" onClick={this.onChangeMenuItem}>
              {menuBar}
            </Menu>
          </Col>

          <Col lg={9} md={24} sm={24} xs={24} style={{ width: '85%' }}>
            <div style={{ paddingLeft: '3%' }}>
              {content}
            </div>

          </Col>
        </Card>
      </LayoutWrapper>
    );
  }
}
