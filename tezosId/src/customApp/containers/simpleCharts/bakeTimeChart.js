import React, { Component } from 'react';

import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import PageHeader from "../../../components/utility/pageHeader";
import Box from "../../../components/utility/box";
import { Col, Row } from "antd";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { fetchActionData } from '../../../redux/charts/actions';
import Chart from '../../components/chart'

class bakeTimeChart extends Component {

  componentDidMount() { 
    const { fetchActionData } = this.props;
    fetchActionData({ type: 'bakeTime' })
  }

  render() {
    const rowStyle = {
      width: "100%",
      display: "flex",
      flexFlow: "row wrap",
      marginBottom: "80px",
      height: "500px"
    };
    const headerStyle = {
      width: "100%",
      height: "10%",
      textAlign: "center"
    };
    const { bakeTime } = this.props;
    const { data } = bakeTime
    const chartDiv = data.length !== 0 ? <Chart data={data} xValue={'date'} yValue={'avg_time'} chartType='xyChart' /> : ''

    return (
      <LayoutContentWrapper>
        <PageHeader>
          <FormattedMessage id="bakeTime.title" />
        </PageHeader>
        <Box>
          <Row style={rowStyle}>
            <Col md={24} sm={24} xs={24}>
              <div style={headerStyle}><b><FormattedMessage id="bakeTime.header" /></b></div>
              {chartDiv}
            </Col>
          </Row>
        </Box>
      </LayoutContentWrapper>
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.Chart,
})

const mapDispatchToProps = dispatch => {
  return {
    fetchActionData: (props) => dispatch(fetchActionData(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(bakeTimeChart)
