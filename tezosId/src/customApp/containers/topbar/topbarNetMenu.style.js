import styled from 'styled-components';
import { palette } from 'styled-theme';

import WithDirection from '../../../settings/withDirection';

const TopbarNetMenuWrapper = styled.div`
  line-height: 1.5;

  .ant-btn {
    cursor: default;
    font-size: 12px;

    &.ant-dropdown-trigger {
      cursor: pointer;
    }

    .anticon {
      padding-right: ${props => props['data-rtl'] === 'rtl' ? '0' : '8px'};
      padding-left: ${props => props['data-rtl'] === 'rtl' ? '8px' : '0'};
    }

    &.ant-btn-icon-only {
      .anticon {
        padding: 0;
      }
    }
  }

  .ant-btn-ghost {
    border: ${palette('primary', 0)} 1px solid;
    color: ${palette('primary', 0)};
  }

  .ant-dropdown {
    .ant-dropdown-menu-item {
      font-size: 12px;
    }
  }

  @media only screen and (max-width: 1220px) {
    .ant-menu-inline {
      border: none;

      .ant-menu-submenu-title, .ant-menu-item {
        font-size: 12px;
      }
    }
  }
`;

export default WithDirection(TopbarNetMenuWrapper);
