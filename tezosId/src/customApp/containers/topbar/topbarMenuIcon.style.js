import styled from 'styled-components';
import WithDirection from '../../../settings/withDirection';

const MenuIconWrapper = styled.i`
  padding-right: ${props => props['data-rtl'] === 'rtl' ? '0' : '8px'};
  padding-left: ${props => props['data-rtl'] === 'rtl' ? '8px' : '0'};
`

export default WithDirection(MenuIconWrapper);
