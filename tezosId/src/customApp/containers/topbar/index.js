import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router";
import { Layout, Row } from "antd";
import classnames from "classnames";

import { menuKeys } from "../../configs/topbarConfigs";
import appActions from "../../../redux/app/actions";
import Logo from "../../components/logo";
import TopbarWrapper from "./topbar.style";
import TopbarMenu from "./topbarMenu";
import TopbarDrawer from "./topbarDrawer";
import TopbarSearchBox from './topbarSearchBox';
import { TopbarDesktopNetMenu } from './topbarNetMenu';

const { Header } = Layout;
const {
  toggleOpenDrawer,
  toggleSearchBox,
  closeDrawer,
  closeSearchBox
} = appActions;

class Topbar extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    collapsed: PropTypes.bool.isRequired,
    customizedTheme: PropTypes.object,
    isOpenDrawer: PropTypes.bool.isRequired,
    isOpenSearchBox: PropTypes.bool.isRequired,
    toggleOpenDrawer: PropTypes.func,
    toggleSearchBox: PropTypes.func,
    closeDrawer: PropTypes.func,
    closeSearchBox: PropTypes.func
  };

  static defaultProps = {
    location: {},
    customizedTheme: {}
  };

  state = {
    defaultOpenKeys: [ 'menu-0' ],
    defaultSelectedKeys: []
  };

  constructor(props) {
    super(props)

    const { location = {} } = props
    const newState = this.getSelectedMenu({ location })
    
    if (newState) {
      this.state = newState
    }
  }

  componentDidUpdate(prevProps) {
    const {
      location,
      collapsed,
      isOpenDrawer,
      closeDrawer,
    } = this.props
    const { key } = location

    if (collapsed && key !== prevProps.location.key) {
      isOpenDrawer && closeDrawer()
    }
  }

  updateOpenKeys = (openKeys = []) => {
    const numKeys = openKeys.length || 0
    const isUpdateKeys = numKeys > 0

    // Don't update defaultOpenKeys when menu is closed
    isUpdateKeys && this.setState({
      defaultOpenKeys: openKeys
    })
  }

  updateSelectedKeys = ({ selectedKeys }) => {
    this.setState({
      defaultSelectedKeys: selectedKeys
    })
  }

  getSelectedMenu = ({ location = {} }) => {
    const { pathname = "" } = location
    const defaultMenuItem = menuKeys.filter(({ link }) => link === pathname)[0]
    const defaultMenukey = defaultMenuItem && defaultMenuItem.key
    const regex = /(\w+)-(\d+)(.*)/
    let parentMenuKey
    let state = null

    if (defaultMenukey) {
       parentMenuKey = defaultMenukey.replace(regex, "$1-$2")

      state = {
        defaultOpenKeys: [ parentMenuKey ],
        defaultSelectedKeys: [ defaultMenukey ]
      }
    }

    return state
  }

  render() {
    const {
      isLoggedIn,
      collapsed,
      customizedTheme,
      isOpenDrawer,
      isOpenSearchBox,
      toggleOpenDrawer,
      toggleSearchBox
    } = this.props;
    const styling = {
      background: customizedTheme.backgroundColor
    };

    return (
      <TopbarWrapper>
        <Header
          style={styling}
          className={classnames("isomorphicTopbar", {
            "active-searchbox": isOpenSearchBox
          })}
        >
          { collapsed && (
              <TopbarDrawer
                isLoggedIn={isLoggedIn}
                isOpenDrawer={isOpenDrawer}
                menuOptions={this.state}
                onMenuOpenChange={this.updateOpenKeys}
                onMenuSelect={this.updateSelectedKeys}
                toggleOpenDrawer={toggleOpenDrawer}
              />
            )
          }
          <Logo />
          <div className="iosRight">
            { !collapsed && (
              <Fragment>
                <Row
                  type="flex"
                  justify="space-around"
                  align="middle"
                  className="topbar__right-top"
                >
                  <TopbarSearchBox />
                  <TopbarDesktopNetMenu />
                </Row>
                <TopbarMenu
                  isLoggedIn={isLoggedIn}
                  menuOptions={this.state}
                  onMenuOpenChange={this.updateOpenKeys}
                  onMenuSelect={this.updateSelectedKeys}
                />
              </Fragment>
             )
            }
            { collapsed && (
                <div className="topbar__icon" onClick={toggleSearchBox}>
                  <i
                    className="ion-ios-search-strong"
                    style={{ color: isOpenSearchBox ? customizedTheme.activeTextColor : customizedTheme.textColor }}
                  />
                </div>
              )
            }
          </div>
          { collapsed && isOpenSearchBox && <TopbarSearchBox /> }
        </Header>
      </TopbarWrapper>
    );
  }
}

export default compose(
  withRouter,
  connect(
    state => ({
      ...state.App,
      isLoggedIn: state.Auth.isLoggedIn,
      customizedTheme: state.ThemeSwitcher.topbarTheme,
      locale: state.LanguageSwitcher.language.locale,
    }),
    {
      toggleOpenDrawer,
      toggleSearchBox,
      closeDrawer,
      closeSearchBox
    }
  )
)(Topbar);
