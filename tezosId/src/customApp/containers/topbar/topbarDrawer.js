import React, { Component, Fragment } from "react";
import { Drawer } from "antd";

import TopbarMenu from "./topbarMenu";
import Logo from "../../components/logo";
import { TopbarMobileNetMenu } from "./topbarNetMenu";

class TopbarDrawer extends Component {
  render() {
    const {
      isLoggedIn,
      isOpenDrawer,
      menuOptions,
      toggleOpenDrawer,
      onMenuOpenChange,
      onMenuSelect,
    } = this.props
    const drawerTitle = (
      <Fragment>
        <Logo width={60} />
        <TopbarMobileNetMenu />
      </Fragment>
    )

    return (
      <div className="topbar__drawer">
        <div className="topbar__icon topbar__drawer__icon" onClick={toggleOpenDrawer}>
          <i className="ion-android-menu" style={{color: "rgb(50, 51, 50)"}} />
        </div>
        <Drawer
          placement="left"
          onClose={toggleOpenDrawer}
          visible={isOpenDrawer}
          style={{ padding: "0" }}
          title={drawerTitle}
        >
          <TopbarMenu
            isLoggedIn={isLoggedIn}
            mode="inline"
            menuOptions={menuOptions}
            onMenuOpenChange={onMenuOpenChange}
            onMenuSelect={onMenuSelect}
          />
        </Drawer>
      </div>
    )
  }
}

export default TopbarDrawer;