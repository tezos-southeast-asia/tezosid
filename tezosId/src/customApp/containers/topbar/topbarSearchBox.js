import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { injectIntl } from 'react-intl';

import { InputSearch } from "../../../components/uielements/input";

class TopbarSearchBox extends Component {
  state = {
    value: ""
  }

  handleChange = (e) => {
    this.setState({
      value: e.target.value
    })
  }

  handleSearch = (inputValue) => {
    const { history } = this.props

    this.setState({
      value: ''
    })

    history.push(`/search?hash=${inputValue.trim()}`)
  };

  render() {
    const { intl } = this.props
    const { value } = this.state

    return (
      <InputSearch
        id="InputTopbarSearch"
        size="large"
        placeholder={intl.formatMessage({ id: "topbar.searchPlaceholder" })}
        enterButton
        ref={this.searchBox}
        value={value}
        onSearch={this.handleSearch}
        onChange={this.handleChange}
      />
    );
  }
}

export default compose(
  withRouter,
  injectIntl
)(TopbarSearchBox);
