import React, { Fragment, PureComponent } from 'react'
import { Icon, Dropdown, Menu, } from 'antd'

import { connectionConfig } from '../../configs/connectionConfigs'
import TopbarNetMenuWrapper from './topbarNetMenu.style'

const { SubMenu } = Menu
const {
  tezosEnv,
  tezosNets,
} = connectionConfig
const selectedKeys= [ tezosEnv ]

const getNetMenuItems = () => {
  const netMenuItems = tezosNets.map(({ net, domain }, index) => {
    const isActive = net === tezosEnv
    const TextWrapper = isActive ? 'span' : 'a'

    return (
      <Menu.Item key={net}>
        <TextWrapper
          href={domain}
          target="_blank"
        >
          {net.toUpperCase()}
        </TextWrapper>
      </Menu.Item>
    )
  })

  return netMenuItems
}

const CurrentNetItem = () => (
  <Fragment>
    <Icon type="cloud" />
    {tezosEnv.toUpperCase()}
  </Fragment>
)

export class TopbarDesktopNetMenu extends PureComponent {
  render() {
    const netMenuItems = getNetMenuItems()
    const desktopNetMenu = (
      <Menu
        selectedKeys={selectedKeys}
        forceSubMenuRender
      >
        {netMenuItems}
      </Menu>
    );

    return (
      <TopbarNetMenuWrapper>
        <Dropdown.Button overlay={desktopNetMenu} type="ghost" className="topbar_net-menu">
          <CurrentNetItem />
        </Dropdown.Button>
      </TopbarNetMenuWrapper>
    )
  }
}


export class TopbarMobileNetMenu extends PureComponent {
  render() {
    const netMenuItems = getNetMenuItems()
    const mobileNetMenu = (
      <Menu
        mode="inline"
        forceSubMenuRender
        selectedKeys={selectedKeys}
      >
        <SubMenu
          key="net-menu"
          title={
            <CurrentNetItem />
          }
        >
          {netMenuItems}
        </SubMenu>
      </Menu>
    )

    return (
      <TopbarNetMenuWrapper>
        {mobileNetMenu}
      </TopbarNetMenuWrapper>
    )
  }
}
