import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";

import { menu as MenuConfigs } from "../../configs/topbarConfigs";
import Menu from "../../../components/uielements/menu";
import IntlMessages from "../../../components/utility/intlMessages";
import MenuIconWrapper from "./topbarMenuIcon.style";
import { getMenuItemKey } from "../../../helpers/utility"

const { SubMenu, Item, Divider } = Menu

class TopBarMenu extends Component {
  static defaultProps = {
    mode: "horizontal"
  }

  getItemContent = ({ icon, title }) => (
    <Fragment>
      <MenuIconWrapper className={`menu-icon ${icon}`} />
      <IntlMessages id={title} />
    </Fragment>
  )

  getItem = (itemProps) => {
    const { isLoggedIn } = this.props
    const {
      type,
      title = '',
      link,
      href,
      target = "_self",
      icon,
      items,
      key,
      index,
      isLoginRequired,
      isLogoutRequired,
      isHidden,
    } = itemProps
    const keyValue = getMenuItemKey({ key, index })
    const itemClassName = title.replace('.', '-');
    let menuItem = null

    if (isHidden || (isLoginRequired && !isLoggedIn) || (isLogoutRequired && isLoggedIn)) {
      return null
    }

    if (type === "divider") {
      menuItem = <Divider key={keyValue} />
    } else if (items) {
      menuItem = (
        <SubMenu
          key={keyValue}
          title={
            <div className={`submenu-title-wrapper topbar__submenu__${itemClassName}`}>
              { this.getItemContent({ icon, title }) }
            </div>
          }
        >
         {
           items.map((item, subIndex) => this.getItem({ ...item, key: keyValue, index: subIndex }))
         }  
        </SubMenu>
      )
    } else {
      let Wrapper = Fragment
      let WrapperProps = {}

      if (link) {
        Wrapper = Link
        WrapperProps = {
          to: link
        }
      } else if (href) {
        Wrapper = 'a'
        WrapperProps = {
          href,
          target
        }
      }
      menuItem = (
        <Item key={keyValue} className={`topbar__menu__${itemClassName}`}>
          <Wrapper {...WrapperProps}>
            { this.getItemContent({ icon, title }) }
          </Wrapper>
        </Item>
      )
    }

    return menuItem;
  }
  render() {
    const {
      mode,
      menuOptions,
      onMenuOpenChange,
      onMenuSelect
    } = this.props
    const {
      defaultOpenKeys,
      defaultSelectedKeys
    } = menuOptions

    return (
      <Menu
        mode={mode}
        forceSubMenuRender
        style={{ overflow: "hidden" }}
        defaultOpenKeys={mode === "inline" ? defaultOpenKeys : []}
        defaultSelectedKeys={defaultSelectedKeys}
        onClick={this.handleMenuClickToLink}
        onOpenChange={onMenuOpenChange}
        onSelect={onMenuSelect}
      >
        {
          MenuConfigs.map((menuItem, index) => this.getItem({ ...menuItem, index }))
        }
      </Menu>
    )
  }
}

export default TopBarMenu