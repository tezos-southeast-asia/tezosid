import styled from 'styled-components';
import WithDirection from '../../../settings/withDirection';
import basicStyle from "../../../settings/basicStyle";

const { layoutStyle, headerStyle } = basicStyle

const TopbarWrapper = styled.div`
  .isomorphicTopbar {
    position: fixed;
    width: 100%;
    height: ${headerStyle.height.large}px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    background-color: #ffffff;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    padding: 0 ${layoutStyle.padding};
    z-index: 1000;

    .iosRight {
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      overflow: hidden;
      height: 100%;
      margin-left: ${props =>
        props['data-rtl'] === 'rtl' ? '0' : '32px'};
      margin-right: ${props =>
        props['data-rtl'] === 'rtl' ? '32px' : '0'};
    }

    .topbar__right-top {
      flex-wrap: nowrap;
      width: 100%;

      .ant-input-search {
        margin-right: 8px;
      }
    }

    .ant-input-search {
      .ant-input {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        padding: ${props =>
          props['data-rtl'] === 'rtl' ? '6px 10px 6px 42px' : '6px 42px 6px 10px'};
      }

      &.ant-input-search-enter-button > .ant-input-suffix {
        right: ${props =>
          props['data-rtl'] === 'rtl' ? 'auto' : '0'};
        left: ${props =>
          props['data-rtl'] === 'rtl' ? '0' : 'auto'};

        & > .ant-input-search-button {
          border-radius: ${props =>
            props['data-rtl'] === 'rtl' ? '4px 0 0 4px' : '0 4px 4px 0'};
        }
      }
    }

    .topbar__drawer {
      height: 100%;
    }

    .topbar__icon {
      padding: 8px;
      font-size: 24px;
      height: 100%;
      display: flex;
      align-items: center;
    }

    @media only screen and (max-width: 1220px) {
      height: ${headerStyle.height.small}px;
      flex-wrap: wrap;

      .isoLogoWrapper {
        height : 100%;
      }

      .iosRight {
        flex-direction: row;
        margin: 0;
      }

      &.active-searchbox {
        height: ${headerStyle.height.large}px;

        .iosRight, .topbar__drawer, .isoLogoWrapper {
          height: ${headerStyle.height.small}px
        }

        .ant-input-search {
          margin: ${(headerStyle.height.large - headerStyle.height.small - 42)/2}px 0;
        }
      }
    }
  }
`;

export default WithDirection(TopbarWrapper);
