import styled from 'styled-components';
import { Col } from "antd";

import WithDirection from '../../../settings/withDirection';

const LatestBlockWrapper = styled(Col)`
  color: white;
  padding: 0 5%;
  margin: auto;
  font-size: 26px;
  text-align: center;

  .home__latest-block {
    &__title {
      font-size: 40px;
      text-decoration: underline;
      margin-bottom: 16px;
    }

    &__info-col {
      margin-bottom: 16px;
    }

    &__info-row, &__info-cycle, &__info-voting-period {
      margin-bottom: 8px;
      width: 100%;
    }

    &__info-title {
      font-size: 20px;
      font-weight: bold;
    }
    
    &__info-content {
      font-size: 26px;
    }
  }
`;

export default WithDirection(LatestBlockWrapper);
