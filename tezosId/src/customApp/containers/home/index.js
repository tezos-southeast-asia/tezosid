import React, { Component } from "react";
import { Card, Col, Row } from "antd";
import { Link } from "react-router-dom";
import { injectIntl } from "react-intl";
import { FormattedMessage } from "react-intl";

import {
  miniBlocksColumns,
  miniTransactionsColumns
} from "../../configs/tableColumnsConfigs";
import { connectionConfig } from "../../configs/connectionConfigs";
import { blockConfigs } from "../../configs/blockConfigs";
import { withPageQuery } from "../../../helpers/pagination";
import SimpleView from "../../components/tableViews/table";
import Box from "../../../components/utility/box";
import LayoutWrapper from "../../../components/utility/layoutWrapper.js";
import { getBakerName } from '../../../helpers/utility';
import { durationFormat, getCurrentTimeDiff, DEFAUL_DURATION } from "../../../helpers/format";
import LatestBlock from "./latestBlock";

class Home extends Component {
  constructor(props) {
    super(props);
    this.checkStatus = this.checkStatus.bind(this);
    this.state = {
      seconds: 0,

      isBlocksLoaded: false,
      blocksDataList: [],
      latestBlock: {
        level: "-",
        baker: "-",
        transNum: "-",
        age: "-",
        cycle: "-",
        cycleLeftTime: DEFAUL_DURATION,
        cycleProgress: "-",
        votingPeriod: "-",
        votingPeriodLeftTime: DEFAUL_DURATION,
        votingPeriodProgress: "-",
      },
      blockUpdate: false,

      isTxnsLoaded: false,
      txnsDataList: [],
      txnUpdate: false,

      votingPeriod: "",

      tez: { price: "-", volume: "-", circulatingSupply: "-", marketCap: "-" },
      isTezLoaded: false,
      block_24h: "-",
      transaction_24h: "-"
    };
  }

  getText = text => {
    return <FormattedMessage id={text} />;
  };

  checkStatus() {
    fetch(
      withPageQuery(connectionConfig.blocks, 1, 1)
    )
      .then(response => response.json())
      .then(parsedJSON => {
        if (parsedJSON[0].level !== this.state.latestBlock.currLevel) {
          this.setState({ blockUpdate: true });
        }
      });
    fetch(
      withPageQuery(
        connectionConfig.transactions,
        1,
        1
      )
    )
      .then(response => response.json())
      .then(parsedJSON => {
        const currentTxHash = parsedJSON && parsedJSON[0] && parsedJSON[0].op && parsedJSON[0].op.opHash
        const previousTxHash = this.state.txnsDataList && this.state.txnsDataList[0] && this.state.txnsDataList[0].hash
        if (previousTxHash && currentTxHash && currentTxHash !== previousTxHash) {
          this.setState({ txnUpdate: true });
        }
      });
  }
  componentDidUpdate() {
    if (this.state.blockUpdate) {
      this.setState({ blockUpdate: false, isBlocksLoaded: false });
      this.getBlocksData();
    }
    if (this.state.txnUpdate) {
      this.setState({ txnUpdate: false, isTxnsLoaded: false });
      this.getTransactionData();
    }
  }

  getTezData() {
    fetch(connectionConfig.tezPrice)
      .then(response => response.json())
      .then(parsedJSON => {
        this.findTezos(parsedJSON.data);
      });
    fetch(connectionConfig.transactions_24h)
      .then(response => response.json())
      .then(parsedJSON => {
        this.setState({
          block_24h: <Link to="/blocks">{parsedJSON.blocks}</Link>,
          transaction_24h: (
            <Link to="/transactions">{parsedJSON.txs}</Link>
          )
        });
      });
  }
  findTezos(dataSet) {
    for (let i = 0; i < dataSet.length; i++) {
      if (dataSet[i].name === "Tezos") {
        this.formatTezData(dataSet[i]);
        break;
      }
    }
  }

  getBlocksData() {
    fetch(
      withPageQuery(connectionConfig.blocks, 1, 10)
    )
      .then(response => response.json())
      .then(parsedJSON => {
        this.formatBlocksData(parsedJSON);
      });
  }

  getTransactionData() {
    let transactionsData = [];
    fetch(withPageQuery(connectionConfig.transactions, 1, 10))
      .then(response => response.json())
      .then(parsedJSON => {
        transactionsData = parsedJSON;
        this.formatTransactionData(transactionsData);
      });
  }

  componentDidMount() {
    this.getBlocksData();
    this.getTransactionData();

    this.timerAdjustTime = setInterval(() => {
      this.adjustTime();
    }, 1000);

    this.timerCheckStatus = setInterval(() => {
      this.checkStatus();
    }, 20000);
    this.getTezData();
  }

  componentWillUnmount() {
    clearInterval(this.timerAdjustTime);
    clearInterval(this.timerCheckStatus);
  }

  formatTezData(dataSet) {
    if (!this.state.isTezLoaded) {
      let temp = [];
      temp.push({
        price: dataSet ? dataSet.quote.USD.price.toFixed(2) : "-",
        volume:
          dataSet
            ? "$ " +
            Number(dataSet.quote.USD.volume_24h.toFixed(2)).toLocaleString()
            : "-",
        priceChange:
          dataSet
            ? dataSet.quote.USD.percent_change_24h.toFixed(2) + " %"
            : "-",
        marketCap:
          dataSet ? "$ " +
            Number(dataSet.quote.USD.market_cap.toFixed(2)).toLocaleString()
            : "-",
        circulatingSupply:
          dataSet ? dataSet.circulating_supply.toFixed(2)
            : "-"
      });
      this.setState({
        tez: temp[0],
        isTezLoaded: true
      });
    }
  }

  getBlockCycleVotingPeriod = (block) => {
    const {
      blocksPerCycle,
      blocksPerPeriod,
    } = blockConfigs
    const {
      blockLevel,
      cycle,
      votingPeriodKind,
      votingPeriodPosition,
      cyclePosition,
    } = block
    let blockData = {
      cycle: "-",
      cycleLeftTime: DEFAUL_DURATION,
      cycleProgress: "-",
      votingPeriod: votingPeriodKind || "-",
      votingPeriodLeftTime: DEFAUL_DURATION,
      votingPeriodProgress: "-",
    }
    const isValidCycle = Number.isInteger(cyclePosition)
    const isValidVotingPeriod = Number.isInteger(votingPeriodPosition) && Number.isInteger(blockLevel)
    const millisecondsPerMinutes = 60 * 1000
    let diffVotingPeriod

    if (isValidCycle) {
      blockData = {
        ...blockData,
        cycle,
        cycleLeftTime: durationFormat({ diff: (blocksPerCycle - cyclePosition) * millisecondsPerMinutes, format: 'H:m:s' }),
        cycleProgress: Math.round(cyclePosition / blocksPerCycle * 10000) / 100,
      }
    }

    if (isValidVotingPeriod) {
      diffVotingPeriod = (blockLevel - votingPeriodPosition + blocksPerPeriod) - blockLevel
      blockData = {
        ...blockData,
        votingPeriod: votingPeriodKind,
        votingPeriodLeftTime: durationFormat({ diff: diffVotingPeriod * millisecondsPerMinutes, format: 'H:m:s' }),
        votingPeriodProgress: Math.round(((blockLevel - (blockLevel - votingPeriodPosition)) / blocksPerPeriod) * 10000) / 100
      }
    }

    return blockData
  }

  getLatestBlock = (blocks) => {
    if (blocks.length <= 0) {
      return null
    }

    const latestBlock = blocks[0] || {}
    const {
      blockLevel,
      level,
      id,
      baker,
      transNum,
      age,
    } = latestBlock
    const cycleVotingPeriod = this.getBlockCycleVotingPeriod(latestBlock)
    let bakerName = getBakerName(baker)

    if (bakerName.replace) {
      bakerName = bakerName.baker;
    } else {
      bakerName = bakerName.baker.substring(0, 7);
    }

    const formattedLatestBlock = {
      level: blockLevel,
      id,
      baker,
      bakerName,
      currLevel: level,
      transNum: transNum,
      age: age,
      ...cycleVotingPeriod,
    };

    return formattedLatestBlock
  }

  formatBlocksData(dataSet) {
    const blocks = dataSet.map((item) => {
      let { block, blocksAlpha, blockStat } = item
      const { level: blockLevel, hash } = block
      const level = [blockLevel, hash];
      let { opsCount } = blockStat;
      opsCount = opsCount !== null ? opsCount : 'Calculating';

      const {
        cycle,
        cyclePosition,
        votingPeriodKind,
        votingPeriodPosition,
      } = blocksAlpha
      let baker = blocksAlpha.baker;
      let blockEmpty = false;

      if (!block) {
        blockEmpty = true;
      }

      if (!baker) {
        baker = "-";
      }

      return {
        key: hash,
        id: hash,
        blockLevel,
        level: blockEmpty ? "-" : level,
        baker: blockEmpty ? "-" : baker,
        transNum: blockEmpty ? "-" : opsCount,
        age: "Calculating",
        timeStamp: blockEmpty
          ? "-"
          : block.timestamp,
        cycle,
        cyclePosition,
        votingPeriodKind: blockEmpty ? "-" : votingPeriodKind,
        votingPeriodPosition,
      }
    })
    const latestBlock = this.getLatestBlock(blocks) || this.state.latestBlock;

    this.setState({
      isBlocksLoaded: true,
      blocksDataList: blocks,
      latestBlock,
    });
  }

  formatTransactionData(dataSet) {
    if (!this.state.isTxnsLoaded) {
      const formattedData = dataSet.map(({ tx, op }, index) => {
        const { opHash, } = op || {}
        const { amount, source, destination, blockTimestamp, uuid } = tx || {}

        return {
          key: `${uuid}-${index}`,
          id: opHash,
          opHash: opHash,
          hash: opHash,
          amount,
          fromTo: [source, destination],
          age: "Calculating",
          timeStamp: blockTimestamp,
        }
      });

      this.setState({
        isTxnsLoaded: true,
        txnsDataList: formattedData,
      });
    }
  }

  adjustTime() {
    if (this.state.isBlocksLoaded) {
      let latestBlock = { ...this.state.latestBlock };
      const blocksData = this.state.blocksDataList.map((block, index) => {
        const age = this.timeDiff(block.timeStamp)

        if (index === 0) {
          latestBlock.age = age;
        }

        return {
          ...block,
          age,
        }
      })

      this.setState({
        blocksDataList: blocksData,
        latestBlock,
      });
    }

    if (this.state.isTxnsLoaded) {
      const transactionsData = this.state.txnsDataList.map(tx => ({
        ...tx,
        age: this.timeDiff(tx.timeStamp),
      }))

      this.setState({
        txnsDataList: transactionsData
      });
    }
  }

  timeDiff = (timestamp) => {
    const { intl } = this.props
    const diff = getCurrentTimeDiff({ timestamp });
    const {
      days,
      hours,
      minutes,
      seconds,
    } = diff
    const diffTime = `${hours}:${minutes}:${seconds}`
    const diffTimeString = days ? `${days}${intl.formatMessage({ id: 'table.days' })} ${diffTime}` : diffTime;

    return diffTimeString;
  }

  render() {
    const rowStyle = {
      width: "100%",
      display: "flex",
      flexFlow: "row wrap",
      align: "top"
    };
    const colStyle = {
      marginBottom: "16px"
    };

    const cardStyle = {
      width: "97%",
      margin: "auto"
    };

    return (
      <LayoutWrapper className="home">
        <Box style={{ padding: 0 }}>
          <div
            style={{
              backgroundImage:
                "url(" + require("../../../image/TezosID_banner.svg") + ")",
              backgroundSize: "cover",
              backgroundColor: "black",
              width: "100%",
              height: "100%"
            }}
          >
            <Row style={rowStyle} justify="around">
              <Col md={12} sm={12} xs={24} style={colStyle} className="home__block-summary">
              <Row type="flex" justify="space-between" align="bottom">
                  <Col lg={16} md={24} sm={12} xs={24} style={colStyle}>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        marginTop: 35
                      }}
                    >
                      <img
                        src={require("../../../image/TezosID_banner_logo_white.svg")}
                        style={{ textAlign: "center" }}
                        width={200}
                        alt="TezosID_banner_logo"
                      />
                    </div>
                  </Col>

                  <Col lg={8} md={24} sm={12} xs={24} style={colStyle}>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontSize: 26,
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>{this.state.tez.marketCap}</Row>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        fontWeight: "bold",
                        color: "white",
                        fontSize: 20,
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>{this.getText("homepage.marketCap")}</Row>
                    </div>
                  </Col>
                </Row>

                <Row style={rowStyle} justify="start">
                  <Col lg={8} md={12} xs={24} style={colStyle}>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontSize: 26,
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>$ {this.state.tez.price}</Row>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontWeight: "bold",
                        fontSize: 20,
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>{this.getText("homepage.priceUsdXtz")}</Row>
                    </div>
                  </Col>

                  <Col lg={8} md={12} xs={24} style={colStyle}>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontSize: 26,
                        fontFamily: "monospace"
                      }}
                    >
                      <Row> {this.state.tez.priceChange} </Row>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontSize: 20,
                        fontWeight: "bold",
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>{this.getText("homepage.24hPriceChange")}</Row>
                    </div>
                  </Col>

                  <Col lg={8} md={12} xs={24} style={colStyle}>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontSize: 26,
                        fontFamily: "monospace"
                      }}
                    >
                      <Row> {this.state.tez.volume} </Row>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontSize: 20,
                        fontWeight: "bold",
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>{this.getText("homepage.24hVolumeUsd")}</Row>
                    </div>
                  </Col>
                </Row>

                <Row style={rowStyle} justify="around">
                  <Col lg={8} md={12} xs={24} style={colStyle}>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontSize: 26,
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>{this.state.block_24h}</Row>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontSize: 20,
                        fontWeight: "bold",
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>{this.getText("homepage.24hBlocks")}</Row>
                    </div>
                  </Col>
                  <Col lg={8} md={12} xs={24} style={colStyle}>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontSize: 26,
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>{this.state.transaction_24h}</Row>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        fontWeight: "bold",
                        color: "white",
                        fontSize: 20,
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>{this.getText("homepage.24Transactions")}</Row>
                    </div>
                  </Col>

                  <Col lg={8} md={12} xs={24} style={colStyle}>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontSize: 26,
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>{this.state.tez.circulatingSupply} ꜩ</Row>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        color: "white",
                        fontWeight: "bold",
                        fontSize: 20,
                        fontFamily: "monospace"
                      }}
                    >
                      <Row>{this.getText("homepage.circulatingSupply")}</Row>
                    </div>
                  </Col>
                </Row>
              </Col>
              <LatestBlock latestBlock={this.state.latestBlock} />
            </Row>
          </div>
        </Box>

        <Row style={rowStyle} type="flex" justify="around">
          <Col md={12} sm={12} xs={24} style={colStyle}>
            <Card
              bordered={true}
              title={
                <p style={{ color: "#818a91" }}>
                  {this.getText("homepage.last10TezosBlocks")}
                </p>
              }
              extra={
                <div>
                  <i
                    className="ion-cube"
                    style={{
                      fontSize: 30,
                      color: "#818a91"
                    }}
                  />
                </div>
              }
              style={cardStyle}
              className="home__top-ten-blocks"
            >
              <SimpleView
                pagination={false}
                columns={miniBlocksColumns}
                dataSource={this.state.blocksDataList}
                loading={!this.state.isBlocksLoaded}
              />
              <p style={{ textAlign: "right" }}>
                (<Link to="/blocks">{this.getText("last10.viewAll")}</Link>)
              </p>
            </Card>
          </Col>

          <Col md={12} sm={12} xs={24} style={colStyle}>
            <Card
              bordered={true}
              title={
                <p style={{ color: "#818a91" }}>
                  {this.getText("homepage.last10TezosTransactions")}
                </p>
              }
              extra={
                <div>
                  <i
                    className="ion-arrow-swap"
                    style={{
                      fontSize: 30,
                      color: "#818a91"
                    }}
                  />
                </div>
              }
              style={cardStyle}
              className="home__top-ten-trans"
            >
              <SimpleView
                pagination={false}
                columns={miniTransactionsColumns}
                dataSource={this.state.txnsDataList}
                loading={!this.state.isTxnsLoaded}
              />
              <p style={{ textAlign: "right" }}>
                (
                <Link to="/transactions">{this.getText("last10.viewAll")}</Link>
                )
              </p>
            </Card>
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}

export default injectIntl(Home)
