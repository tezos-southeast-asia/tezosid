import React from "react";
import { Col, Row } from "antd";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";

import LatestBlockWrapper from "./latestBlock.style";

const InfoCol = ({ title, children, style, lg }) => {
  return (
    <Col
      lg={lg || 6} md={12} xs={24}
      className="home__latest-block__info-col"
    >
      <div
        className="home__latest-block__info-content"
        style={style}
      >
        {children}
      </div>
      <div
        className="home__latest-block__info-title"
      >
        <FormattedMessage id={title} />
      </div>
    </Col>
  )
}

const LatestBlock = (props) => {
  const {
    latestBlock,
  } = props
  const {
    level,
    baker,
    bakerName,
    transNum,
    age,
    id,
    cycle,
    cycleLeftTime,
    cycleProgress,
    votingPeriod,
    votingPeriodLeftTime,
    votingPeriodProgress,
  } = latestBlock

  return (
    <LatestBlockWrapper
      md={12} sm={12} xs={24}
      className="home__latest-block"
    >
      <Row
        type="flex"
        justify="center"
        className="home__latest-block__title"
      >
        <FormattedMessage id="homepage.latestBlock" />
      </Row>
      <Row
        type="flex"
        justify="start"
        className="home__latest-block__info"
      >
        <div
          className="home__latest-block__info-row"
        >
          <InfoCol
            title="homepage.level"
          >
            {id ? (<Link to={`/blocks/${id}`}>{level}</Link>) : level }
          </InfoCol>
          <InfoCol
            title="homepage.baker"
            style={{
              overflow: "hidden",
              whiteSpace: "nowrap",
              textOverflow: "ellipsis",
            }}
          >
            <Link to={baker}>
              {bakerName}
            </Link>
          </InfoCol>
          <InfoCol
            title="table.#op"
          >
            {transNum}
          </InfoCol>
          <InfoCol
            title="homepage.age"
          >
            {age}
          </InfoCol>
        </div>
        <div
          className="home__latest-block__info-cycle"
        >
          <InfoCol
            title="homepage.cycle"
            lg={8}
          >
            {cycle}
          </InfoCol>
          <InfoCol
            title="homepage.cycleLeftTime"
            lg={8}
          >
            <FormattedMessage
              id="homepage.ageTime"
              values={cycleLeftTime}
            />
          </InfoCol>
          <InfoCol
            title="homepage.cycleProgress"
            lg={8}
          >
            {`${cycleProgress}%`}
          </InfoCol>
        </div>
        <div
          className="home__latest-block__info-voting-period"
        >
          <InfoCol
            title="homepage.votingPeriod"
            lg={8}
          >
            <Link to="/voting-periods">
              { votingPeriod === "-" ? votingPeriod : <FormattedMessage id={`votingPeriods.${votingPeriod}`} /> }
            </Link>
          </InfoCol>
          <InfoCol
            title="homepage.votingLeftTime"
            lg={8}
          >
            <FormattedMessage
              id="homepage.ageTime"
              values={votingPeriodLeftTime}
            />
          </InfoCol>
          <InfoCol
            title="homepage.votingProgress"
            lg={8}
          >
            {`${votingPeriodProgress}%`}
          </InfoCol>
        </div>
      </Row>
    </LatestBlockWrapper>
  )
}

export default LatestBlock
