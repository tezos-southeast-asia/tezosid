import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import queryString from "query-string";

import { routes } from "../../configs/hashConfigs";

class Search extends Component {
  static defaultProps = {
    location: {}
  }

  constructor(props) {
    super(props)

    const { search = '' } = props.location
    const { hash } = queryString.parse(search)
    const redirectPath = this.getRedirectPath(hash)
    this.state = {
      redirectPath,
    }
  }

  getRedirectPath = (hash) => {
    const notFoundPath = '/notfound'
    const blockLevel = Number(hash)
    let hashPath

    if (!hash) return notFoundPath
  
    // find matched hash pattern
    routes.some(({ prefixes, path }) => {
      const matchIndex = prefixes.findIndex(prefix => hash.indexOf(prefix) === 0)

      if (matchIndex !== -1) {
        hashPath = path
      }

      return matchIndex !== -1
    })

    if (hashPath) {
      return `${hashPath}${hash}`
    } else if (Number.isInteger(blockLevel) && blockLevel >= 0) {
      return `blocks/${blockLevel}`
    } else {
      return notFoundPath
    }
  }

  render() {
    const { redirectPath } = this.state

    return <Redirect to={redirectPath} />
  }
}

export default Search
