import React, { Component } from "react";
import { Input } from "antd";
import { connect } from "react-redux";

import { updateProfile } from "../../../redux/auth/actions";

import Form from "../../../components/uielements/form";
import Button from "../../../components/uielements/button";
import Notification from "../../../components/notification";
import { FormattedMessage } from "react-intl";

const FormItem = Form.Item;

class FormWIthSubmissionButton extends Component {
  state = {
    confirmDirty: false,
  };

  static defaultProps = {
    updateProfile: () => { },
    updateProfileMsg: null
  }

  componentDidUpdate(prevProps) {
    const { updateProfileMsg } = this.props;
    if (prevProps.updateProfileMsg !== updateProfileMsg) {
      Notification("info", updateProfileMsg.message);
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const { updateProfile } = this.props;
        updateProfile(values.displayName);
      }
    });
  };

  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 14,
          offset: 6
        }
      }
    };
    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem
          {...formItemLayout}
          label={<FormattedMessage id={`profile.newProfileName`} />}
          hasFeedback
        >
          {getFieldDecorator("displayName", {
            rules: [
              {
                required: true,
                message: <FormattedMessage id={`profile.profileNameMessage`} />
              },
            ]
          })(<Input type="text" onBlur={this.handleConfirmBlur} />)}
        </FormItem>
        <FormItem {...tailFormItemLayout} >
          <Button type="primary" htmlType="submit">
            <FormattedMessage id={`profile.updateButton`} />
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const mapStateToProps = (state) => ({
  updateProfileMsg: state.Auth.updateProfileMsg,
})

const WrappedFormWIthSubmissionButton = Form.create()(FormWIthSubmissionButton);

export default connect(
  mapStateToProps,
  { updateProfile }
)(WrappedFormWIthSubmissionButton)
