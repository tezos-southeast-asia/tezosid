import React, { Component } from 'react';
import { Input } from 'antd';
import { connect } from "react-redux";

import { updatePwd } from "../../../redux/auth/actions";

import Form from '../../../components/uielements/form';
import Button from '../../../components/uielements/button';
import Notification from '../../../components/notification';
import { FormattedMessage } from "react-intl";
const FormItem = Form.Item;

class FormWIthSubmissionButton extends Component {
  state = {
    confirmDirty: false,
  };

  static defaultProps = {
    updatePwd: () => { },
    updatePwdMsg: null
  }

  componentDidUpdate(prevProps) {
    const { updatePwdMsg } = this.props;
    if (prevProps.updatePwdMsg !== updatePwdMsg) {
      Notification("info", updatePwdMsg.message);
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const { updatePwd } = this.props;
        updatePwd(values.password);
      }
    });
  };
  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  checkPassword = (_rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback(<FormattedMessage id={`profile.inconsistentPassword`} />);
    } else {
      callback();
    }
  };

  checkConfirm = (_rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    };
    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem {...formItemLayout} label={<FormattedMessage id={`profile.newPassword`} />} hasFeedback>
          {getFieldDecorator('password', {
            rules: [
              {
                required: true,
                message: <FormattedMessage id={`profile.newPasswordMessage`} />,
              },
              {
                validator: this.checkConfirm,
              },
            ],
          })(<Input type="password" autoComplete="new-password" />)}
        </FormItem>
        <FormItem {...formItemLayout} label={<FormattedMessage id={`profile.confirmNewPassword`} />} hasFeedback>
          {getFieldDecorator('confirm', {
            rules: [
              {
                required: true,
                message: <FormattedMessage id={`profile.confirmNewPasswordMessage`} />,
              },
              {
                validator: this.checkPassword,
              },
            ],
          })(<Input type="password" autoComplete="new-password" onBlur={this.handleConfirmBlur} />)}
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            <FormattedMessage id={`profile.changePasswordButton`} />
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const mapStateToProps = (state) => ({
  updatePwdMsg: state.Auth.updatePwdMsg,
})

const WrappedFormWIthSubmissionButton = Form.create()(FormWIthSubmissionButton);

export default connect(
  mapStateToProps,
  { updatePwd }
)(WrappedFormWIthSubmissionButton)

