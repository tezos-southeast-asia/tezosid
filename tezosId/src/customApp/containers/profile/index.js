import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Col, Row, Spin } from "antd";
import { Helmet } from "react-helmet";
import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import ProfileCard from "./profileCard";
import ProfileTableCard from "./profileTableCard";
import AccountWrapper from "../accounts/account.style";
import { setBookmarks, removeBookmarkFromProfile, updateBookmarkAlias, getBookmarkAccInfo, getTransactionsData, setEditValue, setRecordIndex } from '../../../redux/bookmarklist/actions';
import { addEmailNotification, removeEmailNotification, updateNotificationAlias } from '../../../redux/notification/actions';

class Profile extends Component {
  static propTypes = {
    user: PropTypes.object,
    initTrackingAccounts: PropTypes.func,
    getTrackingAccounts: PropTypes.func
  }

  static defaultProps = {
    user: null,
    totalRecords: 1,
    pageSize: 3,
    currentPage: 1,
    startIndex: 0,
    lastIndex: 3,
    initTrackingAccounts: () => { },
    getTrackingAccounts: () => { }
  }

  componentDidMount() {
    this.props.initTrackingAccounts()
    const { uid, setBookmarks, setRecordIndex } = this.props;
    setRecordIndex(this.props.currentPage, this.props.pageSize);
    setBookmarks(uid);
  }

  componentDidUpdate(prevProps) {
    const { bookmarklist, startIndex, lastIndex } = this.props;
    if (prevProps.bookmarklist.bookmarks !== bookmarklist.bookmarks) {
      this.getDetails(startIndex, lastIndex);
    }
  }

  getDetails = (startIndex, lastIndex) => {
    const { bookmarklist, getTransactionsData, getBookmarkAccInfo } = this.props;
    getBookmarkAccInfo(bookmarklist.bookmarks, startIndex, lastIndex);
    getTransactionsData(bookmarklist.bookmarks, startIndex, lastIndex);
  }

  removeButtonHandler = (e) => {
    const accountHash = e.target.value;
    const { uid, user, removeEmailNotification, removeBookmarkFromProfile } = this.props;
    const userEmail = user.email;
    removeBookmarkFromProfile(uid, accountHash);
    removeEmailNotification(userEmail, accountHash);
  }

  inputHandler = (e) => {
    let { accountInfos } = this.props
    const id = e.target.id;
    const text = e.target.value;
    accountInfos[id].editedLabel = text;
  }



  editButtonHandler = (e) => {
    const target = e.target.value;
    let edit;
    let { accountInfos, setEditValue } = this.props;
    const trigger = accountInfos[target].edit;
    if (trigger) {
      edit = false;
      const { uid, updateBookmarkAlias, updateNotificationAlias } = this.props;
      updateBookmarkAlias(uid, target, accountInfos[target].editedLabel);
      updateNotificationAlias(this.props.user.email, target, accountInfos[target].editedLabel)
    }
    else {
      edit = true;
    }
    setEditValue(accountInfos, target, edit);
  }

  cancelButtonHandler = (e) => {
    const { setEditValue } = this.props;
    const target = e.target.value;
    let { accountInfos } = this.props;
    const edit = false;
    setEditValue(accountInfos, target, edit);
  }

  pageOnChange = () => ({
    trackingAccounts: this.props.getTrackingAccounts,
  });

  subcriptionButtonHandler = (e) => {
    const accountHash = e.target.value;
    const userEmail = this.props.user.email;
    const { uid, notification, addEmailNotification, removeEmailNotification, bookmarklist } = this.props;


    const bookmarkIndex = bookmarklist.bookmarks.findIndex(bookmark => bookmark.hash === accountHash);

    if (notification.emailSubscription.find(x => x.accountHash === accountHash).subscriptionTxt === "Subscribe") {
      //if (notification.emailSubscription[notificationIndex].subscriptionTxt === "Subscribe") {
      addEmailNotification(userEmail, accountHash, uid, bookmarklist.bookmarks[bookmarkIndex].alias);
    }
    else {
      removeEmailNotification(userEmail, accountHash, uid);
    }
  }

  onShowSizeChange = (current, pageSize) => {
    const { setRecordIndex } = this.props;
    setRecordIndex(current, pageSize);
    const startIndex = current * pageSize - pageSize;
    const lastIndex = current * pageSize;
    this.getDetails(startIndex, lastIndex);
  }

  onChange = (current, pageSize) => {
    const { setRecordIndex } = this.props;
    setRecordIndex(current, pageSize);
    const startIndex = current * pageSize - pageSize;
    const lastIndex = current * pageSize;
    this.getDetails(startIndex, lastIndex);
  }

  render() {
    const { user, bookmarklist, accountInfos, verificationEmailMsg, transactionsData, balance, amount, startIndex, lastIndex, currentPage, pageSize, notification } = this.props
    return (
      <AccountWrapper>
        <Helmet>
          <meta name="robots" content="noindex" />
        </Helmet>
        <LayoutContentWrapper>
          <Row type="flex" justify="space-around">
            <Col lg={7} md={24} sm={24} xs={24}>
              <ProfileCard
                calculateNetTransaction={this.calculateNetTransaction}
                amount={amount}
                balance={balance}
                verificationEmailMsg={verificationEmailMsg}
              />
            </Col>

            <Col lg={16} md={24} sm={24} xs={24}>
              <Spin spinning={bookmarklist.isLoading}>
                <ProfileTableCard
                  user={user}
                  pageOnChange={this.pageOnChange}
                  emailNotilist={notification.emailSubscription}
                  remove={this.removeButtonHandler}
                  bookmarklist={bookmarklist.bookmarks}
                  transactionData={transactionsData}
                  accountInfo={accountInfos}
                  editButtonHandler={this.editButtonHandler}
                  inputHandler={this.inputHandler}
                  emailSubsHandler={this.subcriptionButtonHandler}
                  cancelButtonHandler={this.cancelButtonHandler}
                  startIndex={startIndex}
                  lastIndex={lastIndex}
                  onShowSizeChange={this.onShowSizeChange}
                  onChange={this.onChange}
                  totalRecords={this.props.totalRecords}
                  currentPage={currentPage}
                  pageSize={pageSize}
                />
              </Spin>
            </Col>
          </Row>
        </LayoutContentWrapper>
      </AccountWrapper>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.Auth.user,
  verificationEmailMsg: state.Auth.verificationEmailMsg,
  uid: state.Auth.user.uid,
  bookmarklist: state.Bookmarklist,
  notification: state.Notification,
  accountInfos: state.Bookmarklist.bookmarkAccountInfo,
  transactionsData: state.Bookmarklist.transactionData,
  balance: state.Bookmarklist.balance,
  amount: state.Bookmarklist.amount,
  totalRecords: state.Bookmarklist.totalRecords,
  startIndex: state.Bookmarklist.startIndex,
  lastIndex: state.Bookmarklist.lastIndex,
  currentPage: state.Bookmarklist.currentPage,
  pageSize: state.Bookmarklist.pageSize
})

const mapDispatchToProps = dispatch => {
  return {
    setBookmarks: (userUid) => dispatch(setBookmarks(userUid)),
    updateBookmarkAlias: (userUid, accountHash, alias) => dispatch(updateBookmarkAlias(userUid, accountHash, alias)),
    removeBookmarkFromProfile: (userUid, accountHash) => dispatch(removeBookmarkFromProfile(userUid, accountHash)),
    addEmailNotification: (userEmail, accountHash, uid, alias) => dispatch(addEmailNotification(userEmail, accountHash, uid, alias)),
    removeEmailNotification: (userEmail, accountHash, uid) => dispatch(removeEmailNotification(userEmail, accountHash, uid)),
    updateNotificationAlias: (userEmail, accountHash, alias) => dispatch(updateNotificationAlias(userEmail, accountHash, alias)),
    getBookmarkAccInfo: (bookmarks, startIndex, lastIndex) => dispatch(getBookmarkAccInfo(bookmarks, startIndex, lastIndex)),
    getTransactionsData: (bookmarks, startIndex, lastIndex) => dispatch(getTransactionsData(bookmarks, startIndex, lastIndex)),
    setEditValue: (accountInfos, accountHash, edit) => dispatch(setEditValue(accountInfos, accountHash, edit)),
    setRecordIndex: (currentPage, pageSize) => dispatch(setRecordIndex(currentPage, pageSize)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile)
