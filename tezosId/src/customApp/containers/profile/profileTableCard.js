import React, { Component } from "react";
import { Card, Input, Pagination } from "antd";
import { FormattedMessage } from "react-intl";
import Button from "../../../components/uielements/button";
import Table from "../../components/tableViews/table";
import { accountProfileColumns } from "../../configs/tableColumnsConfigs";
import Tag from "../../components/tag";
import Tabs, { TabPane } from "../../../components/uielements/tabs";
import { Col, Row } from "antd";
import Amount from "../../components/amount";
import { Link } from "react-router-dom";
import FormValidation from "./changePassword"
import UpdateProfile from "./updateProfile"
import PropTypes from "prop-types";

class ProfileTableCard extends Component {
  static propTypes = {
    user: PropTypes.object,
  }
  static defaultProps = {
    isLoading: false,
    trackingAccounts: {
      data: [],
      currentPage: 0,
      isLoading: false,
      pageOnChange: () => { }
    },
    user: {},
  }



  getTabPanes = () => {
    const { isLoading, bookmarklist, transactionData, accountInfo, remove, editButtonHandler, inputHandler, cancelButtonHandler, startIndex, lastIndex, currentPage, pageSize, emailNotilist, emailSubsHandler } = this.props
    const filteredList = bookmarklist.slice(startIndex, lastIndex);
    const dataTables = filteredList.map((bookmark) => {
      const hash = bookmark.hash;
      const emailNotiObj = emailNotilist.find(obj => obj.accountHash === bookmark.hash);
      const emailSub = emailNotiObj ? emailNotiObj : { accountHash: "", subscriptionTxt: "Loading...", isLoading: true };
      let balance = 0;
      let edit = false;
      let undefine = true;
      if (accountInfo[hash] !== undefined) {
        balance = accountInfo[hash].balance;
        edit = accountInfo[hash].edit;
        undefine = false;
      }
      const title = (edit, undefine, account) => {
        if (!edit) {
          let label = hash
          if (!undefine) {
            label = account.label;
          }
          return <Link to={`/${hash}`}>{label}</Link>;
        }
        else {
          return <Input id={hash} onChange={inputHandler} style={{ width: "600px" }} defaultValue={account.label} />
        }
      }
      const editAddrIcn = (edit) => {
        if (edit) {
          return <div>
            <Button icon="save" type="primary" value={hash} onClick={editButtonHandler}>save</Button>
            {"  "}
            <Button icon="close" type="primary" value={hash} onClick={cancelButtonHandler}>Cancel</Button>
          </div>;
        }
        else {
          return <Button icon="edit" type="primary" value={hash} onClick={editButtonHandler}>Edit</Button>;
        }

      }
      const mailPref = <Button icon="mail" type="primary" style={{ marginRight: "2%" }} value={emailSub.accountHash} onClick={emailSubsHandler}
        loading={emailSub.isLoading} >{emailSub.subscriptionTxt}</Button>;
      const removeAddrIcn = <Button icon="delete" type="danger" onClick={remove} value={hash}>Remove account</Button>;
      const details = <p style={{ textAlign: "left", marginBottom: "20px" }}>
        Balance: <b>{<Amount amount={balance}></Amount>}</b> <br />
        {/* Rewards: {"xx.xx"} */}
      </p>;

      return (
        <div key={hash} style={{ paddingBottom: 30 }}>
          <Row type="flex" justify="space-around">
            <Col md={24} sm={24} xs={24}>
              <Card title={title(edit, undefine, accountInfo[hash])} extra={editAddrIcn(edit)}>
                {details}
                <Table
                  pagination={false}
                  pageTitle=""
                  dataSource={transactionData[hash]}
                  columns={accountProfileColumns}
                  loading={isLoading}
                />
                {/* <p style={{ textAlign: "left", marginTop: "20px" }}> */}

                <Row type="flex" style={{ marginTop: "20px", textAlign: "left" }} >
                  <Col lg={4} md={24} sm={24} xs={24}>
                    {mailPref}
                  </Col>
                  <Col lg={4} md={24} sm={24} xs={24} >
                    {removeAddrIcn}
                  </Col>
                </Row>
                {/* </p> */}
              </Card>
            </Col>
          </Row>
        </div>)
    });
    const accountTabs = (
      <TabPane
        tab={
          <div>
            <FormattedMessage id={`profile.trackingAccounts`} />
            <Tag>{bookmarklist.length}</Tag>
          </div>
        }
        key={`1`}
      >
        <Pagination
          showSizeChanger
          onShowSizeChange={this.props.onShowSizeChange}
          onChange={this.props.onChange}
          pageSizeOptions={['3', '5', '10']}
          defaultPageSize={3}
          current={currentPage}
          //defaultCurrent={this.props.currentPage}
          total={this.props.totalRecords}
          pageSize={pageSize}
        /><br />
        {dataTables}
        <Pagination
          showSizeChanger
          onShowSizeChange={this.props.onShowSizeChange}
          onChange={this.props.onChange}
          pageSizeOptions={['3', '5', '10']}
          defaultPageSize={3}
          current={currentPage}
          //defaultCurrent={this.props.currentPage}
          total={this.props.totalRecords}
          pageSize={pageSize}
        />
      </TabPane>);

    const settingTabs = (
      <TabPane
        tab={
          <div>
            <FormattedMessage id={`profile.settings`} />
          </div>
        }
        key={`2`}>
        {this.showForm()}
        <Card title={<FormattedMessage id={`profile.updateProfileDetails`} />}>
          <UpdateProfile />
        </Card>
      </TabPane>);
    return [accountTabs, settingTabs];
  }

  showForm = () => {
    const { providerData } = this.props.user;
    let changePwdForm = null;

    providerData.forEach(obj => {
      if (obj.providerId === "password") {
        changePwdForm = (<Card title={<FormattedMessage id={`profile.changePassword`} />}>
          <FormValidation />
        </Card>);
      }
    });
    return changePwdForm;
  }

  render() {
    const tabPanes = this.getTabPanes();
    
    return (
      <Card>
        <Tabs defaultActiveKey='1'>
          {tabPanes}
        </Tabs>
      </Card>
    );
  }
}
export default ProfileTableCard
