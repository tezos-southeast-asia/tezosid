import React, { Component } from "react";
import PropTypes from "prop-types";
import { Card } from "antd";
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";

import { verificationEmail } from "../../../redux/auth/actions";

import Box from "../../../components/utility/box";
import Button from "../../../components/uielements/button";
import AccountAvatarBox from "../accounts/accountAvatarBox";

class ProfileCard extends Component {
  static propTypes = {
    user: PropTypes.object,
  }

  static defaultProps = {
    user: {},
    verificationEmailMsg: null,
    verificationEmail: () => { },
  }

  sendVerificationEmail = () => {
    const { verificationEmail } = this.props;
    verificationEmail();
  }

  isVerified = (verified) => {
    if (!verified) {
      return (
        <div>
          <FormattedMessage id="profile.verified1" />
          <br />
          <FormattedMessage id="profile.verified2" />
          <Link to="#" onClick={this.sendVerificationEmail}>
            <FormattedMessage id="profile.verified3" />
          </Link>
          <FormattedMessage id="profile.verified4" />
        </div>);
    } else {
      return null;
    }
  }

  render() {
    const {
      user = {},
      verificationEmailMsg,
    } = this.props
    const { email, uid, emailVerified, displayName = '' } = user || {}
    // const {amount,balance}=this.props;

    return (
      <div>
        <Card>
          <div className="account__info">
            <Box className="account__name-box">
              <span className="account__name">{displayName}</span>
            </Box>
            <AccountAvatarBox name={uid} />
            {email && (
              <Box className="account__email-box">
                <FormattedMessage id="accounts.email" />
                <div className="account__email">{email}</div>
                {this.isVerified(emailVerified)}
                {verificationEmailMsg && (
                  <>
                    <br />
                    <p>{verificationEmailMsg.message}</p>
                  </>
                )
                }
              </Box>
            )
            }
            <Box className="account__signout-box">
              <Link className="account__signout" to="/signout">
                <Button icon="logout" size="large">
                  <FormattedMessage id="profile.logout" />
                </Button>
              </Link>
            </Box>
          </div>
        </Card>
        {/* <Card>
        <div style={{textAlign:"center"}}>
          <span className="account__name">{"Overview"}</span>
          <br/>
          Total Balance for all monitored Accounts: <br/><b>{balance}</b>
          <br/><br/>
          In the last 5 Transaction, your monitored accounts has a net transaction value of <b>{amount}</b>
        </div>
      </Card> */}
      </div>)
  }
}

const mapStateToProps = (state) => ({
  user: state.Auth.user,
})

export default connect(
  mapStateToProps,
  { verificationEmail }
)(ProfileCard)
