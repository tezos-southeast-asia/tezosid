import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { doubleEndorsementColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchDoubleEndorsement } from "../../../redux/doubleEndorsement/actions";

class DoubleEndorsement extends Component {
  render() {
    const {
      isLoading,
      doubleEndorsement = {},
      isError,
      fetchDoubleEndorsement
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = doubleEndorsement

    return (
      <TablePage
      totalPage={total}
      currentPage={currentPage}
      pageSize={pageSize}
      fail={isError}
      pageTitle={<IntlMessages id="doubleEndorsements.title" />}
      dataSource={data}
      columns={doubleEndorsementColumns}
      loading={isLoading}
      isLink={true}
      pageOnChange={fetchDoubleEndorsement}
    />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.DoubleEndorsement
})

const mapDispatchToProps = dispatch => {
  return {
    fetchDoubleEndorsement: (props) => dispatch(fetchDoubleEndorsement(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DoubleEndorsement)