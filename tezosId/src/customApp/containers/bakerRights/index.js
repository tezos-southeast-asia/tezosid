import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import queryString from "query-string";

import Table from "../../components/tableViews/table";
import { bakerRightsColumn } from "../../configs/tableColumnsConfigs";
import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import Box from "../../../components/utility/box";
import LinkTabs, { TabPane, LinkTab } from "../../components/linkTabs";
import {
  vaildTabs,
  updateBakerRightsTab,
  fetchBakerRightsRecords,
  startLevelBackgroundSync,
  stopLevelBackgroundSync,
} from "../../../redux/bakerRights/actions";

class BakerRights extends Component {
  static defaultProps = {
    location: {},
    history: {},
    level: '',
    upcoming: {},
    past: {},
    tabs: {},
    updateBakerRightsTab: () => {},
    fetchBakerRightsRecords: () => {},
    startLevelBackgroundSync: () => {},
    stopLevelBackgroundSync: () => {},
  }

  componentDidMount() {
    const {
      location,
      history,
      updateBakerRightsTab,
      startLevelBackgroundSync,
    } = this.props
    const { tab } = queryString.parse(location.search)
    const isVaildTab = this.verifyTab(tab)

    if (isVaildTab) {
      updateBakerRightsTab({ tab })
      startLevelBackgroundSync()
    } else {
      history.push({ ...location, search: '' })
    }
  }

  componentDidUpdate(prevProps) {
    const {
      location,
      updateBakerRightsTab,
      fetchBakerRightsRecords,
      history,
      level,
    } = this.props
    const { level: preLevel } = prevProps
    const { p, tab, n } = queryString.parse(location.search)
    const { p: prevP, tab: prevTab, n: prevN } = queryString.parse(prevProps.location.search)
    const isVaildTab = this.verifyTab(tab)

    if (isVaildTab) {
      if (tab !== prevTab) {
        updateBakerRightsTab({ tab })
      }
      if (p !== prevP || tab !== prevTab || n !== prevN || level !== preLevel) {
        fetchBakerRightsRecords({ level, tab, currentPage: p, pageSize: n })
      }
    } else {
      history.push({ ...location, search: '' })
    }
  }

  componentWillUnmount() {
    this.props.stopLevelBackgroundSync()
  }

  verifyTab = (tab) => {
    const isVaildTab = tab ? vaildTabs.indexOf(tab) !== -1 : true

    return isVaildTab
  }

  getTabPanes = () => {
    const {
      upcoming,
      past,
      location,
    } = this.props
    const tabs = [
      {
        type: 'upcoming',
        columns: bakerRightsColumn,
        data: upcoming,
      },
      {
        type: 'past',
        columns: bakerRightsColumn,
        data: past,
      }
    ]
    const tabPanes = tabs.map(({ type, data, columns }) => {
      const newSearch = queryString.stringify({
        tab: type
      })
      const { data: tableData, total, isLoading, isError, currentPage, pageSize } = data

      return (
        <TabPane
          tab={
            <LinkTab
              to={{
                ...location,
                search: `?${newSearch}`
              }}
            >
              <FormattedMessage id={`table.${type}`} />
            </LinkTab>
          }
          key={type}
        >
          <Table
            totalPage={total}
            currentPage={currentPage}
            pageSize={pageSize}
            pageTitle=""
            dataSource={tableData}
            columns={columns}
            fail={isError}
            loading={isLoading}
            isLink={true}
          />
        </TabPane>
      )
    })

    return tabPanes
  }

  render() {
    const { tabs } = this.props
    const tabPanes = this.getTabPanes()

    return (
      <LayoutContentWrapper
        pageTitle={<FormattedMessage id="bakerRights.title" />}
      >
        <Box>
          <LinkTabs
            {...tabs}
          >
            {tabPanes}
          </LinkTabs>
        </Box>
      </LayoutContentWrapper>
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.BakerRights
})

const mapDispatchToProps = dispatch => {
  return {
    updateBakerRightsTab: (props) => dispatch(updateBakerRightsTab(props)),
    fetchBakerRightsRecords: (props) => dispatch(fetchBakerRightsRecords(props)),
    startLevelBackgroundSync: () => dispatch(startLevelBackgroundSync()),
    stopLevelBackgroundSync: () => dispatch(stopLevelBackgroundSync()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BakerRights)
