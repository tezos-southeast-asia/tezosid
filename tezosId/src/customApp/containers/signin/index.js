import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { devlogList } from "../../configs/devlogList";
import { login, emailPwdLogin } from "../../../redux/auth/actions";
import CommonMeta from "../../components/commonMeta";
import Input from "../../../components/uielements/input";
import Button from "../../../components/uielements/button";
import Spin from "../../../components/uielements/spin";
import HelperText from '../../../components/utility/helper-text';
import IntlMessages from "../../../components/utility/intlMessages";
import SignInStyleWrapper from "./signin.style";
import { Card, Col } from 'antd';

export class SignIn extends Component {
  static defaultProps = {
    isLoggedIn: false,
    loading: false,
    loginError: null,
    login: () => { },
    emailPwdLogin: () => { },
  }

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      pwd: '',
      visible: false,
    };
  }

  updateEmailValue(evt) {
    this.setState({
      email: evt.target.value
    });
  }

  updatePwdValue(evt) {
    this.setState({
      pwd: evt.target.value
    });
  }

  handleLogin = (provider) => {
    const { login } = this.props;
    login(provider);
  };

  handleEmailPwdLogin = () => {
    const { emailPwdLogin } = this.props;
    emailPwdLogin(this.state.email, this.state.pwd);
  };

  handleLoginSignup = () => {
    if (this.state.hideSignUpForm) {
      this.setState({
        hideSignUpForm: false
      });
    } else {
      const { emailPwdSignup } = this.props;
      emailPwdSignup(this.state.email, this.state.pwd, this.state.confirmPwd);
    }
  }

  getDevlogs() {
    let i = 0;
    const devlogs = [];
    for (i; i < 2; i++) {
      const entries = devlogList[i].entries;
      const log = (
        <div key={i}>
          <h3 style={{ marginBottom: '5px' }}>{devlogList[i].date}</h3>
          <ul style={{ listStyleType: 'disc' }}>
            {entries.map((entry, index) => (<li key={`devlog-entry-${index}`}>{entry}</li>))}
          </ul>
          <br />
        </div>
      )
      devlogs.push(log);
    }
    return devlogs;
  }

  render() {
    const home = { pathname: "/" };
    const { isLoggedIn, loading, loginError } = this.props;

    if (isLoggedIn) {
      return <Redirect to={home} />;
    }

    return (
      <SignInStyleWrapper className="isoSignInPage">
        <CommonMeta />
        <div className="updateChangesList">
          <Col xs={0} sm={0} md={24} lg={24} xl={24}>
            <Card
              title="Recent Updates"
              extra={<a href="/devlog">More</a>}
            >
              {this.getDevlogs()}
            </Card>
          </Col>
        </div>
        <div className="isoLoginContentWrapper">
          <div className="isoLoginContent">
            <div className="isoLogoWrapper">
              <Link to="/">
                <IntlMessages id="page.signInTitle" />
              </Link>
            </div>
            {loginError && (
              <HelperText text={<IntlMessages id="page.signInError" />} />
            )
            }

            <div className="isoSignInForm">
              <div className="isoInputWrapper">
                <Input size="large" value={this.state.email} onChange={evt => this.updateEmailValue(evt)} placeholder="Email" />
              </div>

              <div className="isoInputWrapper">
                <Input size="large" value={this.state.pwd} onChange={evt => this.updatePwdValue(evt)} type="password" placeholder="Password" />
              </div>

              <Button type="primary" onClick={this.handleEmailPwdLogin}>
                <IntlMessages id="page.signInButton" />
              </Button>

              <div className="isoInputWrapper isoOtherLogin">
                <Button onClick={this.handleLogin.bind(this, "google")} type="primary loginBtn btnGoogle">
                  <IntlMessages id="page.signInGoogle" />
                </Button>
              </div>
              <div className="isoCenterComponent isoHelperWrapper">
                <Link to="/forgotpassword" className="isoForgotPass">
                  <IntlMessages id="page.signInForgotPass" />
                </Link>
                <Link to="/signup">
                  <IntlMessages id="page.signInCreateAccount" />
                </Link>
              </div>
            </div>
          </div>
        </div>
        {loading && (
          <div className="isoLoginLoadingWrapper">
            <Spin spinning />
          </div>
        )
        }
      </SignInStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    loginError: state.Auth.loginError,
    loading: state.Auth.loading,
    isLoggedIn: state.Auth.isLoggedIn
  }),
  { login, emailPwdLogin }
)(SignIn);
