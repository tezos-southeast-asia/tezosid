import React, { Component } from "react";
import LayoutContentWrapper from "../../../components/utility/layoutWrapper";
import { FormattedMessage } from "react-intl";
import { Card, Col, Row } from "antd";

class ApiMonitoring extends Component {
  render() {
    const rowStyle = {
      width: "100%",
      align: "top",
      textAlign: "centre",
      margin: 0
    };
    const colStyle = {
      marginBottom: "16px"
    };
    return (
      <LayoutContentWrapper pageTitle={<FormattedMessage id="api.title" />}>
        <Card>
          <Row style={rowStyle}>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/6957c53c"
                  alt="Uptime Report for Blocks API: Last 30 days"
                  title="Uptime Report for Blocks API: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/3c860a6d"
                  alt="Uptime Report for Transactions API: Last 30 days"
                  title="Uptime Report for Transactions API: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/1225885a"
                  alt="Uptime Report for api.tezos.id-babylonnet: Last 30 days"
                  title="Uptime Report for api.tezos.id-babylonnet: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/7fd1f428"
                  alt="Uptime Report for api.tezos.id: Last 30 days"
                  title="Uptime Report for api.tezos.id: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
          </Row>

          <Row style={rowStyle}>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/1efb3032"
                  alt="Uptime Report for node1.lax: Last 30 days"
                  title="Uptime Report for node1.lax: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/14d8b419"
                  alt="Uptime Report for node1.sg: Last 30 days"
                  title="Uptime Report for node1.sg: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/7fd1f178"
                  alt="Uptime Report for node1.tw: Last 30 days"
                  title="Uptime Report for node1.tw: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/0ee91550"
                  alt="Uptime Report for node2.id: Last 30 days"
                  title="Uptime Report for node2.id: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
          </Row>

          <Row style={rowStyle}>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/1bd6ab7c"
                  alt="Uptime Report for node2.lax: Last 30 days"
                  title="Uptime Report for node2.lax: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/5cffa75e"
                  alt="Uptime Report for node2.nyc: Last 30 days"
                  title="Uptime Report for node2.nyc: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/6b2a973f"
                  alt="Uptime Report for node2.sg: Last 30 days"
                  title="Uptime Report for node2.sg: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/87d26ea6"
                  alt="Uptime Report for node2.tw: Last 30 days"
                  title="Uptime Report for node2.tw: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
          </Row>

          <Row style={rowStyle}>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/b43ca268"
                  alt="Uptime Report for node5.lax: Last 30 days"
                  title="Uptime Report for node5.lax: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/5fb87768"
                  alt="Uptime Report for node1.id: Last 30 days"
                  title="Uptime Report for node1.id: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/c46cfc83"
                  alt="Uptime Report for Tezos.ID Web Service: Last 30 days"
                  title="Uptime Report for Tezos.ID Web Service: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
            <Col md={6} sm={12} xs={24} style={colStyle}>
              <div style={{ textAlign: "center" }}>
                <img
                  src="https://share.pingdom.com/banners/aadefc83"
                  alt="Uptime Report for node5.nyc: Last 30 days"
                  title="Uptime Report for node5.nyc: Last 30 days"
                  width="90%"
                  height="90%"
                />
              </div>
            </Col>
          </Row>
        </Card>
      </LayoutContentWrapper>
    );
  }
}

export default ApiMonitoring;
