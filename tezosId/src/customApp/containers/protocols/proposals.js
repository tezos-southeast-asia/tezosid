import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { proposalColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchProposers } from "../../../redux/proposers/actions";

class Proposers extends Component {
  render() {
    const {
      isLoading,
      proposers = {},
      isError,
      fetchProposers
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = proposers
    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="proposals.title" />}
        dataSource={data}
        columns={proposalColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchProposers}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.Proposers
})

const mapDispatchToProps = dispatch => {
  return {
    fetchProposers: (props) => dispatch(fetchProposers(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Proposers)

