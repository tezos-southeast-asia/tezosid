import React, { Component } from "react";
import TablePage from "../../components/tableViews/tablePage";
import { protocolsColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchProtocols } from '../../../redux/protocol/actions';
import { connect } from "react-redux";


class Protocols extends Component {

  render() {
    const {
      isLoading,
      protocols = {},
      isError,
      fetchProtocols
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = protocols

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="protocols.title" />}
        dataSource={data}
        columns={protocolsColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchProtocols}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.Protocols
})

const mapDispatchToProps = dispatch => {
  return {
    fetchProtocols: (props) => dispatch(fetchProtocols(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Protocols)