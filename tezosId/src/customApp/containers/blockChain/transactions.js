import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { transactionsColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchTransactions, updateKeyArray } from "../../../redux/transactions/actions";
import Table from "../../components/tableViews/table";

class Transactions extends Component {
  onExpand = (expanded, record) => {
    const { updateKeyArray, transactions = {} } = this.props;
    const { keyArr } = transactions
    if (!expanded) {
      keyArr.splice(keyArr.indexOf(record.key), 1);
    } else {
      keyArr.push(record.key);
    }
    updateKeyArray(keyArr);
  }
  render() {
    const {
      isLoading,
      transactions = {},
      isError,
      fetchTransactions
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage,
      keyArr
    } = transactions

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="transactions.title" />}
        dataSource={data}
        columns={transactionsColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchTransactions}
        expandedRowKeys={keyArr}
        onExpand={this.onExpand}
        expandedRowRender={(data) => {
          const { internalTx } = data || []
          return (
            <Table
              dataSource={internalTx}
              columns={transactionsColumns}
              fail={isError}
              loading={isLoading}
              pageTitle=""
              pagination={false}
            />
          )
        }}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.Transactions
})

const mapDispatchToProps = dispatch => {
  return {
    fetchTransactions: (props) => dispatch(fetchTransactions(props)),
    updateKeyArray: (props) => dispatch(updateKeyArray(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Transactions)