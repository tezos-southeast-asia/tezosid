import { connect } from "react-redux";

import {
  blockOperationColumns,
} from "../../../configs/tableColumnsConfigs"
import {
  vaildMainTabs,
  vaildOperationTabs,
  updateMainTab,
  fetchOperationAction,
  fetchActionNumber,
  initBlock,
  updateKeyArray
} from "../../../../redux/block/actions";
import OperationPage from "../../../components/operationPage"

const mapStateToProps = (state) => ({
  ...state.Block,
  vaildMainTabs,
  vaildOperationTabs,
  pageName: 'blockInfo',
  pageRoute: 'blocks',
  operationColumns: blockOperationColumns,
})

const mapDispatchToProps = dispatch => {
  return {
    fetchOperationAction: (props) => dispatch(fetchOperationAction(props)),
    fetchActionNumber: (props) => dispatch(fetchActionNumber(props)),
    updateMainTab: (props) => dispatch(updateMainTab(props)),
    initData: (props) => dispatch(initBlock(props)),
    updateKeyArray:(props)=>dispatch(updateKeyArray(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OperationPage)