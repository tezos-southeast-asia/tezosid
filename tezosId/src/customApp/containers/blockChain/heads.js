import React, { Component } from "react";
import * as utils from "../../../helpers/format";
import TablePage from "../../components/tableViews/tablePage";
import { headsColumns } from "../../configs/tableColumnsConfigs";
import { connectionConfig } from "../../configs/connectionConfigs";
import Tag from "../../components/tag";
import data from "../../components/tableViews/data";
import IntlMessages from "../../../components/utility/intlMessages";

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      pageSize: 10,
      isLoaded: false,
      dataList: [],
      fail: false,
      headsCount: 0
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData() {
    let url = { connectionConfig }.connectionConfig.heads;
    fetch(url)
      .then(response => response.json())
      .then(parsedJSON => {
        this.formatData(parsedJSON);
      });
  }
  formatData(dataSet) {
    for (let i = 0; i < dataSet.length; i++) {
      let key = dataSet[i][0];
      this.getDetails(key, i);
    }
    this.setState({
      headsCount: dataSet.length
    });
  }
  getDetails(key, id) {
    fetch({ connectionConfig }.connectionConfig.heads + "/" + key)
      .then(response => {
        if(response.ok) {
          return response.json();
        }
        return null;
      })
      .then(parsedJSON => {
        if (!parsedJSON) return

        let tempList = this.state.dataList;
        let date = utils.dateFormat(parsedJSON.header.timestamp);
        let operations = 0;
        for (let x = 0; x < parsedJSON.operations.length; x++) {
          let operation = 0;
          if (parsedJSON.operations[x] === undefined) {
            operation = 0;
          } else {
            operation = parsedJSON.operations[x].length;
          }
          operations = operations + operation;
        }
        let baker = parsedJSON.metadata.baker;
        let level = [parsedJSON.header.level];
        tempList.push({
          id: id,
          key: key,
          baker: baker,

          level: level,
          timestamp: date,
          priority: parsedJSON.header.priority,
          operations: operations,
          fitness:
            parsedJSON.header.fitness[0] + " " + parsedJSON.header.fitness[1],
          protocolhash: parsedJSON.metadata.protocol
        });
        this.setState({ dataList: tempList, isLoaded: true });
      });
  }
  getheadsCount(address) {
    fetch(address)
      .then(response => response.json())
      .then(parsedJSON => {
        if (parsedJSON.length > 0) {
          this.setState({ headsCount: parsedJSON.length });
        }
      });
  }

  render() {
    return (
      <TablePage
        totalPage={this.state.headsCount}
        pagination={false}
        fail={this.state.fail}
        pageTitle={
          <div>
            <IntlMessages id="heads.title"/>
            <Tag>
              {this.state.headsCount}
            </Tag>
          </div>
        }
        dataSource={new data(this.state.dataList).getSortDesc("timestamp")}
        columns={headsColumns}
        loading={!this.state.isLoaded}
      />
    );
  }
}
