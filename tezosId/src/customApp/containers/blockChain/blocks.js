import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { blocksColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchBlocks } from "../../../redux/blocks/actions";

class Blocks extends Component {
  render() {
    const {
      isLoading,
      blocks = {},
      isError,
      fetchBlocks
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = blocks

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="blocks.title" />}
        dataSource={data}
        columns={blocksColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchBlocks}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.Blocks
})

const mapDispatchToProps = dispatch => {
  return {
    fetchBlocks: (props) => dispatch(fetchBlocks(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Blocks)
