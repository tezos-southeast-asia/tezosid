import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { proposalVotersColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchProposersVoters } from "../../../redux/proposalVoters/actions";

class ProposersVoters extends Component {
  componentDidMount() {
    this.onChange()
  }

  onChange = (pageData = {}) => {
    const { currentPage, pageSize } = pageData

    this.props.fetchProposersVoters({
      currentPage,
      pageSize,
      hash: this.props.match.params.id
    });
  }

  render() {
    const {
      isLoading,
      voters = {},
      isError,
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = voters

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="proposals.title" />}
        dataSource={data}
        columns={proposalVotersColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={this.onChange}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.ProposalVoters
})

const mapDispatchToProps = dispatch => {
  return {
    fetchProposersVoters: (props) => dispatch(fetchProposersVoters(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProposersVoters)