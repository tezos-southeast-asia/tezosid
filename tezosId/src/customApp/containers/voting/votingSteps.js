import React from "react";
import { Steps, Popover } from "antd";
import { FormattedMessage } from "react-intl";

import { votingSteps } from "../../configs/votingConfigs"

const { Step } = Steps;

const VotingSteps = ({ votingPeriod = "" }) => {
  const currentStepIndex = votingSteps.findIndex(({ step }) => votingPeriod === step)
  const content = desc => {
    let newText = desc.split('\n').map((item, i) => {
      return <p key={i}>{item}</p>
    });
    return (
      <div>
        {newText}
      </div>
    )
  }

  return (
    <Steps current={currentStepIndex}>
      {
        votingSteps.map(({ step }) => (
          <Step key={step} title={<Popover content={<FormattedMessage id={`votingDesc.${step}`}>{content}</FormattedMessage>} title={<FormattedMessage id={`votingPeriods.${step}`} /> } trigger="hover"><FormattedMessage id={`votingPeriods.${step}`} /> </Popover>} />
        ))
      }
    </Steps>
  )
}

export default VotingSteps;
