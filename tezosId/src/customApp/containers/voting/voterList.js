import React, { Component, Fragment } from "react";
import { Card } from "antd";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { votersColumns } from "../../configs/tableColumnsConfigs";
import { initVoterList, fetchVoterList } from "../../../redux/voterList/actions";
import Table from "../../components/tableViews/table";
import LayoutWrapper from "../../../components/utility/layoutWrapper";
import VotingSteps from "./votingSteps";
import VotingPeriodsWrapper from "./votingPeriods.style";
import queryString from "query-string";
import { Badge } from 'antd';

class VoterList extends Component {
  componentDidMount() {
    this.props.initData();

  }

  componentDidUpdate(prevProps) {
    const { location } = this.props;
    const { currentProposal, fetchVoterList } = this.props;
    const { p, n } = queryString.parse(location.search)
    const { p: prevP, n: prevN } = queryString.parse(prevProps.location.search)
    if (prevProps.currentProposal !== currentProposal) {
      fetchVoterList({ currentProposal: currentProposal, currentPage: p, pageSize: n })
    }

    if (p !== prevP || n !== prevN) {
      fetchVoterList({ currentProposal: currentProposal, currentPage: p, pageSize: n })
    }
  }

  render() {
    const {
      currentProposal,
      votingPeriod,
      isLoading,
      voters
    } = this.props

    const {
      currentPage,
      pageSize,
      total,
      data
    } = voters

    return (
      <LayoutWrapper
        pageTitle={<FormattedMessage id="voterList.title" />}
        className="voter-list"
      >
        <VotingPeriodsWrapper>
          <Card
            title={currentProposal && (
              <Fragment>
                <FormattedMessage id="votingPeriods.title" />
                <span className="voter-list__current-proposal">{currentProposal}</span>
              </Fragment>
            )}
            bordered={false}
          >
            <VotingSteps votingPeriod={votingPeriod} />
          </Card>
          {/* {data && (
            <SortTable
              title={<FormattedMessage id="voterList.title" />}
              dataList={data}
              loading={isLoading}
              columns={votersColumns}
              pageSize={total}
            />
          )
          } */}
          <Card
            title={total && (
              <Fragment>
                <FormattedMessage id="voterList.title" />
                {` `}<Badge count={total} overflowCount={999} style={{ backgroundColor: '#108ee9'}} />
              </Fragment>
            )}
            bordered={false}
          >
            <Table
              totalPage={total}
              currentPage={currentPage}
              pageSize={pageSize}
              pageTitle={<FormattedMessage id="voterList.title" />}
              dataSource={data}
              columns={votersColumns}
              loading={isLoading}
              //pageOnChange={fetchVoterList}
              isLink={true}
            />
          </Card>

        </VotingPeriodsWrapper>


      </LayoutWrapper>

    );
  }
}

const mapStateToProps = state => ({
  ...state.VoterList,
})

const mapDispatchToProps = dispatch => {
  return {
    initData: () => dispatch(initVoterList()),
    fetchVoterList: (props) => dispatch(fetchVoterList(props))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VoterList)