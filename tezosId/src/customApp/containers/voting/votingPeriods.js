import React, { Component } from "react";
import { Col, Row, Card } from "antd";
import { injectIntl, FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_kelly from "@amcharts/amcharts4/themes/kelly";

import { initVotingPeriods } from "../../../redux/votingPeriods/actions";
import { votingColumns, proposalListColumns } from "../../configs/tableColumnsConfigs";
import LayoutWrapper from "../../../components/utility/layoutWrapper";
import Box from "../../../components/utility/box";
import Table from "../../components/tableViews/table";
import VotingSteps from "./votingSteps";
import VotingPeriodsWrapper from "./votingPeriods.style"

class VotingPeriods extends Component {
  static defaultProps = {
    votes: null,
    currentProposal: null,
    votingPeriod: null,
    voters: null,
    quorum: 0,
    majority: 0,
    participation: 0,
    totalRoll: 0,
    votedRoll: 0,
    proposals: null,
    isLoading: false,
    isShowChart: false,
  }

  componentDidMount() {
    // set chart theme
    am4core.useTheme(am4themes_animated);
    am4core.useTheme(am4themes_kelly);

    this.props.initData();
  }

  componentDidUpdate() {
    const { votes, votedRolls, totalRolls, isShowChart } = this.props

    if (isShowChart && votes) {
      this.createChartDistribution(votes);
      this.createChartMajority(votes);
      this.createChartParticipation(votedRolls, totalRolls);
    }
  }

  createChartDistribution(data = []) {
    const { intl } = this.props
    
    let chart = am4core.create("voting-periods__chart-distribution", am4charts.PieChart3D);
    chart.hiddenState.properties.opacity = 0;

    chart.legend = new am4charts.Legend();
    chart.data = data.map(({ key, ballots, votes }) => ({
      key,
      ballots: intl.formatMessage({ id: ballots }),
      votes,
    }));
    let series = chart.series.push(new am4charts.PieSeries3D());
    series.dataFields.value = "votes";
    series.dataFields.category = "ballots";
    series.labels.template.disabled = "true";
    series.ticks.template.disabled = "true";

    this.chartDistribution = chart;
  }

  createChartMajority(data = []) {
    const { intl } = this.props
    let chart = am4core.create("voting-periods__chart-majority", am4charts.PieChart3D);
    chart.hiddenState.properties.opacity = 0;

    chart.legend = new am4charts.Legend();
    chart.data = data.slice(0,2).map(({ key, ballots, votes }) => ({
      key,
      ballots: intl.formatMessage({ id: ballots }),
      votes,
    }));
    let series = chart.series.push(new am4charts.PieSeries3D());
    series.dataFields.value = "votes";
    series.dataFields.category = "ballots";
    series.labels.template.disabled = "true";
    series.ticks.template.disabled = "true";

    this.chartMajority = chart;
  }

  createChartParticipation(voted, total) {
    const { intl } = this.props
    let chart = am4core.create("voting-periods__chart-participation", am4charts.PieChart3D);
    chart.hiddenState.properties.opacity = 0;

    chart.legend = new am4charts.Legend();
    chart.data = [];
    chart.data.push({
      status: intl.formatMessage({ id: "votingPeriods.voted" }),
      votes: voted
    });
    chart.data.push({
      status: intl.formatMessage({ id: "votingPeriods.notVoted" }),
      votes: total - voted
    });

    let series = chart.series.push(new am4charts.PieSeries3D());
    series.dataFields.value = "votes";
    series.dataFields.category = "status";
    series.labels.template.disabled = "true";
    series.ticks.template.disabled = "true";

    this.chartParticipation = chart;
  }

  componentWillUnmount() {
    if (this.chartDistribution) {
      this.chartDistribution.dispose();
    }

    if (this.chartParticipation) {
      this.chartParticipation.dispose();
    }

    if (this.chartMajority) {
      this.chartMajority.dispose();
    }
  }

  render() {
    const {
      isLoading,
      votes,
      isShowChart,
      currentProposal,
      votingPeriod,
      quorum,
      majority,
      participation,
      proposals
    } = this.props

    return (
      <LayoutWrapper
        pageTitle={<FormattedMessage id="votingPeriods.title" />}
        className="voting-periods"
      >
        <VotingPeriodsWrapper>
          <Card
            title={currentProposal}
            bordered={false}
          >
            <VotingSteps votingPeriod={votingPeriod} />  
          </Card>
          { (isShowChart || votingPeriod === "proposal") && (
              <Box>
                <Row justify="around">
                  <Col xl={8} lg={24} md={24} sm={24} xs={24} className="voting-periods__col">
                    <div className="voting-periods__chart-title" hidden={!isShowChart}>
                      <FormattedMessage id="votingPeriods.distribution" />
                    </div>
                    <div
                      id="voting-periods__chart-distribution"
                      className="voting-periods__chart"
                      hidden={!isShowChart}
                    />
                  </Col>
                  <Col xl={8} lg={12} md={12} sm={24} xs={24} className="voting-periods__col">
                    <div className="voting-periods__chart-title" hidden={!isShowChart}>
                      <FormattedMessage
                        id="votingPeriods.participation"
                        values={{ participation }}
                      />
                      <FormattedMessage
                        id="votingPeriods.quorum"
                        values={{ quorum }}
                      />
                    </div>
                    <div
                      id="voting-periods__chart-participation"
                      className="voting-periods__chart"
                      hidden={!participation || !isShowChart}
                    />
                  </Col>
                  <Col xl={8} lg={12} md={12} sm={24} xs={24} className="voting-periods__col">
                    <div className="voting-periods__chart-title" hidden={!isShowChart}>
                      <FormattedMessage
                        id="votingPeriods.majority"
                        values={{ majority }}
                      />
                      <FormattedMessage
                        id="votingPeriods.majoritySub"
                        values={ {value:80} }
                      />
                    </div>
                    <div
                      id="voting-periods__chart-majority"
                      className="voting-periods__chart"
                      hidden={!isShowChart}
                    />
                  </Col>
                </Row>
                { isShowChart && votes && (
                    <Row justify="around">
                      <Col span={24} className="voting-periods__col">
                        <Table
                          dataSource={votes}
                          columns={votingColumns}
                          loading={isLoading}
                          pagination={false}
                        />
                      </Col>
                    </Row>
                  )
                }
                {
                  votingPeriod === "proposal" && (
                    <Row justify="around">
                      <Col span={24} className="voting-periods__col">
                        <Table
                          dataSource={proposals}
                          columns={proposalListColumns}
                          loading={isLoading}
                          pagination={false}
                        />
                      </Col>
                    </Row>
                  )
                }
              </Box>
            )
          }
        </VotingPeriodsWrapper>
      </LayoutWrapper>
    );
  }
}

const mapStateToProps = state => ({
  ...state.VotingPeriods
})

const mapDispatchToProps = dispatch => {
  return {
    initData: () => dispatch(initVotingPeriods()),
  }
}

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(VotingPeriods))
