import styled from 'styled-components';
import WithDirection from '../../../settings/withDirection';

const VotingPeriodsWrapper = styled.div`
  .ant-row {
    width: 100%;
    padding: 20px;
    display: flex;
    flex-wrap: wrap;
  }

  .ant-card-head-title {
    white-space: pre-line;
    overflow: initial;
    text-overflow: initial;
    word-break: break-word;
    display: block;
  }

  .voting-periods {
    &__chart {
      width: 100%;
      height: 360px;
    }

    &__chart-title {
      font-size: 18px;
      height: 54px;
      font-weight: bold;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: flex-start;
    }

    &__col {
      margin-bottom: 16px;
    }
  }

  .voter-list {
    &__current-proposal {
      display: inline-block;
      margin: ${props =>
        props['data-rtl'] === 'rtl' ? '0 16px 0 0' : '0 0 0 16px'};
    }
  }
`
export default WithDirection(VotingPeriodsWrapper);