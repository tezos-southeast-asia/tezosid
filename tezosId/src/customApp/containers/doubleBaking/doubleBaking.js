import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { doubleBakingColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchDoubleBaking } from "../../../redux/doubleBaking/actions";

class DoubleBaking extends Component {
  render() {
    const {
      isLoading,
      doubleBaking = {},
      isError,
      fetchDoubleBaking
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = doubleBaking

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="doubleBaking.title" />}
        dataSource={data}
        columns={doubleBakingColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchDoubleBaking}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.DoubleBaking
})

const mapDispatchToProps = dispatch => {
  return {
    fetchDoubleBaking: (props) => dispatch(fetchDoubleBaking(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DoubleBaking)