import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { delegationsColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchDelegations } from "../../../redux/delegations/actions";

class Delegations extends Component {
  render() {
    const {
      isLoading,
      delegations = {},
      isError,
      fetchDelegations
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = delegations

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="delegations.title" />}
        dataSource={data}
        columns={delegationsColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchDelegations}
      />
    );
  }
}


const mapStateToProps = (state) => ({
  ...state.Delegations
})

const mapDispatchToProps = dispatch => {
  return {
    fetchDelegations: (props) => dispatch(fetchDelegations(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Delegations)
