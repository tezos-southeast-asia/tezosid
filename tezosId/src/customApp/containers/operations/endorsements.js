import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { endorsementsColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchEndorsements } from "../../../redux/endorsements/actions";

class Endorsements extends Component {
  render() {
    const {
      isLoading,
      endorsements = {},
      isError,
      fetchEndorsements
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = endorsements

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="endorsements.title" />}
        dataSource={data}
        columns={endorsementsColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchEndorsements}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.Endorsements
})

const mapDispatchToProps = dispatch => {
  return {
    fetchEndorsements: (props) => dispatch(fetchEndorsements(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Endorsements)