import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { originationsColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchOriginations } from "../../../redux/originations/actions";

class Originations extends Component {
  render() {
    const {
      isLoading,
      originations = {},
      isError,
      fetchOriginations
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = originations

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="originations.title" />}
        dataSource={data}
        columns={originationsColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchOriginations}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.Originations
})

const mapDispatchToProps = dispatch => {
  return {
    fetchOriginations: (props) => dispatch(fetchOriginations(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Originations)
