import { connect } from "react-redux";

import {
  operationColumns,
} from "../../configs/tableColumnsConfigs"
import {
  vaildMainTabs,
  vaildOperationTabs,
  updateMainTab,
  fetchOperationAction,
  fetchActionNumber,
  initOperation,
  updateKeyArray,
} from "../../../redux/operation/actions";
import OperationPage from "../../components/operationPage"

const mapStateToProps = (state) => ({
  ...state.Operation,
  vaildMainTabs,
  vaildOperationTabs,
  pageName: 'operation',
  pageRoute: 'operations',
  operationColumns,
})

const mapDispatchToProps = dispatch => {
  return {
    fetchOperationAction: (props) => dispatch(fetchOperationAction(props)),
    fetchActionNumber: (props) => dispatch(fetchActionNumber(props)),
    updateMainTab: (props) => dispatch(updateMainTab(props)),
    initData: (props) => dispatch(initOperation(props)),
    updateKeyArray: (props) => dispatch(updateKeyArray(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OperationPage)