import React, { Component } from "react";
import { connectionConfig } from "../../configs/connectionConfigs";

import { Button, Input } from 'antd';
import Box from "../../../components/utility/box";
import LayoutContentWrapper from "../../../components/utility/layoutWrapper";
import IntlMessages from "../../../components/utility/intlMessages";

class InjectOperation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hexStr: '',
            result: '',
            loading: false
        };
    }

    handleInjectOps = async () => {
        try {
            const result = await fetch(connectionConfig.operationActions.injectOperation,
                { method: 'post', body: JSON.stringify(this.state.hexStr), headers: { 'content-type': 'application/json' } });
            this.setState({
                result: result.ok ? "Operation injection success." : "Operation injection failed"
            });
        } catch (err) {
            this.setState({
                result: "An error has occur. Please try again."
            });
        }
    }

    updateHexStr(evt) {
        this.setState({
            hexStr: evt.target.value
        });
    }

    render() {
        const { TextArea } = Input;
        return (
            <LayoutContentWrapper pageTitle="Inject Operation">
                <Box>
                    <h3>
                        <IntlMessages id="operation.injectOperation" />
                    </h3>
                    <br />
                    <TextArea rows={5} placeholder="Insert a hexadecimal signed operation string here" value={this.state.hexStr} onChange={evt => this.updateHexStr(evt)} />
                    <br />
                    <br />
                    <Button type="primary" onClick={this.handleInjectOps}>Inject Operation</Button>
                    <br />
                    <br />
                    <p>{this.state.result}</p>
                </Box>
            </LayoutContentWrapper>
        );
    }
}

export default InjectOperation;