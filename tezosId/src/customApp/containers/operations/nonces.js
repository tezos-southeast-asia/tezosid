import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { noncesColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchNonces } from "../../../redux/nonces/actions";

class Nonces extends Component {
  render() {
    const {
      isLoading,
      nonces = {},
      isError,
      fetchNonces
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = nonces

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="nonce.title" />}
        dataSource={data}
        columns={noncesColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchNonces}
      />
    );
  }
}


const mapStateToProps = (state) => ({
  ...state.Nonces
})

const mapDispatchToProps = dispatch => {
  return {
    fetchNonces: (props) => dispatch(fetchNonces(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Nonces)
