import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { allActivationsColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchActivations } from "../../../redux/activations/actions";

class Activations extends Component {
  render() {
    const {
      isLoading,
      activations = {},
      isError,
      fetchActivations
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = activations

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="activations.title" />}
        dataSource={data}
        columns={allActivationsColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchActivations}
      />
    );
  }
}


const mapStateToProps = (state) => ({
  ...state.Activations
})

const mapDispatchToProps = dispatch => {
  return {
    fetchActivations: (props) => dispatch(fetchActivations(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Activations)
