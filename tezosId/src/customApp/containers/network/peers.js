import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { peersColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchPeers } from "../../../redux/peers/actions";

class Peers extends Component {
  render() {
    const {
      isLoading,
      peers = {},
      isError,
      fetchPeers
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = peers

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="networkPeers.title" />}
        tableSubtitle={<IntlMessages id="networkPeers.description" />}
        dataSource={data}
        columns={peersColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchPeers}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.Peers
})

const mapDispatchToProps = dispatch => {
  return {
    fetchPeers: (props) => dispatch(fetchPeers(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Peers)