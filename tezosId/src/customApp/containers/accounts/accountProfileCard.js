import React, { Component } from "react";
import PropTypes from "prop-types";
import { Card, Button, Modal, Input } from "antd";
import { FormattedMessage } from "react-intl";

import {
  infoColumns
} from "../../configs/tableColumnsConfigs";
import Notification from '../../../components/notification';
import Box from "../../../components/utility/box";
import Table from "../../components/tableViews/table";
import Amount from "../../components/amount";
import AccountAvatarBox, { AccountAvatarSmall } from "./accountAvatarBox";
import { connect } from "react-redux";
import { checkForBookmarkFromAcc, addBookmarkFromAcc, removeBookmarkFromAcc, updateFollowBtnDisplay } from '../../../redux/bookmarklist/actions';
import { updateModalState } from '../../../redux/account/actions'
import { removeEmailNotification } from '../../../redux/notification/actions'

const initialState = {
  editedText: '',
  okBtnState: false
}

class AccountProfileCard extends Component {
  static propTypes = {
    details: PropTypes.object,
    bakerDetails: PropTypes.object,
    tableData: PropTypes.array,
    isContract: PropTypes.bool
  }

  static defaultProps = {
    details: {},
    bakerDetails: {},
    tableData: [],
    isContract: false
  }
  constructor(props) {
    super(props);
    this.state = initialState
  }
  componentDidMount() {
    this.checkForAddressUpdate();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.accountHash !== this.props.accountHash) {
      this.setState(initialState);
      this.checkForAddressUpdate();
    }
    if (prevProps.name !== this.props.name || prevProps.alias !== this.props.alias) {
      const { name, alias } = this.props
      const defaultAliasValue = alias !== '' ? alias : name;
      let editedText = '';
      let okBtnState = false;
      if (defaultAliasValue) {
        editedText = defaultAliasValue
      } else {
        okBtnState = true
      }
      this.setState({
        editedText: editedText,
        okBtnState: okBtnState
      })
    }
  }

  componentWillUnmount(){
    this.setState=initialState
  }

  checkForAddressUpdate() {
    if (this.props.user && this.props.user.emailVerified) {
      const userUid = this.props.user.uid;
      const accountHash = this.props.accountHash;
      this.props.checkForBookmarkFromAcc(userUid, accountHash);
    } else if (this.props.user && !this.props.user.emailVerified) {
      Notification("warning", "Please verify your email to follow this account.");
      this.props.updateFollowBtnDisplay("none");
    }
    else {
      this.props.updateFollowBtnDisplay("none");
    }
  }

  onFollow = () => {
    const userUid = this.props.user.uid;
    const accountHash = this.props.accountHash;
    const userEmail = this.props.user.email;

    if (this.props.bookmarklist.followBtnText === "Follow") {
      this.props.addBookmarkFromAcc(userUid, accountHash, userEmail);
    } else {
      this.props.removeBookmarkFromAcc(userUid, accountHash);
      this.props.removeEmailNotification(userEmail, accountHash);
    }

  }

  openNotificationModal = () => {
    const { updateModalState } = this.props
    updateModalState({ state: true })
  }

  modalHandleOk = () => {
    const { hash } = this.props
    const { updateModalState } = this.props
    const editedText = this.state.editedText.replace(/\s+/g, '').substring(0, 18)
    const url = `https://t.me/TezosNotifierBot?start=tezosid_${hash}_${editedText}`
    window.open(url, '_blank');
    updateModalState({ state: false })
  }

  modalHandleCancel = () => {
    const { updateModalState } = this.props
    updateModalState({ state: false })
  }

  inputOnChange = (e) => {
    if (e.target.value.replace(/\s+/g, '')) {
      this.setState({
        okBtnState: false,
        editedText: e.target.value
      })
    } else {
      this.setState({
        okBtnState: true,
        editedText: e.target.value
      })
    }
  }
  render() {
    const {
      hash,
      name,
      tableData,
      isContract,
      details,
      bakerDetails,
      alias,
      modalState
    } = this.props
    const {
      balance: bakerBalance,
      staking_balance: stakingBalance,
      currentDeposit,
      pendingRewards
    } = bakerDetails || {}
    const {
      balance: accountBalance
    } = details || {}
    const evaluatedBalance = bakerBalance || accountBalance
    const followBtnText = this.props.bookmarklist.followBtnText;
    const followBtnDisplay = this.props.bookmarklist.followBtnDisplay;
    const style = { marginTop: "20px", display: followBtnDisplay };
    const defaultAliasValue = alias !== '' ? alias : name;

    return (
      <Card>
        <div className="account__info">
          <Box className="account__hash-box">
            <h1 className="account__hash">
              {name && (hash !== name) && <div>{name}</div>}
              <div>
                {hash}
                <Button type="primary" shape="circle" icon="bell" onClick={this.openNotificationModal} style={{ marginLeft: '10px' }} />
                <Modal
                  title={<FormattedMessage id="accounts.telegramModalTitle" />}
                  visible={modalState}
                  okText={<FormattedMessage id="accounts.telegramGetNotification" />}
                  onOk={this.modalHandleOk}
                  onCancel={this.modalHandleCancel}
                  maskClosable={false}
                  okButtonProps={{ 'disabled': this.state.okBtnState }}
                >
                  <FormattedMessage id="accounts.telegramMsg" />
                  <div style={{ marginTop: "10px" }}>
                    <h3><FormattedMessage id="accounts.telegramFollowMsg" /></h3>
                    <div><AccountAvatarSmall name={hash} />{hash}</div>
                  </div>
                  <div style={{ marginTop: "10px" }}>
                    <h3><FormattedMessage id="accounts.telegramAliasMsg" /></h3>
                    <Input defaultValue={defaultAliasValue} value={this.state.editedText} placeholder={'Enter an alias for this address'} onChange={this.inputOnChange} />
                  </div>
                </Modal>
              </div>
              <div>{alias}</div>
            </h1>
          </Box>
          <AccountAvatarBox name={hash} />
          <Box className="account__balance-box">
            <div className="account__info-item account__balance">
              <FormattedMessage id="accounts.accountBalance" />
              {accountBalance && <Amount className="account__info-balance" amount={accountBalance} />}
            </div>
            <div className="account__info-item account__evaluated-balance">
              <FormattedMessage id="accounts.evaluatedBalance" />
              {evaluatedBalance && <Amount className="account__info-amount" amount={evaluatedBalance} />}
            </div>
          </Box>
          <Box>
            <div className="account__info-item">
              <FormattedMessage id="accounts.currentDeposit" />
              {<Amount className="account__info-amount" amount={currentDeposit !== null ? currentDeposit : 0} />}
            </div>
            <div className="account__info-item">
              <FormattedMessage id="accounts.pendingRewards" />
              {<Amount className="account__info-amount" amount={pendingRewards !== null ? pendingRewards : 0} />}
            </div>
          </Box>
          {!isContract && stakingBalance && (
            <Box className="account__staking-balance-box">
              <div className="account__info-item account__staking-balance">
                <FormattedMessage id="accounts.stakingBalance" />
                <Amount className="account__info-amount" amount={stakingBalance} />
              </div>
            </Box>
          )
          }
          <Box>
            <Table
              pagination={false}
              dataSource={tableData}
              columns={infoColumns}
              size="small"
            />
            <Button id="follow_btn" type="primary" loading={this.props.bookmarklist.isLoading} style={style} onClick={this.onFollow} block>{followBtnText}</Button>
          </Box>
        </div>
      </Card>
    )
  }
}

const mapStateToProps = state => ({
  user: state.Auth.user,
  bookmarklist: state.Bookmarklist,
  accountHash: state.router.location.pathname.split("/").pop()
});

const mapDispatchToProps = dispatch => ({
  checkForBookmarkFromAcc: (userUid, accountHash) => dispatch(checkForBookmarkFromAcc(userUid, accountHash)),
  addBookmarkFromAcc: (userUid, accountHash, userEmail) => dispatch(addBookmarkFromAcc(userUid, accountHash, userEmail)),
  removeBookmarkFromAcc: (userUid, accountHash) => dispatch(removeBookmarkFromAcc(userUid, accountHash)),
  updateFollowBtnDisplay: display => dispatch(updateFollowBtnDisplay(display)),
  removeEmailNotification: (userEmail, accountHash) => dispatch(removeEmailNotification(userEmail, accountHash)),
  updateModalState: (props) => dispatch(updateModalState(props)),
});


export default connect(mapStateToProps, mapDispatchToProps)(AccountProfileCard)