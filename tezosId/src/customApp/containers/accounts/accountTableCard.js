import React, { Component } from "react";
import { Card, Col, Row, Skeleton } from "antd";
import { FormattedMessage } from "react-intl";
import queryString from "query-string";

import Table from "../../components/tableViews/table";
import {
  accountOperationColumns,
  transactionsColumns
} from "../../configs/tableColumnsConfigs";
import Tag from "../../components/tag";
import LinkTabs, { TabPane, LinkTab } from "../../components/linkTabs";
import {
  vaildMainTabs
} from "../../../redux/account/actions"


class AccountTableCard extends Component {
  static defaultProps = {
    transactions: {},
    delegations: {},
    originations: {},
    endorsements: {},
    activations: {},
    reveals: {},
    proposals: {},
    ballots: {},
    tabs: {},
    contract: {}
  };

  displayContract = (contract, type) => {
    if (contract !== undefined) {
      let temp = [];

      let code;
      if (type === "code") code = this.formatCodeElements(contract[1]);
      else if (type === "contractStorage")
        code = this.formatStorageElements(contract);
      else code = this.formatCodeElements2(contract[1]);
      for (let i = 0; i < code.length; i++) {
        temp.push(code[i]);
      }
      return temp;
    }
  };
  formatStorageElements = array => {
    let padding = 0;
    let divStyle = { paddingLeft: 0, textAlign: "left" };
    let tempArray = [];
    for (let i = 0; i < array.length; i++) {
      if (array[i] === ")" || array[i] === "]") {
        padding -= 15;
      }
      divStyle = { paddingLeft: padding, textAlign: "left" };
      tempArray.push(
        <div key={i} style={divStyle}>
          {array[i]}{" "}
        </div>
      );
      if (array[i] === "(" || array[i] === "[") {
        padding += 15;
      }
    }
    return tempArray;
  };

  formatCodeElements = array => {
    let tempArray = [];
    let padding = 0;
    for (let i = 0; i < array.length; i++) {
      if (array[i] === "}") {
        padding -= 15;
      }

      let divStyle = { paddingLeft: padding, textAlign: "left" };
      tempArray.push(
        <div key={i} style={divStyle}>
          {array[i]}{" "}
        </div>
      );
      if (array[i] === "{") {
        padding += 15;
      }
    }
    return tempArray;
  };

  formatCodeElements2 = array => {
    let tempArray = [];
    let padding = 0;
    for (let i = 0; i < array.length; i++) {
      if (array[i] === "}") {
        padding -= 15;
        continue;
      }
      if (array[i] === "{") {
        padding += 15;
        continue;
      }

      let divStyle = { paddingLeft: padding, textAlign: "left" };
      tempArray.push(
        <div key={i} style={divStyle}>
          {array[i]}{" "}
        </div>
      );
    }
    return tempArray;
  };

  getOperationTabPanes = () => {
    const {
      location,
      operations,
      onExpand
    } = this.props
    const tabs = Object.keys(accountOperationColumns)
    const tabPanes = tabs.map(type => {
      const columns = accountOperationColumns[type]
      const data = operations[type]

      const newSearch = queryString.stringify({
        tab: "operations",
        op: type
      })

      const { data: tableData, total, isLoading, isError, currentPage, pageSize } = data
      if (type === 'transactions') {
        const { keyArr } = data;
        return (
          <TabPane
            tab={
              <LinkTab
                to={{
                  ...location,
                  search: `?${newSearch}`
                }}
              >
                <FormattedMessage id={`topbar.${type}`} />
                <Tag>{total}</Tag>
              </LinkTab>
            }
            key={type}
          >
            <Table
              totalPage={total}
              currentPage={currentPage}
              pageSize={pageSize}
              pageTitle=""
              dataSource={tableData}
              columns={columns}
              fail={isError}
              loading={isLoading}
              isLink={true}
              expandedRowKeys={keyArr}
              onExpand={onExpand}
              expandedRowRender={(data) => {
                const { internalTx } = data || []
                return (
                  <Table
                    dataSource={internalTx}
                    columns={transactionsColumns}
                    fail={isError}
                    loading={isLoading}
                    pageTitle=""
                    pagination={false}
                  />
                )
              }}
            />
          </TabPane>
        )
      } else {
        return (
          <TabPane
            tab={
              <LinkTab
                to={{
                  ...location,
                  search: `?${newSearch}`
                }}
              >
                <FormattedMessage id={`topbar.${type}`} />
                <Tag>{total}</Tag>
              </LinkTab>
            }
            key={type}
          >
            <Table
              totalPage={total}
              currentPage={currentPage}
              pageSize={pageSize}
              pageTitle=""
              dataSource={tableData}
              columns={columns}
              fail={isError}
              loading={isLoading}
              isLink={true}
            />
          </TabPane>
        )
      }
    })

    return tabPanes
  }
  getMainTabContent = (tab) => {
    switch (tab) {
      case "operations": {
        const {
          opTabs,
        } = this.props

        return (
          <LinkTabs
            {...opTabs}
          >
            {this.getOperationTabPanes()}
          </LinkTabs>
        )
      }
      case "contract": {
        const {
          contractTabs,
        } = this.props

        return (
          <LinkTabs
            {...contractTabs}
          >
            {this.getContractTabPanes()}
          </LinkTabs>
        )
      }

      case "baking": {
        const {
          bakingTabs,
        } = this.props

        return (
          <LinkTabs
            {...bakingTabs}
          >
            {this.getBakingTabPanes()}
          </LinkTabs>
        )
      }

      default:
        return null
    }
  }

  getBakingTabPanes = () => {
    const {
      location,
      baking
    } = this.props

    const tabs = ['upcomingBaking', 'upcomingEndorsement']
    const tabPanes = tabs.map(type => {
      const newSearch = queryString.stringify({
        tab: "baking",
        op: type
      })
      const { upcomingBaking, upcomingEndorsement } = baking
      const { nextBaking, prevBaking } = upcomingBaking
      const { nextEndorsement, prevEndorsement } = upcomingEndorsement
      const upcomingBakingCheck = type === 'upcomingBaking' || type === 'upcomingEndorsement' ? '' : 'none'
      const upcomingTitle = type === 'upcomingBaking' ? 'nextBaking' : 'nextEndorsement'
      const pastTitle = type === 'upcomingBaking' ? 'prevBaking' : 'prevEndorsement'
      const cardTitle = type === 'upcomingBaking' ? 'baking' : 'endorsement'
      const upcomingOperation = type === 'upcomingBaking' ? nextBaking : nextEndorsement
      const pastOperation = type === 'upcomingBaking' ? prevBaking : prevEndorsement
      const loading = type === 'upcomingBaking' ? upcomingBaking.isLoading : type === 'upcomingEndorsement' ? upcomingEndorsement.isLoading : 'false'
      const prevMsg = type === 'upcomingBaking' ? 'prevBakingMsg' : 'prevEndorsementMsg'
      const nextMsg = type === 'upcomingBaking' ? 'nextBakingMsg' : 'nextEndorsementMsg'
      const prevMsgVisible = type === 'upcomingBaking' && prevBaking.level === '-' ? '' : type === 'upcomingEndorsement' && prevEndorsement.level === '-' ? '' : 'none'
      const nextMsgVisible = type === 'upcomingBaking' && nextBaking.level === '-' ? '' : type === 'upcomingEndorsement' && nextEndorsement.level === '-' ? '' : 'none'
      //const upcomingEndorsementCheck = type !== "upcomingEndorsement" ? "none" : ''
      // const { data: tableData, total, isLoading, isError, currentPage, pageSize } = data
      return (

        <TabPane
          tab={
            <LinkTab
              to={{
                ...location,
                search: `?${newSearch}`
              }}
            >
              <FormattedMessage id={`topbar.${type}`} />
            </LinkTab>
          }
          key={type}
        >
          <Card title={<FormattedMessage id={`accounts.${cardTitle}`} />} style={{ display: upcomingBakingCheck }} >
            <Row gutter={16} type="flex">
              <Col md={12} sm={24} xs={24} >
                <Skeleton loading={loading} active >
                  <Card title={<FormattedMessage id={`accounts.${pastTitle}`} />} style={{ height: '100%' }}>
                    <h4>{<FormattedMessage id={`table.level`} />} {pastOperation.level}</h4>
                    <h5>{<FormattedMessage id={`homepage.cycle`} />} {pastOperation.cycle}</h5>
                    <h5>{<FormattedMessage id={`accounts.estTime`} />} {pastOperation.estTime}</h5>
                    <div style={{ display: prevMsgVisible }} >{<FormattedMessage id={`accounts.${prevMsg}`} />}</div>
                  </Card>
                </Skeleton>
              </Col>
              <Col md={12} sm={24} xs={24}>
                <Skeleton loading={loading} active>
                  <Card title={<FormattedMessage id={`accounts.${upcomingTitle}`} />} style={{ height: '100%' }}>
                    <h4>{<FormattedMessage id={`table.level`} />} {upcomingOperation.level}</h4>
                    <h5>{<FormattedMessage id={`homepage.cycle`} />} {upcomingOperation.cycle}</h5>
                    <h5>{<FormattedMessage id={`accounts.estTime`} />} {upcomingOperation.estTime}</h5>
                    <div style={{ display: nextMsgVisible }}>{<FormattedMessage id={`accounts.${nextMsg}`} />}</div>
                  </Card>
                </Skeleton>
              </Col>
            </Row>
          </Card>

          {/* <Table
            totalPage={total}
            currentPage={currentPage}
            pageSize={pageSize}
            pageTitle=""
            dataSource={tableData}
            columns={columns}
            fail={isError}
            loading={isLoading}
            isLink={true}
          /> */}
        </TabPane>
      )
    })

    return tabPanes
  }

  getContractTabPanes = () => {
    if (this.props.isContract) {
      const rowStyle = {
        width: "100%",
        display: "flex",
        flexFlow: "row wrap",
        align: "top"
      };
      const colStyle = {
        marginBottom: "16px"
      };
      const {
        location,
        contractCode,
        contractStorage
      } = this.props
      const {
        formattedContract,
        formattedParameter,
        formattedStorage
      } = contractCode || "";
      const {
        storage
      } = contractStorage || "";

      const tabs = ['contractCode', 'contractStorage'];
      const tabPanes = tabs.map(type => {
        const newSearch = queryString.stringify({
          tab: "contract",
          op: type
        })
        const contractStorageDisplay = type === 'contractStorage' ? '' : 'none';
        const contractCodeDisplay = type === 'contractCode' ? '' : 'none';

        return (
          <TabPane
            tab={
              <LinkTab
                to={{
                  ...location,
                  search: `?${newSearch}`
                }}
              >
                <FormattedMessage id={`topbar.${type}`} />
              </LinkTab>
            }
            key={type}
          >
            <Row style={rowStyle}>
              <Card
                style={{ display: contractStorageDisplay }}
                title={
                  <FormattedMessage
                    id={"account.contractStorage"}
                  ></FormattedMessage>
                }
              >
                <div className="card-scroll">
                  {this.displayContract(storage, "contractStorage")}
                </div>
              </Card>
              <Col md={11} sm={12} xs={24} style={colStyle}>
                <Card
                  style={{ display: contractCodeDisplay }}
                  title={
                    <FormattedMessage
                      id={"account.parameter"}
                    ></FormattedMessage>
                  }
                >
                  <div className="card-scroll">
                    {this.displayContract(formattedParameter, "parameter")}
                  </div>
                </Card>
              </Col>
              <Col md={2} sm={12} xs={24}></Col>
              <Col md={11} sm={12} xs={24} style={colStyle}>
                <Card
                  style={{ display: contractCodeDisplay }}
                  title={
                    <FormattedMessage id={"account.storage"}></FormattedMessage>
                  }
                >
                  <div className="card-scroll">
                    {this.displayContract(formattedStorage, "storage")}
                  </div>
                </Card>
              </Col>
            </Row>

            <Card
              style={{ display: contractCodeDisplay }}
              title={<FormattedMessage id={"account.code"}></FormattedMessage>}
            >
              <div className="card-scroll">
                {this.displayContract(formattedContract, "code")}
              </div>
            </Card>
          </TabPane >
        )
      })

      return tabPanes
    }

  }

  getMainTabPanes = () => {
    const {
      location,
      isContract,
      total,
      isBaker
    } = this.props
    //validTabs.slice(validTabs.indexOf('contract'), validTabs.indexOf('contract') + 1)
    let validTabs = vaildMainTabs
    validTabs = !isContract ? validTabs.filter(tabs => tabs !== 'contract') : validTabs
    validTabs = !isBaker ? validTabs.filter(tabs => tabs !== 'baking') : validTabs

    const mainTabPanes = validTabs.map((tab) => (
      <TabPane
        tab={
          <LinkTab
            to={{
              ...location,
              search: `?tab=${tab}`
            }}
          >
            <FormattedMessage id={`table.${tab}`} />
            {tab === 'operations' ? <Tag>{total}</Tag> : null}
          </LinkTab>
        }
        key={tab}
      >
        {this.getMainTabContent(tab)}
      </TabPane>
    ))

    return mainTabPanes
  }

  render() {

    const mainTabPanes = this.getMainTabPanes()
    const {
      tabs
    } = this.props;

    return (
      <Card>
        <LinkTabs
          {...tabs}
        >
          {mainTabPanes}
        </LinkTabs>
      </Card>
    );
  }
}

export default AccountTableCard;
