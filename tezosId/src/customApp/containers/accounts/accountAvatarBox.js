import React from "react";
import Box from "../../../components/utility/box";
import Robohash from "react-robohash";

const AccountAvatarBox = ({ name }) => (
  <Box className="account__avatar-box">
    <Robohash
      className="account__avatar"
      name={name}
      gravatar
      fileType="svg"
      size={150}
    />
  </Box>
)

export const AccountAvatarSmall = ({ name }) => (
  <Robohash
    className="account__avatar"
    name={name}
    gravatar
    fileType="svg"
    size={50}
  />
)
export default AccountAvatarBox
