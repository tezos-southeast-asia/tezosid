import React, { Component } from "react";
import { Col, Row } from "antd";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import queryString from "query-string";

import { connectionConfig } from "../../configs/connectionConfigs";
import { common as commonMeta } from "../../configs/metaConfigs";
import LayoutContentWrapper from "../../../components/utility/layoutWrapper.js";
import AccountProfileCard from "./accountProfileCard";
import AccountTableCard from "./accountTableCard";
import AccountWrapper from "./account.style";
import {
  vaildMainTabs,
  vaildOperationTabs,
  updateMainTab,
  validContractTabs,
  validBakingTabs,
  initOperation,
  initContract,
  initBaking,
  fetchAccountDetails,
  fetchAccountAction,
  fetchActionNumber,
  formatTransactionDetails,
  getAlias,
  updateKeyArray
} from "../../../redux/account/actions"
import { setBookmarks } from '../../../redux/bookmarklist/actions';

class Account extends Component {
  verifyTab = (tab) => {
    const isVaildTab = tab ? vaildMainTabs.indexOf(tab) !== -1 : true

    return isVaildTab
  }

  verifyOperationTab = (opTab) => {
    const isVaildTab = opTab ? vaildOperationTabs.indexOf(opTab) !== -1 : true

    return isVaildTab
  }

  verifyContractTab = (contractTab) => {
    const isVaildTab = contractTab ? validContractTabs.indexOf(contractTab) !== -1 : true

    return isVaildTab
  }

  verifyBakingTab = (bakingTab) => {
    const isVaildTab = bakingTab ? validBakingTabs.indexOf(bakingTab) !== -1 : true

    return isVaildTab
  }

  componentDidUpdate(prevProps) {
    const {
      location,
      match,
      fetchAccountDetails,
      history,
      updateMainTab,
      initOperation
    } = this.props
    const prevHash = prevProps.match.params.id
    const currentHash = match.params.id
    const { p, tab, n, op } = queryString.parse(location.search)
    const { p: prevP, tab: prevTab, n: prevN, op: prevOp } = queryString.parse(prevProps.location.search)
    const isVaildTab = this.verifyTab(tab)
    const isVaildOperationTab = this.verifyOperationTab(op)
    const isVaildContractTab = this.verifyContractTab(op)
    const isValidBakingTab = this.verifyBakingTab(op)
    const searchQueries = queryString.parse(location.search)
    // if (currentHash !== prevHash) {
    //   fetchAccountDetails({ hash: currentHash })
    //   fetchAccountAction({ hash: currentHash, tab, currentPage: p, pageSize: n })
    // }
    if (currentHash !== prevHash) {
      fetchAccountDetails({ hash: currentHash })
      initOperation({ hash: currentHash, ...searchQueries })
      return updateMainTab({ hash: currentHash })
    }

    if (isVaildTab) {
      if (p !== prevP || tab !== prevTab || n !== prevN) {
        updateMainTab({ hash: currentHash, p, tab, n, op })
        // fetchAccountAction({ hash: currentHash, tab, currentPage: p, pageSize: n })
      }
      if (tab !== prevTab) {
        updateMainTab({ hash: currentHash, p, tab, n, op })
        //fetchActionNumber({ hash: currentHash, type: tab })
      }
      if (tab === 'operations') {
        if (isVaildOperationTab) {
          if (p !== prevP || op !== prevOp || n !== prevN) {
            //fetchAccountAction({ hash: currentHash, tab: op, currentPage: p, pageSize: n })
            updateMainTab({ hash: currentHash, p, tab, n, op })
          }
        } else {
          history.push({
            ...location,
            search: {
              tab: 'operations'
            }
          })
        }
      } else if (tab === 'contract') {
        if (isVaildContractTab) {
          if (p !== prevP || op !== prevOp || n !== prevN) {
            //fetchContractAction({ hash: currentHash, tab: op })
            updateMainTab({ hash: currentHash, p, tab, n, contractOp: op })
          }
        } else {
          history.push({
            ...location,
            search: {
              tab: 'contract'
            }
          })
        }
      }else if (tab === 'baking') {
        if (isValidBakingTab) {
          if (p !== prevP || op !== prevOp || n !== prevN) {
            updateMainTab({ hash: currentHash, p, tab, n, bakingOp: op })
          }
        } else {
          history.push({
            ...location,
            search: {
              tab: 'baking'
            }
          })
        }
      }
    } else {
      history.push({ ...location, search: '' })
    }

    const { bookmarklist, operations, formatTransactionDetails, getAlias } = this.props;
    const { transactions } = operations;
    if (prevProps.bookmarklist.bookmarks !== bookmarklist.bookmarks) {
      // transactions.data.map((transaction) => {
      //   // if(bookmarklist.indexOf(transaction.fromElement)){
      //   //   console.log('true')
      //   // }
      //   for (let bookmark of bookmarklist.bookmarks) {
      //     if (bookmark.hash === transaction.fromElement[0]) {
      //       transaction.fromElementAlias = bookmark.alias;

      //     }
      //     if (bookmark.hash === transaction.toElement[0]) {
      //       transaction.toElementAlias = bookmark.alias;
      //     }
      //   }

      // })
      // storeAccountDetail(transactions);
      if (transactions.data !== undefined) {
        formatTransactionDetails({ transactions: transactions.data, bookmarkList: bookmarklist.bookmarks });
        getAlias({ bookmarklist: bookmarklist.bookmarks, hash: this.props.match.params.id })
      }


    }

    if (prevProps.operations.transactions.data !== transactions.data) {
      formatTransactionDetails({ transactions: transactions.data, bookmarkList: bookmarklist.bookmarks });
      getAlias({ bookmarklist: bookmarklist.bookmarks, hash: this.props.match.params.id })
    }
  }



  componentDidMount() {
    const {
      match,
      location,
      history,
      fetchAccountDetails,
      initOperation,
      initContract,
      initBaking
    } = this.props
    const hash = match.params.id
    const searchQueries = queryString.parse(location.search)
    const { tab, op } = searchQueries
    const isVaildTab = this.verifyTab(tab)
    const isVaildOperationTab = this.verifyOperationTab(op)
    const isVaildContractTab = this.verifyContractTab(op)
    const isValidBakingTab = this.verifyBakingTab(op)
    fetchAccountDetails({ hash })
    if (isVaildTab) {
      if (tab === 'operations') {
        if (isVaildOperationTab) {
          initOperation({ hash, ...searchQueries })
        } else {
          history.push({
            ...location,
            search: {
              tab: 'operations'
            }
          })
        }
      } else if (tab === 'contract') {
        if (isVaildContractTab) {
          initContract({ hash, ...searchQueries })
          initOperation({ hash, ...searchQueries, contractOp: op })
        } else {
          history.push({
            ...location,
            search: {
              tab: 'contract'
            }
          })
        }
      } else if (tab === 'baking') {
        if (isValidBakingTab) {
          initBaking({ hash, ...searchQueries })
          initOperation({ hash, ...searchQueries, bakingOp: op })
        } else {
          history.push({
            ...location,
            search: {
              tab: 'baking'
            }
          })
        }
      } else {
        initOperation({ hash, ...searchQueries })
      }
    } else {
      history.push({ ...location, search: '' })
    }

    if (this.props.user) {
      this.props.setBookmarks(this.props.user.uid);
    }
  }

  onExpand = (expanded, record) => {
    const { updateKeyArray, operations } = this.props;
    const { keyArr } = operations['transactions']
    if (!expanded) {
      keyArr.splice(keyArr.indexOf(record.key), 1);
    } else {
      keyArr.push(record.key);
    }
    updateKeyArray(keyArr);
  }

  render() {
    const {
      accountDetails,
      bakerDetails,
      location,
      operations,
      tabs,
      contracts,
      vaildMainTabs,
      opTabs,
      contractTabs,
      baking,
      bakingTabs
    } = this.props
    const {
      transactions,
      delegations,
      originations,
      endorsements,
      proposals,
      ballots,
      activations,
      reveals,
    } = operations
    const {
      contractCode,
      contractStorage
    } = contracts
    const { hash: accountHash, name: accountName, isContract, total, isBaker, modalState } = accountDetails
    const siteName = commonMeta.author
    const metaTitle = `${accountName} | ${siteName}`
    return (
      <AccountWrapper>
        <LayoutContentWrapper>
          <Helmet>
            <title>{metaTitle}</title>
            <meta property="og:title" content={metaTitle} />
            <meta name="twitter:title" content={metaTitle} />
            <link rel="canonical" href={`${connectionConfig.tezosIdWeb}accounts/${accountHash}`} />
            <link rel="alternate" href={`${connectionConfig.tezosIdWeb}${accountHash}`} />
          </Helmet>
          <Row type="flex" justify="space-around">
            <Col lg={7} md={24} sm={24} xs={24}>
              <AccountProfileCard
                {...accountDetails}
                bakerDetails={bakerDetails}
                modalState={modalState}
              />
            </Col>
            <Col lg={16} md={24} sm={24} xs={24}>
              <AccountTableCard
                location={location}
                tabs={tabs}
                transactions={transactions}
                delegations={delegations}
                originations={originations}
                endorsements={endorsements}
                activations={activations}
                reveals={reveals}
                proposals={proposals}
                ballots={ballots}
                contractCode={contractCode}
                contractStorage={contractStorage}
                isContract={isContract}
                vaildMainTabs={vaildMainTabs}
                operations={operations}
                opTabs={opTabs}
                contractTabs={contractTabs}
                total={total}
                isBaker={isBaker}
                baking={baking}
                bakingTabs={bakingTabs}
                onExpand={this.onExpand}
              />
            </Col>
          </Row>
        </LayoutContentWrapper>
      </AccountWrapper>
    )
  }
}

const mapStateToProps = (state) => ({
  ...state.Account,
  user: state.Auth.user,
  bookmarklist: state.Bookmarklist
})

const mapDispatchToProps = dispatch => {
  return {
    fetchAccountDetails: (props) => dispatch(fetchAccountDetails(props)),
    fetchAccountAction: (props) => dispatch(fetchAccountAction(props)),
    fetchActionNumber: (props) => dispatch(fetchActionNumber(props)),
    setBookmarks: (userUid) => dispatch(setBookmarks(userUid)),
    getAlias: (props) => dispatch(getAlias(props)),
    formatTransactionDetails: (props) => dispatch(formatTransactionDetails(props)),
    updateMainTab: (props) => dispatch(updateMainTab(props)),
    initOperation: (props) => dispatch(initOperation(props)),
    initContract: (props) => dispatch(initContract(props)),
    initBaking: (props) => dispatch(initBaking(props)),
    updateKeyArray:(props)=>dispatch(updateKeyArray(props))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Account)
