import styled from 'styled-components';
import WithDirection from '../../../settings/withDirection';

const ProfileWrapper = styled.div`
  text-align: center;
  word-wrap: break-word;

  .ant-row-flex {
    width: 100%;
  }

  .card-scroll{
    overflow-y: scroll;
    height:500px
  } 

  .ant-tabs-nav-wrap {
    .ant-tag {
      padding-left: 4px;
      padding-right: 4px;
      margin-left: 4px;
      margin-right: 4px;
    }
  }

  .isoBoxWrapper {
    margin: 0px;
    border-bottom: none;
    border-left: none;
    border-right: none;

    &:last-of-type {
      border-bottom: 1px solid #e9e9e9;
    }
  }

  .account {
    &__info {
      font-weight: 700;

      .isoSimpleTable {
        font-weight: normal;
      }
    }

    &__info-title {
      display: none;
    }

    &__name, &__hash {
      color: rgba(0, 0, 0, 0.85);
      font-size: 16px;
    }

    &__info-item {
      margin-bottom: 8px;
      display: flex;
      flex-direction: column;

      &:last-of-type {
        margin-bottom: 0;
      }
    }
  }

  @media (max-width: 991px) {
    .ant-col-md-24 {
      margin-bottom: 24px;
    }
  }
`
export default WithDirection(ProfileWrapper);