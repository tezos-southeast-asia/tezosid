import styled from "styled-components";

const LanguageSwitcherWrapper = styled.div`
  margin-top: 8px;
  margin-botton: 8px;

  .ant-select {
    width: 50%;

    .ant-select-selection {
      .ant-select-selection__rendered {
        &:after {
          display: none;
        }

        .ant-select-selection-selected-value {
          float: none;
        }
      }
    }
  }

  @media only screen and (min-width: 768px) {
    .ant-select {
      width: 120px;
    }
  }
`

export default LanguageSwitcherWrapper;