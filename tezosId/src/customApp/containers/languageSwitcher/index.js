import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import langConfigs from "../../configs/langConfigs";
import { changeLanguage } from "../../../redux/languageSwitcher/actions";
import Select, { SelectOption } from "../../../components/uielements/select";
import LanguageSwitcherWrapper from "./languageSwitcher.style";

class LanguageSwitcher extends Component {
  static propsTypes = {
    language: PropTypes.object,
    onChangeLanguage: PropTypes.func
  }

  static defaultProps = {
    language: {
      locale: langConfigs.defaultLanguage
    }
  }

  render() {
    const { options, defaultLanguage } = langConfigs;
    const { language, onChangeLanguage } = this.props;
    const defaultLocale = language.locale || defaultLanguage;

    return (
      <LanguageSwitcherWrapper className="lang-switch">
        <Select
          defaultValue={defaultLocale}
          dropdownStyle={{
            textAlign: "center"
          }}
          onChange={onChangeLanguage}
        >
          { options.map(({ text, locale }) => {
              return <SelectOption key={locale}>{text}</SelectOption>
            })
          }
        </Select>
      </LanguageSwitcherWrapper>
    )
  }
}

const mapStateToProps = ({ LanguageSwitcher }) => ({
  language: LanguageSwitcher.language
})

const mapDispatchToProps = (dispatch) => ({
  onChangeLanguage: (lang) => dispatch(changeLanguage(lang))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LanguageSwitcher);
