import styled from 'styled-components';
import WithDirection from '../../../settings/withDirection';

const SignOutStyleWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 60vh;
  flex-direction: column;
`;

export default WithDirection(SignOutStyleWrapper);
