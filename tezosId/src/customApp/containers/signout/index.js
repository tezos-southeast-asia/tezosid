import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { Helmet } from "react-helmet";

import { logout } from "../../../redux/auth/actions";
import Spin from "../../../components/uielements/spin";
import HelperText from '../../../components/utility/helper-text';
import IntlMessages from "../../../components/utility/intlMessages";
import SignOutStyleWrapper from "./signout.style";

class SignOut extends Component {
  state = {
    isErrorRedirect: false
  }

  componentDidMount() {
    const { logout } = this.props;

    logout();
  }

  componentDidUpdate(prevProps) {
    const { error } = this.props

    if (prevProps.error !== error && error) {
      setTimeout(() => {
        this.setState({
          isErrorRedirect: true
        })
      }, 3000)
    }
  }

  render() {
    const { isErrorRedirect } = this.state
    const { isLoggedIn, error } = this.props
    const home = { pathname: "/" };

    if (!isLoggedIn || isErrorRedirect) {
      return <Redirect to={home} />;
    }

    return (
      <SignOutStyleWrapper className="isoSignOutPage">
        <Helmet>
          <meta name="robots" content="noindex" />
        </Helmet>
        { error ? (
            <HelperText text={<IntlMessages id="page.signOutError" />} />
          ) : (
            <Fragment>
              <HelperText text={<IntlMessages id="page.signingOut" />} />
              <Spin spinning />
            </Fragment>
          )
        }
      </SignOutStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    error: state.Auth.logoutError,
    isLoggedIn: state.Auth.isLoggedIn
  }),
  { logout }
)(SignOut)
