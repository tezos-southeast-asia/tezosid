import styled from 'styled-components';
import WithDirection from '../../../../settings/withDirection';

const RoadmapWrapper = styled.div`
  .roadmap {
    &__card {
      &__eta {
        margin-top: 16px;
        font-weight: bold;
      }
    }
  }
`
export default WithDirection(RoadmapWrapper);