import React, { Component } from "react";
import { Card } from "antd";
import { FormattedMessage } from "react-intl";

import LayoutWrapper from "../../../../components/utility/layoutWrapper";
import { roadmapList } from "../../../configs/roadmapList";
import RoadmapWrapper from "./roadmap.style"

class Roadmap extends Component {
  getRoadmapCards() {
    const roadmaps = roadmapList.map(({ title, content, ETA }, index) => (
      <Card
        key={`roadmap-card-${title}-${index}`}
        title={title}
        className="roadmap__card"
      >
        <p className="roadmap__card__content">{content}</p>
        <p className="roadmap__card__eta">{ETA}</p>
      </Card>
    ))

    return roadmaps;
  }

  render() {
    return (
      <LayoutWrapper
        pageTitle={<FormattedMessage id="roadmap.title" />}
        pageDesc={<FormattedMessage id="roadmap.desc" />}
        className="roadmap"
      >
        <RoadmapWrapper>
          {this.getRoadmapCards()}
        </RoadmapWrapper>    
      </LayoutWrapper>
    );
  }
}

export default Roadmap;
