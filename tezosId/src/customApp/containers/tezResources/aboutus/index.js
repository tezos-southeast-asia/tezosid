import React, { Component } from "react";
import LayoutWrapper from "../../../../components/utility/layoutWrapper";
import { Card } from "antd";
import { Link } from "react-router-dom";
import IntlMessages from "../../../../components/utility/intlMessages";
import AboutUsWrapper from "./aboutus.style"

export default class AboutUs extends Component {
  state = {
    dataSource: []
  };

  getRoadmapLink = () => {
    return <Link to="/roadmap"><IntlMessages id="aboutus.roadmap" /></Link>;
  };

  render() {
    const roadmapLink = this.getRoadmapLink()

    return (
      <LayoutWrapper
        pageTitle={<IntlMessages id="aboutus.title" />}
        className="aboutus"
      >
        <AboutUsWrapper>
          <Card
            className="aboutus__desc"
          >
            <IntlMessages
              id="aboutus.description"
              values={{
                roadmapLink
              }}
            />
          </Card>
        </AboutUsWrapper>
      </LayoutWrapper>
    );
  }
}
