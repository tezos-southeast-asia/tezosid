import React, { Component } from "react";
import LayoutWrapper from "../../../components/utility/layoutWrapper.js";
import { Card } from "antd";
import { Col, Row } from "antd";
import IntlMessages from "../../../components/utility/intlMessages";

export default class extends Component {
  state = {
    dataSource: []
  };

  render() {
    const rowStyle = {
      width: "100%",
      display: "flex",
      flexFlow: "row wrap"
    };
    const colStyle = {
      marginBottom: "16px"
    };
    const gutter = 16;

    return (
      <LayoutWrapper>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={12} xs={24} style={colStyle}>
            <Card
              bordered={true}
              title={
                <p style={{ color: "#818a91", fontSize: 30 }}>
                  <IntlMessages id="terminologies.title"/>
                </p>
              }
              extra={
                <div>
                  <i
                    className="ion-information-circled"
                    style={{ fontSize: 30, paddingRight: 0, color: "#818a91" }}
                  />
                </div>
              }
              style={{ width: "100%" }}
            >
              <p>
                <h1 style={{ fontSize: 25 }}>
                  <a href="https://medium.com/tezos/a-quick-tour-of-the-tezos-code-base-and-the-state-of-its-development-c4e5fcb34b8a">
                  <IntlMessages id="terminologies.baker"/>
                  </a>
                </h1>
                <h3 style={{ fontSize: 15 }}>
                  <div>
                    <p>
                    <IntlMessages id="terminologies.bakerDescription"/>
                    </p>
                  </div>
                </h3>
                <br />

                <h1 style={{ fontSize: 25 }}>
                  <a href="https://www.reddit.com/r/tezos/comments/6m03r1/can_someone_eli5_what_a_tezos_baker_is/djy12vt/?st=j8pbhvt6&sh=c826ff6a">
                  <IntlMessages id="terminologies.baking"/>
                  </a>
                </h1>
                <h3 style={{ fontSize: 15 }}>
                  <div>
                    <p>
                    <IntlMessages id="terminologies.bakingDescription"/>
                    </p>
                  </div>
                </h3>
                <br />

                <h1 style={{ fontSize: 25 }}>
                  <a href="https://medium.com/tezos/a-quick-tour-of-the-tezos-code-base-and-the-state-of-its-development-c4e5fcb34b8a">
                  <IntlMessages id="terminologies.client"/>
                  </a>
                </h1>
                <h3 style={{ fontSize: 15 }}>
                  <div>
                    <p>
                    <IntlMessages id="terminologies.clientDescription"/>
                    <br />
                    <IntlMessages id="terminologies.clientDescription2"/>
                    </p>
                    <br />
                    <IntlMessages id="terminologies.clientDescription3"/>
                    <br />
                    <IntlMessages id="terminologies.clientDescription4"/>
                    <br />
                    <IntlMessages id="terminologies.clientDescription5"/>
                    <br />
                    <IntlMessages id="terminologies.clientDescription6"/>
                    <br />
                    <IntlMessages id="terminologies.clientDescription7"/>
                  </div>
                </h3>
                <br />

                <h1 style={{ fontSize: 25 }}><IntlMessages id="terminologies.staking"/></h1>
                <h3 style={{ fontSize: 15 }}>
                  <div>
                    <p>
                    <IntlMessages id="terminologies.stakingDescription"/>
                    </p>
                  </div>
                </h3>
                <br />

                <h1 style={{ fontSize: 25 }}>
                  <a href="https://medium.com/tezos/a-quick-tour-of-the-tezos-code-base-and-the-state-of-its-development-c4e5fcb34b8a">
                  <IntlMessages id="terminologies.tezosNode"/>
                  </a>
                </h1>
                <h3 style={{ fontSize: 15 }}>
                  <div>
                    <p>
                    <IntlMessages id="terminologies.tezosNodeDescription"/>
                    </p>
                  </div>
                </h3>
                <br />
              </p>
            </Card>
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}
