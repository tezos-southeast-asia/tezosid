import styled from 'styled-components';
import WithDirection from '../../../../settings/withDirection';

const DevlogWrapper = styled.div`
  .devlog {
    &__entries {
      list-style-type: disc;
    }
  }
`
export default WithDirection(DevlogWrapper);