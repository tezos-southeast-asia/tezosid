import React, { Component } from "react";
import { Card } from "antd";
import { FormattedMessage } from "react-intl";

import { devlogList } from "../../../configs/devlogList";
import LayoutWrapper from "../../../../components/utility/layoutWrapper";
import DevlogWrapper from "./devlog.style";

 class Devlogs extends Component {
  getDevlogs() {
    const devlogs = devlogList.map(({ date, entries}) => (
      <Card
        key={`devlog-card-${date}`}
        title={date}
        className="devlog__logcard"
      >
        <ul className="devlog__entries">
          { entries.map((entry, index) => (<li key={`devlog-entry-${index}`}>{entry}</li>))}
        </ul>
      </Card>
    ))
    
    return devlogs;
  }

  render() {
    return (
      <LayoutWrapper
        pageTitle={<FormattedMessage id="devlog.title" />}
        className="devlog"
      >
        <DevlogWrapper>
          {this.getDevlogs()}
        </DevlogWrapper>
      </LayoutWrapper>
    );
  }
}

export default Devlogs;
