import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

import { connectionConfig } from "../../../configs/connectionConfigs";
import { delegationServicesColumns } from "../../../configs/tableColumnsConfigs"
import TablePage from "../../../components/tableViews/tablePage";


export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      bakers: [],
      fail: false
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData() {
    this.setState({
      isLoading: true,
    });

    fetch(connectionConfig.deleServices)
      .then(response => response.json())
      .then(parsedJSON => {
        this.setState({
          isLoading: false,
          bakers: parsedJSON.bakers.map((baker) => ({
            ...baker,
            key: baker.delegation_code,
          }))
        });
      })
      .catch(() => {
        this.setState({
          isLoading: false,
          bakers: [],
          fail: true
        });
      });
  }

  render() {
    const {
      fail,
      isLoading,
      bakers = [],
    } = this.state
    return (
      <TablePage
        pageTitle={<FormattedMessage id="delegationServices.title"/>}
        fail={fail}
        columns={delegationServicesColumns}
        dataSource={bakers}
        loading={isLoading}
        pagination={false}
      />
    );
  }
}
