import React, { Component } from 'react';
import { Row, Col } from 'antd';
import PageHeader from '../../../components/utility/pageHeader';
import Box from '../../../components/utility/box';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import ContentHolder from '../../../components/utility/contentHolder';
import basicStyle from '../../../settings/basicStyle';
import IntlMessages from '../../../components/utility/intlMessages';
import Timeline, {
  TimelineItem,
} from '../../../components/uielements/timeline';
import Tags from '../../../components/uielements/tag';
import TagWrapper from '../../../containers/Uielements/Tag/tag.style'

const Tag = props => (
    <TagWrapper>
      <Tags {...props}>{props.children}</Tags>
    </TagWrapper>
  );

export default class extends Component {

    render() {
      const { rowStyle, colStyle, gutter } = basicStyle;
      return (
        <LayoutWrapper>
          <PageHeader>
            {<IntlMessages id="uiElements.timeline.timeline" />}
          </PageHeader>
        
        <Row style={rowStyle} gutter={gutter} justify="start">
        <Col md={24} sm={12} xs={24} style={colStyle}>
        <Box>
              <ContentHolder>
                <Timeline style={{marginLeft:780}}>
                  <div style={{marginLeft:-18}}>
                  <Tag color="#3db9dc">2018 </Tag>
                  </div>
                 
                  <TimelineItem color="green">
                    <p style={{color:"#1bb99a"}}>TBA</p>
                    <p>Tezos network launches</p>
                  </TimelineItem>

                  <div style={{marginLeft:-18}}>
                  <Tag color="#3db9dc">2018 </Tag>
                  </div>

                  <TimelineItem color="red">
                    <p style={{color:"#ed3507"}}>Nov 26</p>
                    <p>3rd lawsuit filed against Tezos</p>
                  </TimelineItem>

                  <TimelineItem color="red">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}>
                    <p style={{color:"#ed3507"}}>Nov 13</p>
                    <p>2nd lawsuit filed against Tezos</p>
                    </div>
                  </TimelineItem>
                  
                  <TimelineItem color="red">
                    <p style={{color:"#ed3507"}}>Oct 25</p>
                    <p>Lawsuit filed against Tezos</p>
                  </TimelineItem>

                  <TimelineItem color="blue">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}>
                    <p style={{color:"#2821b7"}}>Oct 22-25</p>
                    <p>Money 2020 USA</p>
                    </div>
                  </TimelineItem>

                  <TimelineItem color="red">
                    <p style={{color:"#ed3507"}}>Oct 18</p>
                    <p>Tezos Foundation Dispute</p>
                  </TimelineItem>

                  <TimelineItem color="blue">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}>
                    <p style={{color:"#2821b7"}}>Oct 10</p>
                    <p>TEDxSanFrancisco</p>
                    </div>
                  </TimelineItem>

                  <TimelineItem color="blue">
                    <p style={{color:"#2821b7"}}>Sep 7</p>
                    <p>Coinscrum</p>
                  </TimelineItem>

                  <TimelineItem color="blue">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}>
                    <p style={{color:"#2821b7"}}>Sep 6</p>
                    <p>Singapore Tezos Meet Up</p>
                    </div>
                  </TimelineItem>

                  <TimelineItem color="blue">
                    <p style={{color:"#2821b7"}}>Aug 30</p>
                    <p>Silicon Valley Tezos Presentation</p>
                  </TimelineItem>

                  <TimelineItem color="gold">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}>
                    <p style={{color:"#d2d813"}}>Aug 10</p>
                    <p>Tezos announce $50 million venture capital</p>
                    </div>
                  </TimelineItem>

                  <TimelineItem color="cyan">
                    <p style={{color:"#13c0db"}}>Jul 24</p>
                    <p>Tezos fundraiser statistics released</p>
                  </TimelineItem>

                  <TimelineItem color="cyan">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}>
                    <p style={{color:"#13c0db"}}>Jul 13</p>
                    <p>Tezos fundraiser ends, raising $232 million</p>
                    </div>
                  </TimelineItem>

                  <TimelineItem color="cyan">
                    <p style={{color:"#13c0db"}}>Jul 1</p>
                    <p>Tezos fundraiser starts</p>
                  </TimelineItem>

                  <TimelineItem color="purple">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}>
                    <p style={{color:"#871fb7"}}>May</p>
                    <p>Tezos Foundation launches in Switzerland</p>
                    </div>
                  </TimelineItem>

                  <TimelineItem color="black">
                    <p style={{color:"#211e1e"}}>Feb</p>
                    <p>Emin Gün Sirer joins as a technical advisor in an official capacity</p>
                  </TimelineItem>
                  
                  <div style={{marginLeft:-18}}>
                  <Tag color="#3db9dc">2018 </Tag>
                  </div>

                  <TimelineItem color="gold">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}>
                    <p style={{color:"#d2d813"}}>Sep</p>
                    <p>Polychain Capital and several individuals back Tezos to help scale up development</p>
                    </div>
                  </TimelineItem>

                  <TimelineItem color="black">
                    <p style={{color:"#211e1e"}}>Sep</p>
                    <p>Arthur Breitman presents Tezos as StrangeLoop</p>
                  </TimelineItem>

                  <TimelineItem color="black">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}>
                    <p style={{color:"#211e1e"}}>Jun</p>
                    <p>Andrew Miller joins as an advisor</p>
                    </div>
                  </TimelineItem>

                  <div style={{marginLeft:-18}}>
                  <Tag color="#3db9dc">2018 </Tag>
                  </div>

                  <TimelineItem color="black">
                    <p style={{color:"#211e1e"}}>Jan</p>
                    <p>Zooko becomes an advisor to Tezos</p>
                  </TimelineItem>

                  <div style={{marginLeft:-18}}>
                  <Tag color="#3db9dc">2018 </Tag>
                  </div>

                  <TimelineItem color="purple">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}> 
                    <p style={{color:"#871fb7"}}>Sep</p>
                    <p>Tezos White Paper is released</p>
                    </div>
                  </TimelineItem>

                  <TimelineItem color="purple">
                    <p style={{color:"#871fb7"}}>Aug</p>
                    <p>Tezos Position Paper is released</p>
                  </TimelineItem>

                  <TimelineItem color="purple">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}>
                    <p style={{color:"#871fb7"}}>Mar</p>
                    <p>Tezos development begins self-funded</p>
                    </div>
                  </TimelineItem>

                  <TimelineItem color="black">
                    <p style={{color:"#211e1e"}}>Jan</p>
                    <p>Independent formulation of this idea by L.M. Goodman</p>
                  </TimelineItem>

                  <div style={{marginLeft:-18}}>
                  <Tag color="#3db9dc">2018 </Tag>
                  </div>

                  <TimelineItem color="black">
                    <div style={{ textAlign: 'right', marginRight: 815, marginLeft: -815}}>
                    <p style={{color:"#211e1e"}}>Dec</p>
                    <p>Gordon Mohr suggests NomicCoin on Twitter</p>
                    </div>
                  </TimelineItem>

                </Timeline>
              </ContentHolder>
            </Box>
          </Col>
        </Row>
        </LayoutWrapper>
      );
    }
}
