import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { pwdResetEmail } from "../../../redux/auth/actions";
import Input from "../../../components/uielements/input";
import Button from "../../../components/uielements/button";
import Spin from "../../../components/uielements/spin";
import HelperText from '../../../components/utility/helper-text';
import IntlMessages from "../../../components/utility/intlMessages";
import ForgotPasswordStyleWrapper from "./forgotPassword.style";

export class SignUp extends Component {
    static defaultProps = {
        isLoggedIn: false,
        loading: false,
        pwdResetEmailMsg: null,
        pwdResetEmail: () => { },
    }

    constructor(props) {
        super(props);
        this.state = {
            email: '',
        };
    }

    updateEmailValue(evt) {
        this.setState({
            email: evt.target.value
        });
    }

    handleForgotPwd = () => {
        const { pwdResetEmail } = this.props;
        pwdResetEmail(this.state.email);
    }

    render() {
        const home = { pathname: "/" };
        const { isLoggedIn, loading, pwdResetEmailMsg } = this.props;

        if (isLoggedIn) {
            return <Redirect to={home} />;
        }

        return (
            <ForgotPasswordStyleWrapper className="isoForgotPassPage">
                <div className="isoFormContentWrapper">
                    <div className="isoFormContent">
                        <div className="isoLogoWrapper">
                            <Link to="/dashboard">
                                <IntlMessages id="page.forgetPassTitle" />
                            </Link>
                        </div>

                        {pwdResetEmailMsg && (
                            <HelperText text={pwdResetEmailMsg.message} />
                        )
                        }
                        
                        <div className="isoFormHeadText">
                            <h3>
                                <IntlMessages id="page.forgetPassSubTitle" />
                            </h3>
                            <p>
                                <IntlMessages id="page.forgetPassDescription" />
                            </p>
                        </div>
                        <div className="isoForgotPassForm">
                            <div className="isoInputWrapper">
                                <Input size="large" placeholder="Email" onChange={evt => this.updateEmailValue(evt)} />
                            </div>
                            <div className="isoInputWrapper">
                                <Button type="primary" onClick={this.handleForgotPwd}>
                                    <IntlMessages id="page.sendRequest" />
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
                {loading && (
                    <div className="isoSignUpLoadingWrapper">
                        <Spin spinning />
                    </div>
                )
                }
            </ForgotPasswordStyleWrapper>
        );
    }
}

export default connect(
    state => ({
        pwdResetEmailMsg: state.Auth.pwdResetEmailMsg,
        loading: state.Auth.loading,
        isLoggedIn: state.Auth.isLoggedIn
    }),
    { pwdResetEmail }
)(SignUp);
