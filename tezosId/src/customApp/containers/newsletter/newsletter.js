import React from "react";
import { Card } from "antd";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";

import newsletterConfigs from "../../configs/newsletterConfigs";
import LayoutWrapper from "../../../components/utility/layoutWrapper";
import NewsLetterWrapper from "./newsletter.style";
import NewsletterForm from "./newsletterForm";
import NewsLetterLogo from "./newsletterLogo"

const NewsLetterSubscription = () => (
  <LayoutWrapper
    pageTitle={<FormattedMessage id="subscribe.title" />}
    className="newsletter-subscription"
  >
    <NewsLetterWrapper>
      <Card>
        <NewsLetterLogo />
        <NewsletterForm
          buttonTextId="subscribe.button"
          labelTextId="subscribe.description"
          actionUrl={newsletterConfigs.subscribeUrl}
        />
        <Link to= "/unsubscribe" className="newsletter__unsubscribe-link">
          <FormattedMessage id="subscribe.unsubscribe" />
        </Link>
      </Card>
    </NewsLetterWrapper>
  </LayoutWrapper>
);

export default NewsLetterSubscription;
