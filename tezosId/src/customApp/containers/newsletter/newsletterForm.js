import React from "react";
import { FormattedMessage } from "react-intl";

import newsletterConfigs from "../../configs/newsletterConfigs";
import Input from '../../../components/uielements/input';
import Button from '../../../components/uielements/button';

const NewsLetterForm = (props) => {
  const {
    buttonTextId,
    labelTextId,
    actionUrl
  } = props
  const {
    u,
    id
  } = newsletterConfigs

  return (
    <form action={actionUrl} method="post" className="newsletter__form">
      { labelTextId && (
          <label htmlFor="newsletter__EMAIL" className="newsletter__label newsletter__col">
            <FormattedMessage id={labelTextId} />
          </label>
        )
      }
      <FormattedMessage id="subscribe.emailInput">
        { placeholder=>  
            <Input
              type="email"
              name="EMAIL"
              className="newsletter__email newsletter__col"
              id="newsletter__EMAIL"
              placeholder={placeholder}
            />
        }  
      </FormattedMessage>
      <input
        type="text"
        name="u"
        tabIndex="-1"
        className="newsletter__hidden-input"
        value={u}
        readOnly
      />
      <input
        type="text"
        name="id"
        tabIndex="-1"
        className="newsletter__hidden-input"
        value={id}
        readOnly
      />
      { buttonTextId && (
          <Button type="primary" htmlType="submit" className="newsletter__button newsletter__col">
            <FormattedMessage id={buttonTextId} />
          </Button>
        )
      }
    </form>
  );
}

export default NewsLetterForm;
