import React from "react";
import { Card } from "antd";
import { FormattedMessage } from "react-intl";

import newsletterConfigs from "../../configs/newsletterConfigs";
import LayoutWrapper from "../../../components/utility/layoutWrapper";
import NewsLetterWrapper from "./newsletter.style";
import NewsletterForm from "./newsletterForm";
import NewsLetterLogo from "./newsletterLogo";

const NewsLetterUnsubscription = () => (
  <LayoutWrapper
    pageTitle={<FormattedMessage id="unsubscribe.title" />}
    className="newsletter-unsubscription"
  >
    <NewsLetterWrapper>
      <Card>
        <NewsLetterLogo />
        <NewsletterForm
          buttonTextId="unsubscribe.button"
          labelTextId="unsubscribe.description"
          actionUrl={newsletterConfigs.unsubscribeUrl}
        />
      </Card>
    </NewsLetterWrapper>
  </LayoutWrapper>
);

export default NewsLetterUnsubscription;
