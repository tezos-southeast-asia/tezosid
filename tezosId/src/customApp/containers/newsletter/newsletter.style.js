import styled from 'styled-components';
import WithDirection from '../../../settings/withDirection';

const margin = '16px';
const NewsLetterWrapper = styled.div`
  .ant-card-body {
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
  }
  
  .newsletter {
    &__logo {
      width: 300px;
    }

    &__form {
      display: flex;
      flex-direction: column;
      margin: 0 auto ${margin};
      max-width: 500px;
      width: 100%;
      text-align: center;
    }

    &__col {
      margin-bottom: ${margin};
    }

    &__label {
      font-size: 24px;
    }

    &__hidden-input {
      display: none;
    }
  }

  @media only screen and (max-width: 767px) {
    .newsletter {
      &__logo {
        width: 80%;
      }
    }
  }
`
export default WithDirection(NewsLetterWrapper);