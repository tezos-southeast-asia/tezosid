import React from "react";
import { Card } from "antd";
import { FormattedMessage } from "react-intl";

import LayoutWrapper from "../../../components/utility/layoutWrapper";
import NewsLetterWrapper from "./newsletter.style";
import NewsLetterLogo from "./newsletterLogo";

const NewsLetterThankYou = () => (
  <LayoutWrapper
    pageTitle={<FormattedMessage id="subscribe.title" />}
    className="newsletter-thankyou"
  >
    <NewsLetterWrapper>
      <Card>
        <NewsLetterLogo />
        <div className="newsletter__label">
          <FormattedMessage id="subscribe.thankyou" />
        </div>
      </Card>
    </NewsLetterWrapper>
  </LayoutWrapper>
);

export default NewsLetterThankYou;
