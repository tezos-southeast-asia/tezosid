import React from 'react'

const NewsLetterLogo = () => (
  <img className="newsletter__logo newsletter__col" alt="tezosid-logo" src={require("../../../image/TezosID_banner_logo_black.svg")} />
)

export default NewsLetterLogo;