import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { allContractColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchAllContract } from "../../../redux/allContract/actions";

class AllContract extends Component {
  render() {
    const {
      isLoading,
      allContract = {},
      isError,
      fetchAllContract
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = allContract

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="allContract.title" />}
        dataSource={data}
        columns={allContractColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchAllContract}
      />
    );
  }
}


const mapStateToProps = (state) => ({
  ...state.AllContract
})

const mapDispatchToProps = dispatch => {
  return {
    fetchAllContract: (props) => dispatch(fetchAllContract(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AllContract)
