import React, { Component } from "react";
import { connect } from "react-redux";

import TablePage from "../../components/tableViews/tablePage";
import { allAccountColumns } from "../../configs/tableColumnsConfigs";
import IntlMessages from "../../../components/utility/intlMessages";
import { fetchAllAccount } from "../../../redux/allAccount/actions";

class AllAccount extends Component {
  render() {
    const {
      isLoading,
      allAccount = {},
      isError,
      fetchAllAccount
    } = this.props
    const {
      data,
      total,
      pageSize,
      currentPage
    } = allAccount

    return (
      <TablePage
        totalPage={total}
        currentPage={currentPage}
        pageSize={pageSize}
        fail={isError}
        pageTitle={<IntlMessages id="allAccount.title" />}
        dataSource={data}
        columns={allAccountColumns}
        loading={isLoading}
        isLink={true}
        pageOnChange={fetchAllAccount}
      />
    );
  }
}


const mapStateToProps = (state) => ({
  ...state.AllAccount
})

const mapDispatchToProps = dispatch => {
  return {
    fetchAllAccount: (props) => dispatch(fetchAllAccount(props)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AllAccount)
