import styled from "styled-components";

import basicStyle from "../../settings/basicStyle";

const { layoutStyle } = basicStyle
const LayoutContentWrapper = styled.div`
  padding: 40px ${layoutStyle.padding};
  display: flex;
  flex-flow: row wrap;
  overflow: hidden;

  .isoLayoutContentDesc {
    margin-bottom: 16px;
  }

  .isoLayoutContent {
    width: 100%;
  }

  .ant-card {
    white-space: pre-line;
    font-size: 16px;
    width: 100%;
    margin-bottom: 16px;
  }

  @media only screen and (max-width: 767px) {
    padding: 50px ${layoutStyle.padding};
  }

  @media (max-width: 580px) {
    padding: 15px ${layoutStyle.padding};
  }
`;

export { LayoutContentWrapper };
