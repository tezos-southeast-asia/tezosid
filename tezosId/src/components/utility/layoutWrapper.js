import React from "react";
import classnames from "classnames";
import { Card } from "antd";

import PageHeader from "../../components/utility/pageHeader";
import { LayoutContentWrapper } from "./layoutWrapper.style";
import CommonMeta from "../../customApp/components/commonMeta"

export default ({ className, pageTitle, pageDesc, children, ...restProps }) => (
  <LayoutContentWrapper
    className={classnames('isoLayoutContentWrapper', className)}
    {...restProps}
  >
      <CommonMeta />
    { pageTitle && <PageHeader>{pageTitle}</PageHeader> }
    { pageDesc && <Card className="isoLayoutContentDesc">{ pageDesc }</Card> }
    <div className="isoLayoutContent">
      {children}
    </div>
  </LayoutContentWrapper>
);
