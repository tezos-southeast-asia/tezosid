import styled from 'styled-components';
import { palette } from 'styled-theme';
import WithDirection from '../../settings/withDirection';

const WDComponentTitleWrapper = styled.h1`
  font-size: 20px;
  font-weight: 500;
  color: ${palette('secondary', 2)};
  padding-right: 16px;
  margin-bottom: 30px;
  display: flex;
  align-items: center;
  white-space: nowrap;
  width: 100%;

  &:before {
    content: '';
    width: 5px;
    height: 40px;
    background-color: ${palette('secondary', 3)};
    display: flex;
    margin: ${props =>
      props['data-rtl'] === 'rtl' ? '0 0 0 15px' : '0 15px 0 0'};
  }

  &:after {
    content: '';
    width: 100%;
    height: 1px;
    background-color: ${palette('secondary', 3)};
    display: flex;
    margin: ${props =>
      props['data-rtl'] === 'rtl' ? '0 15px 0 0' : '0 0 0 15px'};
  }

  &.isoComponentTitle {
    .ant-tag {
      margin-right: ${props => props['data-rtl'] === 'rtl' ? '8px' : 'inherit'};
      margin-left: ${props => props['data-rtl'] === 'rtl' ? 'inherit' : '8px'};
    }

    .hash-number {
      margin-right: ${props => props['data-rtl'] === 'rtl' ? '8px' : '0'};
      margin-left: ${props => props['data-rtl'] === 'rtl' ? '0' : '8px'};
    }
  }

  @media only screen and (max-width: 1220px) {
    padding: 0 10px;
    margin-bottom: 30px;
    flex-wrap: wrap;
    white-space: normal;

    &:after {
      display: none;
    }

    &.isoComponentTitle {
      .hash-number {
        word-break: break-all;
      }
    }
  }
`;

const ComponentTitleWrapper = WithDirection(WDComponentTitleWrapper);
export { ComponentTitleWrapper };
