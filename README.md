TEZOS.ID
---
[Tezos.Id](https://tezos.id/) is a blockchain and Identity explorer.

## Installation
The Tezos.Id includes 3 parts.

### Tezos.Id UI

The source code can be found in `tezosId` folder. The UI implements in React. It requires `node.js` >= 10.15.1.

#### The major packages
  * [Create React App](https://facebook.github.io/create-react-app/docs/getting-started)
  * [Isomorphic](https://redq.gitbooks.io/isomorphic/content/)
  * [Ant Design of React](https://ant.design/docs/react/introduce)

#### How to use it
* Install packages
  ```
  cd tezosId
  yarn install
  ```
* Setup Environement variables
  * `REACT_APP_TEZOS_ENV`
    * It's for building static files or starting dev servers for different Tezos environments including `mainnet`, `babylonnet`, and `carthagenet`.
    * Its default value is `mainnet`.
    * It's used in [connectionConfigs](./tezosId/src/customApp/configs/connectionConfigs.js) and [authConfigs](./tezosId/src/customApp/configs/authConfigs.js) to switch API and Authentication configurations.
* For developement, start it:
  ```
  yarn start
  ```

Then, you can access the server by `http://localhost:3000`.

* For production deployment
  * Build static files:
    ```
    yarn build
    ```
  * Start nginx docker first
    ```
    # switch to root folder
    cd ..

    docker build -f ./tezosId/Dockerfile -t tezosid/web:latest .

    docker run -d -p 3000:3000 --rm --name tezosId tezosid/web:latest
    ```
    Then, you can access the server by `http://localhost:3000`.
* For integration tests:
  * On local
    * Run by command line on local
        ```
        yarn cypress:run:dev
        ```
    * Run by UI on local
        ```
        yarn cypress:open:dev
        ```
    * All integration tests are under `tezosId/cypress` folder
    * Notes
      * `CYPRESS_baseUrl` of `cypress:open:dev` in `package.json` could be changed to `https://tezos.id` for production or `http://192.168.1.217:3000` for stage
      * Please start service first in other termial window/tab if you run integration tests for localhost
      * If you run mobile tests by UI, please change `viewportWidth` as `375` in `cypress.json` first
        ```
        "viewportWidth": 375,
        ```
  * On pipeline
    ```
    yarn cypress:run
    ```
    * Environment variables
      * `CYPRESS_baseUrl` is URL used as prefix for `cy.visit()` or `cy.request()` command’s URL
      * `CYPRESS_FIREBASE_AUTH_EMAIL` & `CYPRESS_FIREBASE_AUTH_PASSWORD` are testing firebase email account & password
      * `CYPRESS_FIREBASE_AUTH_GOOGLE_ACCOUNT` & `CYPRESS_FIREBASE_AUTH_GOOGLE_PASSWORD` are testing firebase Google account & password
* References
  * [Cypress Guides](https://docs.cypress.io/guides/overview/why-cypress.html)
  * [Cypress Configuration](https://docs.cypress.io/guides/references/configuration.html)
  * [Cypress Assertions](https://docs.cypress.io/guides/references/assertions.html)
  * [Cypress APIs](https://docs.cypress.io/api/api/table-of-contents.html)
  * [Cypress Supported Browsers](https://docs.cypress.io/guides/guides/launching-browsers.html)
  * Junit report
    * [Cypress Reporters](https://docs.cypress.io/guides/tooling/reporters.html#Reporter-Options)
    * [Gitlab CI Reports](https://docs.gitlab.com/ee/ci/junit_test_reports.html)
      * Reports only can view on Merge request now
      * There are some issues related to show reports on Pipeline
        * [It's not clear how to view jUnit-style test reports via the web interface](https://gitlab.com/gitlab-org/gitlab-ce/issues/54067)
        * [CI View for JUnit-style XML](https://gitlab.com/gitlab-org/gitlab-ce/issues/17081)

We also provide docker image for running the UI. See `./tezosid/Dockerfile`

### Tezos.Id Middleware

See details in this repo [repo](https://gitlab.com/tezos-southeast-asia/mooncake).

### Tezos.Id Crawler/Indexer

Most of data for Tezos.Id is from [baking-soda-tezos](https://gitlab.com/9chapters/baking-soda-tezos).

Only the `/peer` page of Tezos.Id is read the data from crawler in this repo. This crawler is also for indexing the data of  `tezos-node`. Building docker image is the simplest to run it.

```
cd crawler
docker build -t crawler .
```

Before building it, you need to prepare your cron job file and Mysql setting.
For a cron job file, please see `./crawler/crawler_cron.sample` for details
For Mysql setting, please see `./crawler/script/default_variables.sample.php` for details. The schema of Mysql can be found here `./sql/tezosid.sql`
